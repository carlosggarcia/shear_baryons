#!/bin/bash
# Modify these three variables and add the chains that you want to
# launch/resume at the end of the script
queue=berg

######
# Automatic part
if [[ $queue == "cmb" ]]; then
    cores=24
elif  [[ $queue == "berg" ]]; then
    cores=28
elif  [[ $queue == "normal" ]]; then
    cores=1
else
    echo "queue not recognized. Choose cmb or berg"
    exit 1
fi

# addqueue -n 1x$cores -s -q $queue -m 7 -o log/desy3.log  -c "SSC (DESY3)" /usr/bin/python3.8 compute_SSC.py /mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3wl__2x2pt_4096/cls_cov.fits
# addqueue -n 1x$cores -s -q $queue -m 7 -o log/desy3_fsky.log  -c "SSC (DESY3 fsky)" /usr/bin/python3.8 compute_SSC.py /mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3wl__2x2pt_4096/cls_cov.fits --fsky_approx
# addqueue -n 1x$cores -s -q $queue -m 7 -o log/k1000.log  -c "SSC (K1000)" /usr/bin/python3.8 compute_SSC.py /mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov.fits 
# addqueue -n 1x$cores -s -q $queue -m 7 -o log/k1000_fsky.log  -c "SSC (K1000 fsky)" /usr/bin/python3.8 compute_SSC.py /mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov.fits --fsky_approx
# addqueue -n 1x$cores -s -q $queue -m 7 -o log/desy3_nok1000.log  -c "SSC (DESY3 noK1000)" /usr/bin/python3.8 compute_SSC.py /mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3wl_noK1000_2x2pt_4096/cls_cov.fits --no_k1000_overlap
# addqueue -n 1x$cores -s -q $queue -m 7 -o log/desy3_nok1000_fsky.log  -c "SSC (DESY3 noK1000)" /usr/bin/python3.8 compute_SSC.py /mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3wl_noK1000_2x2pt_4096/cls_cov.fits --no_k1000_overlap --fsky_approx

# addqueue -n 1x$cores -s -q $queue -m 7 -o log/hsc.log  -c "SSC (HSC)" /usr/bin/python3.8 compute_SSC.py /mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/HSCDR1wl__2x2pt_4096/cls_cov.fits 
# addqueue -n 1x$cores -s -q $queue -m 7 -o log/hsc_fsky.log  -c "SSC (HSC fsky)" /usr/bin/python3.8 compute_SSC.py /mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/HSCDR1wl__2x2pt_4096/cls_cov.fits --fsky_approx

# addqueue -n 1x$cores -s -q $queue -m 7 -o log/hscAndrina.log  -c "SSC (HSC)" /usr/bin/python3.8 compute_SSC.py ./cls_signal_covG_HSC_Andrina_paper_newNz.fits
# addqueue -n 1x$cores -s -q $queue -m 7 -o log/hscAndrina_fsky.log  -c "SSC (HSC fsky)" /usr/bin/python3.8 compute_SSC.py ./cls_signal_covG_HSC_Andrina_paper_newNz.fits --fsky_approx

# addqueue -n 1x$cores -s -q $queue -m 7 -o log/desy3_nok1000_nohscdr1wl.log  -c "SSC (DESY3 noK1000 noHSCDR1wl)" /usr/bin/python3.8 compute_SSC.py /mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3wl_noK1000_noHSCDR1wl_2x2pt_4096/cls_cov.fits --no_k1000_overlap --no_hscdr1wl_overlap
# addqueue -n 1x$cores -s -q $queue -m 7 -o log/k1000_no_hscdr1wl.log  -c "SSC (K1000 noHSCDR1wl)" /usr/bin/python3.8 compute_SSC.py /mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000_noHSCDR1wl__2x2pt_4096/cls_cov.fits --no_hscdr1wl_overlap


#addqueue -n 1x$cores -s -q $queue -m 7 -o log/desy3_nok1000_nohscdr1wl_fsky.log  -c "SSC (DESY3 noK1000 noHSCDR1wl)" /usr/bin/python3.8 compute_SSC.py /mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3wl_noK1000_noHSCDR1wl_2x2pt_4096/cls_cov.fits --no_k1000_overlap --no_hscdr1wl_overlap --fsky_approx
# addqueue -n 1x$cores -s -q $queue -m 7 -o log/k1000_no_hscdr1wl_fsky.log  -c "SSC (K1000 noHSCDR1wl fsky)" /usr/bin/python3.8 compute_SSC.py /mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000_noHSCDR1wl__2x2pt_4096/cls_cov.fits --no_hscdr1wl_overlap --fsky_approx


# addqueue -n 1x$cores -s -q $queue -m 7 -o log/desy3_estimate_residual_pixwin.log  -c "DES0 pixwin" $HOME/mambaforge/envs/baryons/bin/python estimate_residual_pixwin/desy3_estimate_residual_pixwin.py 0
# addqueue -n 1x$cores -s -q $queue -m 7 -o log/desy3_estimate_residual_pixwin.log  -c "DES1 pixwin" $HOME/mambaforge/envs/baryons/bin/python estimate_residual_pixwin/desy3_estimate_residual_pixwin.py 1
# addqueue -n 1x$cores -s -q $queue -m 7 -o log/k1000_estimate_residual_pixwin_zbin0.log  -c "KiDS0 pixwin" $HOME/mambaforge/envs/baryons/bin/python estimate_residual_pixwin/k1000_estimate_residual_pixwin.py 0
# addqueue -n 1x$cores -s -q $queue -m 7 -o log/k1000_estimate_residual_pixwin_zbin0.log  -c "KiDS0 pixwin" $HOME/mambaforge/envs/baryons/bin/python estimate_residual_pixwin/k1000_estimate_residual_pixwin.py 1
# addqueue -n 1x$cores -s -q $queue -m 7 -o log/k1000_estimate_residual_pixwin_zbin2.log  -c "KiDS2 pixwin" $HOME/mambaforge/envs/baryons/bin/python estimate_residual_pixwin/k1000_estimate_residual_pixwin.py 2
# addqueue -n 1x$cores -s -q $queue -m 7 -o log/k1000_estimate_residual_pixwin_zbin0.log  -c "KiDS0 pixwin" $HOME/mambaforge/envs/baryons/bin/python estimate_residual_pixwin/k1000_estimate_residual_pixwin.py 3
# addqueue -n 1x$cores -s -q $queue -m 7 -o log/k1000_estimate_residual_pixwin_zbin4.log  -c "KiDS4 pixwin" $HOME/mambaforge/envs/baryons/bin/python estimate_residual_pixwin/k1000_estimate_residual_pixwin.py 4
