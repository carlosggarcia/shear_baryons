#!/usr/bin/python3

import numpy as np
import pyccl as ccl
import sacc
import healpy as hp
import os

def get_masks_paths(no_k1000_overlap=False, no_hscdr1wl_overlap=False):
    masks_paths = {'DESY3wl__0': '/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY3wl0_coordC_ns4096.fits.gz',
                   'DESY3wl__1': '/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY3wl1_coordC_ns4096.fits.gz',
                   'DESY3wl__2': '/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY3wl2_coordC_ns4096.fits.gz',
                   'DESY3wl__3': '/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY3wl3_coordC_ns4096.fits.gz',
                   'KiDS1000__0': '/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__0_coordC_ns4096.fits.gz',
                   'KiDS1000__1': '/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__1_coordC_ns4096.fits.gz',
                   'KiDS1000__2': '/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__2_coordC_ns4096.fits.gz',
                   'KiDS1000__3': '/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__3_coordC_ns4096.fits.gz',
                   'KiDS1000__4': '/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__4_coordC_ns4096.fits.gz',
                   'HSCDR1wl__0': '/mnt/extraspace/damonge/Datasets/HSC_DR1/lite/HSCDR1wl_bin0_signal_map_coordC_ns4096.fits.gz',
                   'HSCDR1wl__1': '/mnt/extraspace/damonge/Datasets/HSC_DR1/lite/HSCDR1wl_bin1_signal_map_coordC_ns4096.fits.gz',
                   'HSCDR1wl__2': '/mnt/extraspace/damonge/Datasets/HSC_DR1/lite/HSCDR1wl_bin2_signal_map_coordC_ns4096.fits.gz',
                   'HSCDR1wl__3': '/mnt/extraspace/damonge/Datasets/HSC_DR1/lite/HSCDR1wl_bin3_signal_map_coordC_ns4096.fits.gz'
                   }
    if no_hscdr1wl_overlap and no_k1000_overlap:
        masks_paths.update({
               # DESY3 without the KiDS patch
               'DESY3wl__0': '/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY3wl0_noK1000_noHSCDR1wl_coordC_ns4096.fits.gz',
               'DESY3wl__1': '/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY3wl1_noK1000_noHSCDR1wl_coordC_ns4096.fits.gz',
               'DESY3wl__2': '/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY3wl2_noK1000_noHSCDR1wl_coordC_ns4096.fits.gz',
               'DESY3wl__3': '/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY3wl3_noK1000_noHSCDR1wl_coordC_ns4096.fits.gz',
               'KiDS1000__0': '/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__0_noHSCDR1wl_coordC_ns4096.fits.gz',
               'KiDS1000__1': '/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__1_noHSCDR1wl_coordC_ns4096.fits.gz',
               'KiDS1000__2': '/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__2_noHSCDR1wl_coordC_ns4096.fits.gz',
               'KiDS1000__3': '/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__3_noHSCDR1wl_coordC_ns4096.fits.gz',
               'KiDS1000__4': '/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__4_noHSCDR1wl_coordC_ns4096.fits.gz',
        })
    elif no_k1000_overlap:
        masks_paths.update({
               # DESY3 without the KiDS patch
               'DESY3wl__0': '/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY3wl0_noK1000_coordC_ns4096.fits.gz',
               'DESY3wl__1': '/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY3wl1_noK1000_coordC_ns4096.fits.gz',
               'DESY3wl__2': '/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY3wl2_noK1000_coordC_ns4096.fits.gz',
               'DESY3wl__3': '/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY3wl3_noK1000_coordC_ns4096.fits.gz',
        })

    # Andrina's HSC sacc file
    masks_paths['wl_0'] = masks_paths['HSCDR1wl__0']
    masks_paths['wl_1'] = masks_paths['HSCDR1wl__1']
    masks_paths['wl_2'] = masks_paths['HSCDR1wl__2']
    masks_paths['wl_3'] = masks_paths['HSCDR1wl__3']

    return masks_paths


class SSC():
    def __init__(self, sacc_file, masks_paths, fsky_approx=False):
        self.sacc_file = sacc.Sacc.load_fits(sacc_file)
        self.masks_paths = masks_paths
        self.cosmo = None
        self.tk3D = None
        self.ccl_tracers = None
        # Assuming same binning for all Cells
        self.leff = None
        self.masks = {}
        self.masks_prod_lm = {}
        self.sigma2_B = {}
        self.scale_factor = None
        self.ccl_ssc = {}
        self.cov = None
        self.fsky_approx = fsky_approx

    def get_fsky(self, tr1, tr2, tr3, tr4):
        m1 = self.get_mask(tr1)
        m2 = self.get_mask(tr2)
        m3 = self.get_mask(tr3)
        m4 = self.get_mask(tr4)
        return np.mean(((m1 > 0) & (m2 > 0) & (m3 > 0) & (m4 > 0)))

    def get_scale_factor(self, tr1, tr2, tr3, tr4):
        if self.scale_factor is None:
            # Get range of redshifts. z_min = 0 for compatibility with the
            # limber integrals
            s = self.sacc_file
            z_max = np.inf
            for trn in [tr1, tr2, tr3, tr4]:
                tr = s.tracers[trn]
                z_max = np.min(tr.z.max())
            # Use the a's in the pk spline
            cosmo = self.get_cosmology()
            na = ccl.ccllib.get_pk_spline_na(cosmo.cosmo)
            a, _ = ccl.ccllib.get_pk_spline_a(cosmo.cosmo, na, 0)
            # Cut the array for efficiency
            sel = 1 / a < z_max + 1
            # Include the next node so that z_max is in the range
            sel[np.sum(~sel) - 1] = True
            self.scale_factor = a[sel]

        return self.scale_factor

    def get_leff(self):
        if self.leff is None:
            s = self.sacc_file
            trs = s.get_tracer_combinations()[0]
            self.leff, _ = self.sacc_file.get_ell_cl('cl_ee', trs[0], trs[1])

        return self.leff

    def get_cosmology(self):
        if self.cosmo is None:
            self.cosmo = ccl.CosmologyVanillaLCDM()
        return self.cosmo

    def get_tk3D(self):
        if self.tk3D is not None:
            return self.tk3D
        cosmo = self.get_cosmology()
        mass_def = ccl.halos.MassDef200m()
        hmf = ccl.halos.MassFuncTinker08(cosmo, mass_def=mass_def)
        hbf = ccl.halos.HaloBiasTinker10(cosmo, mass_def=mass_def)
        nfw = ccl.halos.HaloProfileNFW(
            ccl.halos.ConcentrationDuffy08(mass_def), fourier_analytic=True
        )
        hmc = ccl.halos.HMCalculator(cosmo, hmf, hbf, mass_def)


        tk3D = ccl.halos.halomod_Tk3D_SSC_linear_bias(cosmo=cosmo,
                                                      hmc=hmc,
                                                      prof=nfw,
                                                      bias1=1,
                                                      bias2=1,
                                                      bias3=1,
                                                      bias4=1,
                                                      is_number_counts1=False,
                                                      is_number_counts2=False,
                                                      is_number_counts3=False,
                                                      is_number_counts4=False)
        self.tk3D = tk3D
        return self.tk3D

    def get_ccl_tracers(self):
        if self.ccl_tracers is None:
            cosmo = self.get_cosmology()
            tr_ccl = {}
            for trn, tr in self.sacc_file.tracers.items():
                tr_ccl[trn] = ccl.WeakLensingTracer(cosmo, dndz=(tr.z, tr.nz))

            self.ccl_tracers = tr_ccl
        return self.ccl_tracers

    def get_mask(self, tr):
        if tr not in self.masks:
            self.masks[tr] = hp.read_map(self.masks_paths[tr])
        return self.masks[tr]

    def get_mask_products_lm(self, tr1, tr2):
        key = tr1 + '_x_' + tr2
        key2 = tr2 + '_x_' + tr1
        if (key not in self.masks_prod_lm) and \
                (key2 not in self.masks_prod_lm):
            m12 = self.get_mask(tr1) * self.get_mask(tr2)
            self.masks_prod_lm[key]= hp.map2alm(m12)
        return self.masks_prod_lm[key]

    def get_masks_wl(self, tr1, tr2, tr3, tr4):
        alm = self.get_mask_products_lm(tr1, tr2)
        blm = self.get_mask_products_lm(tr3, tr4)
        m12 = self.get_mask(tr1) * self.get_mask(tr2)
        m34 = self.get_mask(tr3) * self.get_mask(tr4)
        area = hp.nside2pixarea(hp.npix2nside(self.get_mask(tr1).size))
        mask_wl = hp.alm2cl(alm, blm)
        mask_wl *= 2 * np.arange(mask_wl.size) + 1
        mask_wl /= np.sum(m12) * np.sum(m34) * area**2

        return mask_wl

    def get_sigma2_B(self, tr1, tr2, tr3, tr4):
        keys = ['_x_'.join([tr1, tr2, tr3, tr4]),
                '_x_'.join([tr2, tr1, tr3, tr4]),
                '_x_'.join([tr1, tr2, tr4, tr3]),
                '_x_'.join([tr3, tr4, tr1, tr2]),
                '_x_'.join([tr4, tr3, tr1, tr2]),
                '_x_'.join([tr3, tr4, tr2, tr1]),
                '_x_'.join([tr3, tr4, tr2, tr1]),
                '_x_'.join([tr2, tr1, tr4, tr3])]

        inkeys = [k in self.sigma2_B for k in keys]
        if any(inkeys):
            sigma2_B = self.sigma2_B[inkeys.index(True)]
        else:
            cosmo = self.get_cosmology()
            a = self.get_scale_factor(tr1, tr2, tr3, tr4)
            if self.fsky_approx:
                fsky = self.get_fsky(tr1, tr2, tr3, tr4)
                sigma2_B = ccl.sigma2_B_disc(cosmo, a=a, fsky=fsky)
            else:
                mask_wl = self.get_masks_wl(tr1, tr2, tr3, tr4)
                sigma2_B = ccl.sigma2_B_from_mask(cosmo, a=a, mask_wl=mask_wl)
            self.sigma2_B[keys[0]] = sigma2_B

        return a, sigma2_B

    def get_ccl_ssc_block(self, tr1, tr2, tr3, tr4):
        keys = ['_x_'.join([tr1, tr2, tr3, tr4]),
                '_x_'.join([tr2, tr1, tr3, tr4]),
                '_x_'.join([tr1, tr2, tr4, tr3]),
                '_x_'.join([tr2, tr1, tr4, tr3])]

        keysT = ['_x_'.join([tr3, tr4, tr1, tr2]),
                '_x_'.join([tr4, tr3, tr1, tr2]),
                '_x_'.join([tr3, tr4, tr2, tr1]),
                '_x_'.join([tr3, tr4, tr2, tr1])]

        inkeys = [k in self.ccl_ssc for k in keys]
        inkeysT = [k in self.ccl_ssc for k in keysT]

        if any(inkeys):
            print('Recovering computed SSC', flush=True)
            cov_ssc = self.ccl_ssc[inkeys.index(True)]
        elif any(inkeysT):
            print('Recovering computed SSC', flush=True)
            cov_ssc = self.ccl_ssc[inkeysT.index(True)].T
        else:
            print('Computing SSC', flush=True)
            cosmo = self.get_cosmology()
            ccl_tracers = self.get_ccl_tracers()
            tk3D = self.get_tk3D()
            ell = self.get_leff()

            a, sigma2_B = self.get_sigma2_B(tr1, tr2, tr3, tr4)
            cov_ssc = \
                ccl.covariances.angular_cl_cov_SSC(cosmo,
                                                   cltracer1=ccl_tracers[tr1],
                                                   cltracer2=ccl_tracers[tr2],
                                                   cltracer3=ccl_tracers[tr3],
                                                   cltracer4=ccl_tracers[tr4],
                                                   ell=ell,
                                                   tkka=tk3D,
                                                   sigma2_B=(a, sigma2_B))
            self.ccl_ssc[keys[0]] = cov_ssc
        return cov_ssc

    def get_SSC_covariance(self):
        if self.cov is None:
            s = self.sacc_file
            cov = np.zeros_like(s.covariance.covmat)
            tracer_combinations = s.get_tracer_combinations()
            for i, trs1 in enumerate(tracer_combinations):
                ind1 = s.indices(data_type='cl_ee', tracers=trs1)
                for trs2 in tracer_combinations[i:]:
                    print(trs1, trs2, flush=True)
                    ind2 = s.indices(data_type='cl_ee', tracers=trs2)
                    cov_ssc = self.get_ccl_ssc_block(*trs1, *trs2)
                    cov[np.ix_(ind1, ind2)] = cov_ssc
                    cov[np.ix_(ind2, ind1)] = cov_ssc.T
            self.cov = cov

        return self.cov

    def get_sacc_file_SSC(self, only_ssc=False, write_to=None):
        s = self.sacc_file.copy()
        covG = s.covariance.dense
        SSC = self.get_SSC_covariance()
        if only_ssc:
            cov = SSC
        else:
            cov = covG+SSC
        s.add_covariance(cov, overwrite=True)

        if write_to is not None:
            s.save_fits(write_to)

        return s


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Compute the SSC",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('sacc_file', type=str,
                        help='Input data file in sacc format' )
    parser.add_argument('-o', '--output', type=str, default='',
                        help='Output file name. If not given, it will add SSC '
                        'to the input file name')
    parser.add_argument('--no_k1000_overlap', action='store_true',
                        help='If True, the DES masks will have KiDS patcha '
                        'removed')
    parser.add_argument('--no_hscdr1wl_overlap', action='store_true',
                        help='If True, the DES & KiDS masks will have HSCDR1wl '
                        'patches removed')
    parser.add_argument('--only_ssc', action='store_true',
                        help='If True, save only the SSC covariance in sacc')
    parser.add_argument('--fsky_approx', action='store_true',
                        help='If True, compact circular footprint will be '
                        'assumed covering a sky fraction fsky.')
    args = parser.parse_args()

    ##############################################################################

    masks_paths = get_masks_paths(args.no_k1000_overlap, args.no_hscdr1wl_overlap)

    ssc = SSC(args.sacc_file, masks_paths, fsky_approx=args.fsky_approx)
    output = args.output
    if output == '':
        bname, ext = os.path.splitext(args.sacc_file)
        if args.only_ssc:
            output = bname + '_SSC'
        else:
            output = bname + '_GNG'
        if args.fsky_approx:
            output += '_fsky'
        output += ext

    ssc.get_sacc_file_SSC(write_to=output, only_ssc=args.only_ssc)
