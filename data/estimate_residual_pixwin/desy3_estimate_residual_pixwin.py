"""
Estimate residual pixel window

1. Choose theory $C_\ell^{th}$
1. Generate 8192 shear maps ($\gamma_1, \gamma_2$)
1. Using the galaxies from the DES catalog (RA, DEC, weights) evaluate the field in those positions
1. Project into 4096 maps
1. Meassure $C_q$ (q = bandpowers)
1. Repeat x10
1. Estimate $w_q = \sqrt{<C_q>/C_q^{th}}$

We only need to do this for 1 case (e.g. 3-3) because all of the different tomobins have approx same number of galaxies
"""


# %matplotlib ipympl
import numpy as np
import healpy as hp
from matplotlib import pyplot as plt
import os
import sys
sys.path.append("/mnt/zfsusers/gravityls_3/codes/xCell")
from xcell.mappers.mapper_DESY3wl import MapperDESY3wl
from xcell.mappers.utils import get_map_from_points
import pymaster as nmt
import sacc
import pyccl as ccl

ZBIN = int(sys.argv[1])

NSIMS = 100

NSIDE_1 = 8192
NSIDE_2 = 4096


# Load data
print('Loading sacc file', flush=True)
s = sacc.Sacc.load_fits("/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3wl__2x2pt_4096/cls_cov.fits")

# Generate theory Cell
print('Generate theory Cell', flush=True)
cosmo = ccl.CosmologyVanillaLCDM()
tr = s.tracers[f'DESY3wl__{ZBIN}']
dndz = (tr.z, tr.nz)
wl_tracer = ccl.WeakLensingTracer(cosmo, dndz=dndz)

ell = np.arange(3*NSIDE_1)
clth = ccl.angular_cl(cosmo, cltracer1=wl_tracer, cltracer2=wl_tracer, ell=ell)

TT = TE = BB = 0 * clth
EE = clth

# Load galaxies
print('Load galaxies', flush=True)
config = {"zbin": ZBIN,
          "mode": "shear",
          "indexcat": f'/mnt/extraspace/damonge/Datasets/DES_Y3/catalogs/DESY3_indexcat_zbin{ZBIN}.h5',
          'file_nz': '/mnt/extraspace/damonge/Datasets/DES_Y3/data_products/2pt_NG_final_2ptunblind_02_26_21_wnz_maglim_covupdate.fits',
          # 'path_rerun': 'prueba/DESY3wl',
          'mask_name': f'mask_DESY3wl{ZBIN}',
          'nside': NSIDE_2,
          'coords': 'C'}
m = MapperDESY3wl(config)
# Pick values from galaxy catalog
pos = m.get_positions().copy()
weights = m.get_weights().copy()
# Mask map
mask = m.get_mask()
goodpix = mask > 0

# Find galaxy locations in pixels
print('Find galaxy locations in pixels', flush=True)
fname = f'./estimate_residual_pixwin/desy3_ipix_zbin{ZBIN}.npz'
if os.path.isfile(fname):
    print('Reading saved locations', flush=True)
    ipix = np.load(fname)['ipix']
else:
    ipix = hp.ang2pix(NSIDE_1, pos['ra'], pos['dec'], lonlat=True)
    np.savez_compressed(fname, ipix=ipix)

# Read workspace
print('Reading NmtWorkspace', flush=True)
wsp = nmt.NmtWorkspace()
wsp.read_from(f'/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3wl__2x2pt_4096/DESY3wl_DESY3wl/w__mask_DESY3wl{ZBIN}__mask_DESY3wl{ZBIN}.fits')

# Loop over simulations
print('Start simulating', flush=True)
cl_realization = []
fname = f'./estimate_residual_pixwin/desy3_gaussian_realizations_zbin{ZBIN}.txt'
if os.path.isfile(fname):
    os.remove(fname)
f = open(fname, 'a')
for i in range(NSIMS):
    print(f'Simulation {i+1}/{NSIMS}', flush=True)
    # Generate high-res map
    maps = hp.synfast([TT, TE, EE, BB], NSIDE_1)[1:]
    shear = maps[:, ipix]

    # Signal maps
    we1, we2 = get_map_from_points(pos, NSIDE_2,
                                   qu=shear,
                                   w=weights,
                                   ra_name='ra',
                                   dec_name='dec',
                                   rot=None)

    we1[goodpix] /= mask[goodpix]
    we2[goodpix] /= mask[goodpix]

    # Pseudo-Cell
    # Field
    f1 = f2 = nmt.NmtField(mask, [we1, we2], n_iter=0)
    cl_realization.append(wsp.decouple_cell(nmt.compute_coupled_cell(f1, f2)))

    np.savetxt(f, cl_realization[-1][0][None, :])
f.close()

print(f'Convolving theory with masks effects', flush=True)
clth_q = wsp.decouple_cell(wsp.couple_cell([clth, 0*clth, 0*clth, 0*clth]))

print(f'Computing residual pixwin', flush=True)
wl = np.sqrt(np.mean(cl_realization, axis=0) / clth_q)

print(f'Saving computations', flush=True)
np.savez_compressed(f'/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3wl__2x2pt_4096/desy3_pixwin_zbin{ZBIN}.npz', cl_realization=cl_realization, clth_q=clth_q, wl=wl, clth=clth)

print(f'Finished successfully', flush=True)
