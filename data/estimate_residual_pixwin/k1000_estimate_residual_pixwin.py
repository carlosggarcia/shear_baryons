"""
Estimate residual pixel window

1. Choose theory $C_\ell^{th}$
1. Generate 8192 shear maps ($\gamma_1, \gamma_2$)
1. Using the galaxies from the KiDS catalog (RA, DEC, weights) evaluate the field in those positions
1. Project into 4096 maps
1. Meassure $C_q$ (q = bandpowers)
1. Repeat x10
1. Estimate $w_q = \sqrt{<C_q>/C_q^{th}}$

For KiDS let's check bins 0, 2, 4 since the number densities are quie different: 0.62, 1.18, 1.85, 1.26, 1.31.
"""


# %matplotlib ipympl
import numpy as np
import healpy as hp
from matplotlib import pyplot as plt
import os
import sys
sys.path.append("/mnt/zfsusers/gravityls_3/codes/xCell")
from xcell.mappers.mapper_KiDS1000 import MapperKiDS1000
from xcell.mappers.utils import get_map_from_points
import pymaster as nmt
import sacc
import pyccl as ccl

ZBIN = sys.argv[1]

NSIMS = 100

NSIDE_1 = 8192
NSIDE_2 = 4096


# Load data
print('Loading sacc file', flush=True)
s = sacc.Sacc.load_fits("/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov_GNG.fits")

# Generate theory Cell
print('Generate theory Cell', flush=True)
cosmo = ccl.CosmologyVanillaLCDM()
tr = s.tracers[f'KiDS1000__{ZBIN}']
dndz = (tr.z, tr.nz)
wl_tracer = ccl.WeakLensingTracer(cosmo, dndz=dndz)

ell = np.arange(3*NSIDE_1)
clth = ccl.angular_cl(cosmo, cltracer1=wl_tracer, cltracer2=wl_tracer, ell=ell)

TT = TE = BB = 0 * clth
EE = clth

# Load galaxies
print('Load galaxies', flush=True)
config = {'mapper_class': 'MapperKiDS1000',
         'data_catalog': '/mnt/extraspace/damonge/Datasets/KiDS1000/KiDS_DR4.1_ugriZYJHKs_SOM_gold_WL_cat.fits',
         'file_nz': '/mnt/extraspace/damonge/Datasets/KiDS1000/SOM_N_of_Z/K1000_NS_V1.0.0A_ugriZYJHKs_photoz_SG_mask_LF_svn_309c_2Dbins_v2_SOMcols_Fid_blindC_TOMO1_Nz.asc',
         'mode': 'shear',
         'zbin': ZBIN,
         'mask_name': f'mask_KiDS1000__{ZBIN}',
         # 'path_rerun': '/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs',
         'nside': NSIDE_2,
         'coords': 'C',
         }
m = MapperKiDS1000(config)
# Pick values from galaxy catalog
kind, e1f, e2f, mod = m._set_mode()
data = m._get_gals_or_stars(kind)
pos = {'ra': data['ALPHA_J2000'],
       'dec': data['DELTA_J2000']}
weights = data['weight']
# Mask map
mask = m.get_mask()
goodpix = mask > 0

# Find galaxy locations in pixels
print('Find galaxy locations in pixels', flush=True)
fname = f'./estimate_residual_pixwin/k1000_ipix_zbin{ZBIN}.npz'
if os.path.isfile(fname):
    print('Reading saved locations', flush=True)
    ipix = np.load(fname)['ipix']
else:
    ipix = hp.ang2pix(NSIDE_1, pos['ra'], pos['dec'], lonlat=True)
    np.savez_compressed(fname, ipix=ipix)

# Read workspace
print('Reading NmtWorkspace', flush=True)
wsp = nmt.NmtWorkspace()
wsp.read_from(f'/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/KiDS1000_KiDS1000/w__mask_KiDS1000__{ZBIN}__mask_KiDS1000__{ZBIN}.fits')

# Loop over simulations
print('Start simulating', flush=True)
cl_realization = []
fname = f'./estimate_residual_pixwin/k1000_gaussian_realizations_zbin{ZBIN}.txt'
if os.path.isfile(fname):
    os.remove(fname)
f = open(fname, 'a')
for i in range(NSIMS):
    print(f'Simulation {i+1}/{NSIMS}', flush=True)
    # Generate high-res map
    maps = hp.synfast([TT, TE, EE, BB], NSIDE_1)[1:]
    shear = maps[:, ipix]

    # Signal maps
    we1, we2 = get_map_from_points(pos, NSIDE_2,
                                   qu=shear,
                                   w=weights,
                                   ra_name='ra',
                                   dec_name='dec',
                                   rot=None)

    we1[goodpix] /= mask[goodpix]
    we2[goodpix] /= mask[goodpix]

    # Pseudo-Cell
    # Field
    f1 = f2 = nmt.NmtField(mask, [we1, we2], n_iter=0)
    cl_realization.append(wsp.decouple_cell(nmt.compute_coupled_cell(f1, f2)))

    np.savetxt(f, cl_realization[-1][0][None, :])
f.close()

print(f'Convolving theory with masks effects', flush=True)
clth_q = wsp.decouple_cell(wsp.couple_cell([clth, 0*clth, 0*clth, 0*clth]))

print(f'Computing residual pixwin', flush=True)
wl = np.sqrt(np.mean(cl_realization, axis=0) / clth_q)

print(f'Saving computations', flush=True)
np.savez_compressed(f'/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/k1000_pixwin_zbin{ZBIN}.npz', cl_realization=cl_realization, clth_q=clth_q, wl=wl, clth=clth)

print(f'Finished successfully', flush=True)
