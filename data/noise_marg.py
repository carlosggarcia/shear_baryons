import os
import sacc
import numpy as np


NL_PRIOR = 10
path = "/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3wl__2x2pt_4096"
fname = os.path.join(path, "cls_cov_GNG.fits")

s = sacc.Sacc.load_fits(fname)
s.keep_selection(data_type="cl_ee")

fname_nlcov = os.path.join(path, "nlmarg_cov_prior10.npz")
if os.path.isfile(fname_nlcov):
    nlcov = np.load(fname_nlcov)['arr_0']
else:
    nlcov = np.zeros_like(s.covariance.covmat)
    for trn in s.tracers.keys():
        survey = trn.split("__")[0]
        ix = s.indices(data_type='cl_ee', tracers=(trn, trn))

        fname = os.path.join(path, f"{survey}_{survey}/cl_{trn}_{trn}.npz")
        nl = np.load(fname)['nl'][0]
        cov = NL_PRIOR**2 * (nl[:, None] * nl[None, :])

        nlcov[np.ix_(ix, ix)] = cov

    # Save the marginalized NL covariance
    np.savez_compressed(fname_nlcov, nlcov)

cov = s.covariance.covmat + nlcov

s.add_covariance(cov, overwrite=True)
fname = os.path.join(path, "cls_cov_GNG_nlmarg_prior10.fits")
s.save_fits(fname, overwrite=True)
