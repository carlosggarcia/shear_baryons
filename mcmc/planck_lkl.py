#!/usr/bin/python
"""
Multivariate Gaussian Likelihood of Planck2018 TTTEEE_lowl_lowE_lensing

Add the following lines to the Cobaya config file

    likelihood:
      p18: import_module("planck_lkl").planck2018_TTTEEE_lensing_Gaussian
"""
import numpy as np


names = ['A_sE9', 'Omega_m', 'Omega_b', 'm_nu', 'n_s', 'h']
cov = np.array([[ 9.50228196e-04,  3.76642840e-07,  2.42298853e-06,
                  4.80261449e-04,  2.48912420e-05, -3.25015730e-06],
                [ 3.76642840e-07,  1.96986535e-04,  1.95397583e-05,
                  9.20679051e-04, -3.64540694e-05, -1.47813050e-04],
                [ 2.42298853e-06,  1.95397583e-05,  2.00574450e-06,
                  9.92488825e-05, -3.32236359e-06, -1.46603050e-05],
                [ 4.80261449e-04,  9.20679051e-04,  9.92488825e-05,
                  5.99201018e-03, -1.02253921e-04, -7.05905771e-04],
                [ 2.48912420e-05, -3.64540694e-05, -3.32236359e-06,
                 -1.02253921e-04,  1.81266356e-05,  2.67536041e-05],
                [-3.25015730e-06, -1.47813050e-04, -1.46603050e-05,
                 -7.05905771e-04,  2.67536041e-05,  1.11679166e-04]])
icov = np.linalg.inv(cov)
mean = np.array([2.10265384, 0.31913089, 0.04972147, 0.08999469,
                 0.9647078, 0.67085764])


def planck2018_TTTEEE_lensing_Gaussian(A_sE9, Omega_m, Omega_b, m_nu, n_s, h):
    """
    Multivariate Gaussian Likelihood of
    public_chains/base_mnu/plikHM_TTTEEE_lowl_lowE_lensing/base_mnu_plikHM_TTTEEE_lowl_lowE_lensing
    """
    th = np.array([A_sE9, Omega_m, Omega_b, m_nu, n_s, h])
    d = th - mean
    chi2 = d.dot(icov.dot(d))

    return -0.5 * chi2
