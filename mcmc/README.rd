All plots were produced with `shear-baryons_plots.ipynb`.

The jupyter notebook is too heavy to be uploaded to GitLab. In order to
recreate it from the python script, use:

```
python -m jupytext --to ipynb:percent shear-baryons_plots.py
```

if you don't want to replace your notebook you can pass `--output
output.ipynb`.

After making your changes to the notebook, if it is not paired automatically
(see [jupytext doc](https://jupytext.readthedocs.io/en/latest/)), use

```
python -m jupytext --to py:percent shear-baryons_plots.ipynb
```
