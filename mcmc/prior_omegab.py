#!/usr/bin/python
"""
Prior based on Table 1 of Giovanni's paper (2303.05537)

Add the following lines to the Cobaya config file

    prior:
      omegab: import_module("prior_omegab").omegab_prior_logp
"""
import numpy as np


def omegab_prior_logp(Omega_b, h):
    omegab = Omega_b * h**2
    if (omegab < 0.009) or (omegab > 0.040):
        chi2 = -np.inf
    else:
        chi2 = 0
    return chi2

def omegab_BBN_prior_logp(Omega_b, h):
    omegab = Omega_b * h**2

    # Planck18 (1807.06209) after Eq. 73: "conservative BBN prior" based on the
    # primordial deuterium abundance measurements of Cooke et al 2018
    mean = 0.0222
    sigma = 0.0005

    return -0.5 * (omegab - mean)**2 / sigma**2
