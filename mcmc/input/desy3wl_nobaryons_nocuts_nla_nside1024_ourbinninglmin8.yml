# Configuration file to reproduce Table 1 in 2303.05537

sampler:
  mcmc:
    burn_in: 0
    max_tries: 1000
    drag: True

# BACCO EMU parameter range. Note that our priors are slightly broader. Bacco
# will complain but the chains will continue
#
# Without baryons, the range is a bit broader. We migh need to enforce the
# parameter range of the case with baryons if we see weird effects.

params:
  # Cosmological parameters
  A_sE9:
    prior:
      min: 0.5
      max: 5.
    ref:
      dist: norm
      loc: 2.02
      scale: 0.01
    latex: A_s
    proposal: 0.001

  Omega_m:
    prior: 
      min: 0.1
      max: 0.7
    ref:
      dist: norm
      loc: 0.30
      scale: 0.01
    latex: \Omega_m
    proposal: 0.01

  Omega_c:
    latex: \Omega_c

  Omega_b:
    prior: 
      min: 0.03
      max: 0.07
    ref:
      dist: norm
      loc: 0.04
      scale: 0.001
    latex: \Omega_b
    proposal: 0.001

      #  omega_b:
      #    value: 'lambda Omega_b, h: Omega_b * h**2'
      #    min: 0.009
      #    max: 0.040
      #    latex: \omega_b

  h: 
    prior: 
      min: 0.55
      max: 0.91
    ref:
      dist: norm
      loc: 0.7
      scale: 0.02
    latex: h
    proposal: 0.02

  n_s:
    prior: 
      min: 0.87
      max: 1.07
    ref:
      dist: norm
      loc: 0.96
      scale: 0.02
    latex: n_s
    proposal: 0.02

  m_nu: 
    prior:
      min: 0.0559
      max: 0.400
    latex: M_\nu

  sigma8:
    latex: \sigma_8

  S8:
    latex: S_8

  # Bias parameters
  bias_DESY3wl__0_m:
    prior: 
      dist: norm
      loc: -0.0063
      scale: 0.0091
    latex: m^{DESY3wl_0}
    proposal: 0.005

  bias_DESY3wl__1_m: 
    prior: 
      dist: norm
      loc: -0.0198
      scale: 0.0078
    latex: m^{DESY3w}_1l
    proposal: 0.005

  bias_DESY3wl__2_m: 
    prior: 
      dist: norm
      loc: -0.0241
      scale: 0.0076
    latex: m^{DESY3wl_2}
    proposal: 0.005

  bias_DESY3wl__3_m:
    prior: 
      dist: norm
      loc: -0.0369
      scale: 0.0076
    latex: m^{DESY3wl_3}
    proposal: 0.005

  limber_DESY3wl__0_dz:
    prior: 
      dist: norm
      loc: 0.0
      scale: 0.018
    latex: \Delta z^{DESY3wl_0}
    proposal: 0.005

  limber_DESY3wl__1_dz:
    prior: 
      dist: norm
      loc: 0.0
      scale: 0.015
    latex: \Delta z^{DESY3wl_1}
    proposal: 0.005
    
  limber_DESY3wl__2_dz:
    prior: 
      dist: norm
      loc: 0.0
      scale: 0.011
    latex: \Delta z^{DESY3wl_2}
    proposal: 0.005

  limber_DESY3wl__3_dz:
    prior: 
      dist: norm
      loc: 0.0
      scale: 0.017
    latex: \Delta z^{DESY3wl_3}
    proposal: 0.005

  # In this paper, the same IA was used for all wl samples
  bias_A_IA:
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: 0.
      scale: 0.1
    latex: A_{IA}
    proposal: 0.1

  limber_eta_IA: 
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: 0.
      scale: 0.1
    latex: \eta_{IA}
    proposal: 0.1

# CCL settings
theory:
  cl_like.CCL:
    transfer_function: boltzmann_camb
    matter_pk: halofit
    baryons_pk: nobaryons
  cl_like.Pk:
    # Linear, EulerianPT, LagrangianPT
    bias_model: "BaccoPT"
    zmax_pks: 1.5
    use_baryon_boost: False
    nonlinear_emu_path: '/mnt/zfsusers/gravityls_3/codes/NN_emulator_PCA6_0.95_300_400n_paired_comb'
    nonlinear_emu_details : 'details.pickle'
    allow_bcm_emu_extrapolation_for_shear: True
    allow_halofit_extrapolation_for_shear: True
    ignore_lbias: True  # Only if shear-shear analysis
  cl_like.Limber:
    nz_model: "NzShift"
    ia_model: "IADESY1"
    input_params_prefix: "limber"
  cl_like.ClFinal:
    input_params_prefix: "bias"
    shape_model: ShapeMultiplicative

# Likelihood settings
likelihood:
  cl_like.ClLike:
    # Input sacc file
    input_file: /mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3wl__2x2pt_ourbinninglmin8/cls_cov.fits
    # List all relevant bins. 
    bins:
      - name: DESY3wl__0
      - name: DESY3wl__1
      - name: DESY3wl__2
      - name: DESY3wl__3

    # List all 2-points that should go into the
    # data vector. For now we only include
    # galaxy-galaxy auto-correlations, but all
    # galaxy-shear and shear-shear correlations.
    twopoints:
      - bins: [DESY3wl__0, DESY3wl__0]
      - bins: [DESY3wl__0, DESY3wl__1]
      - bins: [DESY3wl__0, DESY3wl__2]
      - bins: [DESY3wl__0, DESY3wl__3]
      - bins: [DESY3wl__1, DESY3wl__1]
      - bins: [DESY3wl__1, DESY3wl__2]
      - bins: [DESY3wl__1, DESY3wl__3]
      - bins: [DESY3wl__2, DESY3wl__2]
      - bins: [DESY3wl__2, DESY3wl__3]
      - bins: [DESY3wl__3, DESY3wl__3]

    defaults:
      # These ones will apply to all power spectra (unless the lmax
      # corresponding to the chosen kmax is smaller).
      # lmin = 0 in DESY3 analysis 2203.07128
      # lmax = 2NSide
      lmin: 0
      lmax: 2048 

prior:
  omegab: import_module("prior_omegab").omegab_prior_logp

debug: False
output: 'chains/desy3wl_nobaryons_nocuts_nla_nside1024_ourbinninglmin8/desy3wl_nobaryons_nocuts_nla_nside1024_ourbinninglmin8'
