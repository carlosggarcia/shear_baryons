# Configuration file to reproduce Table 1 in 2303.05537

sampler:
  mcmc:
    burn_in: 0
    max_tries: 1000
    drag: True
    Rminus1_stop: 0.03  # Good enough (see desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_Rminus1003)

# BACCO EMU parameter range. Note that our priors are slightly broader. Bacco
# will complain but the chains will continue
#
# Without baryons, the range is a bit broader. We migh need to enforce the
# parameter range of the case with baryons if we see weird effects.

params:
  # Cosmological parameters
  A_sE9:
    prior:
      min: 0.5
      max: 5.
    ref:
      dist: norm
      loc: 2.02
      scale: 0.01
    latex: A_s
    proposal: 0.001

  Omega_m:
    prior: 
      min: 0.1
      max: 0.7
    ref:
      dist: norm
      loc: 0.30
      scale: 0.01
    latex: \Omega_m
    proposal: 0.01

  Omega_c:
    latex: \Omega_c

  Omega_b:
    prior: 
      min: 0.03
      max: 0.07
    ref:
      dist: norm
      loc: 0.04
      scale: 0.001
    latex: \Omega_b
    proposal: 0.001

  h: 
    prior: 
      min: 0.55
      max: 0.91
    ref:
      dist: norm
      loc: 0.7
      scale: 0.02
    latex: h
    proposal: 0.02

  n_s:
    prior: 
      min: 0.87
      max: 1.07
    ref:
      dist: norm
      loc: 0.96
      scale: 0.02
    latex: n_s
    proposal: 0.02

  m_nu: 
    prior:
      min: 0.0559
      max: 0.400
    latex: M_\nu

  sigma8:
    latex: \sigma_8

  S8:
    latex: S_8

  # Bias and dz parameters from 1809.09148
  bias_wl_0_m:
    prior: 
      dist: norm
      loc: 0
      scale: 0.01
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: m^{HSC}_0
    proposal: 0.005

  bias_wl_1_m: 
    prior: 
      dist: norm
      loc: 0
      scale: 0.01
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: m^{HSC}_1
    proposal: 0.005

  bias_wl_2_m: 
    prior: 
      dist: norm
      loc: 0
      scale: 0.01
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: m^{HSC}_2
    proposal: 0.005

  bias_wl_3_m:
    prior: 
      dist: norm
      loc: 0
      scale: 0.01
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: m^{HSC}_3
    proposal: 0.005

  limber_wl_0_dz:
    prior: 
      dist: norm
      loc: 0
      scale: 0.0285
    latex: \Delta z^{HSC}_0
    proposal: 0.016

  limber_wl_1_dz:
    prior: 
      dist: norm
      loc: 0
      scale: 0.0135
    latex: \Delta z^{HSC}_1
    proposal: 0.009
    
  limber_wl_2_dz:
    prior: 
      dist: norm
      loc: 0
      scale: 0.0383
    latex: \Delta z^{HSC}_2
    proposal: 0.010

  limber_wl_3_dz:
    prior: 
      dist: norm
      loc: 0
      scale: 0.0376
    latex: \Delta z^{HSC}_3
    proposal: 0.01

  # In this paper, the same IA was used for all wl samples
  bias_A_IA:
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: 0.
      scale: 0.1
    latex: A_{IA}
    proposal: 0.1

  limber_eta_IA: 
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: 0.
      scale: 0.1
    latex: \eta_{IA}
    proposal: 0.1

# CCL settings
theory:
  cl_like.CCL:
    transfer_function: boltzmann_camb
    matter_pk: halofit
    baryons_pk: nobaryons
  cl_like.Pk:
    # Linear, EulerianPT, LagrangianPT
    bias_model: "BaccoPT"
    zmax_pks: 1.5
    use_baryon_boost: False
    nonlinear_emu_path: '/mnt/zfsusers/gravityls_3/codes/NN_emulator_PCA6_0.95_300_400n_paired_comb'
    nonlinear_emu_details: 'details.pickle'
    allow_bcm_emu_extrapolation_for_shear: True
    allow_halofit_extrapolation_for_shear: True
    ignore_lbias: True  # Only if shear-shear analysis
  cl_like.Limber:
    nz_model: "NzShift"
    ia_model: "IADESY1"
    input_params_prefix: "limber"
  cl_like.ClFinal:
    input_params_prefix: "bias"
    shape_model: ShapeMultiplicative

# Likelihood settings
likelihood:
  cl_like.ClLike:
    # Input sacc file
    # We need to use Andrina's one or compute the SSC somehow to add it to our
    # NaMaster cov
    input_file:  '/mnt/extraspace/gravityls_3/sacc_files/cls_signal_covGNG_HSC_Andrina_paper_newNz.fits'
    # List all relevant bins. 
    bins:
      - name: wl_0
      - name: wl_1
      - name: wl_2
      - name: wl_3

    # List all 2-points that should go into the
    # data vector. For now we only include
    # galaxy-galaxy auto-correlations, but all
    # galaxy-shear and shear-shear correlations.
    twopoints:
      - bins: [wl_0, wl_0]
      - bins: [wl_0, wl_1]
      - bins: [wl_0, wl_2]
      - bins: [wl_0, wl_3]
      - bins: [wl_1, wl_1]
      - bins: [wl_1, wl_2]
      - bins: [wl_1, wl_3]
      - bins: [wl_2, wl_2]
      - bins: [wl_2, wl_3]
      - bins: [wl_3, wl_3]

    defaults:
      # These ones will apply to all power spectra (unless the lmax
      # corresponding to the chosen kmax is smaller).
      # We use lmin = 300 as in the official analysis 1809.09148
      lmin: 300
      lmax: 4500

prior:
  omegab: import_module("prior_omegab").omegab_prior_logp

debug: False
output: 'chains/hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin300/hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin300'
