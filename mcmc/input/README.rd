# BACCOemu range in version NN_emulator_PCA6_0.95_300_400n_paired_comb

## With baryons
 - omega_cold [0.23 0.4 ] 
 - sigma8_cold [0.73 0.9 ] 
 - omega_baryon [0.04 0.06] 
 - ns [0.92 1.01] 
 - hubble [0.6 0.8] 
 - neutrino_mass [0.  0.4] 
 - w0 [-1.15 -0.85] 
 - wa [-0.3 0.3] 
 - M_c [ 9. 15.] 
 - eta [-0.69897  0.69897] 
 - beta [-1. 0.69897] 
 - M1_z0_cen [ 9. 13.] 
 - theta_inn [-2. -0.52287875] 
 - M_inn [ 9.  13.5] 
 - theta_out [0.  0.47712125] 
 - expfactor [0.4 1. ]

## With no baryons
 - omega_cold [0.23 0.4 ] 
 - sigma8_cold [0.65 0.9 ] 
 - omega_baryon [0.04 0.06] 
 - ns [0.92 1.01] 
 - hubble [0.6 0.8] 
 - neutrino_mass [0.  0.4] 
 - w0 [-1.15 -0.85] 
 - wa [-0.3 0.3] 
 - expfactor [0.3 1.1]

# BACCOemu range with baryons and version NN_emulator_PCA6_0.95_300_400n_paired

## With no baryons
 - omega_cold [0.15 0.47] 
 - sigma8_cold [0.4  1.15] 
 - omega_baryon [0.03 0.07] 
 - ns [0.83 1.1 ] 
 - hubble [0.5 0.9] 
 - neutrino_mass [0.  0.4] 
 - w0 [-1.4 -0.6] 
 - wa [-0.5 0.5] 
 - expfactor [0.275 1.1  ]
