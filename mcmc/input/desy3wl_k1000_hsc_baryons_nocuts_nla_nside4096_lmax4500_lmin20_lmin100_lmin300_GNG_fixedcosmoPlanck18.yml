# Configuration file with DESY3 + HSCDR1 + KiDS1000, baryons and fixed
# cosmology

sampler:
  mcmc:
    burn_in: 0
    max_tries: 1000
    drag: True
    Rminus1_stop: 0.03  # Good enough (see desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_Rminus1003)

# BACCO EMU parameter range. Note that our priors are slightly broader. Bacco
# will complain but the chains will continue
#
# Without baryons, the range is a bit broader. We migh need to enforce the
# parameter range of the case with baryons if we see weird effects.

params:
  # Cosmological parameters from Planck18 with TTTEEE+lowE+lensing in Table 2
  # https://arxiv.org/pdf/1807.06209.pdf
  A_sE9: 2.100

  Omega_m: 0.3153

  Omega_b: 0.04930  # omega_b / h^2: 0.02237 / 0.6736^2

  h: 0.6736

  n_s: 0.9649

  m_nu: 0.06

  Omega_c:
    latex: \Omega_c
      
  sigma8:
    latex: \sigma_8

  S8:
    latex: S_8

  # DESY3wl biases
  # Bias parameters
  bias_DESY3wl__0_m:
    prior: 
      dist: norm
      loc: -0.0063
      scale: 0.0091
    latex: m^{DESY3wl_0}
    proposal: 0.005

  bias_DESY3wl__1_m: 
    prior: 
      dist: norm
      loc: -0.0198
      scale: 0.0078
    latex: m^{DESY3w}_1l
    proposal: 0.005

  bias_DESY3wl__2_m: 
    prior: 
      dist: norm
      loc: -0.0241
      scale: 0.0076
    latex: m^{DESY3wl_2}
    proposal: 0.005

  bias_DESY3wl__3_m:
    prior: 
      dist: norm
      loc: -0.0369
      scale: 0.0076
    latex: m^{DESY3wl_3}
    proposal: 0.005

  limber_DESY3wl__0_dz:
    prior: 
      dist: norm
      loc: 0.0
      scale: 0.018
    latex: \Delta z^{DESY3wl_0}
    proposal: 0.005

  limber_DESY3wl__1_dz:
    prior: 
      dist: norm
      loc: 0.0
      scale: 0.015
    latex: \Delta z^{DESY3wl_1}
    proposal: 0.005
    
  limber_DESY3wl__2_dz:
    prior: 
      dist: norm
      loc: 0.0
      scale: 0.011
    latex: \Delta z^{DESY3wl_2}
    proposal: 0.005

  limber_DESY3wl__3_dz:
    prior: 
      dist: norm
      loc: 0.0
      scale: 0.017
    latex: \Delta z^{DESY3wl_3}
    proposal: 0.005

  # Intrinsic alignments for DES sample
  bias_DESY3wl_A_IA:
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: 0.
      scale: 0.1
    latex: A^{\rm DESY3wl}_{IA}
    proposal: 0.1

  limber_DESY3wl_eta_IA: 
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: 0.
      scale: 0.1
    latex: \eta^{\rm DESY3wl}_{IA}
    proposal: 0.1

  # KiDS1000 biases
  # Bias parameters
  # Note: dz's in ClLike are defined as in DES (nz_true = nz(z + dz)). So the
  # sign is the opposite for KiDS
  limber_KiDS1000__0_dz:
    prior: 
      dist: norm
      loc: 0.000
      scale: 0.0106
    latex: \Delta z^{KiDS1000_0}
    proposal: 0.005

  limber_KiDS1000__1_dz:
    prior: 
      dist: norm
      loc: -0.002
      scale: 0.0113
    latex: \Delta z^{KiDS1000_1}
    proposal: 0.005

  limber_KiDS1000__2_dz:
    prior: 
      dist: norm
      loc: -0.013
      scale: 0.0118
    latex: \Delta z^{KiDS1000_2}
    proposal: 0.005

  limber_KiDS1000__3_dz:
    prior: 
      dist: norm
      loc: -0.011
      scale: 0.0087
    latex: \Delta z^{KiDS1000_3}
    proposal: 0.005

  limber_KiDS1000__4_dz:
    prior: 
      dist: norm
      loc: 0.006
      scale: 0.0097
    latex: \Delta z^{KiDS1000_4}
    proposal: 0.005

  bias_KiDS1000__0_m:
    prior: 
      dist: norm
      loc: 0.000
      scale: 0.019
    latex: m^{KiDS1000_0}
    proposal: 0.005

  bias_KiDS1000__1_m:
    prior: 
      dist: norm
      loc: 0.000
      scale: 0.020
    latex: m^{KiDS1000_1}
    proposal: 0.005

  bias_KiDS1000__2_m:
    prior: 
      dist: norm
      loc: 0.000
      scale: 0.017
    latex: m^{KiDS1000_2}
    proposal: 0.005

  bias_KiDS1000__3_m:
    prior: 
      dist: norm
      loc: 0.000
      scale: 0.012
    latex: m^{KiDS1000_3}
    proposal: 0.005

  bias_KiDS1000__4_m:
    prior: 
      dist: norm
      loc: 0.000
      scale: 0.010
    latex: m^{KiDS1000_4}
    proposal: 0.005

  # Intrinsic alignments for the KiDS1000 sample
  bias_KiDS1000_A_IA:
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: 0.
      scale: 0.1
    latex: A^{\rm KiDS1000}_{IA}
    proposal: 0.1

  limber_KiDS1000_eta_IA: 
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: 0.
      scale: 0.1
    latex: \eta^{\rm KiDS1000}_{IA}
    proposal: 0.1
     
  # Bias and dz parameters from 1809.09148
  bias_HSCDR1wl__0_m:
    prior: 
      dist: norm
      loc: 0
      scale: 0.01
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: m^{HSC_0}
    proposal: 0.005

  bias_HSCDR1wl__1_m: 
    prior: 
      dist: norm
      loc: 0
      scale: 0.01
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: m^{HSC_1}
    proposal: 0.005

  bias_HSCDR1wl__2_m: 
    prior: 
      dist: norm
      loc: 0
      scale: 0.01
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: m^{HSC_2}
    proposal: 0.005

  bias_HSCDR1wl__3_m:
    prior: 
      dist: norm
      loc: 0
      scale: 0.01
    ref:
      dist: norm
      loc: 0.
      scale: 0.005
    latex: m^{HSC_3}
    proposal: 0.005

  limber_HSCDR1wl__0_dz:
    prior: 
      dist: norm
      loc: 0
      scale: 0.0285
    latex: \Delta z^{HSC_0}
    proposal: 0.016

  limber_HSCDR1wl__1_dz:
    prior: 
      dist: norm
      loc: 0
      scale: 0.0135
    latex: \Delta z^{HSC_1}
    proposal: 0.009
    
  limber_HSCDR1wl__2_dz:
    prior: 
      dist: norm
      loc: 0
      scale: 0.0383
    latex: \Delta z^{HSC_2}
    proposal: 0.010

  limber_HSCDR1wl__3_dz:
    prior: 
      dist: norm
      loc: 0
      scale: 0.0376
    latex: \Delta z^{HSC_3}
    proposal: 0.01

  # In this paper, the same IA was used for all HSCDR1wl samples
  bias_HSCDR1wl_A_IA:
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: 0.
      scale: 0.1
    latex: A^{\rm HSCDR1wl}_{IA}
    proposal: 0.1

  limber_HSCDR1wl_eta_IA: 
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: 0.
      scale: 0.1
    latex: \eta^{\rm HSCDR1wl}_{IA}
    proposal: 0.1
     
  # Baryon's parameters. They are actually log of the quantity
  # Priors based on Giovanni's paper: 2303.05537
  M_c:
    prior:
      min: 9.0
      max: 15.0
    ref:
      dist: norm
      loc: 13
      scale: 1
    proposal: 0.5
    latex: \log(M_c)
  eta:
    prior:
      min: -0.7 # -0.69897 is the exact boundary in baryon emu
      max: 0.7  # 0.69897 is the exact boundary in baryon emu
    ref:
      dist: norm
      loc: -0.3
      scale: 0.1
    proposal: 0.05
    latex: \log(\eta)
  beta:
    prior:
      min: -1.0
      max: 0.7  # 0.69897 is the exact boundary in baryon emu
    ref:
      dist: norm
      loc: -0.22
      scale: 0.1
    proposal: 0.05
    latex: \log(\beta)
  M1_z0_cen:
    prior:
      min: 9.0
      max: 13
    ref:
      dist: norm
      loc: 12.5
      scale: 0.5
    proposal: 0.5
    latex: \log(M_{z_0, {\rm cen}})
  theta_inn:
    prior:
      min: -2.00
      max: -0.53  # -0.52287875 is the exact boundary in baryon emu
    ref:
      dist: norm
      loc: -0.86
      scale: 0.1
    proposal: 0.1
    latex: \log(\theta_{\rm inn})
  theta_out:
    # This is -log10(theta_out_in_paper)
    prior:
      min: 0.0
      max: 0.48  # 0.47712125 is the exact boundary in baryon emu
    ref:
      dist: norm
      loc: 0.25
      scale: 0.05
    proposal: 0.05
    latex: \log(\theta_{\rm out})
  M_inn:
    prior:
      min: 9.0
      max: 13.5
    ref:
      dist: norm
      loc: 12
      scale: 0.5
    proposal: 0.1
    latex: \log(M_{\rm inn})

# CCL settings
theory:
  cl_like.CCL:
    transfer_function: boltzmann_camb
    matter_pk: halofit
    baryons_pk: nobaryons
  cl_like.Pk:
    # Linear, EulerianPT, LagrangianPT
    bias_model: "BaccoPT"
    zmax_pks: 1.5
    use_baryon_boost: True
    nonlinear_emu_path: '/mnt/zfsusers/gravityls_3/codes/NN_emulator_PCA6_0.95_300_400n_paired_comb'
    nonlinear_emu_details : 'details.pickle'
    allow_bcm_emu_extrapolation_for_shear: True
    allow_halofit_extrapolation_for_shear: True
    ignore_lbias: True  # Only if shear-shear analysis
  cl_like.Limber:
    nz_model: "NzShift"
    ia_model: "IADESY1_PerSurvey"
    input_params_prefix: "limber"
  cl_like.ClFinal:
    input_params_prefix: "bias"
    shape_model: ShapeMultiplicative

# Likelihood settings
likelihood:
  cl_like.ClLike:
    # Input sacc file
    input_file: /mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/cls_DESY3_noK1000_noHSCDR1wl_plus_KiDS1000_noHSCDR1wl_cov_4096_GNG.fits
    # List all relevant bins. 
    bins:
      - name: DESY3wl__0
      - name: DESY3wl__1
      - name: DESY3wl__2
      - name: DESY3wl__3
      - name: KiDS1000__0
      - name: KiDS1000__1
      - name: KiDS1000__2
      - name: KiDS1000__3
      - name: KiDS1000__4
      - name: HSCDR1wl__0
      - name: HSCDR1wl__1
      - name: HSCDR1wl__2
      - name: HSCDR1wl__3

    # List all 2-points that should go into the
    # data vector. For now we only include
    # galaxy-galaxy auto-correlations, but all
    # galaxy-shear and shear-shear correlations.
    twopoints:
      - bins: [DESY3wl__0, DESY3wl__0]
      - bins: [DESY3wl__0, DESY3wl__1]
      - bins: [DESY3wl__0, DESY3wl__2]
      - bins: [DESY3wl__0, DESY3wl__3]
      - bins: [DESY3wl__1, DESY3wl__1]
      - bins: [DESY3wl__1, DESY3wl__2]
      - bins: [DESY3wl__1, DESY3wl__3]
      - bins: [DESY3wl__2, DESY3wl__2]
      - bins: [DESY3wl__2, DESY3wl__3]
      - bins: [DESY3wl__3, DESY3wl__3]
      - bins: [KiDS1000__0, KiDS1000__0]
      - bins: [KiDS1000__0, KiDS1000__1]
      - bins: [KiDS1000__0, KiDS1000__2]
      - bins: [KiDS1000__0, KiDS1000__3]
      - bins: [KiDS1000__0, KiDS1000__4]
      - bins: [KiDS1000__1, KiDS1000__1]
      - bins: [KiDS1000__1, KiDS1000__2]
      - bins: [KiDS1000__1, KiDS1000__3]
      - bins: [KiDS1000__1, KiDS1000__4]
      - bins: [KiDS1000__2, KiDS1000__2]
      - bins: [KiDS1000__2, KiDS1000__3]
      - bins: [KiDS1000__2, KiDS1000__4]
      - bins: [KiDS1000__3, KiDS1000__3]
      - bins: [KiDS1000__3, KiDS1000__4]
      - bins: [KiDS1000__4, KiDS1000__4]
      - bins: [HSCDR1wl__0, HSCDR1wl__0]
      - bins: [HSCDR1wl__0, HSCDR1wl__1]
      - bins: [HSCDR1wl__0, HSCDR1wl__2]
      - bins: [HSCDR1wl__0, HSCDR1wl__3]
      - bins: [HSCDR1wl__1, HSCDR1wl__1]
      - bins: [HSCDR1wl__1, HSCDR1wl__2]
      - bins: [HSCDR1wl__1, HSCDR1wl__3]
      - bins: [HSCDR1wl__2, HSCDR1wl__2]
      - bins: [HSCDR1wl__2, HSCDR1wl__3]
      - bins: [HSCDR1wl__3, HSCDR1wl__3]

    defaults:
      # These ones will apply to all power spectra (unless the lmax
      # corresponding to the chosen kmax is smaller).
      # lmin = 0 in DESY3 analysis 2203.07128
      # lmax = 2048, like 2Nside for Nside = 1024
      lmax: 4500
      DESY3wl__0:
        lmin: 20
      DESY3wl__1:
        lmin: 20
      DESY3wl__2:
        lmin: 20
      DESY3wl__3:
        lmin: 20
      KiDS1000__0:
        lmin: 100
      KiDS1000__1:
        lmin: 100
      KiDS1000__2:
        lmin: 100
      KiDS1000__3:
        lmin: 100
      KiDS1000__4:
        lmin: 100
      HSCDR1wl__0:
        lmin: 300
      HSCDR1wl__1:
        lmin: 300
      HSCDR1wl__2:
        lmin: 300
      HSCDR1wl__3:
        lmin: 300



prior:
  omegab: import_module("prior_omegab").omegab_prior_logp

debug: False
output: 'chains/desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18/desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18'
