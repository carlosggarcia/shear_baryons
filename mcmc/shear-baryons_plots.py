# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.1
#   kernelspec:
#     display_name: baryons-sigma12
#     language: python
#     name: baryons-sigma12
# ---

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# # Imports and function definitions

# %% hidden=true
from getdist import plots, MCSamples
# %matplotlib inline
from matplotlib import pyplot as plt
import matplotlib.patches as patches
import getdist
import h5py

import numpy as np
import os
import sacc
import scipy.stats
import sys
import yaml
from scipy.interpolate import interp1d
import pyccl as ccl
import latextable
from texttable import Texttable

from cobaya.samplers.mcmc import plot_progress
from getdist.mcsamples import MCSamplesFromCobaya

from scipy import stats
from scipy.interpolate import interp1d

from cobaya.model import get_model
from cobaya.input import load_input
from cl_like import ClLike

import healpy as hp
import sacc
import fitsio

#### Import baccoemu to check within ranges
import baccoemu
import warnings

nonlinear_emu_path='/mnt/zfsusers/gravityls_3/codes/NN_emulator_PCA6_0.95_300_400n_paired_comb'
nonlinear_emu_details='details.pickle'

mpk = baccoemu.Matter_powerspectrum(nonlinear_emu_path=nonlinear_emu_path,
                                    nonlinear_emu_details=nonlinear_emu_details)

#### Chain handling ####
class Chain(dict):
    def __init__(self, MCSamples, label, color, path, Cobaya=True, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self['MCSamples'] = MCSamples
        self['label'] = label
        self['color'] = color
        self['path'] = path
        # We subtract 4 for minuslogprior, minuslogprior__0, chi2, chi2__cl_like.ClLike
        # We load this at the beginning for correct R-1
        self['npar'] = len(MCSamples.getParamNames().list()) - 4      
        # self['R-1'] = MCSamples.getGelmanRubin(npar)
        self.model = None
        self.lkl = None
        self.Rminus1 = None
        self.chi2bf = None
        self.parsbf = None
        self.pvaluebf = None
        self.clsacc_bf = None
        self.cldata = None
        self._derived_added = False
        self.pk_data_bf = None
        self.ndata = None
        
    def _get_model(self):
        if self.model is None:
            path = self['path'] + '.input.yaml'
            if not os.path.isfile(path):
                raise ValueError('These chains are not from cobaya, so you cannot load the model')
            self.model = get_model(path)
        return self.model
    
    def _get_Rminus1(self):
        if self.Rminus1 is None:
            self.Rminus1 = self['MCSamples'].getGelmanRubin(self.get('npar'))
        return self.Rminus1
    
    def _get_chi2bf(self):
        if self.chi2bf is None:
            pars = self['MCSamples'].getParams()
            self.chi2bf = pars.chi2__cl_like.ClLike.min()
        return self.chi2bf
    
    def _get_parsbf(self):
        if self.parsbf is None:
            chain = self['MCSamples']
            pars = chain.getParams()
            self.parsbf = chain.getParamSampleDict(pars.chi2__cl_like.ClLike.argmin())
        return self.parsbf
    
    def _get_ndata(self):
        if self.ndata is None:
            model = self._get_model()
            self.ndata = model.likelihood['cl_like.ClLike'].ndata
        return self.ndata
    
    def get_pvalue(self, pars):
        chi2 = self.evaluate_model(pars)
        ndata = self._get_ndata()
        return 1 - stats.chi2.cdf(chi2, df=ndata - 2)
    
    def _get_pvaluebf(self):
        if self.pvaluebf is None:
            # pars = self._get_parsbf()
            # self.pvaluebf = self.get_pvalue(pars)
            chi2bf = self._get_chi2bf()
            ndata = self._get_ndata()
            self.pvaluebf = 1 - stats.chi2.cdf(chi2bf, df=ndata - 2)
        return self.pvaluebf
    
    def add_derived(self):
        if self._derived_added is True:
            return
        MCSamples = self['MCSamples']
        p = MCSamples.getParams()
        pn = MCSamples.getParamNames().list()
        
        # S8 = p.sigma8 * np.sqrt(p.Omega_m / 0.3)
        if ('S8' not in pn) and ('S_8' in pn):
            S8 = p.S_8
            MCSamples.addDerived(S8, 'S8', label='S_8')
           
        if ('h' not in pn) and ('H0' in pn):
            MCSamples.addDerived(p.H0/100, 'h', label='h')
        if ('Omega_c' not in pn) and ('Omega_cdm' in pn):
            MCSamples.addDerived(p.Omega_cdm, 'Omega_c', label=r'\Omega_c')

        p = MCSamples.getParams()
        pn = MCSamples.getParamNames().list()
        if 'Omega_m' not in pn:
            Omega_m = p.Omega_b + p.Omega_c
            if 'm_nu' in pn:
                Omega_m +=  p.Omega_nu
            MCSamples.addDerived(Omega_m, 'Omega_m', label=r'\Omega_m')
        if 'sigma8' not in pn:
            if 'sigma_8' in pn:
                MCSamples.addDerived(p.sigma_8, 'sigma8', label=r'\sigma_8')
            elif 'S8' in pn:
                MCSamples.addDerived(p.S8 * np.sqrt(0.3 / p.Omega_m), 'sigma8', label=r'\sigma_8')
        if 'm_nu' in pn:
            m_nu = p.m_nu
            MCSamples.addDerived(p.m_nu/(93.14*p.h**2), 'Omega_nu', label=r'\Omega_\nu')
        else:
            m_nu = np.zeros_like(p.h)
            
        p = MCSamples.getParams()
        pn = MCSamples.getParamNames().list()            
        if ('S8' not in pn) and ('sigma8' in pn and 'Omega_m' in pn):
             MCSamples.addDerived(p.sigma8 * np.sqrt(p.Omega_m / 0.3), 'S8', label='S_8')
            
        # Baryon fraction
        MCSamples.addDerived(p.Omega_b / (p.Omega_c + p.Omega_b), 'baryon_fraction', label=r'\frac{\Omega_b}{\Omega_{\rm cold}}')
        
        p = MCSamples.getParams()
        pn = MCSamples.getParamNames().list()
        h = p.h 
        if 'omega_b' not in pn:
            MCSamples.addDerived(p.Omega_b * h**2, 'omega_b', label='\omega_b')
        if 'omega_c' not in pn:
            MCSamples.addDerived(p.Omega_c * h**2, 'omega_cdm', label='\omega_{cdm}')
        if 'omega_m' not in pn:
            MCSamples.addDerived(p.Omega_m * h**2, 'omega_m', label='\omega_{m}')
        
        p = MCSamples.getParams()
        pn = MCSamples.getParamNames().list()
        # Sigma12
        if 'S12' not in pn and (('A_s' in pn) or ('A_sE9' in pn)):
            pars = {
            'omega_cold':p.Omega_c + p.Omega_b,
            'omega_baryon': p.Omega_b,
            'A_s' : p.A_sE9*1e-9 if 'A_sE9' in pn else p.A_s,
            'ns': p.n_s,
            'hubble': p.h,
            'neutrino_mass': m_nu,
            'w0': np.full(p.h.size, -1),
            'wa': np.full(p.h.size, 0),
            'expfactor': np.full(p.h.size, 1)
            }
            try:
                sigma12 = mpk.get_sigma12(cold=False, **pars)
                S12 = sigma12 * (p.omega_m / 0.14)**0.4
                MCSamples.addDerived(sigma12, 'sigma12', label='\sigma_{12}')
                MCSamples.addDerived(S12, 'S12', label='S_{12}')
            except AssertionError as e:
                if 'out of bounds' in str(e):
                    warnings.warn(str(e) + " Not adding S12 for this chain.")
                    
        if 'sigma8_cold' not in pn and (('A_s' in pn) or ('A_sE9' in pn)):
            pars = {
            'omega_cold':p.Omega_c + p.Omega_b,
            'omega_baryon': p.Omega_b,
            'A_s' : p.A_sE9*1e-9 if 'A_sE9' in pn else p.A_s,
            'ns': p.n_s,
            'hubble': p.h,
            'neutrino_mass': m_nu,
            'w0': np.full(p.h.size, -1),
            'wa': np.full(p.h.size, 0),
            'expfactor': np.full(p.h.size, 1)
            }
            try:
                sigma8_cold = mpk.get_sigma8(cold=True, **pars)
                MCSamples.addDerived(sigma8_cold, 'sigma8_cold', label='\sigma_{8,cold}')
            except AssertionError as e:
                if 'out of bounds' in str(e):
                    warnings.warn(str(e) + " Not adding sigma8_cold for this chain.")
            
        return
    
    def __getitem__(self, key):
        if key not in self.keys():
            if key == 'model':
                self[key] = self._get_model()
            elif key == 'lkl':
                self[key] = self.get_lkl()
            elif key == 'R-1':
                self[key] = self._get_Rminus1()
            elif key == 'chi2bf':
                self[key] = self._get_chi2bf()
            elif key == 'pars_bf':
                self[key] = self._get_parsbf()
            elif key == 'ndata':
                self[key] = self._get_ndata()
            elif key == 'pvalue_bf':
                self[key] = self._get_pvaluebf()
            elif key == 'data_sacc':
                self[key] = self.get_cl_data_sacc()
            elif key == 'bf_sacc':
                self[key] = self.get_cl_theory_sacc_bf()
            elif key == 'pk_data_bf':
                self[key] = self.get_pk_data_bf()
            elif key == 'Sk_bf':
                self[key] = self.get_Sk_bf()
            else:
                raise ValueError('You need to implement this!')
        return self.get(key)

    def evaluate_model(self, params):
        model = self._get_model()
        pars = {p: params[p] for p in model.parameterization.sampled_params().keys()}
        return -2 * model.loglikes(pars)[0]

    def get_lkl(self):
        if self.lkl is None:
            model = self._get_model()
            self.lkl = model.likelihood['cl_like.ClLike']
        return self.lkl
    
    def get_cl_theory(self, pars):
        self.evaluate_model(model, pars)
        return model.provider.get_cl_theory()

    def get_cl_theory_sacc(self, pars):
        self.evaluate_model(pars)
        lkl = self.get_lkl()
        return lkl.get_cl_theory_sacc()
    
    def get_cl_theory_sacc_bf(self):
        if self.clsacc_bf is None:
            pars = self._get_parsbf()
            self.clsacc_bf = self.get_cl_theory_sacc(pars)
        return self.clsacc_bf
    
    def get_cl_data_sacc(self):
        if self.cldata is None:
            lkl = self.get_lkl()
            self.cldata = lkl.get_cl_data_sacc()
        return self.cldata
    
    def get_pk_data(self, pars):
        self.evaluate_model(pars)
        lkl = self.get_lkl()
        return lkl.provider.get_Pk()['pk_data']
    
    def get_pk_data_bf(self):
        if self.pk_data_bf is None:
            pars = self._get_parsbf()
            self.pk_data_bf = self.get_pk_data(pars)
        return self.pk_data_bf

class BaryonBoost():
    def __init__(self, fname, ignore_rows=0):
        self.boost = []
        self.weights = []
        self.loglikes = []
        if '.npz' in fname:
            npf = np.load(f"{fname}")
            self.boost.append(npf['Sk'])
            self.weights.append(npf['weights'])
            if 'loglikes' in npf.keys():
                self.loglikes.append(npf['loglikes'])
        else:
            for i in range(4):
                npf = np.load(f"{fname}.{i+1}_Sk.npz")
                self.boost.append(npf['Sk'])
                self.weights.append(npf['weights'])
                if 'loglikes' in npf.keys():
                    self.loglikes.append(npf['loglikes'])
        
        if self.loglikes == []:
            self.loglikes = None
        
        self.k_arr = npf['k_arr']
        self.InlineLatexList = None
        self.post1D = None
        self.post_interp = None
        
        # For the MCSamples
        self.MCSamples_baryons = None
        self._names =  npf['colnames']
        self._labels = [f'S(k={ki:.2f})' for ki in self.k_arr]
        self.ignore_rows = ignore_rows

        # Decide whether you risk with limits or only uses InlineLatex with \pm
        self.only_pm = True
        self.getdist_1D = False

    def set_getdist_1D(self, getdist_1D):
        if getdist_1D is not self.getdist_1D:
            self.getdist_1D = getdist_1D
            self.post1D = None
            self.post_interp = None        

    def set_only_pm(self, only_pm):
        if only_pm is not self.only_pm:
            self.only_pm = only_pm
            self.post1D = None
            self.post_interp = None
        
    def get_k_arr(self):
        return self.k_arr
    
    def get_names(self):
        return self._names
    
    def get_labels(self):
        return self._labels
    
    def get_MCSamples_baryons(self):
        if self.MCSamples_baryons is None:
            self.MCSamples_baryons = getdist.MCSamples(samples=self.boost,
                                                       weights=self.weights,
                                                       loglikes=self.loglikes,
                                                       names=self._names,
                                                       labels=self._labels,
                                                       ignore_rows=self.ignore_rows)
        return self.MCSamples_baryons
    
    def _transaleInlineLatex(self, InlineLatex):
        if ' = ' in InlineLatex:
            rhs = InlineLatex.split(' = ')[-1]
            if '\pm' in rhs:
                mean, errors = rhs.split('\pm')
                up = down = float(errors)
            else:
                mean, errors = rhs.split('^{')
                if '{+' in errors: 
                    # Weird case but it's there
                    up, down = errors.split('}_{+')
                    up = float(up)
                    down = float(down[:-1])
                    up = max(up, down)
                    down = 0
                else:
                    up, down = errors.split('}_{-')
                    up = float(up)
                    down = float(down[:-1]) # Remove the '}' at the end
            mean = float(mean)
        else:
            if self.only_pm:
                raise ValueError(f"Not well constrained {InlineLatex}")
            else:
                if '<' in InlineLatex:
                    lhs, rhs = InlineLatex.split('<')
                    mean = float(rhs) / 2.
                    up = down = mean
                elif '>' in InlineLatex:
                    warning.warns(f"Getting upper bound in {Inlinelatex}. Check if it makes sense to have Sk = 1 as maximum!!")
                    lhs, rhs = InlineLatex.split('>')
                    mean = (float(rhs) + 1.) / 2
                    down = mean - float(rhs) 
                    up = 1 - mean
        
        # Return the upper and lower bounds instead of the errors
        return mean, up, down
    
    def get_baryon_post1D(self):
        if self.post1D is None:
            samples = self.get_MCSamples_baryons()
            post1D = []
            if self.getdist_1D:
                InlineLatexList = []
                for namei in self.get_names():
                    InlineLatex = samples.getInlineLatex(namei)
                    post1D.append(self._transaleInlineLatex(InlineLatex))
                    InlineLatexList.append(InlineLatex)
                self.InlineLatexList = InlineLatexList
            else:
                w = samples.weights
                Sk_mean = np.average(samples.samples, weights=w, axis=0)
                Sk2_mean = np.average(samples.samples**2, weights=w, axis=0)
                Sk_std = np.sqrt(Sk2_mean - Sk_mean**2)
                post1D = np.transpose((Sk_mean, Sk_std, Sk_std))
            self.post1D = np.array(post1D)
        return self.post1D
    
    def get_baryon_post_interp(self, kind='quadratic'):
        if self.post_interp is None:
            mean, up, down = self.get_baryon_post1D().T
            post_interp = interp1d(self.k_arr, mean, kind=kind)
            post_interp_p1sigma = interp1d(self.k_arr, mean + up, kind=kind)
            post_interp_m1sigma = interp1d(self.k_arr, mean - down, kind=kind)
        
            self.post_interp = (post_interp, post_interp_p1sigma, post_interp_m1sigma)
        return self.post_interp
    
    def get_baryon_post_at_k(self, k):
        mean, up, down = self.get_baryon_post_interp(kind)
        return mean(k), up(k), down(k)
    
    def plot_baryon_post(self, ax, label, color, kind='quadratic', k_fine=None, alpha=0.5):
        if k_fine is None:
            k_fine = np.logspace(-1, np.log10(self.k_arr.max()), 100)
            k_fine[0] = 0.1
        mean, up, down = self.get_baryon_post_interp(kind)
        ax.semilogx(k_fine, mean(k_fine), color=color)
        ax.fill_between(k_fine, up(k_fine), down(k_fine),
                        alpha=alpha, color=color, label=label)
        
    def plot_baryon_post_errorbar(self, ax, label=None, color=None, fmt='.'):
        post1d = self.get_baryon_post1D()
        ax.errorbar(self.k_arr, post1d[:, 0], yerr=post1d[:, 1:].T[::-1],
                    fmt=fmt, label=label, color=color)

    
def load_chain(fname, label, color, ignore_rows=None, add_derived_params=True, baccoemu=True, **kwargs):
    d = Chain(**{'MCSamples': getdist.loadMCSamples(fname, settings={'ignore_rows': ignore_rows}),
         'label': label,
         'color': color,
         'path': fname})
    
    if add_derived_params:
        d.add_derived()
    
    if baccoemu and ('fixedcosmo' not in fname):
        d['inbounds_baccoemu'] = count_inbounds_baccoemu_range(d['MCSamples'])
        
    if os.path.isfile(f"{fname}.1_Sk.npz"):
        d['boost'] = BaryonBoost(fname, ignore_rows=ignore_rows)
        
    return d


def count_inbounds_baccoemu_range(mcsamples):
    params = mcsamples.getParamNames().list()
    p = mcsamples.getParams()
    
    def sigma8tot_2_sigma8cold(emupars, sigma8tot):
        """Use baccoemu to convert sigma8 total matter to sigma8 cdm+baryons
        """
        if hasattr(emupars['omega_cold'], '__len__'):
            _emupars = {}
            for pname in emupars:
                _emupars[pname] = emupars[pname][0]
        else:
            _emupars = emupars
        A_s_fid = 2.1e-9
        sigma8tot_fid = mpk.get_sigma8(cold=False,
                                       A_s=A_s_fid, **_emupars)
        A_s = (sigma8tot / sigma8tot_fid)**2 * A_s_fid
        return mpk.get_sigma8(cold=True, A_s=A_s, **_emupars)    
    
    cospar = {
        'omega_cold': p.Omega_c + p.Omega_b,
        'omega_baryon': p.Omega_b,
        'ns': p.n_s,
        'hubble': p.h,
        'neutrino_mass': p.m_nu,
        'w0': np.ones_like(p.m_nu) * -1,
        'wa': np.zeros_like(p.m_nu)}
    if 'A_s' in params:
        cospar['A_s'] = p.A_s
    elif 'A_sE9' in params:
        cospar['A_s'] = p.A_sE9 * 1e-9
    elif 'sigma8' in params:
        cospar['sigma8_cold'] = sigma8tot_2_sigma8cold(cospar, p.sigma8)
        
    if 'sigma8_cold' not in cospar:
        cospar['sigma8_cold'] = mpk.get_sigma8(**cospar, cold=True)
        del cospar['A_s']
    
    within_bounds = {'nonlinear': [],
                     'baryon': []}
    for i, parname in enumerate(mpk.emulator['nonlinear']['keys']):
        if parname in ['expfactor', 'w0', 'wa']:
            continue
        val = cospar[parname]
        
        for k in ['baryon', 'nonlinear']:
            lower_bound = mpk.emulator[k]['bounds'][i][0]
            upper_bound = mpk.emulator[k]['bounds'][i][1]
                        
            within_bounds[k].append((val >= lower_bound) * (val <= upper_bound))

    output = {}
    for k in ['baryon', 'nonlinear']:
        inbounds = np.all(np.array(within_bounds[k]), axis=0)
        output[k] = np.sum(inbounds) / inbounds.size
    
    # baryon_fraction \in [0.1, 0.26]
    inbounds = (p.baryon_fraction >= 0.1) * (p.baryon_fraction <= 0.26)
    output['baryon_fraction'] = np.sum(inbounds) / inbounds.size

    return output

##### Functions ######
def evaluate_model(model, params):
    pars = {p: params[p] for p in model.parameterization.sampled_params().keys()}
    return -2 * model.loglikes(pars)[0]

def get_cl_theory(model, pars):
    evaluate_model(model, pars)
    return model.provider.get_cl_theory()

def get_cl_theory_sacc(model, pars):
    evaluate_model(model, pars)
    lkl = model.likelihood['cl_like.ClLike']
    return lkl.get_cl_theory_sacc()

def get_cl_data_sacc(model):
    lkl = model.likelihood['cl_like.ClLike']
    return lkl.get_cl_data_sacc()

def transaleInlineLatex(InlineLatex):
    if ' = ' in InlineLatex:
        rhs = InlineLatex.split(' = ')[-1]
        if '\pm' in rhs:
            mean, errors = rhs.split('\pm')
            up = down = float(errors)
        else:
            mean, errors = rhs.split('^{')
            up, down = errors.split('}_{-')
            up = float(up)
            down = float(down[:-1])
        mean = float(mean)
    else:
        raise ValueError('This is not well constrained')

    # Return the upper and lower errors
    return mean, up, down

###### For debugging ######

def get_errors(fpath, fpath_dicts=None):
    errors = np.load(fpath)
    
    output = {}
    for k in errors.keys():
        output[k] = errors[k]
        
    if fpath_dicts is not None:
        d = []
        with open(fpath_dicts, 'r') as f:
            for line in f:
                d.append(eval(line))
        output['params_dict'] = d
    
    return output

def get_array_from_dicts(dicts, pname):
    p = []
    for d in dicts:
        p.append(d[pname])
    return np.array(p)


def write_ini(pars, fname):
    outname = os.path.splitext(os.path.basename(fname))[0]
    with open(fname, 'w') as f:
        for k, v in pars.items():
            line = f'{k} = {v} \n'
            f.write(line)
        
        f.write(f'root= output/{outname}_')
        
def save_some_errors(results, chain):
    bn_index = results[chain]['errors']['bn_index']
    etype = results[chain]['errors']['error_type'][bn_index:]
    edicts = results[chain]['errors']['params_dict'][bn_index:]
    
    ix_input = np.where(etype==0)[0]
    if ix_input.size != 0:
        ix_input = np.random.choice(np.where(etype==0)[0], 5)
        
    ix_th = np.where(etype==2)[0]
    if ix_th.size != 0:
        ix_th = np.random.choice(np.where(etype==2)[0], 5)
        
    ix_pert = np.random.choice(np.where(etype==3)[0], 5)
    
    for ix in ix_input:
        fname = f'{chain}_errors_ix{ix}_input.ini'
        write_ini(edicts[ix], fname)
    
#     for ix in ix_th:
#         fname = f'{chain}_errors_ix{ix}_thermo.ini'
#         write_ini(edicts[ix], fname)
        
#     for ix in ix_pert:
#         fname = f'{chain}_errors_ix{ix}_pert.ini'
#         write_ini(edicts[ix], fname)

####### For plotting the maps ########
def scale_bin_map(mask, c):
    m = mask.copy()
    goodpix = mask != 0
    m[goodpix] = c
    m[~goodpix] = hp.UNSEEN
    return m

##### Global variables

results = {}


# %%
def load_priors():
    # Create a mock chain for Gaussian priors
    mean = []
    sigma = []
    names = []
    labels = []
    
    ######## HSC
    # dz
    mean += [0, 0, 0, 0]
    sigma += [0.0285, 0.0135, 0.0383, 0.0376]
    names += [fr'limber_HSCDR1wl__{i}_dz' for i in range(4)]
    labels += [r"\Delta z^{HSC_{%d}}" % i for i in range(4)]
    # m
    mean += [0, 0, 0, 0]
    sigma += [0.01] * 4
    names += [fr'bias_HSCDR1wl__{i}_m' for i in range(4)]
    labels += [r"m^{HSC_{%d}}" % i for i in range(4)]

    ######## DESY3wl
    # dz
    mean += [0, 0, 0, 0]
    sigma += [0.018, 0.015, 0.011, 0.017]
    names += [fr'limber_DESY3wl__{i}_dz' for i in range(4)]
    labels += [r"\Delta z^{DESY3_{%d}}" % i for i in range(4)]
    # m
    mean += [-0.0063, -0.0198, -0.0241, -0.0369]
    sigma += [0.0091, 0.0078, 0.0076, 0.0076]
    names += [fr'bias_DESY3wl__{i}_m' for i in range(4)]
    labels += [r"m^{DESY3_{%d}}" % i for i in range(4)]

    ######## KiDS-1000
    # dz
    mean += [0, -0.002, -0.013, -0.011, 0.006]
    sigma += [0.0106, 0.0113, 0.0118, 0.0087, 0.0097]
    names += [fr'limber_KiDS1000__{i}_dz' for i in range(5)]
    labels += [r"\Delta z^{KiDS1000_{%d}}" % i for i in range(5)]
    # m
    mean += [0] * 5
    sigma += [0.019, 0.020, 0.017, 0.012, 0.010]
    names += [fr'bias_KiDS1000__{i}_m' for i in range(5)]
    labels += [r"m^{KiDS1000_{%d}}" % i for i in range(5)]

    # Generate
    cov = np.diag(sigma)**2
    random_state = np.random.default_rng(10)
    samps = random_state.multivariate_normal(mean, cov, size=int(1e5))
    d = {'MCSamples': getdist.MCSamples(samples=samps, names=names, labels=labels),
         'label': 'Priors',
         'color': 'Purple',
         'path': None}
    
    s = d['MCSamples']
    p = s.getParams()
    for i in range(4):
        s.addDerived(eval(f"p.limber_HSCDR1wl__{i}_dz"), f"limber_wl_{i}_dz", r"\Delta z^{HSC_{%d}}" % i)
        
    return d

results['priors'] = load_priors()

# Sampled priors
key = 'priors_chain'
results[key] = load_chain(f'./chains/priors/priors_with_sigma8_S8',
                           r'Priors chain', 'orange', 0.15, baccoemu=False)
print(key, f"R-1 = {results[key]['R-1']}")

# %% hidden=true
# Create a sacc file with KiDS1000 and DESY3 data

# Build sacc file with official KiDS1000 Cells
official_cls_path = "/mnt/extraspace/damonge/Datasets/KiDS1000/KiDS1000_cosmis_shear_data_release/data_fits/bp_KIDS1000_BlindC_with_m_bias_V1.0.0A_ugriZYJHKs_photoz_SG_mask_LF_svn_309c_2Dbins_v2_goldclasses_Flag_SOM_Fid.fits"
fits = fitsio.FITS(official_cls_path)
# print(fits)
# print(fits[1])
# print(fits[3])
# print(fits[5])

sOfficial = sacc.Sacc()
for i in range(5):
    sOfficial.add_tracer('NZ', f'KiDS1000__{i}',
                         z=fits['nz_source']['Z_MID'].read(), nz=fits['nz_source'][f'BIN{i+1}'].read())
official_cls = fitsio.read(official_cls_path, ext=3, columns=['BIN1', 'BIN2', 'VALUE', 'ANG'])
for dp in official_cls:
    j = dp[0] - 1
    i = dp[1] - 1
    value = dp[2]
    ell = dp[3]
    sOfficial.add_data_point('cl_ee', (f'KiDS1000__{i}', f'KiDS1000__{j}'), value, ell=ell)
sOfficial.add_covariance(fits['COVMAT'].read()[80:][:, 80:])

s1 = sOfficial.copy()

# Build sacc file with official DESY3 Cells
sOfficial = sacc.Sacc()
official_cls_path = "/mnt/extraspace/damonge/Datasets/DES_Y3/data_products/twopoint_cls_Y3_mastercat_unblinded_data_cov_nopureE_countmode_C1_apo0.0_nside1024_improved_NKA_NG_v2_cov.fits"
fits = fitsio.FITS(official_cls_path)
# print(fits)
# print(fits[2])

for i in range(4):
    sOfficial.add_tracer('NZ', f'DESY3wl__{i}',
                         z=fits['nz_source']['Z_MID'].read(), nz=fits['nz_source'][f'BIN{i+1}'].read())
official_cls = fitsio.read(official_cls_path, ext='shear_cl', columns=['BIN1', 'BIN2', 'VALUE', 'ANG'])
for dp in official_cls:
    j = dp[0] - 1
    i = dp[1] - 1
    value = dp[2]
    ell = dp[3]
    sOfficial.add_data_point('cl_ee', (f'DESY3wl__{i}', f'DESY3wl__{j}'), value, ell=ell)
sOfficial.add_covariance(fits['COVMAT'].read())
# sOfficial.save_fits(official_cls_path.replace('.fits', '_convertedto.sacc'))
s2 = sOfficial.copy()

# Combine both
sOfficial = sacc.concatenate_data_sets(s1, s2)


# TODO: Add HSC to the sum



##### Planck 18 chain #####
def load_P18(fname, label, color, ignore_rows):
    d = {'MCSamples': getdist.loadMCSamples(fname, settings={'ignore_rows': ignore_rows}),
         'label': label,
         'color': color,
         'path': fname}
    # We subtract 4 for minuslogprior, minuslogprior__0, chi2, chi2__cl_like.ClLike  
    npar = len(d['MCSamples'].getParamNames().list()) - 4
    d['R-1'] = d['MCSamples'].getGelmanRubin(npar)
    MCSamples = d['MCSamples']
    p = MCSamples.getParams()
    pn = MCSamples.getParamNames().list()

    if 'S8' not in pn:
        S8 = p.S_8
        MCSamples.addDerived(S8, 'S8', label='S_8')
    if 'sigma8' not in pn:
        MCSamples.addDerived(p.sigma_8, 'sigma8', label=r'\sigma_8')
    if 'h' not in pn:
        MCSamples.addDerived(p.H0/100, 'h', label='h')
    if ('Omega_c' not in pn) and ('Omega_cdm' in pn):
        MCSamples.addDerived(p.Omega_cdm, 'Omega_c', label=r'\Omega_c')
    # For Planck chains
    h = p.H0/100
    if ('omegabh2' in pn):
        MCSamples.addDerived(p.omegabh2 , 'omega_b', label=r'\omega_b')
        MCSamples.addDerived(p.omegabh2 / h**2 , 'Omega_b', label=r'\Omega_b')
    if ('omegach2' in pn):
        h = p.H0/100
        MCSamples.addDerived(p.omegach2 / h**2 , 'Omega_c', label=r'\Omega_c')
    if 'tau' in pn:
        MCSamples.addDerived(p.tau, 'tau_reio', label=r'\tau')
    if 'ns' in pn:
        MCSamples.addDerived(p.ns, 'n_s', label='n_s')
    if 'Omega_m' not in pn:
        MCSamples.addDerived(p.omegam, 'Omega_m', label=r'\Omega_m')
    if 'A_sE9' not in pn:
        MCSamples.addDerived(p.A, 'A_sE9', label=r'A_s 10^{9}')
    if 'mnu' in pn:
        MCSamples.addDerived(p.mnu, 'm_nu', label=r'M_\nu')
        

        
    MCSamples.addDerived(p.omegam * h**2, 'omega_m', label=r'\omega_m')
    
    p = MCSamples.getParams()
    # Sigma12
    pars = {
    'omega_cold':p.Omega_c + p.Omega_b,
    'omega_baryon': p.Omega_b,
    'A_s' : p.A_sE9*1e-9,
    'ns': p.n_s,
    'hubble': p.h,
    'neutrino_mass': np.full(p.h.size, 0.06),
    'w0': np.full(p.h.size, -1),
    'wa': np.full(p.h.size, 0),
    'expfactor': np.full(p.h.size, 1)
    }
    sigma12 = mpk.get_sigma12(cold=False, **pars)
    S12 = sigma12 * (p.omega_m / 0.14)**0.4
    MCSamples.addDerived(sigma12, 'sigma12', label='\sigma_{12}')
    MCSamples.addDerived(S12, 'S12', label='S_{12}') 
            
    return d

# Planck 2018 constraints (mnu = 0.06eV)
key = 'P18_official'
results[key] = load_P18('/mnt/extraspace/gravityls_3/data/Planck18/public_chains/base/plikHM_TTTEEE_lowl_lowE_lensing/base_plikHM_TTTEEE_lowl_lowE_lensing',
                           r'Planck 2018', 'black', 0.15)
print(results[key]['R-1'])

# Planck with varying neutrino mass
key = 'P18_official_mnu'
results[key] = load_P18('/mnt/extraspace/gravityls_3/data/Planck18/public_chains/base_mnu/plikHM_TTTEEE_lowl_lowE_lensing/base_mnu_plikHM_TTTEEE_lowl_lowE_lensing',
                           r'Planck 2018 ($m_\nu$)', 'black', 0.15)
print(results[key]['R-1'])

# BAO likelihood
key = 'BAOlkl'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'BAO', 'blue', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# P18 Gaussianized likelihood
key = 'P18_Gauss'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'Planck 2018 ($m_\nu$) Gaussianized', 'blue', 0.15, add_derived_params=False, baccoemu=False)
print(key, f"R-1 = {results[key]['R-1']}")

# Compute covariance of P18 cosmological paramters to use as multivariate gaussian
# s = results['P18_official_mnu']['MCSamples']
# pnames = s.getParamNames().list()
# cov_pnames = ['A_sE9', 'Omega_m', 'Omega_b', 'm_nu', 'n_s', 'h']
# ixs = [pnames.index(pn) for pn in cov_pnames]
# s.getCov(pars=ixs)

# np.sqrt(np.diag(s.getCov(pars=ixs)))
# s.getMeans(ixs)

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true toc-hr-collapsed=true
# # DESY3

# %% hidden=true
# All runs here have the priors of Giovanni's paper (2303.05537). These are slightly broader than in baccoemu with baryons. 
# In the case of no-baryons, the emulator allow a broader parameters space than with them. We are using the same priors as before
# so if we see any weird effect it might be that we have to enforce the priors of baccoemu with baryons in all runs in order to
# make the comparison fair.

def load_Arico23(label, color, MCSamples=None):
    # Arico 2022 results (only contains S8 & Omega_m)
    Arico2cobaya = {'omega_matter': 'Omega_m',
                    'omega_baryon': 'Omega_b', 
                    'hubble': 'h',
                    'ns': 'n_s',
                    'neutrino_mass': 'm_nu',
                    'm1': 'bias_DESY3wl__0_m',
                    'm2': 'bias_DESY3wl__1_m',
                    'm3': 'bias_DESY3wl__2_m',
                    'm4': 'bias_DESY3wl__3_m',
                    'dz_s1': 'limber_DESY3wl__0_dz',
                    'dz_s2': 'limber_DESY3wl__1_dz',
                    'dz_s3': 'limber_DESY3wl__2_dz',
                    'dz_s4': 'limber_DESY3wl__3_dz',
                    'eta1_ia': 'limber_eta_IA',
                    'a1_ia': 'bias_A_IA'
               }
    
    fname = 'chains/posteriors_DESY3_arico (2).hdf5'
    d = {}
    samples = []
    names = []
    ranges = {}
    with h5py.File(fname, 'r') as f:
        for n in f.keys():
            nts = n if n not in Arico2cobaya else Arico2cobaya[n]
            names.append(nts)
            samples.append(f[n][:])
            if MCSamples is not None:
                ranges[nts] = [MCSamples.ranges.getLower(nts), MCSamples.ranges.getUpper(nts)]
    d['MCSamples'] = getdist.MCSamples(samples=np.array(samples).T,
                                       names=names,
                                       #labels=['S_8', '\Omega_{\rm m}', '\sigma_8'],
                                       sampler='nested', ranges=ranges)
    p = d['MCSamples'].getParams()
    d['MCSamples'].addDerived(p.Omega_m - p.Omega_b - p.m_nu/(93.14*p.h**2), 'Omega_c', '\Omega_c')
    d['MCSamples'].addDerived(p.m_nu/(93.14*p.h**2), 'Omega_nu', '\Omega_\nu')

    # Baryon fraction
    d['MCSamples'].addDerived(p.Omega_b / p.Omega_m, 'baryon_fraction', label=r'\frac{\Omega_b}{\Omega_m}')

    d['label'] = label
    d['color'] = color
    d['fname'] = fname
    # d['R-1'] = d['MCSamples'].getGelmanRubin()
    d['R-1'] = None
    pars_bf = {'A_s': 1.40179038321592e-09,
                    'M1_z0_cen': 9.81558476866249,
                    'M_c': 14.1259092240329,
                    'M_inn': 10.2360411967192,
                    'a1_ia': 0.509008189452402,
                    'beta': 0.152060558929362,
                    'dz_s1': 0.00226312022770828,
                    'dz_s2': -2.24966424779111e-05,
                    'dz_s3': 0.00432907369464818,
                    'dz_s4': -0.0128902642087041,
                    'eta': -0.55509796540381,
                    'eta1_ia': 3.27590577435062,
                    'hubble': 0.811111641377408,
                    'm1': -0.00273086790778818,
                    'm2': -0.0290949309843267,
                    'm3': -0.0491060526869128,
                    'm4': -0.0164044410398353,
                    'neutrino_mass': 0.180430438312174,
                    'ns': 0.929912819977126,
                    'omega_baryon': 0.0579130720099904,
                    'omega_matter': 0.328963719056971,
                    'theta_inn': -0.874294852422897,
                    'theta_out': 0.476285346741361}
    d['pars_bf'] = {}
    for n, v in pars_bf.items():
        if n not in Arico2cobaya:
            d['pars_bf'][n] = v
        else:
            d['pars_bf'][Arico2cobaya[n]] = v
            
    # Add derived
    pbf = d['pars_bf']
    pbf['Omega_c'] = pbf['Omega_m'] - pbf['Omega_b'] - pbf['m_nu']/(93.14*pbf['h']**2)
    pbf['A_sE9'] = pbf['A_s']*1e9
    cosmo = ccl.Cosmology(Omega_c=pbf['Omega_c'], Omega_b=pbf['Omega_b'], n_s=pbf['n_s'], A_s=pbf['A_s'], m_nu=pbf['m_nu'], h=pbf['h'])
    pbf['sigma8'] = cosmo.sigma8()
    pbf['S8'] = pbf['sigma8'] * np.sqrt(pbf['Omega_m'] / 0.3)
    pbf['baryon_fraction'] = pbf['Omega_b'] / (pbf['Omega_c'] + pbf['Omega_b'])
    
    d['boost'] = BaryonBoost('chains/posteriors_DESY3_arico (2)_Sk.npz')
    
    d['inbounds_baccoemu'] = count_inbounds_baccoemu_range(d['MCSamples'])
        
    return d


def load_DES_official_chain(label, color, statistics):
    if statistics == 'real':
        # Real space ones (Amon+21)
        fname = '/mnt/extraspace/damonge/Datasets/DES_Y3/chains/chain_1x2pt_lcdm_SR_maglim_opt.txt'
        
        ix = [0, 1, 2, 3, 4, 5,
              6, 7, 8, 9,
              10, 11, 12, 13,
              23, 24,
              28, # sigma8
              33, 34] # post, weight
    elif statistics == 'Fourier':
        # Fourier space ones (Cyrille+21)
        fname = '/mnt/extraspace/damonge/Datasets/DES_Y3/chains/chain_nla_twopoint_cls_Y3_mastercat_unblinded_data_cov_nopureE_countmode_C1_apo0.0_nside1024_improved_NKA_NG_v2_cov.fits_chi2_1.0.ini_lcdm.txt'
        ix = [0, 1, 2, 3, 4, 5,
              6, 7, 8, 9,
              10, 11, 12, 13,
              14, 15,
              16, # sigma8
              21, 22] # post, weight
    # with open(fname) as f:
    #     for i, p in enumerate(f.readline().split()):
    #         print(i, p)
    
    # For v3
    pnames = ['Omega_m', 'h', 'Omega_b', 'n_s', 'A_s', 'omega_nu',
              'bias_DESY3wl__0_m', 'bias_DESY3wl__1_m', 'bias_DESY3wl__2_m', 'bias_DESY3wl__3_m',
              'limber_DESY3wl__0_dz', 'limber_DESY3wl__1_dz', 'limber_DESY3wl__2_dz', 'limber_DESY3wl__3_dz',
              'bias_DESY3wl_A_IA', 'limber_DESY3wl_eta_IA',
              'sigma8'
              ]

    labels = ['\Omega_m', 'h', '\Omega_b', 'n_s', 'A_s',
              'm^{DESY3_0}', 'm^{DESY3_0}', 'm^{DESY3_0}', 'm^{DESY3_0}',
              '\Delta z^{DESY3_0}', '\Delta z^{DESY3_1}', '\Delta z^{DESY3_2}', '\Delta z^{DESY3_3}',
              'A^{DESY3}_{IA}', '\eta^{DESY3}_{IA}',
               '\sigma_8', 
              ]


    
    lchains = np.loadtxt(fname, usecols=ix, unpack=True)
    steps, lkl, w = lchains[:-2], lchains[-2], lchains[-1]
    # Convention
    # steps[15:24] *= -1
    d_official = getdist.mcsamples.MCSamples(samples=steps.T, loglikes=lkl, weights=w,
                                             names=pnames, labels=labels, sampler='nested')
    p = d_official.getParams()
    d_official.addDerived(p.sigma8 * np.sqrt(p.Omega_m / 0.3), 'S8', 'S_8')
    d_official.addDerived(p.omega_nu*93.14, 'm_nu', 'M_\nu')
    d_official.addDerived(p.bias_DESY3wl_A_IA, 'bias_A_IA', 'A_{\rm IA}')
    d_official.addDerived(p.limber_DESY3wl_eta_IA, 'limber_eta_IA', '\eta_{\rm IA}')

    d = {'MCSamples': d_official,
         'label': label,
         'color': color,
         'path': fname}

    return d

def load_DESY3_hybridpriors(label, color):
    # Result of the DES+KiDS official analysis: https://arxiv.org/pdf/2305.17173.pdf
    fname = '/mnt/extraspace/gravityls_3/data/DESY3wl_K1000/chain_desy3_hybrid_analysis.txt'
    # with open(fname) as f:
    #     for i, p in enumerate(f.readline().split()):
    #         print(i, p)
    
    pnames_dict = {
        0: "limber_DESY3wl__0_dz",
        1: "limber_DESY3wl__1_dz",
        2: "limber_DESY3wl__2_dz",
        3: "limber_DESY3wl__3_dz",
        4: "bias_DESY3wl__0_m",
        5: "bias_DESY3wl__1_m",
        6: "bias_DESY3wl__2_m",
        7: "bias_DESY3wl__3_m",
        8: "bias_A_IA",
        9: 'limber_eta_IA',
        10: 'HMCode_logT_AGN',
        11: 'omega_c',
        12: 'omega_b',
        13: 'h',
        14: 'n_s',
        15: 'S8_input',
        16: 'm_nu',
        17: 'S8',
        18: 'sigma8',
        19: 'A_s',
        20: 'Omega_m',
    }
    # 30: post
    # 31: weight
    ix = list(pnames_dict.keys()) + [30, 31]
    pnames = list(pnames_dict.values())
    labels = [
              '\Delta z^{DESY3_0}', '\Delta z^{DESY3_1}', '\Delta z^{DESY3_2}', '\Delta z^{DESY3_3}',
              'm^{DESY3_0}', 'm^{DESY3_0}', 'm^{DESY3_0}', 'm^{DESY3_0}',
              'A^{DESY3}_{IA}', '\eta^{DESY3}_{IA}',
              r'\log(T_{\rm AGN})',
              '\omega_c', '\omega_b', 'h', 'n_s', r'S_8^{\rm input}', 'M_\nu', 'S_8', '\sigma_8', 'A_s', '\Omega_m'
    ]

    
    lchains = np.loadtxt(fname, usecols=ix, unpack=True)
    steps, lkl, w = lchains[:-2], lchains[-2], lchains[-1]
    # Convention
    # steps[15:24] *= -1
    d_official = getdist.mcsamples.MCSamples(samples=steps.T, loglikes=lkl, weights=w,
                                             names=pnames, labels=labels, sampler='nested')
    p = d_official.getParams()
    Omega_nu = p.m_nu/(93.14*p.h**2)
    d_official.addDerived(Omega_nu, 'Omega_nu', '\Omega_\nu')
    d_official.addDerived(p.omega_c / p.h**2, 'Omega_c', '\Omega_c')
    d_official.addDerived(p.omega_b / p.h**2, 'Omega_b', '\Omega_b')
    d_official.addDerived(p.A_s*1e9, 'A_sE9', 'A_s 10^{9}')

    d = {'MCSamples': d_official,
         'label': label,
         'color': color,
         'path': fname}

    return d


###############################
########### DESY3 ###########
###############################
# We use NSIDE 4096 with our binning for all cases to compare the effect of baryons and scale cuts 
# We set lmax=20 to remove the first bin that has the covariance underestimated

########## lmax = Doux+22 ##########

# No Baryons (lmax=Doux+22)
key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with No-Baryons ($20 < \ell < l_{\rm max}^{\rm Doux+22}$)', 'orange', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons + HALOFIT (lmax=Doux+22)
key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_halofit_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with No-Baryons + HALOFIT ($20 < \ell < l_{\rm max}^{\rm Doux+22}$)', 'orange', 0.15, baccoemu=False)
print(key, f"R-1 = {results[key]['R-1']}")

########## lmax = 1000 ##########

# Baryons (4096) lmax=1000, lmin=20
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons ($20 < \ell < 1000$)', 'orange', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096) lmax=1000, lmin=20
key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with No-Baryons ($20 < \ell < 1000$)', 'orange', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096) lmax=2048, lmin=20 with priors from the hybrid pipeline of DESY3+K1000
key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG_hybridpriors'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with No-Baryons ($20 < \ell < 1000$) HybridPipeline priors',
                          'orange', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")


# No Baryons (4096) lmax=2048, lmin=20 with priors from the hybrid pipeline of DESY3+K1000 + HMCode2020 + TAGN as them
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG_hybridpriors_hmcode2020'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons ($T_{\rm AGN}$) ($20 < \ell < 1000$) HybridPipeline priors + HMCode2020',
                          'orange', 0.15, baccoemu=False)
print(key, f"R-1 = {results[key]['R-1']}")


# No Baryons + HALOFIT (4096)
key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_halofit_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with No-Baryons + HALOFIT ($20 < \ell < 1000$)', 'blue', 0.15, baccoemu=False)
print(key, f"R-1 = {results[key]['R-1']}")

########## lmax = 2048 ##########

# Baryons (4096) lmax=2048, lmin=20
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons ($20 < \ell < 2048$)', 'orange', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096) lmax=2048, lmin=20
key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with No-Baryons ($20 < \ell < 2048$)', 'orange', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons + HALOFIT (4096)
key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_halofit_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with No-Baryons + HALOFIT ($20 < \ell < 2048$)', 'blue', 0.15, baccoemu=False)
print(key, f"R-1 = {results[key]['R-1']}")

########## lmax = 4500 ##########

# Baryons + Pixel Window function (4096)
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons ($20 < \ell < 4500$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096) lmax=4500, lmin=20
key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with No-Baryons ($20 < \ell < 4500$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons + Pixel Window function (4096)
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_pixwin'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons + PixWin ($20 < \ell < 4500$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

########## lmax = 6000 ##########
# Baryons + Pixel Window function (4096)
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmax6000_lmin20_GNG_pixwin'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons + PixWin ($20 < \ell < 6000$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")


########## lmax = 8192 ##########
# Baryons (4096)
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons ($20 < \ell < 8192$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096)
key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with No-Baryons ($20 < \ell < 8192$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons + Pixel Window function (4096)
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons + PixWin ($20 < \ell < 8192$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons + Noise marginalized (prior 10%) (4096)
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_nlmarg'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons + Noise Marg (10%) ($20 < \ell < 8192$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons + Pixel Window function + Noise marginalized (prior 10%) (4096)
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin_nlmarg'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons + PixWin + Noise Marg (10%) ($20 < \ell < 8192$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons + Noise marginalized (prior 1000%) (4096)
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_nlmarg_prior10'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons + Noise Marg (1000%) ($20 < \ell < 8192$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons + Pixel Window function + Noise marginalized (prior 1000%) (4096)
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin_nlmarg_prior10'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons + PixWin + Noise Marg (1000%) ($20 < \ell < 8192$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

###############################################
############## Mock data ######################
###############################################
# Baryons (4096) lmax=1000 (EuclidEmulator2 Pkmm + BAHAMAS Sk from baccoemu)
key = "desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons,  Mock Data, BF (no baryons) cosmology, noiseless ($\ell < 1000$)', 'pink', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")


# Baryons (4096) lmax=4500 (EuclidEmulator2 Pkmm + BAHAMAS Sk from baccoemu)
key = "desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons,  Mock Data noiseless ($\ell < 4500$)', 'pink', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (EuclidEmulator2 Pkmm + BAHAMAS Sk from baccoemu)
key = "desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons,  Mock Data, BF (no baryons) cosmology, noiseless ($\ell < 4500$)', 'pink', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (EuclidEmulator2 Pkmm + BAHAMAS Sk from table)
key = "desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons,  Mock Data (EE2 + BAHAMAS Sk table) noiseless ($\ell < 4500$)', 'pink', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (EuclidEmulator2 Pk (k< 0.05/Mpc) + BAHAMAS Pk from table)
key = "desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockBAHAMAStable_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons,  Mock Data (BAHAMAS table) noiseless ($\ell < 4500$)', 'pink', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (EuclidEmulator2 Pkmm + GrO from baccoemu)
key = "desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_GrO_5kmax_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with No-Baryons,  Mock Data (GrO 5kmax) noiseless ($\ell < 4500$)', 'pink', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (EuclidEmulator2 Pkmm + GrO from baccoemu)
key = "desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_GrO_10kmax_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with No-Baryons,  Mock Data (GrO 10kmax) noiseless ($\ell < 4500$)', 'pink', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

###############################################
############## HALOFIT extrap ######################
###############################################
# Baryons + HALOFIT extrap (4096)
key = 'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons HALOFIT extrap ($20 < \ell < 4500$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (EuclidEmulator2 Pkmm + BAHAMAS Sk from baccoemu)
key = 'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons HALOFIT extrap, Mock Data EE2+BAHAMAS Sk table ($20 < \ell < 4500$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (EuclidEmulator2 Pk (k< 0.05/Mpc) + BAHAMAS Pk from table)
key = "desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG_mockBAHAMAStable_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons HALOFIT extrap,  Mock Data (BAHAMAS table) noiseless ($\ell < 4500$)', 'pink', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")


###############################################
############## Other analysis #################
###############################################

# Arico+23
key = 'Arico+23'
results[key] = load_Arico23(r'Aricò el al. 2023', 'black', results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']['MCSamples'])
print(key, f"R-1 = {results[key]['R-1']}")

########## Official Real Space (2105.13543)
key = 'Amon+21'
results[key] = load_DES_official_chain('Amon et al. 2021 (Optimized)', 'gray', 'real')

########## Official Fourier Space (2203.07128)
key = 'Doux+22'
results[key] = load_DES_official_chain('Doux et al. 2022 (NLA)', 'black', 'Fourier')

########## Official Real Space reanalysis with the hybrid pipeline (2305.17173)
key = 'DESY3_Abbott+23'
results[key] = load_DESY3_hybridpriors('Abbot et al 2023 (DESY3 hybrid pipeline)', 'black')

###############################################
########### DESY3 no K1000, noHSC ###########
###############################################
# DES binning
########## lmax = 2048 ##########
# No Baryons, No K1000 (4096)
key = 'desy3wl_noK1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 w/o K1000 overlap with No-Baryons ($20 < \ell < 2048$)', 'pink', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons, no K1000, no HSC (4096)
key = 'desy3wl_noK1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 w/o K1000 & HSC overlap with No-Baryons ($20 < \ell < 2048$)', 'red', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

########## lmax = 4500 ##########
# No Baryons, no K1000 (4096)
key = "desy3wl_noK1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 w/o K1000 overlap with No-Baryons ($20 < \ell < 4500$)', 'pink', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons, no K1000, no HSC (4096)
key = "desy3wl_noK1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 w/o K1000 & HSC overlap with No-Baryons ($20 < \ell < 4500$)', 'red', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

########## lmax = 8192 ##########
# No Baryons, noK1000, noHSC (4096)
key = 'desy3wl_noK1000_nohsc_nobaryons_nocuts_nla_nside4096_lmin20_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 w/o K1000 & HSC overlap with No-Baryons ($20 < \ell < 8192$)', 'red', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Within baccoemu ranges?

# %% hidden=true
k = "CHAIN"
print(f"{k:<70} Nonlinear emulator   baryon emulator   baryon fraction")
for k in results:
    if (k.startswith('desy3wl') or k == 'Arico+23') and ('k1000' not in k):
        if 'inbounds_baccoemu' in results[k]:
            print (f"{k:<80}{results[k]['inbounds_baccoemu']['nonlinear']:<10.2f}   {results[k]['inbounds_baccoemu']['baryon']:<15.2f}   {results[k]['inbounds_baccoemu']['baryon_fraction']:.2f}")

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Mock data

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### Planck18 cosmology

# %%
# Parameters used to generate the power spectrum and baryonic boost
cosmopars = {
    'omega_cold'    :  0.31,
    'omega_baryon'  :  0.045,
    'sigma8_cold'   :  0.82,
    'ns'            :  0.965,
    'hubble'        :  0.7,
    'neutrino_mass' :  0.06,
    'w0'            : -1,
    'wa'            :  0,
    'M_c'           :  13.58,
    'eta'           : -0.27,
    'beta'          : -0.33,
    'M1_z0_cen'     :  12.04,
    'theta_out'     :  0.25,
    'theta_inn'     : -0.86,
    'M_inn'         :  13.4
}

cosmopars['omega_matter'] = cosmopars['omega_cold'] + cosmopars['neutrino_mass'] / 93.14 / cosmopars['hubble']**2
cosmopars['A_s'] = 1.8753844640255685e-09
cosmopars['sigma8_tot'] = 0.8163992592396454

input_params = {'Omega_c': cosmopars['omega_cold'] - cosmopars['omega_baryon'],
                'Omega_m': cosmopars['omega_matter'],
                'Omega_b': cosmopars['omega_baryon'],
                'sigma8': cosmopars['sigma8_tot'],
                'S8': cosmopars['sigma8_tot'] * np.sqrt(cosmopars['omega_matter'] / 0.3),
                'A_s': cosmopars['A_s'],
                'h': cosmopars['hubble'],
                'omega_m': cosmopars['omega_matter'] * cosmopars['hubble']**2,
                'limber_DESY3wl__0_dz': 0,
                'limber_DESY3wl__1_dz': 0,
                'limber_DESY3wl__2_dz': 0, 
                'limber_DESY3wl__3_dz': 0,
                'limber_KiDS1000__0_dz': 0,
                'limber_KiDS1000__1_dz': -0.002,
                'limber_KiDS1000__2_dz': -0.013,
                'limber_KiDS1000__3_dz': -0.011,
                'limber_KiDS1000__4_dz': 0.006,
                'limber_HSCDR1wl__0_dz': 0,
                'limber_HSCDR1wl__1_dz': 0,
                'limber_HSCDR1wl__2_dz': 0,
                'limber_HSCDR1wl__3_dz': 0,
                'bias_DESY3wl__0_m': -0.0063,
                'bias_DESY3wl__1_m': -0.0198,
                'bias_DESY3wl__2_m': -0.0241, 
                'bias_DESY3wl__3_m': -0.0369,
                'bias_KiDS1000__0_m': 0,
                'bias_KiDS1000__1_m': 0.,
                'bias_KiDS1000__2_m': 0.,
                'bias_KiDS1000__3_m': 0.,
                'bias_KiDS1000__4_m': 0.,
                'bias_HSCDR1wl__0_m': 0,
                'bias_HSCDR1wl__1_m': 0,
                'bias_HSCDR1wl__2_m': 0,
                'bias_HSCDR1wl__3_m': 0,
                'bias_DESY3wl_A_IA': 0,
                'limber_DESY3wl_eta_IA': 0,
                'bias_KiDS1000_A_IA': 0,
                'limber_KiDS1000_eta_IA': 0,
                'bias_HSCDR1wl_A_IA': 0,
                'limber_HSCDR1wl_eta_IA': 0,
               }
input_params.update({k: cosmopars[k] for k in ['M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_out', 'theta_inn', 'M_inn']})

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# #### GrO [effect of extrapolation]

# %%
chains = [
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_GrO_10kmax_noiseless',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_GrO_5kmax_noiseless',
         ]
print('Omega_m')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('Omega_m'))
print()
print('S8')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('S8'))

# %%
chains = [
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_GrO_10kmax_noiseless',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_GrO_5kmax_noiseless',
         ]

print('p-value = ', results[chains[0]]['pvalue_bf'], 'chi2 = ', results[chains[0]]['chi2bf'])
print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['lightgray', 'red'] # [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='black')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='black', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# #### Baryons (baccoemu)

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG'
         ]
print('Omega_m')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('Omega_m'))
print()
print('S8')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('S8'))

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless',
         ]

print('p-value = ', results[chains[0]]['pvalue_bf'])
print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='black')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='black', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### Baryons (BAHAMAS table)

# %%
input_params = {'Omega_m': 0.3067,
                'Omega_b': 0.0482,
                'h': 0.6787,
                'm_nu': 0.06,
                'sigma8': 0.8085,
                'n_s':0.9701,
                'limber_DESY3wl__0_dz': 0,
                'limber_DESY3wl__1_dz': 0,
                'limber_DESY3wl__2_dz': 0, 
                'limber_DESY3wl__3_dz': 0,
                'limber_KiDS1000__0_dz': 0,
                'limber_KiDS1000__1_dz': -0.002,
                'limber_KiDS1000__2_dz': -0.013,
                'limber_KiDS1000__3_dz': -0.011,
                'limber_KiDS1000__4_dz': 0.006,
                'limber_HSCDR1wl__0_dz': 0,
                'limber_HSCDR1wl__1_dz': 0,
                'limber_HSCDR1wl__2_dz': 0,
                'limber_HSCDR1wl__3_dz': 0,
                'bias_DESY3wl__0_m': -0.0063,
                'bias_DESY3wl__1_m': -0.0198,
                'bias_DESY3wl__2_m': -0.0241, 
                'bias_DESY3wl__3_m': -0.0369,
                'bias_KiDS1000__0_m': 0,
                'bias_KiDS1000__1_m': 0.,
                'bias_KiDS1000__2_m': 0.,
                'bias_KiDS1000__3_m': 0.,
                'bias_KiDS1000__4_m': 0.,
                'bias_HSCDR1wl__0_m': 0,
                'bias_HSCDR1wl__1_m': 0,
                'bias_HSCDR1wl__2_m': 0,
                'bias_HSCDR1wl__3_m': 0,
                'bias_DESY3wl_A_IA': 0,
                'limber_DESY3wl_eta_IA': 0,
                'bias_KiDS1000_A_IA': 0,
                'limber_KiDS1000_eta_IA': 0,
                'bias_HSCDR1wl_A_IA': 0,
                'limber_HSCDR1wl_eta_IA': 0,
               }
input_params['S8'] = input_params['sigma8'] * np.sqrt(input_params['Omega_m'] / 0.3)

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockBAHAMAStable_noiseless',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless',
          'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG_mockBAHAMAStable_noiseless',
          'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG'
         ]
print('Omega_m')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('Omega_m'))
print()
print('S8')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('S8'))

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockBAHAMAStable_noiseless',
         ]

print('p-value = ', results[chains[0]]['pvalue_bf'], 'chi2 = ', results[chains[0]]['chi2bf'])
print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['gray', 'pink']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='black')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='black', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless',
          'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless',
         ]

print('p-value = ', results[chains[0]]['pvalue_bf'], 'chi2 = ', results[chains[0]]['chi2bf'])
print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['gray', 'pink']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='black')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='black', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockBAHAMAStable_noiseless',
          'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG_mockBAHAMAStable_noiseless',
         ]

print('p-value = ', results[chains[0]]['pvalue_bf'], 'chi2 = ', results[chains[0]]['chi2bf'])
print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['gray', 'pink']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='black')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='black', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 


# %%
def get_k_Sk_from_table(pktable_dmo, pktable_b):
    sel_dmo = pktable_dmo[0] == 0 
    sel_b = pktable_b[0] == 0

    assert np.all(pktable_dmo[0, sel_dmo] == pktable_b[0, sel_b])
    assert np.all(pktable_dmo[1, sel_dmo] == pktable_b[1, sel_b])

    h = 0.678  # Planck 2015
    k = pktable_dmo[1, sel_dmo] * h
    Sk = pktable_b[2, sel_b] / pktable_dmo[2, sel_dmo]

    sel = (k > 0.1) * (k < 2.5)
    k = k[sel]
    Sk = Sk[sel]
    return k, Sk

chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockBAHAMAStable_noiseless',
          'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG_mockBAHAMAStable_noiseless',
         ]

colors = ['brown', 'black']
labels = ['DESY3 (CCL extrap)', 'DESY3 (HALOFIT extrap)']

f, ax = plt.subplots()

# Add BAHAMAS line
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_DMONLY_2fluid_nu0.06_Planck2015_L400N1024.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_BAHAMAS_nu0.06_Planck2015.dat", unpack=True, usecols=(0, 1, 2))
k, Sk = get_k_Sk_from_table(pktable_dmo, pktable_b)
ax.semilogx(k, Sk, color='red', ls='--', label='BAHAMAS')    

# Add chain constraints
for i, c in enumerate(chains):
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    label = dict(zip(chains, labels))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color, alpha=0.3 - 0.1*i)

# Finalize plot
ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$', fontsize=14)
ax.set_ylabel('$S_k(a=1)$', fontsize=14)
ax.tick_params(axis='x', labelsize=12)
ax.tick_params(axis='y', labelsize=12)
ax.legend(loc='lower left', fontsize=12, frameon=False)

plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### BF (no baryons) cosmology

# %%
pkfile = np.load('./test/BlindCarlosPk2.npz', allow_pickle=True)

cosmopars = dict(pkfile['cosmopars'].tolist())
cosmopars.update({'sigma8_tot': float(pkfile['sigma8 tot'])})
cosmopars['omega_matter'] = cosmopars['omega_cold'] + cosmopars['neutrino_mass'] / 93.14 / cosmopars['hubble']**2

input_params = {'Omega_c': cosmopars['omega_cold'] - cosmopars['omega_baryon'],
                'Omega_m': cosmopars['omega_matter'],
                'Omega_b': cosmopars['omega_baryon'],
                'sigma8': cosmopars['sigma8_tot'],
                'S8': cosmopars['sigma8_tot'] * np.sqrt(cosmopars['omega_matter'] / 0.3),
                'A_s': cosmopars['A_s'],
                'h': cosmopars['hubble'],
                'omega_m': cosmopars['omega_matter'] * cosmopars['hubble']**2,
                'limber_DESY3wl__0_dz': 0,
                'limber_DESY3wl__1_dz': 0,
                'limber_DESY3wl__2_dz': 0, 
                'limber_DESY3wl__3_dz': 0,
                'limber_KiDS1000__0_dz': 0,
                'limber_KiDS1000__1_dz': -0.002,
                'limber_KiDS1000__2_dz': -0.013,
                'limber_KiDS1000__3_dz': -0.011,
                'limber_KiDS1000__4_dz': 0.006,
                'limber_HSCDR1wl__0_dz': 0,
                'limber_HSCDR1wl__1_dz': 0,
                'limber_HSCDR1wl__2_dz': 0,
                'limber_HSCDR1wl__3_dz': 0,
                'bias_DESY3wl__0_m': -0.0063,
                'bias_DESY3wl__1_m': -0.0198,
                'bias_DESY3wl__2_m': -0.0241, 
                'bias_DESY3wl__3_m': -0.0369,
                'bias_KiDS1000__0_m': 0,
                'bias_KiDS1000__1_m': 0.,
                'bias_KiDS1000__2_m': 0.,
                'bias_KiDS1000__3_m': 0.,
                'bias_KiDS1000__4_m': 0.,
                'bias_HSCDR1wl__0_m': 0,
                'bias_HSCDR1wl__1_m': 0,
                'bias_HSCDR1wl__2_m': 0,
                'bias_HSCDR1wl__3_m': 0,
                'bias_DESY3wl_A_IA': 0,
                'limber_DESY3wl_eta_IA': 0,
                'bias_KiDS1000_A_IA': 0,
                'limber_KiDS1000_eta_IA': 0,
                'bias_HSCDR1wl_A_IA': 0,
                'limber_HSCDR1wl_eta_IA': 0,
               }
input_params.update({k: cosmopars[k] for k in ['M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_out', 'theta_inn', 'M_inn']})

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG'
         ]
print('Omega_m')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('Omega_m'))
print()
print('S8')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('S8'))

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG'
         ]
print('Omega_m')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('Omega_m'))
print()
print('S8')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('S8'))

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
         ]

print('p-value = ', results[chains[0]]['pvalue_bf'])
print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['gray', 'orange']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='black')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='black', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
         ]

print('p-value = ', results[chains[0]]['pvalue_bf'])
print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='black')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='black', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## HALOFIT vs BACCOEmu

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### lmax=1000

# %% hidden=true
# HALOFIT vs BACOEmu
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_halofit_GNG'
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### lmax = 2048

# %% hidden=true
# HALOFIT vs BACOEmu with lmax=2048
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_halofit_GNG'
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# HALOFIT vs BACOEmu with lmax=2048
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_halofit_GNG'
         ]

parameters = ['Omega_m', 'sigma8', 'S8', 'bias_A_IA', 'limber_eta_IA']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no Baryons ($\ell_{\rm max} = 1000$)

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors[0] = 'gray'

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no Baryons ($\ell_{\rm max} = 2048$)

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG'
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
labels = ['No baryons', 'Baryons']
colors = ['gray', 'black']

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors[0] = 'gray'

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no Baryons ($\ell_{\rm max} = 4500$)

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['gray', 'black']

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no Baryons ($\ell_{\rm max} = 8192$)

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNG']

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## $\ell_{\rm max} = 2048$ vs $\ell_{\rm max} = 8192$

# %% hidden=true
print('ell_max = 2048; chi2 =', results['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG']['chi2_bf'],
      ' || ndata = ', results['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG']['ndata'], 
      ' || p-value = ', results['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG']['pvalue_bf'])
print('ell_max = 8196; chi2 =', results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']['chi2_bf'],
      ' || ndata = ', results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']['ndata'],
      ' || p-value = ', results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']['pvalue_bf'])

# %% hidden=true
ndata = results['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG']['ndata']
print(f"Values with data 20 < ell < 2048. This makes {ndata} data points")
print('chi2(ell_max=2048 with BF with ell_max = 2048) =',
      results['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG']['chi2_bf'],
      'p-value = ', results['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG']['pvalue_bf'])

chi2 = evaluate_model(results['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG']['model'],
                     results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']['pars_bf'])[0]
p = 1 - stats.chi2.cdf(chi2, df=ndata - 2)
print('chi2(ell_max=2048 with BF with ell_max = 8192) =', chi2,'p-value = ', p)

print()
print()
ndata = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']['ndata']
print(f"Values with data up to ell = 8192. This makes {ndata} data points")
chi2 = evaluate_model(results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']['model'],
                     results['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG']['pars_bf'])[0]
p = 1 - stats.chi2.cdf(chi2, df=ndata - 2)
print('chi2(ell_max=8192 with BF with ell_max = 2048) =', chi2, 'p-value = ', p)
print('chi2(ell_max=8192 with BF with ell_max = 8192) =',
      results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']['chi2_bf'],
      'p-value = ', results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']['pvalue_bf'])

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']

parameters = ['S8', 'sigma8', 'Omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['black', 'gray', 'blue', 'red'] #[results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']

parameters = ['S8', 'sigma8', 'Omega_m', 'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['gray', 'black', 'blue', 'red'] #[results[i]['color'] for i in chains]

print(results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']['MCSamples'].getInlineLatex('M_c'))
g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryon constraints with different lmax

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          # 'desy3wl_baryons_nocuts_nla_nside4096_lmax6000_lmin20_GNG_pixwin',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'Arico+23'
          # 'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red', 'purple']
labels = [r"$\ell < 1000$", r"$\ell < 2048$", r"$\ell < 4500$", r"$\ell < 8192$", "Arico+23"]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 2],
                contour_ls=[None, '-', '-', '-', '--'])


# plt.suptitle('DESY3 only')        
plt.show()

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          # 'desy3wl_baryons_nocuts_nla_nside4096_lmax6000_lmin20_GNG_pixwin',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'Arico+23'
          # 'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red', 'purple']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 2],
                contour_ls=[None, '-', '-', '-', '--'])

# bf1 = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']['pars_bf']
# bf2 = results['Arico+23']['pars_bf']

# for i, p1 in enumerate(parameters):
#     ax = g.get_axes((i, i))
#     if p1 in bf1:
#         ax.axvline(bf1[p1], ls='--', color='b')
#     if p1 in bf2:
#         ax.axvline(bf2[p1], ls='--', color='r')
#     for p2 in parameters[i+1:]:
#         ax = g.get_axes_for_params(p1, p2)
#         if (p1 in bf1) and (p2 in bf1):
#             ax.plot(bf1[p1], bf1[p2], 'bo')
#         if (p1 in bf2) and (p2 in bf2):
#             ax.plot(bf2[p1], bf2[p2], 'ro') 

        
plt.show()

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          # 'desy3wl_baryons_nocuts_nla_nside4096_lmax6000_lmin20_GNG_pixwin',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          # 'P18_official'
         ]

parameters = parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red', 'orange']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# bf1 = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']['pars_bf']
# bf2 = results['Arico+23']['pars_bf']

# for i, p1 in enumerate(parameters):
#     ax = g.get_axes((i, i))
#     if p1 in bf1:
#         ax.axvline(bf1[p1], ls='--', color='b')
#     if p1 in bf2:
#         ax.axvline(bf2[p1], ls='--', color='r')
#     for p2 in parameters[i+1:]:
#         ax = g.get_axes_for_params(p1, p2)
#         if (p1 in bf1) and (p2 in bf1):
#             ax.plot(bf1[p1], bf1[p2], 'bo')
#         if (p1 in bf2) and (p2 in bf2):
#             ax.plot(bf2[p1], bf2[p2], 'ro') 

        
plt.show()

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Pixel window function

# %% hidden=true
s = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']['data_sacc']
s_pw = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin']['data_sacc']

# Get the ells
ell, cl = s.get_ell_cl('cl_ee', 'DESY3__3', 'DESY3__3')
    
for tr1, tr2 in s_pw.get_tracer_combinations():
    ell, cl = s.get_ell_cl('cl_ee', tr1, tr2)
    ell, cl2, cov = s_pw.get_ell_cl('cl_ee', tr1, tr2, return_cov=True)
    err = np.sqrt(np.diag(cov))
    plt.semilogx(ell, (cl-cl2)/err)
plt.axvline(8192, ls='--', color='gray')
plt.xlabel(r"$\ell$")
plt.ylabel(r"$(C_\ell - C_\ell^{\rm pixwin}) / \sigma_\ell$")
plt.show()

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin',
          'Arico+23']

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = [#'desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax6000_lmin20_GNG_pixwin',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin',
          'Arico+23'
]

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = [#'desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax6000_lmin20_GNG_pixwin',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin',
]

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Noise marginalization

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### Prior 10%

# %% hidden=true
s = sacc.Sacc.load_fits("/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov_GNG_pixwin_nlmarg.fits")

# %% hidden=true
plt.imshow(np.log10(np.abs(s.covariance.covmat)), vmin=-26)
plt.colorbar()
plt.show()

d = np.diag(s.covariance.covmat)
plt.imshow(np.log10(np.abs(s.covariance.covmat / np.sqrt(d[:, None] * d[None, :]))), vmin=-3)
plt.colorbar()

# %% hidden=true
nlcov = np.load("/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/nlmarg_cov.npz")['arr_0']
d = np.diag(nlcov)
# plt.imshow(np.log10(nlcov))
plt.imshow(np.log10(np.abs(nlcov / np.sqrt(d[:, None] * d[None, :]))), vmin=-2)
plt.colorbar()

# %% hidden=true
# nlcov = np.load("/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/nlmarg_cov.npz")['arr_0']

cov = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin']['data_sacc'].covariance.covmat
cov_nlmarg = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin_nlmarg']['data_sacc'].covariance.covmat

nlcov = cov - cov_nlmarg
d = np.diag(nlcov)

plt.imshow(np.log10(np.abs(nlcov)))
plt.colorbar()
plt.title('log10(|Cov - Cov_nlmarg|)')
plt.show()

plt.imshow(np.log10(np.abs(nlcov / np.sqrt(d[:, None] * d[None, :]))), vmin=-2)
plt.colorbar()
plt.show()

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_nlmarg',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin_nlmarg',
          'Arico+23']

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin_nlmarg',
          'Arico+23']

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1])

# %% [markdown] hidden=true
# ### Prior 1000%

# %% hidden=true
# s = sacc.Sacc.load_fits("/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov_GNG_pixwin_nlmarg_prior10.fits")
plt.imshow(np.log10(np.abs(s.covariance.covmat)))
plt.colorbar()
plt.show()

nlcov = np.load("/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/nlmarg_cov_prior10.npz")['arr_0']
plt.imshow(np.log10(np.abs(nlcov)))
plt.colorbar()

# %% hidden=true
cov = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin']['data_sacc'].covariance.covmat
cov_nlmarg = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin_nlmarg']['data_sacc'].covariance.covmat
cov_nlmarg10 = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin_nlmarg_prior10']['data_sacc'].covariance.covmat


nlcov = cov - cov_nlmarg
plt.imshow(np.log10(np.abs(nlcov)))
plt.colorbar()
plt.title('log10(|Cov - Cov_nlmarg|)')
plt.show()

nlcov = cov - cov_nlmarg10
plt.imshow(np.log10(np.abs(nlcov)))
plt.colorbar()
plt.title('log10(|Cov - Cov_nlmarg_prior10|)')
plt.show()

# %% hidden=true
cov = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin']['data_sacc'].covariance.covmat
cov_nlmarg = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin_nlmarg']['data_sacc'].covariance.covmat
cov_nlmarg10 = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin_nlmarg_prior10']['data_sacc'].covariance.covmat

icov = np.linalg.inv(cov)
icov_nlmarg = np.linalg.inv(cov_nlmarg)
icov_nlmarg10 = np.linalg.inv(cov_nlmarg10)

nlcov = icov - icov_nlmarg
plt.imshow(np.log10(np.abs(nlcov)))
plt.colorbar()
plt.title('log10(|inv(Cov) - inv(Cov_nlmarg)|)')
plt.show()

nlcov10 = icov - icov_nlmarg10
plt.imshow(np.log10(np.abs(nlcov)))
plt.colorbar()
plt.title('log10(|inv(Cov) - inv(Cov_nlmarg_prior10)|)')
plt.show()

plt.imshow(np.log10(np.abs(icov - icov_nlmarg10)))
plt.colorbar()
plt.title('log10(|inv(Cov) - inv(Cov_nlmarg) - (inv(Cov) - inv(Cov_nlmarg_prior10))|)')
plt.show()

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin_nlmarg',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin_nlmarg_prior10',
          'Arico+23']

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs Official (Doux+22 & Amon+21)

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_halofit_GNG',
          'Doux+22']

parameters = ['Omega_m', 'S8', 'sigma8', 'Omega_b', 'm_nu', 'h']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_halofit_GNG',  
          'Doux+22']

parameters = ['Omega_m', 'S8', 'sigma8']
parameters += [f'limber_DESY3__{i}_dz' for i in range(4)]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_halofit_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_halofit_GNG',  
          'Doux+22',
          'Amon+21']

parameters = ['Omega_m', 'S8', 'sigma8']
parameters += [f'limber_DESY3__{i}_dz' for i in range(4)]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_halofit_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_halofit_GNG',  
          'Doux+22',
          'Amon+21']


parameters = ['Omega_m', 'S8', 'sigma8']
parameters += [f'limber_DESY3__{i}_dz' for i in range(4)]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1]) #, markers={'limber_DESY3__0_dz': 0.018})

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_halofit_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_halofit_GNG',  
          'Doux+22',
          'Amon+21']


parameters = ['Omega_m', 'S8', 'sigma8']
parameters+= [f'bias_DESY3__{i}_m' for i in range(4)]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1]) #, markers={'limber_DESY3__0_dz': 0.018})

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## vs DESY3 reanalysis in the DES + KiDS paper

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG_hybridpriors_hmcode2020',
          'DESY3_Abbott+23']

parameters = ['Omega_m', 'S8', 'sigma8', 'Omega_b', 'm_nu', 'h', 'HMCode_logT_AGN']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG_hybridpriors_hmcode2020',
          'DESY3_Abbott+23']

parameters = [f'limber_DESY3__{i}_dz' for i in range(4)]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG_hybridpriors_hmcode2020',
          'DESY3_Abbott+23']

parameters = [f'bias_DESY3__{i}_m' for i in range(4)]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG_hybridpriors_hmcode2020',
          'DESY3_Abbott+23']

parameters = ['bias_A_IA', 'limber_eta_IA']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=4)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG_hybridpriors_hmcode2020'
print(results[key]['label'], results[key]['MCSamples'].getInlineLatex('S8'))
key = 'DESY3_Abbott+23'
print(results[key]['label'], results[key]['MCSamples'].getInlineLatex('S8'))

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons (lmax=4500) vs Arico+23

# %% [markdown] hidden=true
# ### Best fit, $P_k$ and $C_\ell$

# %% hidden=true
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'Arico+23']
for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('M_c'))

# %% hidden=true
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'Arico+23']
for c in chains:
    print(c, '----> A_sE9 = ', results[c]['pars_bf']['A_sE9'])
    
print('Arico+23', '-----> A_sE9 = ', results['Arico+23']['pars_bf']['A_s'] * 1e9)

# %% hidden=true
results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']['pars_bf']

# %% hidden=true
results['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG']['pars_bf']

# %% hidden=true
results['Arico+23']['pars_bf']

# %%

# %% hidden=true
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          ]

k_arr = np.logspace(-2, 1, 100)

f, ax = plt.subplots()
for c in chains:
    pk = results[c]['pk_data_bf']['pk_ww'].eval(k_arr, 1)
    ax.loglog(k_arr, pk, label=results[c]['label'])
    
# Arico+23
pars = results['Arico+23']['pars_bf'].copy()
pars['A_sE9'] = pars['A_s'] * 1e9
pkdata = results['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG'].get_pk_data(pars)
pk = pkdata['pk_ww'].eval(k_arr, 1)
ax.loglog(k_arr, pk, label=results["Arico+23"]['label'])

ax.set_xlabel('$k$ [1/Mpc]')
ax.set_ylabel('$P_k(z=0)$')
ax.legend()
plt.show()

# %% hidden=true
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'Arico+23'
         ]
survey = 'DES'

sdata = results[chains[0]].get_cl_data_sacc()
sbf = results[chains[0]].get_cl_theory_sacc_bf()
pars = results['Arico+23']['pars_bf'].copy()
pars['A_sE9'] = pars['A_s'] * 1e9
sbf_Arico = results[chains[0]].get_cl_theory_sacc(pars)

chi2bf = results[chains[0]]._get_chi2bf()
chi2_Arico23 = results[chains[0]].evaluate_model(pars)[0]
pvaluebf = results[chains[0]]['pvalue_bf']

print(f'{survey}: chi2 = {chi2bf:.2f} vs {chi2_Arico23:.2f} = chi2(Arico+23)')

pvalue_Arico23 = results[chains[0]].get_pvalue(pars)
print(f'{survey}: p-value = {pvaluebf[0]:.2f} vs {pvalue_Arico23[0]:.2f} = p-value(Arico+23)')



nbin = 4
f, ax = plt.subplots(nbin, nbin, figsize=(12, 10), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in sdata.get_tracer_combinations():
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])

    # Data
    ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8)    

    # BF
    ell, cl = sbf_Arico.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='blue', label='BF (Arico+23)', alpha=0.8)

    # BF baryons
    ell, cl = sbf.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='orange', label=results[chains[0]]['label'], alpha=0.8)

for i in range(nbin):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")
    ax[0, 0].legend()

f.suptitle(f'{survey}', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### Contours

# %% hidden=true
results['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG']['MCSamples'].getInlineLatex('M_c')

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'Arico+23']

parameters = ['Omega_m', 'S8', 'sigma8', 'A_sE9',  'M_c', 'baryon_fraction']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[1, 1, 1, 1])

bf1 = results['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG']['pars_bf']
bf2 = results['Arico+23']['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='b')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color='r')
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], 'bo')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], 'ro') 

        
plt.show()

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'Arico+23']

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

bf1 = results['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG']['pars_bf']
bf2 = results['Arico+23']['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='b')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color='r')
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], 'bo')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], 'ro') 

        
plt.show()

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'Arico+23']

parameters = ['Omega_m', 'S8', 'sigma8', 'Omega_b', 'm_nu', 'bias_A_IA', 'limber_eta_IA', 'h']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

bf1 = results['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG']['pars_bf']
bf2 = results['Arico+23']['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='b')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color='r')
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], 'bo')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], 'ro') 

        
plt.show()

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'Arico+23']

parameters = ['Omega_m', 'S8', 'sigma8']
parameters+= [f'limber_DESY3__{i}_dz' for i in range(4)]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1]) #, markers={'limber_DESY3__0_dz': 0.018})

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'Arico+23']

parameters = ['Omega_m', 'S8', 'sigma8']
parameters+= [f'bias_DESY3__{i}_m' for i in range(4)]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## DESY3 vs DESY3 w/o KiDS-1000 overlapping area

# %% hidden=true
m = hp.read_map("/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY30_noK1000_coordC_ns4096.fits.gz")

# For the transaprency to work you need to comment healpy/projaxes.py#L189
# See https://github.com/healpy/healpy/issues/765
m = scale_bin_map(m, 1)
cbound = {'min': 0, 
          'max': 1}

hp.mollview(m, badcolor='lightgray', cbar=False, title='', **cbound)
hp.graticule()

plt.text(-1.25, -0.5, 'DESY3', fontsize=15, color='black', horizontalalignment='left')

# plt.savefig('')
plt.show()
plt.close()

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### lmax = 2048

# %% hidden=true
# Compare the results with the full area of DESY3 and the case where we remove the overlap with KiDS1000
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_noK1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG'
         ]

parameters = ['S8', 'sigma8', 'Omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Compare the results with the full area of DESY3 and the case where we remove the overlap with KiDS1000
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_noK1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG'
         ]

# parameters = ['S8', 'sigma8', 'Omega_m']
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### lmax = 4500

# %% hidden=true
# Compare the results with the full area of DESY3 and the case where we remove the overlap with KiDS1000
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_noK1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG'
         ]

parameters = ['S8', 'sigma8', 'Omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# All params
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## DESY3 vs DESY3 w/o KiDS-1000 w/o HSC overlapping area

# %% hidden=true
m = hp.read_map("/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY30_noK1000_noHSCDR1wl_coordC_ns4096.fits.gz")

# For the transaprency to work you need to comment healpy/projaxes.py#L189
# See https://github.com/healpy/healpy/issues/765
m = scale_bin_map(m, 1)
cbound = {'min': 0, 
          'max': 1}

hp.mollview(m, badcolor='lightgray', cbar=False, title='', **cbound)
hp.graticule()

plt.text(-1.25, -0.5, 'DESY3', fontsize=15, color='black', horizontalalignment='left')

# plt.savefig('')
plt.show()
plt.close()

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### $\ell_{\max} = 2048$

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['desy3wl_noK1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_noK1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors[0] = 'gray'

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### $\ell_{\max} = 4500$

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['desy3wl_noK1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_noK1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors[0] = 'gray'

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### $\ell_{\max} = 8192$

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['desy3wl_noK1000_nohsc_nobaryons_nocuts_nla_nside4096_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNG',
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors[0] = 'gray'

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true toc-hr-collapsed=true
# # KiDS1000

# %% hidden=true
# fname = '/mnt/extraspace/damonge/Datasets/KiDS1000/KiDS1000_cosmis_shear_data_release/chains_and_config_files/main_chains_iterative_covariance/bp/chain/output_multinest_C.txt'

# with open(fname) as f:
#     l = f.readline()
#     s = l.split()
#     for i, s in enumerate(s):
#         print(i, s)

# %% hidden=true
#################################
########### KiDS10000 ###########
#################################

def load_K1000_official(label, color):
    pnames = ['Omega_m', 'h', 'omega_c', 'omega_b', 'A_s', 'sigma8', 'S8', 'Omega_nu',
              'limber_KiDS1000__0_dz', 'limber_KiDS1000__1_dz', 'limber_KiDS1000__2_dz', 'limber_KiDS1000__3_dz', 'limber_KiDS1000__4_dz',
              'bias_KiDS1000_A_IA'
              ]

    labels = ['\Omega_m', 'h', '\omega_c', '\omega_b', 'A_s', '\sigma_8', 'S_8', '\Omega_nu', 
              '\Delta z_{K1000_0}', '\Delta z_{K1000_1}', '\Delta z_{K1000_2}', '\Delta z_{K1000_3}', '\Delta z_{K1000_4}',
              'A_{IA}']

    ix = [15, 2, 0, 1, 14, 13, 12, 16,
         24, 25, 26, 27, 28, 
         6]

    fname = '/mnt/extraspace/damonge/Datasets/KiDS1000/KiDS1000_cosmis_shear_data_release/chains_and_config_files/main_chains_iterative_covariance/bp/chain/output_multinest_C.txt'
    lchains = np.loadtxt(fname, usecols=ix + [31, 32], unpack=True)
    steps, lkl, w = lchains[:len(ix)], lchains[len(ix)], lchains[len(ix)+1]
    # Convention
    # steps[15:24] *= -1
    d_official = getdist.mcsamples.MCSamples(samples=steps.T, loglikes=lkl, weights=w,
                                                names=pnames, labels=labels, sampler='nested')
    p = d_official.getParams()
    d_official.addDerived(p.omega_c / p.h**2, 'Omega_c', '\Omega_c')
    d_official.addDerived(p.omega_b / p.h**2, 'Omega_b', '\Omega_b')

    d = {'MCSamples': d_official,
         'label': label,
         'color': color,
         'path': fname,
         'R-1': None}
    
    return d

########## lmax = 1000 ##########
## lmin = 100 as in KiDS papers
# Baryons (4096)
key = 'k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'KiDS1000 with Baryons ($100 < \ell < 1000$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096)
key = 'k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'KiDS1000 with No-Baryons ($100 < \ell < 1000$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

########## lmax = 2048 ##########
## lmin = 100 as in KiDS papers
# Baryons (4096)
key = 'k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'KiDS1000 with Baryons ($100 < \ell < 2048$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096)
key = 'k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'KiDS1000 with No-Baryons ($100 < \ell < 2048$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096)
key = 'k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_halofit_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'KiDS1000 with No-Baryons ($100 < \ell < 2048$) Halofit', 'black', 0.15, baccoemu=False)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096) lmin=300 to compare with HSC
key = 'k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'KiDS1000 with No-Baryons ($300 < \ell < 2048$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmin=300 to compare with HSC
key = 'k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin300_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'KiDS1000 with Baryons ($300 < \ell < 2048$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

########## lmax = 4500 ##########
# Baryons (4096) lmin=300 to compare with HSC
key = 'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'KiDS1000 with Baryons ($100 < \ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No-Baryons (4096) lmin=300 to compare with HSC
key = 'k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'KiDS1000 with No-Baryons ($100 < \ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

########## lmax = 8192 ##########
## lmin = 100 as in KiDS papers
# Baryons (4096)
key = 'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'KiDS1000 with Baryons ($100 < \ell < 8192$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096)
key = 'k1000_nobaryons_nocuts_nla_nside4096_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'KiDS1000 with No-Baryons ($100 < \ell < 8192$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons + Pixwin (4096)
key = 'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG_pixwin'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'KiDS1000 with Baryons + PixWin ($100 < \ell < 8192$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")


########## Official (2007.15633)
key = 'Asgari+20'
results[key] = load_K1000_official('Asgari et al. 2020', 'black')


###############################################
########### K1000 noHSC ###########
###############################################
########## lmax = 2048 ##########
# No Baryons (4096)
key = 'k1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'K1000 w/o HSC overlap with No-Baryons ($100 < \ell < 2048$)', 'pink', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

########## lmax = 4500 ##########
# No Baryons (4096)
key = 'k1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'K1000 w/o HSC overlap with No-Baryons ($100 < \ell < 4500$)', 'pink', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")


########## lmax = 8192 ##########
# No Baryons (4096)
key = 'k1000_nohsc_nobaryons_nocuts_nla_nside4096_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'K1000 w/o HSC overlap with No-Baryons ($100 < \ell < 8192$)', 'red', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Within baccoemu ranges?

# %% hidden=true
k = "CHAIN"
print(f"{k:<70} Nonlinear emulator   baryon emulator   baryon fraction")
for k in results:
    if k.startswith('k1000'):
        if 'inbounds_baccoemu' in results[k]:
            print (f"{k:<80}{results[k]['inbounds_baccoemu']['nonlinear']:<10.2f}   {results[k]['inbounds_baccoemu']['baryon']:<15.2f}   {results[k]['inbounds_baccoemu']['baryon_fraction']:.2f}")

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Pixwin

# %% [markdown] hidden=true
# 1. Choose theory $C_\ell^{th}$
# 1. Generate 8192 shear maps ($\gamma_1, \gamma_2$)
# 1. Using the galaxies from the DES catalog (RA, DEC, weights) evaluate the field in those positions
# 1. Project into 4096 maps
# 1. Meassure $C_q$ (q = bandpowers)
# 1. Repeat x100
# 1. Estimate $w_q = \sqrt{<C_q>/C_q^{th}}$

# %% hidden=true
# s = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov_GNG.fits')
f, ax = plt.subplots(2, 1, sharex='col', gridspec_kw={'hspace': 0})

ell, cl = s.get_ell_cl('cl_ee', 'KiDS1000__3', 'KiDS1000__3')
sel = ell <= 8192

# pw = {}
for i in range(5):
    key = f'KiDS1000__{i}'
    # pw[key] = np.load(f'/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/k1000_pixwin_zbin{i}.npz')

    for j in range(100):
        ax[0].loglog(ell[sel], pw[key]['cl_realization'][j][0][sel])
    ax[0].loglog(ell[sel], pw[key]['clth_q'][0][sel], color='black')

    err = np.std(pw[key]['cl_realization'][:, 0, sel], axis=0)
    err = 0.5 * err / pw[key]['wl'][0, sel] / pw[key]['clth_q'][0, sel]
    ax[1].errorbar(ell[sel], pw[key]['wl'][0][sel], yerr=err, fmt='.', label=key, alpha=1 - 0.2 * i)
ax[1].axhline(1, ls='--', color='gray')

ax[1].set_ylim([0.97, 1.03])

ax[1].legend()
ax[1].set_xlabel(r'$\ell$')
ax[0].set_ylabel(r'$C_\ell$')
ax[1].set_ylabel(r'$w_\ell = \sqrt{<C_\ell> / C_\ell^{\rm theory}}$')
plt.show()

# %% hidden=true
# s = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov_GNG.fits')
s_pw = s.copy()

# Get the ells
ell, cl = s.get_ell_cl('cl_ee', 'KiDS1000__1', 'KiDS1000__1')

# Let's remove the B-modes to read the file faster
s_pw.keep_selection(data_type='cl_ee')
sel = ell < 2000

mean = []
for tr1, tr2 in s_pw.get_tracer_combinations():
    ell, cl = s.get_ell_cl('cl_ee', tr1, tr2)
    wl2 = pw[tr1]['wl'][0] * pw[tr2]['wl'][0]
    wl2[sel] = 1
    mean.extend(cl / wl2)
s_pw.mean = np.array(mean)

for tr1, tr2 in s_pw.get_tracer_combinations():
    ell, cl = s.get_ell_cl('cl_ee', tr1, tr2)
    ell, cl2 = s_pw.get_ell_cl('cl_ee', tr1, tr2)
    plt.semilogx(ell, cl2/cl)
plt.axvline(8192, ls='--', color='gray')
plt.ylim([0.95, 1.05])
plt.show()
# s_pw.save_fits("/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov_GNG_pixwin.fits")

# %% hidden=true
# s = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov_GNG.fits')
pw2 = np.load('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/k1000_pixwin_zbin2.npz')
pw3 = np.load('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/desy3_pixwin_zbin3.npz')

f, ax = plt.subplots(2, 1, sharex='col', gridspec_kw={'hspace': 0})

ell, cl = s.get_ell_cl('cl_ee', 'KiDS1000__3', 'KiDS1000__3')
sel = ell <= 8192

for i in range(2):
    ax[0].loglog(ell[sel], pw2['cl_realization'][i][0][sel])
ax[0].loglog(ell[sel], pw2['clth_q'][0][sel], color='black')

for i in range(100):
    ax[0].loglog(ell[sel], pw3['cl_realization'][i][0][sel])
ax[0].loglog(ell[sel], pw3['clth_q'][0][sel], color='black')

err = np.std(pw2['cl_realization'][:, 0, sel], axis=0)
err = 0.5 * err / pw2['wl'][0, sel] / pw2['clth_q'][0, sel]
ax[1].errorbar(ell[sel], pw2['wl'][0][sel], yerr=err, fmt='.', label='KiDS 3rd zbin')
ax[1].axhline(1, ls='--', color='gray')

err = np.std(pw3['cl_realization'][:, 0, sel], axis=0)
err = 0.5 * err / pw3['wl'][0, sel] / pw3['clth_q'][0, sel]
ax[1].errorbar(ell[sel], pw3['wl'][0][sel], yerr=err, fmt='.', label='DES 4th zbin')
ax[1].axhline(1, ls='--', color='gray')

ax[1].set_ylim([0.97, 1.03])

ax[1].legend()
ax[1].set_xlabel(r'$\ell$')
ax[0].set_ylabel(r'$C_\ell$')
ax[1].set_ylabel(r'$w_\ell = \sqrt{<C_\ell> / C_\ell^{\rm theory}}$')
plt.show()

# %% hidden=true
s = results['k1000_baryons_nocuts_nla_nside4096_lmin100_GNG']['data_sacc']
s_pw = results['k1000_baryons_nocuts_nla_nside4096_lmin100_GNG_pixwin']['data_sacc']

# Get the ells
ell, cl = s.get_ell_cl('cl_ee', 'KiDS1000__3', 'KiDS1000__3')
    
for tr1, tr2 in s_pw.get_tracer_combinations():
    ell, cl = s.get_ell_cl('cl_ee', tr1, tr2)
    ell, cl2, cov = s_pw.get_ell_cl('cl_ee', tr1, tr2, return_cov=True)
    err = np.sqrt(np.diag(cov))
    plt.semilogx(ell, (cl-cl2)/err)
plt.axvline(8192, ls='--', color='gray')
plt.xlabel(r"$\ell$")
plt.ylabel(r"$(C_\ell - C_\ell^{\rm pixwin}) / \sigma_\ell$")
plt.show()

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG_pixwin',
         ]

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red'] #[results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## HALOFIT vs BACCOEmu

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_halofit_GNG',
          'Asgari+20'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = None #[results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no Baryons ($\ell_{\rm max} = 1000$)

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG'
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no Baryons ($\ell_{\rm max} = 2048$ with $\ell_{\rm min} = 300$ as HSC)

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin300_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300_GNG'
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no Baryons ($\ell_{\rm max} = 2048$)

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG'          
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
labels = ['No baryons', 'Baryons']
colors = ['gray', 'black']

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG'
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no Baryons ($\ell_{\rm max} = 4500$)

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG'
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors[0] = 'gray'

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no Baryons ($\ell_{\rm max} = 8192$)

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmin100_GNG'
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## $\ell_{\rm max} = 2048$ vs $\ell_{\rm max} = 8192$

# %% hidden=true
print('ell_max = 2048; chi2 =', results['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG']['chi2_bf'],
      ' || ndata = ', results['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG']['ndata'], 
      ' || p-value = ', results['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG']['pvalue_bf'])
print('ell_max = 8196; chi2 =', results['k1000_baryons_nocuts_nla_nside4096_lmin100_GNG']['chi2_bf'],
      ' || ndata = ', results['k1000_baryons_nocuts_nla_nside4096_lmin100_GNG']['ndata'],
      ' || p-value = ', results['k1000_baryons_nocuts_nla_nside4096_lmin100_GNG']['pvalue_bf'])

# %% hidden=true
ndata = results['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG']['ndata']
print(f"Values with data up to ell = 2048. This makes {ndata} data points")
print('chi2(ell_max=2048 with BF with ell_max = 2048) =',
      results['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG']['chi2_bf'],
      'p-value = ', results['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG']['pvalue_bf'])

chi2 = evaluate_model(results['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG']['model'],
                     results['k1000_baryons_nocuts_nla_nside4096_lmin100_GNG']['pars_bf'])[0]
p = 1 - stats.chi2.cdf(chi2, df=ndata - 2)
print('chi2(ell_max=2048 with BF with ell_max = 8192) =', chi2,'p-value = ', p)

print()
print()
ndata = results['k1000_baryons_nocuts_nla_nside4096_lmin100_GNG']['ndata']
print(f"Values with data up to ell = 8192. This makes {ndata} data points")
chi2 = evaluate_model(results['k1000_baryons_nocuts_nla_nside4096_lmin100_GNG']['model'],
                     results['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG']['pars_bf'])[0]
p = 1 - stats.chi2.cdf(chi2, df=ndata - 2)
print('chi2(ell_max=8192 with BF with ell_max = 2048) =', chi2, 'p-value = ', p)
print('chi2(ell_max=8192 with BF with ell_max = 8192) =',
      results['k1000_baryons_nocuts_nla_nside4096_lmin100_GNG']['chi2_bf'],
      'p-value = ', results['k1000_baryons_nocuts_nla_nside4096_lmin100_GNG']['pvalue_bf'])

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG']

parameters = ['S8', 'sigma8', 'Omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['black', 'gray', 'blue', 'red'] #[results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=8192
chains = ['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG']


parameters = ['S8', 'sigma8', 'Omega_m', 'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['gray', 'black', 'blue', 'red'] #[results[i]['color'] for i in chains]

print(results['k1000_baryons_nocuts_nla_nside4096_lmin100_GNG']['MCSamples'].getInlineLatex('M_c'))

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
lkl1024 = results['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG']['model'].likelihood['cl_like.ClLike']
lkl4096 = results['k1000_baryons_nocuts_nla_nside4096_lmin100_GNG']['model'].likelihood['cl_like.ClLike']
s = lkl4096.get_cl_data_sacc()

m = results['k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG']['model']
bf = results['k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG']['pars_bf']
cl1024 = get_cl_theory_sacc(m, bf)

m = results['k1000_nobaryons_nocuts_nla_nside4096_lmin100_GNG']['model']
bf = results['k1000_nobaryons_nocuts_nla_nside4096_lmin100_GNG']['pars_bf']
cl4096 = get_cl_theory_sacc(m, bf)

# E-modes
f, ax = plt.subplots(5, 5, figsize=(14, 14), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in s.get_tracer_combinations():
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])
    
    # Data
    ell, cl, cov, ind = s.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', alpha=0.8)
    
    # Data official
    ell, cl, cov, ind = sOfficial.get_ell_cl('cl_ee', trs[1], trs[0], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, cl/ell * 1e7, yerr=err/ell * 1e7, fmt='.', color='black', label='Official data')    

    # BF lmax=2048
    ell, cl = cl1024.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].plot(ell, ell * cl * 1e7, color='cyan', label='BF $100 < \ell < 2048$', alpha=0.8)
    
    
    # BF lmax=8192
    ell, cl = cl4096.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].plot(ell, ell * cl * 1e7, color='darkorange', label=r'BF $100 < \ell < 8192$', alpha=0.8)

#     # Scale cuts line
#     ax[i, j].axvline(2048, color='gray', ls='--')
#     ax[i, j].axvline(8192, color='black', ls='--')

#     ax[i, j].text(0.99, 0.99, f"{i}-{j}", horizontalalignment='right',
#      verticalalignment='top', transform=ax[i,j].transAxes, fontsize=14)
#     ax[i, j].axhline(0, ls='--', color='gray')
    
for i in range(5):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")
    ax[0, 0].legend()

f.suptitle('EE', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryon constraints with different lmax

# %% hidden=true
chains = ['k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
         ]
for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('M_c'))

# %% hidden=true
chains = ['k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
         ]

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
chains = ['k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
         ]

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Comparison with official results

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          # 'k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG',
          # 'k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'Asgari+20',
         ]

parameters = ['Omega_m', 'S8', 'sigma8',]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = None # [results[i]['color'] for i in chains]
# colors[0] = 'gray'

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          # 'k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG',
          # 'k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'Asgari+20',
         ]

parameters = ['Omega_m', 'S8', 'sigma8',]
parameters += [f'limber_KiDS1000__{i}_dz' for i in range(5)]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = None # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], marker={f'limber_KiDS1000__{i}_dz': 0 for i in range(5)})

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## KiDS-1000 vs KiDS-1000 w/o HSC overlapping area

# %% hidden=true
m = hp.read_map("/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__0_noHSCDR1wl_coordC_ns4096.fits.gz")

# # For the transaprency to work you need to comment healpy/projaxes.py#L189
# # See https://github.com/healpy/healpy/issues/765
# m = scale_bin_map(m, 1)
cbound = {'min': 0, 
          'max': 1}

hp.mollview(m, badcolor='lightgray', cbar=False, title='', **cbound)
hp.graticule()

plt.text(1.8, 0.1, 'KiDS-1000', fontsize=15, color='black', horizontalalignment='right')

# plt.savefig('')
plt.show()
plt.close()

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### $\ell_{\max} = 2048$

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['k1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG'
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors[0] = 'gray'

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### $\ell_{\max} = 4500$

# %% hidden=true
for c in chains:
    print(c, f"ndof = {results[c]['ndata']}", f"p-value = {results[c]['pvalue_bf']}")


# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['k1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG'
         ]

parameters = ['Omega_m', 'sigma8', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors[0] = 'gray'

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
chains = ['k1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG'
         ]

sdata_nohsc = results[chains[0]].get_cl_data_sacc()
sdata_full = results[chains[1]].get_cl_data_sacc()

plt.plot(np.sqrt(np.diag(sdata_nohsc.covariance.covmat) / np.diag(sdata_full.covariance.covmat)))
plt.ylabel(r"$\sigma_\ell^{\rm no HSC} / \sigma_\ell^{\rm Full}$")
plt.xlabel('Data element')
plt.title('KiDS-1000')
plt.show()

# %% hidden=true
chains = ['k1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG'
         ]
survey = 'KiDS'

sdata_nohsc = results[chains[0]].get_cl_data_sacc()
sdata_full = results[chains[1]].get_cl_data_sacc()

nbin = 5
f, ax = plt.subplots(nbin, nbin, figsize=(12, 10), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in sdata_full.get_tracer_combinations():
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])

    # Data
    ell, cl, cov, ind = sdata_full.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Full', alpha=0.8)    

    # Data
    ell, cl, cov, ind = sdata_nohsc.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='gray', label='No HSC', alpha=0.8)    

for i in range(nbin):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")
    ax[0, 0].legend()

f.suptitle(f'{survey}', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% hidden=true
chains = ['k1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG'
         ]
survey = 'KiDS'

sdata_nohsc = results[chains[0]].get_cl_data_sacc()
sdata_full = results[chains[1]].get_cl_data_sacc()

nbin = 5
f, ax = plt.subplots(nbin, nbin, figsize=(12, 10), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in sdata_full.get_tracer_combinations():
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])

    # Data
    ell, cl, cov, ind = sdata_full.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))

    ell, cl2, cov, ind = sdata_nohsc.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err2 = np.sqrt(np.diag(cov))
    
    ax[i, j].errorbar(ell, (cl - cl2) / np.sqrt(err**2 + err2**2), yerr=np.ones_like(ell), fmt='.', color='k', alpha=0.8)
    ax[i, j].axhline(0, ls='--', color='gray')

for i in range(nbin):
    ax[i, 0].set_ylabel(r"$\Delta C_\ell / \sqrt{\sigma^{\rm Full, 2}_\ell + \sigma^{2, \rm no HSC}_\ell}$")
    ax[-1, i].set_xlabel(r"$\ell$")
    ax[0, i].set_xscale("log")
    # ax[0, 0].legend()

f.suptitle(f'{survey}', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### $\ell_{\max} = 8192$

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['k1000_nohsc_nobaryons_nocuts_nla_nside4096_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmin100_GNG',
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors[0] = 'gray'

for c in chains:
    print(c, f"ndof = {results[c]['ndata']}", f"p-value = {results[c]['pvalue_bf']}")



g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true toc-hr-collapsed=true
# # HSCDR1

# %% hidden=true
#################################
########### KiDS10000 ###########
#################################

def load_hsc_official(label, color):
    # For v3
    pnames = ['omega_b', 'omega_cdm', 'n_s', 'h', 'ln10^{10}A_s',
              'clk_HSC_A_IA', 'clk_HSC_eta_IA', 'm_corr', 'a_psf', 'b_psf',
              'clk_HSC__0_dz100', 'clk_HSC__1_dz100', 'clk_HSC__2_dz100', 'clk_HSC__3_dz100',
              'Omega_m', 'sigma_8'
              ]

    labels = ['\omega_b', '\omega_{cdm}', 'n_s', 'h', 'ln10^{10}A_s', 
              'A_{IA}', '\eta_{IA}', 'm_corr', 'a_{psf}', 'b_{psf}',
              '100HSC_{0 z}', '100HSC_{1 z}', '100HSC_{2 z}', '100HSC_{3 z}',
              '\Omega_m', '\sigma_8'
              ]

    fname = '/mnt/extraspace/gravityls_3/data/HSC_DR1/HSC_Y1_LCDM_post_fid.txt'
    lchains = np.loadtxt(fname, unpack=True)
    steps, lkl, w = lchains[2:], lchains[1], lchains[0]
    # Convention
    # steps[15:24] *= -1
    d_official = getdist.mcsamples.MCSamples(samples=steps.T, loglikes=lkl, weights=w,
                                                names=pnames, labels=labels, sampler='nested')
    p = d_official.getParams()
    d_official.addDerived(p.sigma_8 * np.sqrt(p.Omega_m / 0.3), 'S8', 'S_8')
    d_official.addDerived(p.omega_cdm / p.h**2, 'Omega_c', '\Omega_c')
    d_official.addDerived(p.omega_b / p.h**2, 'Omega_b', '\Omega_b')
    d_official.addDerived(p.sigma_8, 'sigma8', '\sigma_8')
    
    # for i in range(4):
    #     d_official.addDerived(eval(f'p.clk_HSC__{i}_dz100') * 100, f'clk_wl__{i}_dz', 
    #                           'HSC_{%d \Delat z}' % i)
    for i in range(4):
        d_official.addDerived(eval(f'p.clk_HSC__{i}_dz100') / 100, f'limber_wl_{i}_dz', 
                              '\Delta z_{HSC_{%d}}' % i)
        d_official.addDerived(eval(f'p.clk_HSC__{i}_dz100') / 100, f'limber_HSCDR1wl__{i}_dz', 
                          '\Delta z_{HSC_{%d}}' % i)

    d = {'MCSamples': d_official,
         'label': label,
         'color': color,
         'path': fname, 
         'R-1': None}
    
    return d

def translate_hsc():
    for key in results.keys():
        if not key.startswith('hsc_'):
            continue
        s = results[key]['MCSamples']
        p = s.getParams()
        pn = s.getParamNames()
        pnames = pn.list()
        
        for pni in pnames:
            t = pni.split('_')
            if 'wl' not in pni or t[1] != 'wl':
                continue
            tnew = '_'.join([t[0], 'HSCDR1wl__' + t[2], t[3]])
            
            if tnew in pnames:
                continue
                        
            s.addDerived(eval(f"p.{pni}"), tnew, pn.parWithName(pni).label)
            
########## lmax = 1000 ##########
# lmin = 300 as in HSC papers
# Baryons (4096)
key = 'hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin300'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'HSCDR1wl with Baryons ($300 < \ell < 1000$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096)
key = 'hsc_nobaryons_nocuts_nla_nside4096_lmax1000_lmin300'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'HSCDR1wl with No-Baryons ($300 < \ell < 1000$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")


########## lmax = 2048 ##########
## lmin = 300 as in HSC papers
# Baryons (4096)
key = 'hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'HSCDR1wl with Baryons ($300 < \ell < 2048$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096)
key = 'hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'HSCDR1wl with No-Baryons ($300 < \ell < 2048$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096)
# key = 'hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300_halofit'
# results[key] = load_chain(f'./chains/{key}/{key}',
#                            r'HSCDR1wl with No-Baryons (1024, $300 < \ell < 2048$) Halofit', 'black', 0.15)
# print(key, f"R-1 = {results[key]['R-1']}")

########## lmax = 4500 ##########
## lmin = 300 as in HSC papers
# Baryons (4096)
key = 'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'HSCDR1wl with Baryons ($300 < \ell < 4500$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096)
key = 'hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin300'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'HSCDR1wl with No-Baryons ($300 < \ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

########## lmax = 8192 ##########
## lmin = 300 as in HSC papers
# Baryons (4096)
key = 'hsc_baryons_nocuts_nla_nside4096_lmin300'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'HSCDR1wl with Baryons ($300 < \ell < 8192$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096)
key = 'hsc_nobaryons_nocuts_nla_nside4096_lmin300'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'HSCDR1wl with No-Baryons ($300 < \ell < 8192$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")


# Translate HSC wl -> HSCDR1wl (before loading the old ones)
translate_hsc()

########## DES+HSC paper (Garcia-Garcia et al 2023)
key = 'hsc_gg2023'
results[key] = load_chain(f'../../DESxHSC/mcmc/chains/hsc_dz/hsc_dz', 'HSC (Garcia-Garcia et al 2023)', 'gray', 0.15, baccoemu=False)
s = results[key]['MCSamples']
p = s.getParams()
for i in range(4):
    for parname in [f"limber_HSCDR1wl__{i}_dz", f'limber_wl_{i}_dz']:
        s.addDerived(eval(f"p.clk_HSC__{i}_dz"), parname, r"\Delta z^{HSC_{%d}" % i)
    
########## Official ()
key = 'Hikage+2018'
results[key] = load_hsc_official('HSC Hikage et al. 2018', 'black')

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Within baccoemu ranges?

# %% hidden=true
k = "CHAIN"
print(f"{k:<70} Nonlinear emulator   baryon emulator   baryon fraction")
for k in results:
    if k.startswith('hsc'):
        if 'inbounds_baccoemu' in results[k]:
            print (f"{k:<80}{results[k]['inbounds_baccoemu']['nonlinear']:<10.2f}   {results[k]['inbounds_baccoemu']['baryon']:<15.2f}   {results[k]['inbounds_baccoemu']['baryon_fraction']:.2f}")

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no Baryons ($\ell_{\rm max} = 1000$)

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin300',
          'hsc_nobaryons_nocuts_nla_nside4096_lmax1000_lmin300'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

for i in chains:
    print(i, results[i]['R-1'])
    
g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no Baryons ($\ell_{\rm max} = 2048$)

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
labels = ['No baryons', 'Baryons']
colors = ['gray', 'black']

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

for i in chains:
    print(i, results[i]['R-1'])
    
g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
lkl1024 = results['hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300']['model'].likelihood['cl_like.ClLike']
lkl4096 = results['hsc_baryons_nocuts_nla_nside4096_lmin300']['model'].likelihood['cl_like.ClLike']
s = lkl4096.get_cl_data_sacc()

m = results['hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300']['model']
bf = results['hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300']['pars_bf']
cl1024 = get_cl_theory_sacc(m, bf)

m = results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['model']
bf = results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['pars_bf']
cl4096 = get_cl_theory_sacc(m, bf)

# E-modes
f, ax = plt.subplots(4, 4, figsize=(14, 14), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in s.get_tracer_combinations():
    i = int(trs[1].split('_')[1])
    j = int(trs[0].split('_')[1])
    
    # Data
    ell, cl, cov, ind = s.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', alpha=0.8)
    
    # Data official
    ell, cl, cov, ind = sOfficial.get_ell_cl('cl_ee', trs[1], trs[0], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, cl/ell * 1e7, yerr=err/ell * 1e7, fmt='.', color='black', label='Official data')    

    # BF lmax=2048
    ell, cl = cl1024.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].plot(ell, ell * cl * 1e7, color='cyan', label='BF $100 < \ell < 2048$ (No baryons)', alpha=0.8)
    
    
    # BF lmax=8192
    ell, cl = cl4096.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].plot(ell, ell * cl * 1e7, color='darkorange', label=r'BF $100 < \ell < 2048$ (Baryons)', alpha=0.8)

#     # Scale cuts line
#     ax[i, j].axvline(2048, color='gray', ls='--')
#     ax[i, j].axvline(8192, color='black', ls='--')

#     ax[i, j].text(0.99, 0.99, f"{i}-{j}", horizontalalignment='right',
#      verticalalignment='top', transform=ax[i,j].transAxes, fontsize=14)
#     ax[i, j].axhline(0, ls='--', color='gray')
    
for i in range(4):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")
    ax[0, 0].legend()

f.suptitle('EE', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no Baryons ($\ell_{\rm max} = 4500$)

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin300'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

for i in chains:
    print(i, results[i]['R-1'])
    
g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no Baryons ($\ell_{\rm max} = 8192$)

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['hsc_baryons_nocuts_nla_nside4096_lmin300',
          'hsc_nobaryons_nocuts_nla_nside4096_lmin300'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

for i in chains:
    print(i, results[i]['R-1'])
    
g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
lkl1024 = results['hsc_nobaryons_nocuts_nla_nside4096_lmin300']['model'].likelihood['cl_like.ClLike']
lkl4096 = results['hsc_baryons_nocuts_nla_nside4096_lmin300']['model'].likelihood['cl_like.ClLike']
s = lkl4096.get_cl_data_sacc()

m = results['hsc_nobaryons_nocuts_nla_nside4096_lmin300']['model']
bf = results['hsc_nobaryons_nocuts_nla_nside4096_lmin300']['pars_bf']
cl1024 = get_cl_theory_sacc(m, bf)

m = results['hsc_baryons_nocuts_nla_nside4096_lmin300']['model']
bf = results['hsc_baryons_nocuts_nla_nside4096_lmin300']['pars_bf']
cl4096 = get_cl_theory_sacc(m, bf)

# E-modes
f, ax = plt.subplots(4, 4, figsize=(14, 14), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in s.get_tracer_combinations():
    i = int(trs[1].split('_')[1])
    j = int(trs[0].split('_')[1])
    
    # Data
    ell, cl, cov, ind = s.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', alpha=0.8)
    
    # Data official
    ell, cl, cov, ind = sOfficial.get_ell_cl('cl_ee', trs[1], trs[0], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, cl/ell * 1e7, yerr=err/ell * 1e7, fmt='.', color='black', label='Official data')    

    # BF lmax=2048
    ell, cl = cl1024.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].plot(ell, ell * cl * 1e7, color='cyan', label='BF $100 < \ell < 8192$ (No baryons)', alpha=0.8)
    
    
    # BF lmax=8192
    ell, cl = cl4096.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].plot(ell, ell * cl * 1e7, color='darkorange', label=r'BF $100 < \ell < 8192$ (Baryons)', alpha=0.8)

#     # Scale cuts line
#     ax[i, j].axvline(2048, color='gray', ls='--')
#     ax[i, j].axvline(8192, color='black', ls='--')

#     ax[i, j].text(0.99, 0.99, f"{i}-{j}", horizontalalignment='right',
#      verticalalignment='top', transform=ax[i,j].transAxes, fontsize=14)
#     ax[i, j].axhline(0, ls='--', color='gray')
    
for i in range(4):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")
    ax[0, 0].legend()

f.suptitle('EE', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## $\ell_{\rm max} = 2048$ vs $\ell_{\rm max} = 8192$

# %% hidden=true
print('ell_max = 2048; chi2 =', results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['chi2_bf'],
      ' || ndata = ', results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['ndata'], 
      ' || p-value = ', results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['pvalue_bf'])
print('ell_max = 8196; chi2 =', results['hsc_baryons_nocuts_nla_nside4096_lmin300']['chi2_bf'],
      ' || ndata = ', results['hsc_baryons_nocuts_nla_nside4096_lmin300']['ndata'],
      ' || p-value = ', results['hsc_baryons_nocuts_nla_nside4096_lmin300']['pvalue_bf'])

# %% hidden=true
ndata = results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['ndata']
print(f"Values with data up to ell = 2048. This makes {ndata} data points")
print('chi2(ell_max=2048 with BF with ell_max = 2048) =',
      results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['chi2_bf'],
      'p-value = ', results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['pvalue_bf'])

chi2 = evaluate_model(results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['model'],
                     results['hsc_baryons_nocuts_nla_nside4096_lmin300']['pars_bf'])[0]
p = 1 - stats.chi2.cdf(chi2, df=ndata - 2)
print('chi2(ell_max=2048 with BF with ell_max = 8192) =', chi2,'p-value = ', p)

print()
print()
ndata = results['hsc_baryons_nocuts_nla_nside4096_lmin300']['ndata']
print(f"Values with data up to ell = 8192. This makes {ndata} data points")
chi2 = evaluate_model(results['hsc_baryons_nocuts_nla_nside4096_lmin300']['model'],
                     results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['pars_bf'])[0]
p = 1 - stats.chi2.cdf(chi2, df=ndata - 2)
print('chi2(ell_max=8192 with BF with ell_max = 2048) =', chi2, 'p-value = ', p)
print('chi2(ell_max=8192 with BF with ell_max = 8192) =',
      results['hsc_baryons_nocuts_nla_nside4096_lmin300']['chi2_bf'],
      'p-value = ', results['hsc_baryons_nocuts_nla_nside4096_lmin300']['pvalue_bf'])

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['hsc_baryons_nocuts_nla_nside4096_lmin300',
          'hsc_nobaryons_nocuts_nla_nside4096_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = None # [results[i]['color'] for i in chains]

for i in chains:
    print(i, results[i]['R-1'])

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
lkl1024 = results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['model'].likelihood['cl_like.ClLike']
lkl4096 = results['hsc_baryons_nocuts_nla_nside4096_lmin300']['model'].likelihood['cl_like.ClLike']
s = lkl4096.get_cl_data_sacc()

m = results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['model']
bf = results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['pars_bf']
cl1024 = get_cl_theory_sacc(m, bf)

m = results['hsc_baryons_nocuts_nla_nside4096_lmin300']['model']
bf = results['hsc_baryons_nocuts_nla_nside4096_lmin300']['pars_bf']
cl4096 = get_cl_theory_sacc(m, bf)

# E-modes
f, ax = plt.subplots(4, 4, figsize=(14, 14), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in s.get_tracer_combinations():
    i = int(trs[1].split('_')[1])
    j = int(trs[0].split('_')[1])
    
    # Data
    ell, cl, cov, ind = s.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', alpha=0.8)
    
    # Data official
    ell, cl, cov, ind = sOfficial.get_ell_cl('cl_ee', trs[1], trs[0], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, cl/ell * 1e7, yerr=err/ell * 1e7, fmt='.', color='black', label='Official data')    

    # BF lmax=2048
    ell, cl = cl1024.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].plot(ell, ell * cl * 1e7, color='cyan', label='BF $100 < \ell < 2048$', alpha=0.8)
    
    
    # BF lmax=8192
    ell, cl = cl4096.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].plot(ell, ell * cl * 1e7, color='darkorange', label=r'BF $100 < \ell < 8192$', alpha=0.8)

#     # Scale cuts line
#     ax[i, j].axvline(2048, color='gray', ls='--')
#     ax[i, j].axvline(8192, color='black', ls='--')

#     ax[i, j].text(0.99, 0.99, f"{i}-{j}", horizontalalignment='right',
#      verticalalignment='top', transform=ax[i,j].transAxes, fontsize=14)
#     ax[i, j].axhline(0, ls='--', color='gray')
    
for i in range(4):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")
    ax[0, 0].legend()

f.suptitle('EE', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% [markdown] heading_collapsed=true hidden=true
# ## Baryons constraints with different lmax

# %% hidden=true
chains = ['hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmin300'
         ]
for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('M_c'))

# %% hidden=true
chains = ['hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmin300'
         ]

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
chains = ['hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmin300'
         ]

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
chains = ['hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmin300'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'n_s', 'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Comparison with official or previous results [Hikage uses different priors. It'd be better to compare with Andrina's or the DES+HSC paper]

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'Hikage+2018'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = None # [results[i]['color'] for i in chains]

for i in chains:
    print(i, results[i]['R-1'])

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'Hikage+2018'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']
parameters += [f'limber_wl_{i}_dz' for i in range(4)]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange'] # [results[i]['color'] for i in chains]

for i in chains:
    print(i, results[i]['R-1'])

g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'hsc_gg2023',
          # 'Hikage+2018',
          'priors'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']
parameters += [f'limber_wl_{i}_dz' for i in range(4)]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange', 'gray', 'purple'] # [results[i]['color'] for i in chains]

# for i in chains:
#     print(i, results[i]['R-1'])

g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 2])


# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true toc-hr-collapsed=true
# # DES+KiDS1000

# %% hidden=true
def load_DESY3_K1000(label, color):
    # Result of the DES+KiDS official analysis: https://arxiv.org/pdf/2305.17173.pdf
    fname = '/mnt/extraspace/gravityls_3/data/DESY3wl_K1000/chain_desy3_and_kids1000_hybrid_analysis.txt'
    # with open(fname) as f:
    #     for i, p in enumerate(f.readline().split()):
    #         print(i, p)
    
    pnames_dict = {
        0: "limber_DESY3__0_dz",
        1: "limber_DESY3__1_dz",
        2: "limber_DESY3__2_dz",
        3: "limber_DESY3__3_dz",
        # 4: "limber_KiDS1000__0_dz",
        # 5: "limber_KiDS1000__1_dz",
        # 6: "limber_KiDS1000__2_dz",
        # 7: "limber_KiDS1000__3_dz",
        # 8: "limber_KiDS1000__4_dz",
        9: "bias_DESY3__0_m",
        10: "bias_DESY3__1_m",
        11: "bias_DESY3__2_m",
        12: "bias_DESY3__3_m",
        13: "bias_DESY3_A_IA",
        14: 'limber_DESY3_eta_IA',
        15: 'bias_KiDS1000_A_IA',
        16: 'limber_KiDS1000_eta_IA',
        17: 'HMCode_logT_AGN',
        18: 'omega_c',
        19: 'omega_b',
        20: 'h',
        21: 'n_s',
        22: 'S8_input',
        23: 'm_nu',
        24: 'S8',
        25: 'sigma8',
        26: 'A_s',
        27: 'Omega_m',
        35: "limber_KiDS1000__0_dz",
        36: "limber_KiDS1000__1_dz",
        37: "limber_KiDS1000__2_dz",
        38: "limber_KiDS1000__3_dz",
        39: "limber_KiDS1000__4_dz",
        # 40: "limber_DESY3__0_dz",
        # 41: "limber_DESY3__1_dz",
        # 42: "limber_DESY3__2_dz",
        # 43: "limber_DESY3__3_dz",
        
    }
    # 49: post
    # 50: weight
    ix = list(pnames_dict.keys()) + [49, 50]
    pnames = list(pnames_dict.values())
    labels = [
              '\Delta z^{DESY3_0}', '\Delta z^{DESY3_1}', '\Delta z^{DESY3_2}', '\Delta z^{DESY3_3}',
              # '\Delta z^{K1000_0}', '\Delta z^{K1000_1}', '\Delta z^{K1000_2}', '\Delta z^{K1000_3}', '\Delta z^{K1000_4}',
              'm^{DESY3_0}', 'm^{DESY3_0}', 'm^{DESY3_0}', 'm^{DESY3_0}',
              'A^{DESY3}_{IA}', '\eta^{DESY3}_{IA}',
              'A^{K1000}_{IA}', '\eta^{K1000}_{IA}',
              r'\log(T_{\rm AGN})',
              '\omega_c', '\omega_b', 'h', 'n_s', r'S_8^{\rm input}', 'M_\nu',
              'S_8', '\sigma_8', 'A_s','\Omega_m',
              '\Delta z^{K1000_0}', '\Delta z^{K1000_1}', '\Delta z^{K1000_2}', '\Delta z^{K1000_3}', '\Delta z^{K1000_4}',
              # '\Delta z^{DESY3_0}', '\Delta z^{DESY3_1}', '\Delta z^{DESY3_2}', '\Delta z^{DESY3_3}',

    ]

    
    lchains = np.loadtxt(fname, usecols=ix, unpack=True)
    steps, lkl, w = lchains[:-2], lchains[-2], lchains[-1]
    # Convention
    # steps[15:24] *= -1
    d_official = getdist.mcsamples.MCSamples(samples=steps.T, loglikes=lkl, weights=w,
                                             names=pnames, labels=labels, sampler='nested')
    p = d_official.getParams()
    Omega_nu = p.m_nu/(93.14*p.h**2)
    d_official.addDerived(Omega_nu, 'Omega_nu', '\Omega_\nu')
    d_official.addDerived(p.omega_c / p.h**2, 'Omega_c', '\Omega_c')
    d_official.addDerived(p.omega_b / p.h**2, 'Omega_b', '\Omega_b')
    d_official.addDerived(p.A_s*1e9, 'A_sE9', 'A_s 10^{9}')

    d = {'MCSamples': d_official,
         'label': label,
         'color': color,
         'path': fname}

    return d

########## lmax=1000 ##########
# No Baryons (4096) lmax=1000
key = 'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 with No-Baryons ($\ell < 1000$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=1000
key = 'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 with Baryons ($\ell < 1000$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# # Baryons (4096) lmax=1000 (Hybrid priors)
# key = 'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG_hybridpriors'
# results[key] = load_chain(f'./chains/{key}/{key}',
#                            r'DESY3 + KiDS1000 with Baryons + Hybrid priors ($\ell < 1000$, $\ell_{\rm min} = 20 (DES), 100 (KiDS)$)', 'black', 0.15)
# print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=1000 (Hybrid priors + HMCode2020)
key = 'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG_hybridpriors_hmcode2020'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 with Baryons ($T_{\rm AGN}$) + Hybrid priors + HMCode2020 ($\ell < 1000$)',
                          'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")


########## lmax=2048 ##########
# No Baryons (4096) lmax=2048
key = 'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 with No-Baryons ($\ell < 2048$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=2048
key = 'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 with Baryons ($\ell < 2048$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

########## lmax=4500 ##########
# No Baryons (4096) lmax=4500
key = 'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 with No-Baryons ($\ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500
key = 'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 with Baryons ($\ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

########## lmax=8129 ##########
# No Baryons (4096) lmax=8192
key = 'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 with No-Baryons ($\ell < 8192$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=8192
key = 'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 with Baryons ($\ell < 8192$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmin=2048 lmax=8192
key = 'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin2048_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 with Baryons ($2048 < \ell < 8192$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

####################################
###### Official result (2305.17173)
key = 'Abbot+23'
results[key] = load_DESY3_K1000('DES+KiDS (Abbot et al 2023)', 'black')

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## Within baccoemu ranges?

# %% hidden=true
k = "CHAIN"
print(f"{k:<70} Nonlinear emulator   baryon emulator   baryon fraction")
for k in results:
    if k.startswith('desy3wl_k1000'):
        if 'inbounds_baccoemu' in results[k]:
            print (f"{k:<80}{results[k]['inbounds_baccoemu']['nonlinear']:<10.2f}   {results[k]['inbounds_baccoemu']['baryon']:<15.2f}   {results[k]['inbounds_baccoemu']['baryon_fraction']:.2f}")

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## Map

# %% hidden=true
desy3mask = hp.read_map("/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY30_noK1000_coordC_ns4096.fits.gz")
k1000mask = hp.read_map("/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__0_coordC_ns4096.fits.gz")

# For the transaprency to work you need to comment healpy/projaxes.py#L189
# See https://github.com/healpy/healpy/issues/765
k1000mask = scale_bin_map(k1000mask, 1)
desy3mask = scale_bin_map(desy3mask, 2)
cbound = {'min': 1, 
          'max': 2}

hp.mollview(k1000mask, badcolor='lightgray', cbar=False, title='', **cbound)
hp.mollview(desy3mask, badcolor='none', reuse_axes=True, alpha=0.9 * np.ones_like(desy3mask), cbar=False, title='', **cbound)
hp.graticule()

plt.text(1.8, 0.1, 'KiDS-1000', fontsize=15, color='black', horizontalalignment='right')
plt.text(-1.25, -0.5, 'DESY3', fontsize=15, color='black', horizontalalignment='left')

# plt.savefig('')
plt.show()
plt.close()

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs No-Baryons ($\ell_{\rm max} = 1000$)

# %% hidden=true
# No Baryons with ell_max=1000
chains = ['desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs No-Baryons ($\ell_{\rm max} = 2048$)

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
labels = ['No baryons', 'Baryons']
colors = ['gray', 'black']

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs No-Baryons ($\ell_{\rm max} = 4500$)

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs No-Baryons ($\ell_{\rm max} = 8192$)

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### Check $C_\ell$ Why is $\Omega_m$ so tight?

# %% hidden=true
p = results[chains[0]]['pars_bf']
print(p['sigma8'])
Om = 0.33
sigma8 = p['S8']/np.sqrt(Om/0.3)
Onu = p['m_nu']/(93.14*p['h']**2)
Ob = p['Omega_b']
Oc = Om-Ob-Onu
As = 1.508E-9
cosmo = ccl.Cosmology(Omega_c=Oc, Omega_b=Ob, h=p['h'], n_s=p['n_s'], m_nu=p['m_nu'], A_s=As)
print(cosmo.sigma8(), sigma8)

# %% hidden=true
results['desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG']['pvalue_bf']

# %% hidden=true
chains = ['desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG'
         ]

sdata = results[chains[0]].get_cl_data_sacc()
labels = ['DES+KiDS BF', '$\Omega_m = 0.33$']

for survey in ['DES', 'KiDS']:
    nbin = 5 if survey == 'KiDS' else 4
    f, ax = plt.subplots(nbin, nbin, figsize=(14, 14), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
    for trs in sdata.get_tracer_combinations():
        i = int(trs[1].split('__')[1])
        j = int(trs[0].split('__')[1])
        
        if survey not in trs[0]:
            continue

        # Data
        ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
        err = np.sqrt(np.diag(cov))
        ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8)    

        # BF
        s = results[chains[0]]['bf_sacc']
        ell, cl = s.get_ell_cl('cl_ee', trs[0], trs[1])
        ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='x', color='blue', label=labels[0], alpha=0.8)  

        # BF
        pars = results[chains[0]]['pars_bf'].copy()
        pars['Omega_m'] = Om
        pars['A_sE9'] = As*1E9
        s = results[chains[0]].get_cl_theory_sacc(pars)
        ell, cl = s.get_ell_cl('cl_ee', trs[0], trs[1])
        ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='red', label=Om, alpha=0.8)        

    for i in range(nbin):
        ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
        ax[-1, i].set_xlabel("$\ell$")
        ax[0, i].set_xscale("log")
        ax[0, 0].legend()

    f.suptitle(f'{survey}', fontsize=16)
    plt.tight_layout()
    plt.show()
    plt.close()

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## $\ell_{max} = 2048$ vs $\ell_{min} = 2048$

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin2048_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['red', 'blue', 'black'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons constraints at different lmax

# %% hidden=true
chains = ['desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG'
         ]
for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('M_c'))

# %% hidden=true
chains = ['desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG',
          # 'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          # 'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'green', 'black']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
chains = ['desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
chains = ['desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'n_s', 'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
chains = ['desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## Comparison with DES and KiDS alone ($\ell_{\rm max} = 1000$)

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c']

for c in chains:
    print(c)
    for p in parameters:
        print(results[c]['MCSamples'].getInlineLatex(p))
    print('####')
    print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = None #[results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], title_limit=1)

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## Comparison with DES and KiDS alone ($\ell_{\rm max} = 2048$)

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c']

for c in chains:
    print(c)
    for p in parameters:
        print(results[c]['MCSamples'].getInlineLatex(p))
    print('####')
    print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = None #[results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], title_limit=1)

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## Comparison with DES and KiDS alone ($\ell_{\rm max} = 4500$)

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c']

for c in chains:
    print(c)
    for p in parameters:
        print(results[c]['MCSamples'].getInlineLatex(p))
    print('####')
    print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = None #[results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], title_limit=1)

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## Comparison with DES and KiDS alone ($\ell_{\rm max} = 8192$)

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c']

for c in chains:
    print(c)
    for p in parameters:
        print(results[c]['MCSamples'].getInlineLatex(p))
    print('####')
    print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = None #[results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], title_limit=1)

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## vs DES + KiDS paper

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'Abbot+23'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'S8_input']

for c in chains:
    print(c)
    for p in parameters:
        if p == 'S8_input' and c != 'Abbot+23':
            continue
        print(results[c]['MCSamples'].getInlineLatex(p))
    print('####')
    print()

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['Abbot+23',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG_hybridpriors_hmcode2020',
          
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'black', 'red']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['Abbot+23',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG_hybridpriors_hmcode2020'
         ]

parameters = ['bias_DESY3_A_IA', 'limber_DESY3_eta_IA', 'bias_KiDS1000_A_IA', 'limber_KiDS1000_eta_IA']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'black', 'red']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], title_limit=1)

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['Abbot+23',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG_hybridpriors_hmcode2020'
         ]

parameters = [f'limber_DESY3__{i}_dz' for i in range(4)] + [f'limber_KiDS1000__{i}_dz' for i in range(5)]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'black', 'red']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], title_limit=1)

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['Abbot+23',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG_hybridpriors_hmcode2020'
         ]

parameters = [f'bias_DESY3__{i}_m' for i in range(4)]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'black', 'red']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], title_limit=1)

# %% hidden=true

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'Abbot+23'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'Omega_b', 'm_nu', 'h', 'omega_b']

MCSamples = [results[i]['MCSamples'] for i in chains]

# Apply cut in omega_b as in official analysis
m = MCSamples[0].copy()
p = m.getParams()
sel = (p.omega_b >= 0.019) * (p.omega_b <= 0.026)
sel *= (p.h >= 0.64) * (p.h <= 0.82)
sel *= (p.Omega_c * p.h**2 >= 0.051) * (p.Omega_c * p.h**2 <= 0.255)
loglk = np.zeros(sel.size)
loglk[~sel] = np.inf
m.reweightAddingLogLikes(loglk)

MCSamples += [m]
labels = [results[i]['label'] for i in chains] + ['Ours with $\omega_b$ & $h$ & $\omega_c$ cut']
colors = ['blue', 'black', 'red']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], title_limit=1)

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true toc-hr-collapsed=true
# # DES + KiDS + HSC

# %%
######### TODO: Load and plot
# desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless
# desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax8192_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless
# desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax8192_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless

# %% hidden=true
######### lmax = 2048 ########

# No Baryons (4096) lmax=1000
key = 'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with No-Baryons ($\ell < 1000$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=1000
key = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons ($\ell < 1000$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

######### lmax = 2048 ########
# No Baryons (4096) lmax=2048
key = 'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with No-Baryons ($\ell < 2048$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=2048
key = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons ($\ell < 2048$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

######### lmax = 4500 ########
# No Baryons (4096) lmax=4500
key = 'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with No-Baryons ($\ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=8192
key = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons ($\ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

######### lmax = 8192 ########
# No Baryons (4096) lmax=8192
key = 'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with No-Baryons ($\ell < 8192$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=8192
key = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons ($\ell < 8192$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

########## BBN prior ###########
# Baryons (4096) lmax=4500
key = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BBNprior'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons + BBN Prior ($\ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")


########## ALTERNATIVE BARYONS MODELS ###########
### BCM
def apply_cuts_to_BCM(key):
    # This is to match Mc>12 and eta<1 as in ST15 paper
    ST15 = results[key]['MCSamples']
    p = ST15.getParams()
    sel = (p.M_c < 12) + (p.eta >= 0)
    ST15.filter(~sel)
    
    b = results[key]['boost']
    b.set_getdist_1D(True) # Because the posteriors are very one-sided
    bST15 = b.get_MCSamples_baryons()
    bST15.filter(~sel)

def change_units_Mc_BCM(key):
    # For BCM in CCL, M_c has Ms units but baccoemu uses Ms/h units
    s = results[key]['MCSamples']
    pn = s.getParamNames().list()
    s.samples[:, pn.index('M_c')] += np.log10(s.samples[:, pn.index('h')])
    s.setRanges({'M_c': [12 + np.log10(s.ranges.getLower('h')),
                         12 + np.log10(s.ranges.getUpper('h'))],
                 'eta': [s.ranges.getLower('eta'), 0]})
    s.updateBaseStatistics()
    return
    
# Baryons BCM CCL (lmax=4500)
key = "desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with ST15 Baryons ($\ell < 4500$)', 'orange', 0.15, baccoemu=False)
print(key, f"R-1 = {results[key]['R-1']}")
# Apply cuts to match ST15 paper
apply_cuts_to_BCM(key)
# For BCM in CCL, M_c has Ms units but baccoemu uses Ms/h units
change_units_Mc_BCM(key)

# Baryons BCM CCL (lmax=8192)
key = 'desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with ST15 Baryons ($\ell < 8192$)', 'orange', 0.15, baccoemu=False)
print(key, f"R-1 = {results[key]['R-1']}")
# Apply cuts to match ST15 paper
apply_cuts_to_BCM(key)
# For BCM in CCL, M_c has Ms units but baccoemu uses Ms/h units
change_units_Mc_BCM(key)


### A-E
# Baryons Amon-Efstathiou (lmax=4500)
key = "desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with A-E Baryons ($\ell < 4500$)', 'orange', 0.15, baccoemu=False)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons Amon-Efstathiou (lmax=4500)
key = "desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with A-E Baryons + Fixed Cosmology ($\ell < 4500$)', 'orange', 0.15, baccoemu=False,
                            add_derived_params=False)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons Amon-Efstathiou (lmax=8192)
key = 'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with A-E Baryons ($\ell < 8192$)', 'orange', 0.15, baccoemu=False)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons Amon-Efstathiou + HALOFIT (lmax=8192)
key = 'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_halofit_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with A-E Baryons + HALOFIT ($\ell < 8192$)', 'orange', 0.15, baccoemu=False)
print(key, f"R-1 = {results[key]['R-1']}")


########## FIXED COSMOLOGY ###########
# Baryons (4096) lmax=4500 (Planck18 cosmology)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons + Fixed Cosmology (Planck18) ($\ell < 4500$)', 'black', 0.15,
                         add_derived_params=False)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096) lmax=4500 (Planck18 cosmology + Omega_m from BF cosmology to check whether is the S8 or Omega_m tension what drives the shift in baryon suppresion)
key = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18_OmBF'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons + Fixed Cosmology (Planck18 + $\Omega_m$ from BF) ($\ell < 4500$)', 'black', 0.15,
                         add_derived_params=False)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096) lmax=4500 (BF with no baryons cosmology)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons + Fixed Cosmology (BF noBaryons) ($\ell < 4500$)', 'black', 0.15,
                         add_derived_params=False)
# Load boost comptuded with P18 cosmology as the BF cosmology is out of the boundaries of the emulator to check impact
fname = f'./chains/{key}/{key}_P18cosmoSk'
results['boost_with_fixedcosmoP18'] = BaryonBoost(fname, ignore_rows=0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# No Baryons (4096) lmax=8192 (Planck18 cosmology)
key = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons + Fixed Cosmology (Planck18) ($\ell < 8192$)', 'black', 0.15,
                         add_derived_params=False)
print(key, f"R-1 = {results[key]['R-1']}")


# No Baryons (4096) lmax=8192 (BF with no baryons cosmology)
key = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoBF'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons + Fixed Cosmology (BF noBaryons) ($\ell < 8192$)', 'black', 0.15,
                         add_derived_params=False)
print(key, f"R-1 = {results[key]['R-1']}")

########## BAO (SDSS DR16) lkl ##########

key = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons + BAO ($\ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (Riess H0 prior)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess_BAOlkl"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons + BAO + $H_0$ SNe prior ($\ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (Planck18 prior)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck_BAOlkl"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons + BAO + $H_0$ Planck18 prior ($\ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

########## H0 prior ###########
# Baryons (4096) lmax=4500 (Riess H0 prior)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons + $H_0$ SNe prior ($\ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (Planck18 prior)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons + $H_0$ Planck18 prior ($\ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

########## + Planck lkl (Gaussian approx)  ###########
# Baryons (4096) lmax=4500 (Planck 2018 Gaussian likelihood)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons + Planck2018 ($\ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

########## FIXED BARYONS ###########
# Baryons (4096) lmax=4500 (BAHAMAS cosmology)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedbaryonsBAHAMAS"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons + BAHAMAS prior ($\ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (clusters prior)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_clusters"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons + clusters prior ($\ell < 4500$)', 'black', 0.15)

print(key, f"R-1 = {results[key]['R-1']}")

########## HALOFIT extrap ###########
# No Baryons (4096) lmax=4500
key = 'desy3wl_k1000_hsc_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons HALOFIT extrap ($\ell < 4500$)', 'black', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

########## MOCK DATA ###########
# Baryons (4096) lmax=4500 (EuclidEmulator2 Pkmm + BAHAMAS Sk from baccoemu, Gaussian realization)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons, Mock Data ($\ell < 4500$)', 'purple', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (EuclidEmulator2 Pkmm + BAHAMAS Sk from baccoemu)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons,  Mock Data noiseless ($\ell < 4500$)', 'pink', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")


##
# Baryons (4096) lmax=4500 (EuclidEmulator2 Pkmm + BAHAMAS Sk from baccoemu, Gaussian realization, BF no baryons cosmology)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons, Mock Data, BF (no baryons) cosmology ($\ell < 4500$)', 'purple', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (EuclidEmulator2 Pkmm + BAHAMAS Sk from baccoemu, BF no baryons cosmology)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons,  Mock Data noiseless, BF (no baryons) cosmology ($\ell < 4500$)', 'pink', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")


# Baryons (4096) lmax=1000 (EuclidEmulator2 Pkmm + BAHAMAS Sk from baccoemu, BF no baryons cosmology)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons, Mock Data noiseless, BF (no baryons) cosmology ($\ell < 1000$)', 'purple', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")


## Mock data for paper
# Baryons (4096) lmax=4500 (BACCOEmu Pkmm + BAHAMAS Sk from table, BF w/ baryons cosmology)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons, Mock Data noiseless, BF (no baryons) cosmology ($\ell < 4500$)', 'purple', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=8192 (BACCOEmu Pkmm + BAHAMAS Sk from table, BF w/ baryons cosmology)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax8192_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons, Mock Data noiseless, BF (no baryons) cosmology ($\ell < 8192$)', 'purple', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (EE2 Pkmm at k<1/Mpc + BAHAMAS Pk from table)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons, Mock Data noiseless, BAHAMAStable & EE2 $k<1/$Mpc ($\ell < 4500$)', 'purple', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=8192 (EE2 Pkmm at k<1/Mpc + BAHAMAS Sk from table)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax8192_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons, Mock Data noiseless, BAHAMAStable & EE2 $k<1/$Mpc ($\ell < 8192$)', 'purple', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# %%

# %%
# Baryons (4096) lmax=8192 (EE2 Pkmm at k<1/Mpc + BAHAMAS Sk from table)
key = "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax8192_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 + KiDS1000 + HSC with Baryons, Mock Data noiseless, BAHAMAStable & EE2 $k<1/$Mpc ($\ell < 8192$)', 'purple', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Within baccoemu ranges?

# %% hidden=true
k = "CHAIN"
print(f"{k:<70} Nonlinear emulator   baryon emulator   baryon fraction")
for k in results:
    if k.startswith('desy3wl_k1000_hsc'):
        if 'inbounds_baccoemu' in results[k]:
            print (f"{k:<80}{results[k]['inbounds_baccoemu']['nonlinear']:<10.2f}   {results[k]['inbounds_baccoemu']['baryon']:<15.2f}   {results[k]['inbounds_baccoemu']['baryon_fraction']:.2f}")

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Map

# %% hidden=true
hscmask = hp.read_map("/mnt/extraspace/damonge/Datasets/HSC_DR1/lite/mask_mask_HSC_wl0_coordC_ns4096.fits.gz")
desy3mask = hp.read_map("/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY30_noK1000_noHSCDR1wl_coordC_ns4096.fits.gz")
k1000mask = hp.read_map("/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__0_noHSCDR1wl_coordC_ns4096.fits.gz")

# For the transaprency to work you need to comment healpy/projaxes.py#L189
# See https://github.com/healpy/healpy/issues/765
k1000mask = scale_bin_map(k1000mask, 1)
desy3mask = scale_bin_map(desy3mask, 2)
hscmask = scale_bin_map(hscmask, 3)
cbound = {'min': 0, 
          'max': 3}

hp.mollview(k1000mask, badcolor='lightgray', cbar=False, title='', **cbound)
hp.mollview(desy3mask, badcolor='none', alpha=0.8 * np.ones_like(desy3mask), reuse_axes=True, cbar=False, title='', **cbound)
hp.mollview(hscmask, badcolor='none', alpha=0.8 * np.ones_like(desy3mask), reuse_axes=True, cbar=False, title='', **cbound)

hp.graticule()

plt.text(1.8, 0.1, 'KiDS-1000', fontsize=15, color='darkblue', horizontalalignment='right')
plt.text(-1.25, -0.5, 'DESY3', fontsize=15, color='green', horizontalalignment='left')
plt.text(0.8, 0.45, 'HSCDR1', fontsize=15, color='gold', horizontalalignment='left')

# plt.savefig('')
plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Mock data (not polished at all and quickly ugly plots)

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### Planck-like cosmology

# %%
# Parameters used to generate the power spectrum and baryonic boost
cosmopars = {
    'omega_cold'    :  0.31,
    'omega_baryon'  :  0.045,
    'sigma8_cold'   :  0.82,
    'ns'            :  0.965,
    'hubble'        :  0.7,
    'neutrino_mass' :  0.06,
    'w0'            : -1,
    'wa'            :  0,
    'M_c'           :  13.58,
    'eta'           : -0.27,
    'beta'          : -0.33,
    'M1_z0_cen'     :  12.04,
    'theta_out'     :  0.25,
    'theta_inn'     : -0.86,
    'M_inn'         :  13.4
}

cosmopars['omega_matter'] = cosmopars['omega_cold'] + cosmopars['neutrino_mass'] / 93.14 / cosmopars['hubble']**2
cosmopars['A_s'] = 1.8753844640255685e-09
cosmopars['sigma8_tot'] = 0.8163992592396454

input_params = {'Omega_c': cosmopars['omega_cold'] - cosmopars['omega_baryon'],
                'Omega_m': cosmopars['omega_matter'],
                'Omega_b': cosmopars['omega_baryon'],
                'sigma8': cosmopars['sigma8_tot'],
                'S8': cosmopars['sigma8_tot'] * np.sqrt(cosmopars['omega_matter'] / 0.3),
                'A_s': cosmopars['A_s'],
                'h': cosmopars['hubble'],
                'omega_m': cosmopars['omega_matter'] * cosmopars['hubble']**2,
                'limber_DESY3wl__0_dz': 0,
                'limber_DESY3wl__1_dz': 0,
                'limber_DESY3wl__2_dz': 0, 
                'limber_DESY3wl__3_dz': 0,
                'limber_KiDS1000__0_dz': 0,
                'limber_KiDS1000__1_dz': -0.002,
                'limber_KiDS1000__2_dz': -0.013,
                'limber_KiDS1000__3_dz': -0.011,
                'limber_KiDS1000__4_dz': 0.006,
                'limber_HSCDR1wl__0_dz': 0,
                'limber_HSCDR1wl__1_dz': 0,
                'limber_HSCDR1wl__2_dz': 0,
                'limber_HSCDR1wl__3_dz': 0,
                'bias_DESY3wl__0_m': -0.0063,
                'bias_DESY3wl__1_m': -0.0198,
                'bias_DESY3wl__2_m': -0.0241, 
                'bias_DESY3wl__3_m': -0.0369,
                'bias_KiDS1000__0_m': 0,
                'bias_KiDS1000__1_m': 0.,
                'bias_KiDS1000__2_m': 0.,
                'bias_KiDS1000__3_m': 0.,
                'bias_KiDS1000__4_m': 0.,
                'bias_HSCDR1wl__0_m': 0,
                'bias_HSCDR1wl__1_m': 0,
                'bias_HSCDR1wl__2_m': 0,
                'bias_HSCDR1wl__3_m': 0,
                'bias_DESY3wl_A_IA': 0,
                'limber_DESY3wl_eta_IA': 0,
                'bias_KiDS1000_A_IA': 0,
                'limber_KiDS1000_eta_IA': 0,
                'bias_HSCDR1wl_A_IA': 0,
                'limber_HSCDR1wl_eta_IA': 0,
               }
input_params.update({k: cosmopars[k] for k in ['M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_out', 'theta_inn', 'M_inn']})

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('Omega_m'))

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless',
         ]

# print('p-value = ', results[chains[0]]['pvalue_bf'])
# print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)

bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='magenta')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='magenta', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)

# %%
# Check z-shifts
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = [k for k in input_params.keys() if '_dz' in k]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)

# %%
# Check multiplicative biases
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = [k for k in input_params.keys() if '_m' in k]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)

# %%
# Check Intrinsic alignments
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless',
         ]

parameters = [k for k in input_params.keys() if '_IA' in k]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### BF (no baryons) cosmology

# %%
pkfile = np.load('./test/BlindCarlosPk2.npz', allow_pickle=True)

cosmopars = dict(pkfile['cosmopars'].tolist())
cosmopars.update({'sigma8_tot': float(pkfile['sigma8 tot'])})
cosmopars['omega_matter'] = cosmopars['omega_cold'] + cosmopars['neutrino_mass'] / 93.14 / cosmopars['hubble']**2

input_params = {'Omega_c': cosmopars['omega_cold'] - cosmopars['omega_baryon'],
                'Omega_m': cosmopars['omega_matter'],
                'Omega_b': cosmopars['omega_baryon'],
                'sigma8': cosmopars['sigma8_tot'],
                'S8': cosmopars['sigma8_tot'] * np.sqrt(cosmopars['omega_matter'] / 0.3),
                'A_s': cosmopars['A_s'],
                'h': cosmopars['hubble'],
                'omega_m': cosmopars['omega_matter'] * cosmopars['hubble']**2,
                'limber_DESY3wl__0_dz': 0,
                'limber_DESY3wl__1_dz': 0,
                'limber_DESY3wl__2_dz': 0, 
                'limber_DESY3wl__3_dz': 0,
                'limber_KiDS1000__0_dz': 0,
                'limber_KiDS1000__1_dz': -0.002,
                'limber_KiDS1000__2_dz': -0.013,
                'limber_KiDS1000__3_dz': -0.011,
                'limber_KiDS1000__4_dz': 0.006,
                'limber_HSCDR1wl__0_dz': 0,
                'limber_HSCDR1wl__1_dz': 0,
                'limber_HSCDR1wl__2_dz': 0,
                'limber_HSCDR1wl__3_dz': 0,
                'bias_DESY3wl__0_m': -0.0063,
                'bias_DESY3wl__1_m': -0.0198,
                'bias_DESY3wl__2_m': -0.0241, 
                'bias_DESY3wl__3_m': -0.0369,
                'bias_KiDS1000__0_m': 0,
                'bias_KiDS1000__1_m': 0.,
                'bias_KiDS1000__2_m': 0.,
                'bias_KiDS1000__3_m': 0.,
                'bias_KiDS1000__4_m': 0.,
                'bias_HSCDR1wl__0_m': 0,
                'bias_HSCDR1wl__1_m': 0,
                'bias_HSCDR1wl__2_m': 0,
                'bias_HSCDR1wl__3_m': 0,
                'bias_DESY3wl_A_IA': 0,
                'limber_DESY3wl_eta_IA': 0,
                'bias_KiDS1000_A_IA': 0,
                'limber_KiDS1000_eta_IA': 0,
                'bias_HSCDR1wl_A_IA': 0,
                'limber_HSCDR1wl_eta_IA': 0,
               }
input_params.update({k: cosmopars[k] for k in ['M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_out', 'theta_inn', 'M_inn']})

# %%
chains = [
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS',
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
         ]
print('Omega_m')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('Omega_m'))
print()
print('S8')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('S8'))

# %%
chains = [
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
         ]

print('p-value = ', results[chains[0]]['pvalue_bf'], 'chi2 = ', results[chains[0]]['chi2bf'])
print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='magenta')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='magenta', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 

# %%
chains = [
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG'
         ]

print('p-value = ', results[chains[0]]['pvalue_bf'], 'chi2 = ', results[chains[0]]['chi2bf'])
print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='magenta')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='magenta', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 

# %%
chains = [
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

print('p-value = ', results[chains[0]]['pvalue_bf'], 'chi2 = ', results[chains[0]]['chi2bf'])
print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['pink', 'gray']


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='magenta')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='magenta', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 

# %%
chains = [
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)

# %%
# Check z-shifts
chains = [
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = [k for k in input_params.keys() if '_dz' in k]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)

# %%
# Check multiplicative biases
chains = [
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = [k for k in input_params.keys() if '_m' in k]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)

# %%
# Check Intrinsic alignments
chains = [
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
]

parameters = [k for k in input_params.keys() if '_IA' in k]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)

# %%
s = results['k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG']['MCSamples']
p = s.getParams()
s.addDerived(p.bias_A_IA, 'bias_KiDS1000_A_IA', 'A_{IA}^{KiDS1000}')
s.addDerived(p.limber_eta_IA, 'limber_KiDS1000_eta_IA', '\eta_{IA}^{KiDS1000}')

# %%
# Check Intrinsic alignments
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG'
]

parameters = ['bias_KiDS1000_A_IA', 'limber_KiDS1000_eta_IA']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['black', 'blue', 'red']

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### BF (baryons) cosmology

# %%
pkfile = np.load('./test/BACCOEmu_kmax10_baryonbf.npz', allow_pickle=True)

cosmopars = dict(pkfile['cosmopars'].tolist())
cosmopars.update({'sigma8_tot': float(pkfile['sigma8 tot'])})
cosmopars['omega_matter'] = cosmopars['omega_cold'] + cosmopars['neutrino_mass'] / 93.14 / cosmopars['hubble']**2

input_params = {'Omega_c': cosmopars['omega_cold'] - cosmopars['omega_baryon'],
                'Omega_m': cosmopars['omega_matter'],
                'Omega_b': cosmopars['omega_baryon'],
                'sigma8': cosmopars['sigma8_tot'],
                'S8': cosmopars['sigma8_tot'] * np.sqrt(cosmopars['omega_matter'] / 0.3),
                'A_s': cosmopars['A_s'],
                'h': cosmopars['hubble'],
                'omega_m': cosmopars['omega_matter'] * cosmopars['hubble']**2,
                'limber_DESY3wl__0_dz': 0,
                'limber_DESY3wl__1_dz': 0,
                'limber_DESY3wl__2_dz': 0, 
                'limber_DESY3wl__3_dz': 0,
                'limber_KiDS1000__0_dz': 0,
                'limber_KiDS1000__1_dz': -0.002,
                'limber_KiDS1000__2_dz': -0.013,
                'limber_KiDS1000__3_dz': -0.011,
                'limber_KiDS1000__4_dz': 0.006,
                'limber_HSCDR1wl__0_dz': 0,
                'limber_HSCDR1wl__1_dz': 0,
                'limber_HSCDR1wl__2_dz': 0,
                'limber_HSCDR1wl__3_dz': 0,
                'bias_DESY3wl__0_m': -0.0063,
                'bias_DESY3wl__1_m': -0.0198,
                'bias_DESY3wl__2_m': -0.0241, 
                'bias_DESY3wl__3_m': -0.0369,
                'bias_KiDS1000__0_m': 0,
                'bias_KiDS1000__1_m': 0.,
                'bias_KiDS1000__2_m': 0.,
                'bias_KiDS1000__3_m': 0.,
                'bias_KiDS1000__4_m': 0.,
                'bias_HSCDR1wl__0_m': 0,
                'bias_HSCDR1wl__1_m': 0,
                'bias_HSCDR1wl__2_m': 0,
                'bias_HSCDR1wl__3_m': 0,
                'bias_DESY3wl_A_IA': 0,
                'limber_DESY3wl_eta_IA': 0,
                'bias_KiDS1000_A_IA': 0,
                'limber_KiDS1000_eta_IA': 0,
                'bias_HSCDR1wl_A_IA': 0,
                'limber_HSCDR1wl_eta_IA': 0,
               }
# input_params.update({k: cosmopars[k] for k in ['M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_out', 'theta_inn', 'M_inn']})

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
         ]
print('Omega_m')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('Omega_m'))
print()
print('S8')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('S8'))

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'priors_chain'
         ]

# print('p-value = ', results[chains[0]]['pvalue_bf'], 'chi2 = ', results[chains[0]]['chi2bf'])
# print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='magenta')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='magenta', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'priors_chain'
         ]

# print('p-value = ', results[chains[0]]['pvalue_bf'], 'chi2 = ', results[chains[0]]['chi2bf'])
# print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='magenta')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='magenta', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless',
         ]

print('p-value = ', results[chains[0]]['pvalue_bf'], 'chi2 = ', results[chains[0]]['chi2bf'])
print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red']


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless',
         ]

print('p-value = ', results[chains[0]]['pvalue_bf'], 'chi2 = ', results[chains[0]]['chi2bf'])
print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red']


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### P18 (or P15?) & BF

# %%
# Parameters used to generate the power spectrum and baryonic boost (P18 or P15??)
cosmopars = {
    'omega_cold'    :  0.31,
    'omega_baryon'  :  0.045,
    'sigma8_cold'   :  0.82,
    'ns'            :  0.965,
    'hubble'        :  0.7,
    'neutrino_mass' :  0.06,
    'w0'            : -1,
    'wa'            :  0,
    'M_c'           :  13.58,
    'eta'           : -0.27,
    'beta'          : -0.33,
    'M1_z0_cen'     :  12.04,
    'theta_out'     :  0.25,
    'theta_inn'     : -0.86,
    'M_inn'         :  13.4
}

cosmopars['omega_matter'] = cosmopars['omega_cold'] + cosmopars['neutrino_mass'] / 93.14 / cosmopars['hubble']**2
cosmopars['A_s'] = 1.8753844640255685e-09
cosmopars['sigma8_tot'] = 0.8163992592396454

input_params_p18 = {'Omega_c': cosmopars['omega_cold'] - cosmopars['omega_baryon'],
                'Omega_m': cosmopars['omega_matter'],
                'Omega_b': cosmopars['omega_baryon'],
                'sigma8': cosmopars['sigma8_tot'],
                'S8': cosmopars['sigma8_tot'] * np.sqrt(cosmopars['omega_matter'] / 0.3),
                'A_s': cosmopars['A_s'],
                'h': cosmopars['hubble'],
                'omega_m': cosmopars['omega_matter'] * cosmopars['hubble']**2,
                'limber_DESY3wl__0_dz': 0,
                'limber_DESY3wl__1_dz': 0,
                'limber_DESY3wl__2_dz': 0, 
                'limber_DESY3wl__3_dz': 0,
                'limber_KiDS1000__0_dz': 0,
                'limber_KiDS1000__1_dz': -0.002,
                'limber_KiDS1000__2_dz': -0.013,
                'limber_KiDS1000__3_dz': -0.011,
                'limber_KiDS1000__4_dz': 0.006,
                'limber_HSCDR1wl__0_dz': 0,
                'limber_HSCDR1wl__1_dz': 0,
                'limber_HSCDR1wl__2_dz': 0,
                'limber_HSCDR1wl__3_dz': 0,
                'bias_DESY3wl__0_m': -0.0063,
                'bias_DESY3wl__1_m': -0.0198,
                'bias_DESY3wl__2_m': -0.0241, 
                'bias_DESY3wl__3_m': -0.0369,
                'bias_KiDS1000__0_m': 0,
                'bias_KiDS1000__1_m': 0.,
                'bias_KiDS1000__2_m': 0.,
                'bias_KiDS1000__3_m': 0.,
                'bias_KiDS1000__4_m': 0.,
                'bias_HSCDR1wl__0_m': 0,
                'bias_HSCDR1wl__1_m': 0,
                'bias_HSCDR1wl__2_m': 0,
                'bias_HSCDR1wl__3_m': 0,
                'bias_DESY3wl_A_IA': 0,
                'limber_DESY3wl_eta_IA': 0,
                'bias_KiDS1000_A_IA': 0,
                'limber_KiDS1000_eta_IA': 0,
                'bias_HSCDR1wl_A_IA': 0,
                'limber_HSCDR1wl_eta_IA': 0,
               }
input_params_p18.update({k: cosmopars[k] for k in ['M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_out', 'theta_inn', 'M_inn']})

# %%
pkfile = np.load('./test/BlindCarlosPk2.npz', allow_pickle=True)

cosmopars = dict(pkfile['cosmopars'].tolist())
cosmopars.update({'sigma8_tot': float(pkfile['sigma8 tot'])})
cosmopars['omega_matter'] = cosmopars['omega_cold'] + cosmopars['neutrino_mass'] / 93.14 / cosmopars['hubble']**2

input_params_bf = {'Omega_c': cosmopars['omega_cold'] - cosmopars['omega_baryon'],
                'Omega_m': cosmopars['omega_matter'],
                'Omega_b': cosmopars['omega_baryon'],
                'sigma8': cosmopars['sigma8_tot'],
                'S8': cosmopars['sigma8_tot'] * np.sqrt(cosmopars['omega_matter'] / 0.3),
                'A_s': cosmopars['A_s'],
                'h': cosmopars['hubble'],
                'omega_m': cosmopars['omega_matter'] * cosmopars['hubble']**2,
                'limber_DESY3wl__0_dz': 0,
                'limber_DESY3wl__1_dz': 0,
                'limber_DESY3wl__2_dz': 0, 
                'limber_DESY3wl__3_dz': 0,
                'limber_KiDS1000__0_dz': 0,
                'limber_KiDS1000__1_dz': -0.002,
                'limber_KiDS1000__2_dz': -0.013,
                'limber_KiDS1000__3_dz': -0.011,
                'limber_KiDS1000__4_dz': 0.006,
                'limber_HSCDR1wl__0_dz': 0,
                'limber_HSCDR1wl__1_dz': 0,
                'limber_HSCDR1wl__2_dz': 0,
                'limber_HSCDR1wl__3_dz': 0,
                'bias_DESY3wl__0_m': -0.0063,
                'bias_DESY3wl__1_m': -0.0198,
                'bias_DESY3wl__2_m': -0.0241, 
                'bias_DESY3wl__3_m': -0.0369,
                'bias_KiDS1000__0_m': 0,
                'bias_KiDS1000__1_m': 0.,
                'bias_KiDS1000__2_m': 0.,
                'bias_KiDS1000__3_m': 0.,
                'bias_KiDS1000__4_m': 0.,
                'bias_HSCDR1wl__0_m': 0,
                'bias_HSCDR1wl__1_m': 0,
                'bias_HSCDR1wl__2_m': 0,
                'bias_HSCDR1wl__3_m': 0,
                'bias_DESY3wl_A_IA': 0,
                'limber_DESY3wl_eta_IA': 0,
                'bias_KiDS1000_A_IA': 0,
                'limber_KiDS1000_eta_IA': 0,
                'bias_HSCDR1wl_A_IA': 0,
                'limber_HSCDR1wl_eta_IA': 0,
               }
input_params_bf.update({k: cosmopars[k] for k in ['M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_out', 'theta_inn', 'M_inn']})

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

print('p-value = ', results[chains[0]]['pvalue_bf'], 'chi2 = ', results[chains[0]]['chi2bf'])
print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['blue', 'orange']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[1, 1, 1, 1])


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], color=colors[0])
        ax.axvline(input_params_p18[p1], color=colors[0], ls=':')
    if p1 in bf2:
        ax.axvline(bf2[p1], color=colors[1])
        ax.axvline(input_params_bf[p1], color=colors[1], ls=':')
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color=colors[0], marker='o')
            ax.axvline(input_params_p18[p1], color=colors[0], ls=':')
            ax.axhline(input_params_p18[p2], color=colors[0], ls=':')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o')
            ax.axvline(input_params_bf[p1], color=colors[1], ls=':')
            ax.axhline(input_params_bf[p2], color=colors[1], ls=':')

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
         ]

labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange']
labels = ['Planck 2018 cosmology + BAHAMAS baryonic boost', 'BF cosmology + BAHAMAS baryonic boost']

f, ax = plt.subplots()

# Add BAHAMAS line
pkfile = np.load('./test/BlindCarlosPk.npz', allow_pickle=True)
k = pkfile['k [1/Mpc]']
Sk = pkfile['Sk'][-1]
ax.semilogx(k, Sk, color='lightblue', ls='--', label='BAHAMAS baryonic boost')

# Extrapolated for BF cosmo
pkfile = np.load('./test/BlindCarlosPk2.npz', allow_pickle=True)
k = pkfile['k [1/Mpc]']
Sk = pkfile['Sk'][-1]
ax.semilogx(k, Sk, color='gold', ls='--', label='BAHAMAS baryonic boost (extrap for BF cosmo)')

# Add chain constraints
for i, c in enumerate(chains):
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    label = dict(zip(chains, labels))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color, alpha=0.3 - 0.1*i)

# Finalize plot
ax.set_xlim([0.1, 2.55])
ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$', fontsize=14)
ax.set_ylabel('$S_k(a=1)$', fontsize=14)
ax.tick_params(axis='x', labelsize=12)
ax.tick_params(axis='y', labelsize=12)
ax.legend(loc='lower left', fontsize=12, frameon=False)

# plt.savefig('../paper_figures/boost_baccoemu.pdf', bbox_inches='tight')
plt.show()
plt.close()

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs No-Baryons ($\ell_{\rm max} = 1000$)

# %% hidden=true
# Baryons vs No Baryons with ell_max=1000
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG',
          # 'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
# colors[-1] = 'blue'

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs No-Baryons ($\ell_{\rm max} = 2048$)

# %% hidden=true
# Baryons vs No Baryons with ell_max=2048
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG',
          # 'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
labels = ['No baryons', 'Baryons']
colors = ['gray', 'black']

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Baryons vs No Baryons with ell_max=2048
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG',
          # 'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
# colors[-1] = 'blue'

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs No-Baryons ($\ell_{\rm max} = 4500$)

# %%
# bf
print("No baryons")
results['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['pars_bf']

# %%
# bf
print("Baryons")
results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['pars_bf']

# %%
# bf
print("No Baryons")
results['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG']['pars_bf']


# %% hidden=true
def get_tension_with_Planck_Omegam(samples):
    p = results['P18_official']['MCSamples'].getParams()
    w = results['P18_official']['MCSamples'].weights
    mean_Planck = np.average(p.Omega_m, weights=w)
    cov_Planck = np.cov(p.Omega_m, fweights=w)
        
    p = samples.getParams()
    w = samples.weights
    mean = np.average(p.Omega_m, weights=w)
    cov = np.cov(p.Omega_m, fweights=w)
    # Table 2 of https://arxiv.org/pdf/1807.06209.pdf
    # with lensing: 0.3153 ± 0.0073
    # w/o lensing: 0.3166 ± 0.0084 
    # return (0.832 - mean) / np.sqrt((0.013**2 + cov))
    print(mean_Planck, cov_Planck)
    return (mean_Planck - mean) / np.sqrt(cov_Planck  + cov)

get_tension_with_Planck_Omegam(results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'])

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          # 'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
# colors[-1] = 'blue'

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


### Full parameter space
# parameters = results[chains[0]]['MCSamples'].getParamNames().list()
# parameters.remove('minuslogprior__0')
# parameters.remove('chi2__cl_like.ClLike')

# g = plots.get_subplot_plotter()
# g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
#                 contour_colors=colors, legend_labels=labels,
#                 contour_lws=[None, 1, 1, 1])

# %%
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
         ]

parameters = ['Omega_m', 'omega_m', 'h', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['Blue', 'red']

g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[1, 1, 1, 1])


bf1 = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['pars_bf']
bf2 = results['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='b')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color='r')
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], 'bo')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], 'ro') 

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BBNprior',
          'P18_official'
         ]

parameters = ['omega_m', 'S12']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_single_plotter(width_inch=6)
g.plot_2d(MCSamples, parameters, filled=[True, False, False, False],
                colors=['blue', 'orange', 'black'],
                contour_lws=[None, 1, 1, 1])
g.add_legend(['No-Baryons', 'Baryons', 'Baryons + BBN Prior', 'Planck'], legend_loc='upper left', colored_text=True);

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'P18_official'
         ]

parameters = ['Omega_m', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_single_plotter(width_inch=6)
g.plot_2d(MCSamples, parameters, filled=[True, False, False, False],
                colors=['blue', 'orange', 'black'],
                contour_lws=[None, 1, 1, 1])
g.add_legend(['No-Baryons', 'Baryons', 'Planck'], legend_loc='upper left', colored_text=True);

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BBNprior',
          'P18_official'
         ]

parameters = ['omega_m', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_single_plotter(width_inch=6)
g.plot_2d(MCSamples, parameters, filled=[True, False, False, False],
                colors=['blue', 'orange', 'red', 'black'],
                contour_lws=[None, 1, 1, 1])
g.add_legend(['No-Baryons', 'Baryons', 'Baryons + BBN Prior', 'Planck'], legend_loc='upper left', colored_text=True);

# %%
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BBNprior',
          'P18_official'
         ]

parameters = ['Omega_m', 'omega_m', 'h', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
labels = ['No-Baryons', 'Baryons', 'Baryons + BBN Prior', 'Planck']
colors = ['blue', 'orange', 'red', 'black']

g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


### Full parameter space
# parameters = results[chains[0]]['MCSamples'].getParamNames().list()
# parameters.remove('minuslogprior__0')
# parameters.remove('chi2__cl_like.ClLike')

# g = plots.get_subplot_plotter()
# g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
#                 contour_colors=colors, legend_labels=labels,
#                 contour_lws=[None, 1, 1, 1])

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['orange', 'green', 'blue']

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


### Full parameter space
# parameters = results[chains[0]]['MCSamples'].getParamNames().list()
# parameters.remove('minuslogprior__0')
# parameters.remove('chi2__cl_like.ClLike')

# g = plots.get_subplot_plotter()
# g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
#                 contour_colors=colors, legend_labels=labels,
#                 contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons vs No-Baryons ($\ell_{\rm max} = 8192$)

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


### Full parameter space
# parameters = results[chains[0]]['MCSamples'].getParamNames().list()
# parameters.remove('minuslogprior__0')
# parameters.remove('chi2__cl_like.ClLike')

# g = plots.get_subplot_plotter()
# g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
#                 contour_colors=colors, legend_labels=labels,
#                 contour_lws=[None, 1, 1, 1])

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'P18_official'
         ]

parameters = ['Omega_m', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_single_plotter(width_inch=6)
g.plot_2d(MCSamples, parameters, filled=[True, False, False, False],
                colors=['blue', 'orange', 'black'],
                contour_lws=[None, 1, 1, 1])
g.add_legend(['No-Baryons', 'Baryons', 'Planck'], legend_loc='upper left', colored_text=True);


######
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'Arico+23',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

MCSamples = [results[i]['MCSamples'] for i in chains]

g = plots.get_single_plotter(width_inch=6)
g.plot_1d(MCSamples, 'M_c', normalized=True, colors=['green', 'gray', 'red'])
g.add_legend(['DES Y3 (Fourier space)', 'DES Y3 (Arico et al. 2023)',  'DES Y3 + KiDS-1000 + HSC DR1'], colored_text=True);

g = plots.get_single_plotter(width_inch=6)
g.plot_1d(MCSamples, 'M_c', colors=['green', 'gray', 'red'])
g.add_legend(['DES Y3 (Fourier space)', 'DES Y3 (Arico et al. 2023)',  'DES Y3 + KiDS-1000 + HSC DR1'], colored_text=True);
plt.ylabel('Posterior', fontsize=14)

MCSamples.remove(MCSamples[1])
g = plots.get_single_plotter(width_inch=6)
g.plot_1d(MCSamples, 'M_c', colors=['green', 'red'])
g.add_legend(['DES Y3 (Fourier space)',  'DES Y3 + KiDS-1000 + HSC DR1'], colored_text=True);
plt.ylabel('Posterior', fontsize=14)

# %% hidden=true
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]
sdata = results[chains[0]].get_cl_data_sacc()
sbf_nob = results[chains[0]].get_cl_theory_sacc_bf()
sbf_b = results[chains[1]].get_cl_theory_sacc_bf()

for survey in ['DES', 'KiDS', 'HSC']:
    nbin = 5 if survey == 'KiDS' else 4
    f, ax = plt.subplots(nbin, nbin, figsize=(14, 14), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
    for trs in sdata.get_tracer_combinations():
        i = int(trs[1].split('__')[1])
        j = int(trs[0].split('__')[1])
        
        if survey not in trs[0]:
            continue

        # Data
        ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
        err = np.sqrt(np.diag(cov))
        ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8)    

        # BF no baryons
        ell, cl = sbf_nob.get_ell_cl('cl_ee', trs[0], trs[1])
        ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='blue', label='BF (No Baryons)', alpha=0.8)

        # BF baryons
        ell, cl = sbf_b.get_ell_cl('cl_ee', trs[0], trs[1])
        ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='orange', label='BF (Baryons)', alpha=0.8)

    for i in range(nbin):
        ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
        ax[-1, i].set_xlabel("$\ell$")
        ax[0, i].set_xscale("log")
        ax[0, 0].legend()

    f.suptitle(f'{survey}', fontsize=16)
    plt.tight_layout()
    plt.show()
    plt.close()

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## No-Baryons constraints at different lmax

# %% hidden=true
chains = [
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'h']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
labels = [r"$\ell < 1000$", r"$\ell < 2048$", r"$\ell < 4500$", r"$\ell < 8192$", 'Baryons ($\ell < 4500$)', 'Baryons ($\ell < 8192$)']
colors= ['gray', 'black', 'blue', 'red', 'pink', 'purple']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1.25, 1.25],
                contour_ls=['-', '-', '-', '-', ':', '--'])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons constraints at different lmax

# %% hidden=true
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]
for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('M_c'))

# %% hidden=true
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
labels = [r"$\ell < 1000$", r"$\ell < 2048$", r"$\ell < 4500$", r"$\ell < 8192$"]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'omega_m', 'h', 'S8', 'S12', 'sigma8', 'sigma12', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
labels = [r"$\ell < 1000$", r"$\ell < 2048$", r"$\ell < 4500$", r"$\ell < 8192$"]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'n_s', 'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Baryons with BAHAMAS \& cluster priors

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedbaryonsBAHAMAS',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_clusters',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
# labels = [r"$\ell < 1000$", r"$\ell < 2048$", r"$\ell < 4500$", r"$\ell < 8192$"]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedbaryonsBAHAMAS',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_clusters',
         ]

colors = ['blue', 'green', 'red']
labels = ['Baryons', 'BAHAMAS prior', 'Clusters prior']

f, ax = plt.subplots()
for i, c in enumerate(chains):
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    label = dict(zip(chains, labels))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color, alpha=0.3 - 0.1*i)

# Finalize plot
ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$', fontsize=14)
ax.set_ylabel('$S_k(a=1)$', fontsize=14)
ax.tick_params(axis='x', labelsize=12)
ax.tick_params(axis='y', labelsize=12)
ax.legend(loc='lower left', fontsize=12, frameon=False)

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedbaryonsBAHAMAS',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_clusters',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors= ['gray', 'black', 'blue', 'red']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## vs DES, KiDS & HSC (lmax=1000)

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### No-baryons

# %% hidden=true
# All surveys (lmax = 4500)
chains = [
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG',
          'hsc_nobaryons_nocuts_nla_nside4096_lmax1000_lmin300',
          'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG', 
          # 'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange', 'red', 'green', 'brown', 'black']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, False, True, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[1, 1, 1, 1, None, None],
                contour_ls=['-', '-', '-', '--', None, None]
                )

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### Baryons

# %% hidden=true
chains = ['hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG',
         ]
for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('M_c'))
    
print()

for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('Omega_m'))

# %% hidden=true
# All surveys (lmax = 4500)
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG', 
          # 'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange', 'red', 'green', 'brown', 'black']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, False, True, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[1, 1, 1, 1, None, None],
                contour_ls=['-', '-', '-', '--', None, None]
                )

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## vs DES, KiDS & HSC (lmax=2048)

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### No-baryons

# %% hidden=true
# All surveys (lmax = 2048)
chains = [
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG', 
          # 'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange', 'red', 'green', 'brown', 'black']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, False, True, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[1, 1, 1, 1, None, None],
                contour_ls=['-', '-', '-', '--', None, None]
                )

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### Baryons

# %% hidden=true
chains = ['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG',
         ]
for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('M_c'))
    
print()

for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('Omega_m'))

# %% hidden=true
# All surveys (lmax = 4500)
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG', 
          # 'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange', 'red', 'green', 'brown', 'black']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, False, True, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[1, 1, 1, 1, None, None],
                contour_ls=['-', '-', '-', '--', None, None]
                )

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## vs DES, KiDS & HSC (lmax=4500)

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### No-baryons

# %% hidden=true
# All surveys (lmax = 4500)
chains = [
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
          # 'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange', 'red', 'green', 'brown', 'black']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, False, True, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[1, 1, 1, 1, None, None],
                contour_ls=['-', '-', '-', '--', None, None]
                )

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### Baryons

# %% hidden=true
chains = ['hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
         ]
for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('M_c'))
    
print()

for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('Omega_m'))

# %% hidden=true
# All surveys (lmax = 4500)
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
          'P18_official'
         ]

parameters = ['omega_m', 'S12', 'sigma8']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = ['DESY3', 'KiDS-1000', 'HSCDR1', 'DESY3 + KiDS-1000', 'DESY3 + KiDS-1000 + HSCDR1', 'Planck 2018']
colors = ['blue', 'orange', 'red', 'green', 'brown', 'black']

g = plots.get_single_plotter(width_inch=6)
g.plot_2d(MCSamples, 'omega_m', 'S12', filled=[False, False, False, False, True, True],
                colors=colors,
                lws=[1, 1, 1, 1, None, None],
                ls=['-', '-', '-', '--', None, None]
                )
g.add_legend(labels, legend_ncol=2, legend_loc='lower center')

# %% hidden=true
# All surveys (lmax = 4500)
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
          'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = ['DESY3', 'KiDS-1000', 'HSCDR1', 'DESY3 + KiDS-1000', 'DESY3 + KiDS-1000 + HSCDR1', 'Planck 2018']
colors = ['blue', 'orange', 'red', 'green', 'brown', 'black']

g = plots.get_single_plotter(width_inch=6)
g.plot_2d(MCSamples, 'Omega_m', 'S8', filled=[False, False, False, False, True, True],
                colors=colors,
                lws=[1, 1, 1, 1, None, None],
                ls=['-', '-', '-', '--', None, None]
                )
g.add_legend(labels, legend_ncol=2, legend_loc='lower center')El

# %% hidden=true
# All surveys (lmax = 4500)
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
          'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange', 'red', 'green', 'brown', 'black']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, False, True, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[1, 1, 1, 1, None, None],
                contour_ls=['-', '-', '-', '--', None, None]
                )

# %% hidden=true
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


### Full parameter space
# parameters = results[chains[0]]['MCSamples'].getParamNames().list()
# parameters.remove('minuslogprior__0')
# parameters.remove('chi2__cl_like.ClLike')

# g = plots.get_subplot_plotter()
# g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
#                 contour_colors=colors, legend_labels=labels,
#                 contour_lws=[None, 1, 1, 1])

# %% hidden=true
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
         ]

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors = ['blue', 'red', 'green']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %%
s = results['hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300']['MCSamples']
p = s.getParams()
for i in range(4):
    s.addDerived(eval(f"p.limber_wl_{i}_dz"), f"limber_HSCDR1wl__{i}_dz", r"\Delta z^{HSC_{%d}" % i)
s.addDerived(p.bias_A_IA, 'bias_HSCDR1wl_A_IA', 'A^{\rm HSCDR1}_{\rm IA}')
s.addDerived(p.limber_eta_IA, 'limber_HSCDR1wl_eta_IA', '\eta^{\rm HSCDR1}_{\rm IA}')

# s = results['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG']['MCSamples']
# p = s.getParams()
# s.addDerived(p.bias_A_IA, 'bias_DESY3wl_A_IA', 'A^{\rm DESY3}_{\rm IA}')
# s.addDerived(p.limber_eta_IA, 'limber_DESY3wl_eta_IA', '\eta^{\rm DESY3}_{\rm IA}')

# s = results['k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG']['MCSamples']
# p = s.getParams()
# s.addDerived(p.bias_A_IA, 'bias_KiDS1000_A_IA', 'A^{\rm KiDS-1000}_{\rm IA}')
# s.addDerived(p.limber_eta_IA, 'limber_KiDS1000_eta_IA', '\eta^{\rm KiDS-1000}_{\rm IA}')

# %%
# Check z-shifts
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'priors'
         ]

s = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples']
pn = s.getParamNames().list()
parameters = [k for k in pn if ('_dz' in k)]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors = ['black', 'blue', 'red', 'yellow', 'purple']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1, 2])

# %%
# Check z-shifts
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
         ]

s = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples']
pn = s.getParamNames().list()
parameters = [k for k in pn if ('IA' in k)]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors = ['black', 'blue', 'red', 'yellow']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=input_params)

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          # 'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'Asgari+20',
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'bias_KiDS1000_A_IA']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'black']

g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], marker={f'limber_KiDS1000__{i}_dz': 0 for i in range(5)})

# %%

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## vs DES, KiDS & HSC (lmax = 8192)

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### No-Baryons

# %% hidden=true
chains = ['hsc_nobaryons_nocuts_nla_nside4096_lmin300',
          'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
         ]

for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('Omega_m'))

# %% hidden=true
# All surveys (lmax = 8192)
chains = [
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmin100_GNG',
          'hsc_nobaryons_nocuts_nla_nside4096_lmin300',
          'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG', 
          # 'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange', 'red', 'green', 'brown', 'black']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, False, True, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[1, 1, 1, 1, None, None],
                contour_ls=['-', '-', '-', '--', None, None]
                )

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
          'hsc_nobaryons_nocuts_nla_nside4096_lmin300'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


### Full parameter space
# parameters = results[chains[0]]['MCSamples'].getParamNames().list()
# parameters.remove('minuslogprior__0')
# parameters.remove('chi2__cl_like.ClLike')

# g = plots.get_subplot_plotter()
# g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
#                 contour_colors=colors, legend_labels=labels,
#                 contour_lws=[None, 1, 1, 1])

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### Baryons

# %% hidden=true
chains = ['hsc_baryons_nocuts_nla_nside4096_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
         ]
for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('M_c'))
    
print()

for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('Omega_m'))

# %% hidden=true
# All surveys (lmax = 8192)
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG', 
          # 'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange', 'red', 'green', 'brown', 'black']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, False, True, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[1, 1, 1, 1, None, None],
                contour_ls=['-', '-', '-', '--', None, None]
                )

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmin300'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


### Full parameter space
# parameters = results[chains[0]]['MCSamples'].getParamNames().list()
# parameters.remove('minuslogprior__0')
# parameters.remove('chi2__cl_like.ClLike')

# g = plots.get_subplot_plotter()
# g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
#                 contour_colors=colors, legend_labels=labels,
#                 contour_lws=[None, 1, 1, 1])

# %% hidden=true
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmin300'
         ]

parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors = ['blue', 'red', 'green']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons with BBN prior (lmax = 4500)

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BBNprior',
          'P18_official'
#           'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18',
#           'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'S12', 'omega_m', 'Omega_b', 'omega_b', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# labels = ['Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['blue', 'red', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BBNprior',
          'P18_official'
#           'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18',
#           'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'Omega_b', 'omega_b', 'M_c', 'baryon_fraction']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# labels = ['Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['blue', 'red', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BBNprior',
          'P18_official'
#           'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18',
#           'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# labels = ['Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['blue', 'red', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Baryons with H0 prior (lmax = 4500)

# %%
def get_tension_with_Planck_Omegam(samples):
    p = results['P18_official']['MCSamples'].getParams()
    w = results['P18_official']['MCSamples'].weights
    mean_Planck = np.average(p.Omega_m, weights=w)
    cov_Planck = np.cov(p.Omega_m, aweights=w)
        
    p = samples.getParams()
    w = samples.weights
    mean = np.average(p.Omega_m, weights=w)
    cov = np.cov(p.Omega_m, aweights=w)
    # Table 2 of https://arxiv.org/pdf/1807.06209.pdf
    # with lensing: 0.832 ± 0.013
    # w/o lensing: 0.834 ± 0.016
    # return (0.832 - mean) / np.sqrt((0.013**2 + cov))
    return (mean_Planck - mean) / np.sqrt(cov_Planck  + cov)


# %%
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'P18_official'
         ]
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('h'))

# %%
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BBNprior',
          'P18_official'
         ]

# for c in chains[:-1]:
#     # print(c, get_tension_with_Planck_Omegam(results[c]['MCSamples']))
#     print(c, 'chi2bf = ', results[c]['chi2bf'], 'pvalue = ', results[c]['pvalue_bf'])


parameters = ['Omega_m', 'S8', 'sigma8', 'S12', 'Omega_b', 'omega_b', 'omega_m', 'h', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# labels = ['Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['moccasin', 'red', 'green', 'purple', 'black'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1],
                contour_ls=['-', '-', '--', '-', '-'])

# %%
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'P18_official'
         ]

# for c in chains[:-1]:
#     # print(c, get_tension_with_Planck_Omegam(results[c]['MCSamples']))
#     print(c, 'chi2bf = ', results[c]['chi2bf'], 'pvalue = ', results[c]['pvalue_bf'])


parameters = ['Omega_m', 'S8', 'sigma8', 'S12', 'Omega_b', 'omega_b', 'omega_m', 'h', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# labels = ['Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['moccasin', 'red', 'green', 'black'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1],
                contour_ls=['-', '-', '--', '-'])


# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Baryons with BAO lkl (lmax = 4500)

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### Contours

# %%
def get_pvalue(chi2, ndata):
    return 1 - stats.chi2.cdf(chi2, df=ndata - 2)

def get_tension_with_Planck(samples):
    p = samples.getParams()
    w = samples.weights
    mean = np.average(p.S8, weights=w)
    cov = np.cov(p.S8, aweights=w)
    # Table 2 of https://arxiv.org/pdf/1807.06209.pdf
    # with lensing: 0.832 ± 0.013
    # w/o lensing: 0.834 ± 0.016
    # print(mean, samples.getInlineLatex('S8'))
    return (0.832 - mean) / np.sqrt((0.013**2 + cov))

def print_chi2(chi2_Cell, chi2_BAO):
    return ('chi2_Cell =', chi2_Cell, f'(pvalue = {get_pvalue(chi2_Cell, ndata):.3f})',
           'chi2_BAO =', chi2_BAO, f'(pvalue = {get_pvalue(chi2_BAO, 8 + 2):.3f})',
           'chi2_total = ', chi2_Cell + chi2_BAO, f'(pvalue = {get_pvalue(chi2_Cell + chi2_BAO, ndata + 8):.3f})')

r = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl']
ndata = r['ndata']

pars_bf = {}
pars_bf['Cell'] = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['pars_bf'].copy()

# Doing it manually since the results dict looks for the min chi2 of only the Cell lkl...
chain = results['BAOlkl']['MCSamples']
pars = chain.getParams()
pars_bf['BAO'] = chain.getParamSampleDict(pars.chi2__cl_like.BAOLike.argmin())

chain = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl']['MCSamples']
pars = chain.getParams()
pars_bf['both'] = chain.getParamSampleDict(pars.chi2.argmin())

# Note that we have 8 points for the BAO and I'm adding 2 to avoid substracting 2 for the dof.
print('BF to Cell:', print_chi2(*r.evaluate_model(pars_bf['Cell'])))
# print('BF to BAO:',  print_chi2(*r.evaluate_model(pars_bf['BAO'])))
print('BF to both:',  print_chi2(*r.evaluate_model(pars_bf['both'])))

print()
s = r['MCSamples']
print('Tension with planck in S8: ', get_tension_with_Planck(s))

# %%
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl',
          'BAOlkl',
          'P18_official'
         ]

# for c in chains[:-1]:
#     # print(c, get_tension_with_Planck_Omegam(results[c]['MCSamples']))
#     print(c, 'chi2bf = ', results[c]['chi2bf'], 'pvalue = ', results[c]['pvalue_bf'])


parameters = ['Omega_m', 'S8', 'sigma8', 'S12', 'Omega_b', 'omega_b', 'omega_m', 'h', 'n_s', 'm_nu', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# labels = ['Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['moccasin', 'red', 'green', 'purple', 'gray', 'black'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1, 1],
                contour_ls=['-', '-', '--', '-', '--'])

# %%
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl',
          'priors_chain',
          'P18_official'
         ]

# for c in chains[:-1]:
#     # print(c, get_tension_with_Planck_Omegam(results[c]['MCSamples']))
#     print(c, 'chi2bf = ', results[c]['chi2bf'], 'pvalue = ', results[c]['pvalue_bf'])


parameters = ['Omega_m', 'S8', 'sigma8', 'S12', 'Omega_b', 'omega_b', 'omega_m', 'h', 'n_s', 'm_nu', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# labels = ['Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['moccasin', 'red', 'green', 'purple', 'gray', 'black'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1, 1],
                contour_ls=['-', '-', '--', '-', '--'])

# %%
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl',
          'P18_official'
         ]

# for c in chains[:-1]:
#     # print(c, get_tension_with_Planck_Omegam(results[c]['MCSamples']))
#     print(c, 'chi2bf = ', results[c]['chi2bf'], 'pvalue = ', results[c]['pvalue_bf'])


parameters = ['Omega_m', 'S8', 'sigma8', 'S12', 'Omega_b', 'omega_b', 'omega_m', 'h', 'n_s', 'm_nu', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# labels = ['Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['moccasin', 'red', 'green', 'purple', 'black'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1],
                contour_ls=['-', '-', '--', '-'])

# %%
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl',
          'P18_official'
         ]

for c in chains[:-1]:
    # print(c, get_tension_with_Planck_Omegam(results[c]['MCSamples']))
    print(c, 'chi2bf = ', results[c]['chi2bf'], 'pvalue = ', results[c]['pvalue_bf'])


parameters = ['Omega_m', 'S8', 'sigma8', 'S12', 'h',  'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# labels = ['Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['moccasin', 'red', 'green', 'purple', 'black'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1],
                contour_ls=['-', '-', '--', '-'])

# %%
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl',
          'P18_official'
         ]

for c in chains[:-1]:
    # print(c, get_tension_with_Planck_Omegam(results[c]['MCSamples']))
    print(c, 'chi2bf = ', results[c]['chi2bf'], 'pvalue = ', results[c]['pvalue_bf'])


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# labels = ['Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['moccasin', 'red', 'green', 'purple', 'black'] # [results[i]['color'] for i in chains]

pn = MCSamples[0].getParamNames().list()
parameters = [k for k in pn if ('_dz' in k)]


g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1],
                contour_ls=['-', '-', '--', '-'])

# %%
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl',
          'P18_official'
         ]

for c in chains[:-1]:
    # print(c, get_tension_with_Planck_Omegam(results[c]['MCSamples']))
    print(c, 'chi2bf = ', results[c]['chi2bf'], 'pvalue = ', results[c]['pvalue_bf'])


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# labels = ['Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['moccasin', 'red', 'green', 'purple', 'black'] # [results[i]['color'] for i in chains]

pn = MCSamples[0].getParamNames().list()
parameters = [k for k in pn if ('_IA' in k)]


g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1],
                contour_ls=['-', '-', '--', '-'])

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### $C_\ell$

# %%
survey = 'DESY3'
nbin = 4

#######
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl'
         ]

sdata = results[chains[0]].get_cl_data_sacc()

sbf = results[chains[0]].get_cl_theory_sacc_bf()
sbf_nb = results[chains[1]].get_cl_theory_sacc_bf()

f, ax = plt.subplots(nbin, nbin, figsize=(9.6, 8), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in sdata.get_tracer_combinations():
    if survey not in ''.join(trs):
        continue
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])

    # Data
    ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8)    

    # BF No-Baryons
    ell, cl = sbf_nb.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='blue', label='BF (WL + BAO)', alpha=0.8)

    # BF baryons
    ell, cl = sbf.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='orange', label='BF (Baryons)', alpha=0.8)

    t = ax[i, j].text(0.99, 0.94, f'({i}, {j})', horizontalalignment='right',
                      verticalalignment='top', transform=ax[i, j].transAxes, fontsize=12) 
    
    if i != j:
        ax[j, i].axis('off')

for i in range(nbin):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")

t = ax[0, 2].text(0.95, 0.95, f'{survey}', horizontalalignment='right',
                  verticalalignment='top', transform=ax[0, 2].transAxes, fontsize=18) 
ax[0, 0].legend(bbox_to_anchor=(1., 1.05))

plt.show()
plt.close()

# %%
survey = 'KiDS1000'
nbin = 5

#######
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl'
         ]

sdata = results[chains[0]].get_cl_data_sacc()

sbf = results[chains[0]].get_cl_theory_sacc_bf()
sbf_nb = results[chains[1]].get_cl_theory_sacc_bf()

f, ax = plt.subplots(nbin, nbin, figsize=(12, 10), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in sdata.get_tracer_combinations():
    if survey not in ''.join(trs):
        continue
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])

    # Data
    ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8)    

    # BF No-Baryons
    ell, cl = sbf_nb.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='blue', label='BF (WL + BAO)', alpha=0.8)

    # BF baryons
    ell, cl = sbf.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='orange', label='BF (Baryons)', alpha=0.8)

    t = ax[i, j].text(0.99, 0.94, f'({i}, {j})', horizontalalignment='right',
                      verticalalignment='top', transform=ax[i, j].transAxes, fontsize=12) 
    
    if i != j:
        ax[j, i].axis('off')

for i in range(nbin):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")

t = ax[0, 2].text(0.95, 0.95, f'KiDS-1000', horizontalalignment='right',
                  verticalalignment='top', transform=ax[0, 2].transAxes, fontsize=18) 
ax[0, 0].legend(bbox_to_anchor=(1., 1.05))

plt.show()
plt.close()

# %%
survey = 'HSC'
nbin = 4

#######
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl'
         ]

sdata = results[chains[0]].get_cl_data_sacc()

sbf = results[chains[0]].get_cl_theory_sacc_bf()
sbf_nb = results[chains[1]].get_cl_theory_sacc_bf()

f, ax = plt.subplots(nbin, nbin, figsize=(9.6, 8), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in sdata.get_tracer_combinations():
    if survey not in ''.join(trs):
        continue
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])

    # Data
    ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8)    

    # BF No-Baryons
    ell, cl = sbf_nb.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='blue', label='BF (WL + BAO)', alpha=0.8)

    # BF baryons
    ell, cl = sbf.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='orange', label='BF (Baryons)', alpha=0.8)

    t = ax[i, j].text(0.99, 0.94, f'({i}, {j})', horizontalalignment='right',
                      verticalalignment='top', transform=ax[i, j].transAxes, fontsize=12) 
    
    if i != j:
        ax[j, i].axis('off')

for i in range(nbin):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")

t = ax[0, 2].text(0.95, 0.95, f'HSCDR1', horizontalalignment='right',
                  verticalalignment='top', transform=ax[0, 2].transAxes, fontsize=18) 
ax[0, 0].legend(bbox_to_anchor=(1., 1.05))

plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Baryons with BAO + H0 prior (lmax = 4500)

# %%
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess_BAOlkl',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck_BAOlkl',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl',
          # 'BAOlkl',
          'P18_official',
          'priors_chain'
         ]

# for c in chains[:-1]:
#     # print(c, get_tension_with_Planck_Omegam(results[c]['MCSamples']))
#     print(c, 'chi2bf = ', results[c]['chi2bf'], 'pvalue = ', results[c]['pvalue_bf'])


parameters = ['Omega_m', 'S8', 'sigma8', 'S12', 'Omega_b', 'omega_b', 'omega_m', 'h', 'n_s', 'm_nu', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# labels = ['Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['moccasin', 'red', 'green', 'purple',  'black', 'gray'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1],
                contour_ls=['-', '-', '--', '-', '--'])

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Baryons with BAO + BBN prior

# %%
s_bao_bbn = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl']['MCSamples'].copy()
p = s_bao_bbn.getParams()

# Planck18 (1807.06209) after Eq. 73: "conservative BBN prior" based on the
# primordial deuterium abundance measurements of Cooke et al 2018
mean = 0.0222
sigma = 0.0005

weights = 0.5 * (p.omega_b - mean)**2 / sigma**2
s_bao_bbn.reweightAddingLogLikes(weights)

# %%
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess_BAOlkl',
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck_BAOlkl',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl',
          'P18_official',
          # 'priors_chain'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'S12', 'Omega_b', 'omega_b', 'omega_m', 'h', 'n_s', 'm_nu', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# labels = ['Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['moccasin', 'red', 'green', 'purple',  'black', 'gray'] # [results[i]['color'] for i in chains]

MCSamples.append(s_bao_bbn)
labels.append('All WL + BAO + BBN prior')

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1],
                contour_ls=['-', '-', '--', '-', '--'])

# %%
# No Baryons with ell_max=8192
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl',    
          'P18_official_mnu'
         ]

MCSamples = [results[i]['MCSamples'] for i in chains] + [s_bao_bbn]
labels = ['DESY3 + KiDS-1000 + HSCDR1', '$H_0$ prior (SNe)', '$H_0$ prior (Planck)', 'BAO prior', 'Planck 2018'] + ['BAO + BBN priors']
colors = ['brown', 'blue', 'gold', 'green', 'black'] + ['cyan']
filled=[True, False, False, False, True] + [False]
line_w=[1, 1, 1, 1, None] + [2]
line_s=['-', '-', '-', '--', None] + [':']

# parameters = ['Omega_m', 'S8', 'sigma8', 'S12', 'Omega_b', 'omega_b', 'omega_m', 'h', 'n_s', 'm_nu', 'M_c']
parameters = ['Omega_m', 'S8', 'sigma8', 'S12', 'omega_m', 'h', 'omega_b']


g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=filled,
                contour_colors=colors, legend_labels=labels,
                contour_lws=line_w,
                contour_ls=line_s)

# %%
s_bao_bbn.getInlineLatex('h')

# %%
get_shift_mcsamples(s_bao_bbn, results['P18_official_mnu']['MCSamples'], 'h')

# %%
# All surveys (lmax = 4500)
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl',    
          'P18_official_mnu'
         ]

MCSamples = [results[i]['MCSamples'] for i in chains] + [s_bao_bbn]
labels = ['DESY3 + KiDS-1000 + HSCDR1', '$H_0$ prior (SNe)', '$H_0$ prior (Planck)', 'BAO prior', 'Planck 2018'] + ['BAO + BBN priors']
colors = ['brown', 'blue', 'gold', 'green', 'blue', 'gold', 'black'] + ['cyan']
filled=[True, False, False, False, False, False, True] + [False]
line_w=[1, 1, 1, 1, 1, 1, None] + [2]
line_s=['-', '-', '-', '--', '--', '--', None] + [':']

# Plot
g = plots.get_single_plotter()
g.settings.legend_frame = False

f, ax = plt.subplots(1, 2, figsize=(9, 3),  gridspec_kw={'wspace': 0.25}, sharex='col')
ax = ax[None, :]
i = 0 

plot_roots = MCSamples[:4] + MCSamples[-2:]
plot_colors = colors[:4] + colors[-2:]
plot_filled = filled[:4] + filled[-2:]
plot_lw = line_w[:4] + line_w[-2:]
plot_ls = line_s[:4] + line_s[-2:]
    
for j in range(2):
    if j == 0:
        params = ['Omega_m', 'S8']
    else:
        params = ['omega_m', 'S12']

    g.plot_2d(plot_roots, *params,
              ax=ax[i, j], colors=plot_colors,
              filled=plot_filled, lws=plot_lw, ls=plot_ls
              )

# Place the legends
plot_labels = labels[:3]
g.add_legend(plot_labels, ax=ax[0, 0], legend_loc='upper left', frameon=False, fontsize=10)

line1 = plt.Line2D([0], [0], color=colors[3], linestyle='--', label=labels[3])
# ax[0, 1].legend(handles=[line1], loc='upper left', frameon=False)

rect = patches.Rectangle((0, 0), 1, 1, color="black", label=labels[-2])
# ax[0, 1].legend(handles=[line1, rect], loc='upper left', ncol=2, frameon=False)

line2 = plt.Line2D([0], [0], color=colors[-1], linestyle='--', label=labels[-1])
ax[0, 1].legend(handles=[line1, line2, rect], loc='upper left', ncol=2, frameon=False)

# Move the S12 label to the right
# ax[0, 1].yaxis.set_label_position("right")
# ax[0, 1].set_yticklabels(['0.75', '0.80', '0.85', '0.90'])

ax[0, 0].set_ylim([0.73, 0.93])
ax[0, 1].set_ylim([0.73, 0.93])

plt.show()


# %%

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Baryons with Planck2018 lkl (lmax = 4500)

# %%
def get_pvalue(chi2, ndata):
    return 1 - stats.chi2.cdf(chi2, df=ndata - 2)

def print_chi2(chi2_Cell, chi2_P18):
    return ('chi2_Cell =', chi2_Cell, f'(pvalue = {get_pvalue(chi2_Cell, ndata):.3f})',
           'chi2_P18 =', chi2_P18, # f'(pvalue = {get_pvalue(chi2_P18, 6 - 5):.3f})',
           'chi2_total = ', chi2_Cell + chi2_P18, f'(pvalue = {get_pvalue(chi2_Cell + chi2_P18, ndata -4):.3f})')

r = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop']
ndata = r['ndata']

pars_bf = {}
pars_bf['Cell'] = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['pars_bf'].copy()

# Doing it manually since the results dict looks for the min chi2 of only the Cell lkl...
chain = results['P18_official_mnu']['MCSamples']
bf = chain.getBestFit()
pars_bf['P18'] = bf.getParamDict()

chain = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop']['MCSamples']
pars = chain.getParams()
pars_bf['both'] = chain.getParamSampleDict(pars.chi2.argmin())

# Note that we have 8 points for the P18 and I'm adding 2 to avoid substracting 2 for the dof.
print('BF to Cell:', print_chi2(*r.evaluate_model(pars_bf['Cell'])))
# print('BF to P18:',  print_chi2(*r.evaluate_model(pars_bf['P18'])))
print('BF to both:',  print_chi2(*r.evaluate_model(pars_bf['both'])))

print()
s = r['MCSamples']
print('Tension with planck: ', get_tension_with_Planck(s))

# %%
# Baryons vs No Baryons with ell_max=1000
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop',
          'P18_official_mnu'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'S12', 'omega_m', 'h', 'n_s', 'A_sE9', 'm_nu', 'Omega_b', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['blue', 'orange', 'black']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %%
# Baryons vs No Baryons with ell_max=1000
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop',
          'P18_official_mnu'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'S12', 'omega_m', 'h', 'n_s', 'A_sE9', 'm_nu', 'Omega_b', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['blue', 'orange', 'black']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %%
# Baryons vs No Baryons with ell_max=1000
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop',
          'P18_official_mnu'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['blue', 'orange', 'black']

g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot(MCSamples, parameters, filled=[True, False, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %%
# Baryons vs No Baryons with ell_max=1000
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop',
          'P18_official_mnu'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'S12', 'omega_m', 'h', 'n_s', 'A_sE9', 'm_nu', 'Omega_b', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['blue', 'orange', 'black']

g = plots.get_single_plotter(width_inch=6)
g.plot_2d(MCSamples, 'omega_m', 'S12', filled=[True, False, True],
                colors=colors,
                lws=[None, 1, 1, 1])
g.add_legend(legend_labels=labels)


# %%
# Suspiciousness: S = <loglike>_AB - <loglike>_A - <loglike>_B
def get_mean_lkl(key, power=1):
    s = results[key]['MCSamples']
    p = s.getParams()
    lkl = -p.chi2/2
    return np.average(lkl**power, weights=s.weights)

mean_lkl_AB = get_mean_lkl('desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop')
mean_lkl_A = get_mean_lkl('desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG')
mean_lkl_B = get_mean_lkl('P18_Gauss')
lnS = mean_lkl_AB - mean_lkl_A - mean_lkl_B
print(f'mean_lkl_both = {mean_lkl_AB}')
print(f'mean_lkl_Cell = {mean_lkl_A}')
print(f'mean_lkl_B = {mean_lkl_B}')
print(f'Suspciousness = {lnS}')


# %%
# In the Gaussian case, lnS = d/2 - chi2/2. We do as DESY3 (https://arxiv.org/pdf/2207.05766.pdf) and estimate chi2_BMD = d_BMD - 2lnS
# We estimate d_BMD as d_BMD = d_A + d_B - d_AB and d = 2 ((<loglike>)^2 - <loglike^2>)
def get_dim(key):
    mean_lkl2 = get_mean_lkl(key, 2)
    mean_lkl_2 = get_mean_lkl(key, 1)**2
    return 2 * (mean_lkl2 - mean_lkl_2)

d_AB = get_dim('desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop')
d_A = get_dim('desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG')
d_B = get_dim('P18_Gauss')
d_BMD = d_A + d_B - d_AB
print(f'd_both = {d_AB}')
print(f'd_Cell = {d_A}')
print(f'd_P18 = {d_B}')
print(f'd_BMD = {d_BMD}')

chi2 = d_BMD - 2*lnS
print(f'p-value = {1 - stats.chi2.cdf(chi2, d_BMD)}')

# %%
# Using directly chi2 = d - 2lnS, and d = number of cosmological parameters
chi2 = 6 - 2*lnS
pvalue = 1 - stats.chi2.cdf(chi2, 6)
nsigma = np.sqrt(2) * scipy.special.erfcinv(pvalue)
print(f'chi2 = {chi2:.3f}')
print(f'p-value = {pvalue:.3f}')
print(f'n-sigma = {nsigma:.3f}')

# %%
# Using directly chi2 = d - 2lnS, and d = number of cosmological parameters
# lnS = -3.8919203696185414
chi2 = 1 - 2*lnS
pvalue = 1 - stats.chi2.cdf(chi2, 1)
nsigma = np.sqrt(2) * scipy.special.erfcinv(pvalue)
print(f'chi2 = {chi2:.3f}')
print(f'p-value = {pvalue:.3f}')
print(f'n-sigma = {nsigma:.3f}')

# %%
# Compute covariance of P18 cosmological paramters to use as multivariate gaussian
means = {}
covs = {}
post_pnames = ['A_sE9', 'Omega_m', 'Omega_b', 'm_nu', 'n_s', 'h']

chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']
for chain in chains:
    s = results[chain]['MCSamples']
    pnames = s.getParamNames().list()
    ixs = [pnames.index(pn) for pn in post_pnames]
    covs[chain] = s.getCov(pars=ixs)
    means[chain] = s.getMeans(ixs)

delta = means[chains[0]] - means[chains[1]]
icov = np.linalg.inv(covs[chains[0]] + covs[chains[1]])
chi2 = delta.dot(icov).dot(delta)
print(f"chi2 = (mean_A - mean_B) (cov_A + cov_B)^1 (mean_A - mean_B) = {chi2}")
print(f"Suspiciousness: log(S) = d/2 - chi2/2 = {len(post_pnames)/2 - chi2/2:.2f}")

pvalue = 1 - stats.chi2.cdf(chi2, 6)
nsigma = np.sqrt(2) * scipy.special.erfcinv(pvalue)
print(f'p-value = {pvalue:.3f}')
print(f'n-sigma = {nsigma:.3f}')

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons with fixed cosmology (lmax = 4500)

# %% hidden=true
chains = ['Arico+23',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF'
]
for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('M_c'))
    
for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('eta'))

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18_OmBF',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF'
         ]

parameters = ['M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = ['Free cosmology', 'Planck18 cosmology', r'Planck18 cosmology ($\Omega_m$ from BF)', 'BF cosmology']
colors = ['blue', 'red', 'green', 'brown'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %%
results[c].keys()

# %%
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF']

colors = ['blue', 'orange', 'black']
labels = ['DESY3 + KiDS-1000 + HSC-DR1 ($\ell < 4500$) + Fix cosmo BF']

f, ax = plt.subplots()
for c in chains:
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    label = dict(zip(chains, labels))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color)
    results[c]['boost_with_fixedcosmoP18'].plot_baryon_post(ax, 'with P18 cosmology', 'red')


ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$')
ax.set_ylabel('$S_k(a=1)$')
ax.legend(loc='lower left') # center', bbox_to_anchor=(0.5, 1.31),)
plt.show()
plt.close()

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons with fixed cosmology (lmax = 8192)

# %% hidden=true
chains = ['Arico+23',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoBF'
]
for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('M_c'))
    
for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('eta'))

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoBF'
         ]

parameters = ['M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = ['Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['blue', 'red', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['Arico+23',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoBF'
         ]

parameters = ['M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = ['Arico+23', 'Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['blue', 'red', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoBF'
         ]

sdata = results[chains[0]].get_cl_data_sacc()

for survey in ['DES', 'KiDS', 'HSC']:
    nbin = 5 if survey == 'KiDS' else 4
    f, ax = plt.subplots(nbin, nbin, figsize=(14, 14), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
    for trs in sdata.get_tracer_combinations():
        i = int(trs[1].split('__')[1])
        j = int(trs[0].split('__')[1])
        
        if survey not in trs[0]:
            continue

        # Data
        ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
        err = np.sqrt(np.diag(cov))
        ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8)    

        # BF
        for c in chains:
            s = results[c]['bf_sacc']
            label = {chains[0]: 'Cosmology free', chains[1]: 'Planck18 cosmology', chains[2]: 'BF cosmology'}[c]
            color = {chains[0]: 'blue', chains[1]: 'orange', chains[2]: 'brown'}[c]
            ell, cl = s.get_ell_cl('cl_ee', trs[0], trs[1])
            ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color=color, label=label, alpha=0.8)


    for i in range(nbin):
        ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
        ax[-1, i].set_xlabel("$\ell$")
        ax[0, i].set_xscale("log")
        ax[0, 0].legend()

    f.suptitle(f'{survey}', fontsize=16)
    plt.tight_layout()
    plt.show()
    plt.close()

# %% hidden=true
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoBF'
         ]

labels = ['No Baryons', 'Cosmology free', 'Fixed cosmology (Planck18)', 'Fixed cosmology (BF)']
colors = ['blue', 'orange', 'brown', 'red']
linestyles = ['-', '--', '-.', ':']

k = np.logspace(-2, 0.4)
f, ax = plt.subplots(1, 1)

for c in chains:
    pk2d = results[c]['pk_data_bf']['pk_ww']
    
    label = dict(zip(chains, labels))[c]
    color = dict(zip(chains, colors))[c]
    ls = dict(zip(chains, linestyles))[c]
    
    plt.loglog(k, pk2d.eval(k, 1), color=color, label=label, ls=ls)

plt.legend()
plt.tight_layout()
plt.show()
plt.close()

# %% hidden=true
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoBF'
         ]

labels = ['No Baryons', 'Cosmology free', 'Fixed cosmology (Planck18)', 'Fixed cosmology (BF)']
colors = ['blue', 'orange', 'brown', 'red']
linestyles = ['-', '--', '-.', ':']

k = np.logspace(-2, 0.4, 100)
f, ax = plt.subplots(1, 1)

pk2d_A = results[chains[0]]['pk_data_bf']['pk_ww'].eval(k, 1)
pk2d_B = results[chains[1]]['pk_data_bf']['pk_ww'].eval(k, 1)
    
plt.semilogx(k, pk2d_B/pk2d_A)

plt.legend()
plt.tight_layout()
plt.show()
plt.close()

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Baryons emulator vs ST15 (CCL BLCM)

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### lmax=4500

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %%
s = results['desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'].copy()
s2 = s.copy()
p = s.getParams()
sel = p.M_c < 12
weights = np.zeros_like(p.M_c)
weights[sel] = np.inf
s.reweightAddingLogLikes(weights)

s3 = s.copy()
p = s3.getParams()
sel = p.M_c < 12
weights = np.zeros_like(p.M_c)
weights[sel] = np.inf
sel = p.eta >= 0
weights[sel] = np.inf
s3.reweightAddingLogLikes(weights)

sOurs = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'].copy()
sOurs_cut = sOurs.copy()
p = sOurs_cut.getParams()
sel = p.M_c < 12
weights = np.zeros_like(p.M_c)
weights[sel] = np.inf
sOurs_cut.reweightAddingLogLikes(weights)

parameters = ['Omega_m', 'S8', 'Omega_b', 'M_c', 'eta']


g = plots.get_subplot_plotter(width_inch=12)
g.triangle_plot([s, s3, s2, sOurs, sOurs_cut], parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=['cut', 'cut on Mc and eta', 'full ST15', 'Fiducial', 'Fiducial (cut)'],
                contour_lws=[None, 1, 1, 1])

# %%
b = results['desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['boost']

ST15 = results['desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'].copy()

p = ST15.getParams()
sel = p.M_c < 12
weights = np.zeros_like(p.M_c)
weights[sel] = np.inf
sel = p.eta >= 0
weights[sel] = np.inf
ST15.reweightAddingLogLikes(weights)

bST15 = b.get_MCSamples_baryons()
bST15.reweightAddingLogLikes(weights)

f, ax = plt.subplots()
b.plot_baryon_post(ax, 'ST15', color, alpha=0.3)
b.set_getdist_1D(True)
b.plot_baryon_post(ax, 'GetDist', 'yellow', alpha=0.5)
print(b.InlineLatexList)
b.set_getdist_1D(False)

b = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['boost']
b.plot_baryon_post(ax, 'Fiducial', 'brown', alpha=0.3)



plt.legend()
ax.axhline(1, ls='--', color='gray')
plt.xlabel('k [1/Mpc]')
plt.ylabel('Sk')
plt.show()
plt.close()

# %%
g = plots.get_single_plotter()
g.plot_1d(bST15, 'Sk0p91')

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### lmax=8192

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Amon-Efstathiou

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### lmax=4500

# %% hidden=true
chains = ['desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
]
for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('A_AE'))

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'A_AE']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red', 'black'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### lmax=8192

# %% hidden=true
chains = ['desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_halofit_GNG',
]
for key in chains:
    print(key, results[key]['MCSamples'].getInlineLatex('A_AE'))

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_halofit_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'A_AE']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red', 'black'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
chains = ['desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_halofit_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

sdata = results[chains[0]].get_cl_data_sacc()

for survey in ['DES', 'KiDS', 'HSC']:
    nbin = 5 if survey == 'KiDS' else 4
    f, ax = plt.subplots(nbin, nbin, figsize=(14, 14), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
    for trs in sdata.get_tracer_combinations():
        i = int(trs[1].split('__')[1])
        j = int(trs[0].split('__')[1])
        
        if survey not in trs[0]:
            continue

        # Data
        ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
        err = np.sqrt(np.diag(cov))
        ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8)    

        # BF
        for c in chains:
            s = results[c]['bf_sacc']
            label = {chains[0]: 'A-E', chains[1]: 'A-E (halofit)', chains[2]: 'BaccoEmu'}[c]
            color = {chains[0]: 'blue', chains[1]: 'orange', chains[2]: 'brown'}[c]
            ell, cl = s.get_ell_cl('cl_ee', trs[0], trs[1])
            ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color=color, label=label, alpha=0.8)


    for i in range(nbin):
        ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
        ax[-1, i].set_xlabel("$\ell$")
        ax[0, i].set_xscale("log")
        ax[0, 0].legend()

    f.suptitle(f'{survey}', fontsize=16)
    plt.tight_layout()
    plt.show()
    plt.close()

# %% hidden=true
chains = ['desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_halofit_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]


labels = ['A-E', 'A-E (halofit)', 'Baccoemu']
colors = ['blue', 'orange', 'brown']
linestyles = ['-', '--', '-.']

k = np.logspace(-2, 0.4)
f, ax = plt.subplots(1, 1)

for c in chains:
    pk2d = results[c]['pk_data_bf']['pk_ww']
    
    label = dict(zip(chains, labels))[c]
    color = dict(zip(chains, colors))[c]
    ls = dict(zip(chains, linestyles))[c]
    
    plt.loglog(k, pk2d.eval(k, 1), color=color, label=label, ls=ls)

plt.legend()
plt.tight_layout()
plt.show()
plt.close()

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### Comparison

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'A_AE']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red', 'black']

g = plots.get_subplot_plotter(width_inch=10)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], contour_ls=[None, '-.', '--'])

# %% hidden=true
# Plot the Cell for the two chains above
chains = [ 'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
           'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG']

sdata = results[chains[1]].get_cl_data_sacc()
saccs = {c: results[c]['bf_sacc'] for c in chains}
colors = {c: l  for c, l in zip(chains,  ['blue', 'red'])}
labels = {c: l  for c, l in zip(chains, ['A-E (lmax=4500)', 'A-E (lmax=8192)'])}

for survey in ['DES', 'KiDS', 'HSC']:
    nbin = 5 if survey == 'KiDS' else 4
    f, ax = plt.subplots(nbin, nbin, figsize=(14, 8), sharex='col', sharey='row',
                         gridspec_kw={'hspace':0, 'wspace':0})
    for trs in sdata.get_tracer_combinations():
        i = int(trs[1].split('__')[1])
        j = int(trs[0].split('__')[1])
        
        if survey not in trs[0]:
            continue
        
        # Data
        ell, cl, cov = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True)
        err = np.sqrt(np.diag(cov))
        ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8)
        
        # Chains
        for c in chains:
            ell, cl  = saccs[c].get_ell_cl('cl_ee', trs[0], trs[1])
            ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.',
                              color=colors[c], label=labels[c], alpha=0.8)
            
    for i in range(nbin):
        ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
        ax[-1, i].set_xlabel("$\ell$")
        ax[0, i].set_xscale("log")
        h, l = ax[0, 0].get_legend_handles_labels()
        ax[0, 1].legend(h, l, loc="upper left")
        
    f.suptitle(f'{survey}', fontsize=16)
    plt.show()
    plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Extrapolation

# %%
# No Baryons with ell_max=2048
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          # 'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
# colors[-1] = 'blue'

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


# %% [markdown] jp-MarkdownHeadingCollapsed=true toc-hr-collapsed=true
# # LSST mock data

# %%
########## MOCK DATA ###########
# Baryons (4096) lmax=4500 (EuclidEmulator2 Pkmm + BAHAMAS Sk from baccoemu, Gaussian realization)
key = "lsst_baryons_nocuts_nla_nside4096_lmax4500_lmin20_mockEuclidEmulator2_BAHAMAS"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'LSST-like with Baryons, Mock Data ($\ell < 4500$)', 'purple', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (EuclidEmulator2 Pkmm + BAHAMAS Sk from baccoemu)
key = "lsst_baryons_nocuts_nla_nside4096_lmax4500_lmin20_mockEuclidEmulator2_BAHAMAS_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'LSST-like with Baryons,  Mock Data noiseless ($\ell < 4500$)', 'pink', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=8192 (EuclidEmulator2 Pkmm + BAHAMAS Sk from baccoemu, Gaussian realization)
key = "lsst_baryons_nocuts_nla_nside4096_lmax8192_lmin20_mockEuclidEmulator2_BAHAMAS"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'LSST-like with Baryons, Mock Data ($\ell < 8192$)', 'blue', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=8192 (EuclidEmulator2 Pkmm + BAHAMAS Sk from baccoemu)
key = "lsst_baryons_nocuts_nla_nside4096_lmax8192_lmin20_mockEuclidEmulator2_BAHAMAS_noiseless"
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'LSST-like with Baryons,  Mock Data noiseless ($\ell < 8192$)', 'red', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# %%
# Parameters used to generate the power spectrum and baryonic boost
cosmopars = {
    'omega_cold'    :  0.31,
    'omega_baryon'  :  0.045,
    'sigma8_cold'   :  0.82,
    'ns'            :  0.965,
    'hubble'        :  0.7,
    'neutrino_mass' :  0.06,
    'w0'            : -1,
    'wa'            :  0,
    'M_c'           :  13.58,
    'eta'           : -0.27,
    'beta'          : -0.33,
    'M1_z0_cen'     :  12.04,
    'theta_out'     :  0.25,
    'theta_inn'     : -0.86,
    'M_inn'         :  13.4
}

cosmopars['omega_matter'] = cosmopars['omega_cold'] + cosmopars['neutrino_mass'] / 93.14 / cosmopars['hubble']**2
cosmopars['A_s'] = 1.8753844640255685e-09
cosmopars['sigma8_tot'] = 0.8163992592396454

input_params = {'Omega_c': cosmopars['omega_cold'] - cosmopars['omega_baryon'],
                'Omega_m': cosmopars['omega_matter'],
                'Omega_b': cosmopars['omega_baryon'],
                'sigma8': cosmopars['sigma8_tot'],
                'S8': cosmopars['sigma8_tot'] * np.sqrt(cosmopars['omega_matter'] / 0.3),
                'A_s': cosmopars['A_s'],
                'h': cosmopars['hubble'],
                'omega_m': cosmopars['omega_matter'] * cosmopars['hubble']**2,
                'limber_LSST__0_dz': 0,
                'limber_LSST__1_dz': 0,
                'limber_LSST__2_dz': 0, 
                'limber_LSST__3_dz': 0,
                'limber_LSST__4_dz': 0,
                'bias_LSST__0_m': 0,
                'bias_LSST__1_m': 0,
                'bias_LSST__2_m': 0, 
                'bias_LSST__3_m': 0,
                'bias_LSST__4_m': 0,
                'bias_LSST_A_IA': 0,
                'limber_LSST_eta_IA': 0,
               }
input_params.update({k: cosmopars[k] for k in ['M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_out', 'theta_inn', 'M_inn']})

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Contours

# %%
chains = [
          'lsst_baryons_nocuts_nla_nside4096_lmax4500_lmin20_mockEuclidEmulator2_BAHAMAS',
          'lsst_baryons_nocuts_nla_nside4096_lmax4500_lmin20_mockEuclidEmulator2_BAHAMAS_noiseless',
          'lsst_baryons_nocuts_nla_nside4096_lmax8192_lmin20_mockEuclidEmulator2_BAHAMAS',
          'lsst_baryons_nocuts_nla_nside4096_lmax8192_lmin20_mockEuclidEmulator2_BAHAMAS_noiseless',
         ]

print('p-value = ', results[chains[0]]['pvalue_bf'])
print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])

parameters = ['Omega_m', 'S8', 'sigma8', 'h', 'omega_m',  'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_ls=['-', '--', '-', '--'],
                contour_lws=[None, 1, 1, 1], markers=input_params)

# %%
chains = [
          'lsst_baryons_nocuts_nla_nside4096_lmax4500_lmin20_mockEuclidEmulator2_BAHAMAS',
          'lsst_baryons_nocuts_nla_nside4096_lmax4500_lmin20_mockEuclidEmulator2_BAHAMAS_noiseless',
          'lsst_baryons_nocuts_nla_nside4096_lmax8192_lmin20_mockEuclidEmulator2_BAHAMAS',
          'lsst_baryons_nocuts_nla_nside4096_lmax8192_lmin20_mockEuclidEmulator2_BAHAMAS_noiseless',
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_ls=['-', '--', '-', '--'],
                contour_lws=[None, 1, 1, 1], markers=input_params)

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## $C_\ell$

# %%
survey = 'LSST'
nbin = 5

#######
chains = ['lsst_baryons_nocuts_nla_nside4096_lmax4500_lmin20_mockEuclidEmulator2_BAHAMAS',
          'lsst_baryons_nocuts_nla_nside4096_lmax4500_lmin20_mockEuclidEmulator2_BAHAMAS_noiseless'
         ]

sdata = results[chains[0]].get_cl_data_sacc()
sdata_nn = results[chains[1]].get_cl_data_sacc()


sbf = results[chains[0]].get_cl_theory_sacc_bf()
sbf_nn = results[chains[1]].get_cl_theory_sacc_bf()

f, ax = plt.subplots(nbin, nbin, figsize=(14, 12), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in sdata.get_tracer_combinations():
    if survey not in ''.join(trs):
        continue
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])

    # Data
    ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8)    

    # Data Noiseless
    ell, cl, cov, ind = sdata_nn.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='gray', label='Data (noiseless)', alpha=0.8)    
    
    # BF Noiseless
#     ell, cl = sbf_nn.get_ell_cl('cl_ee', trs[0], trs[1])
#     ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='blue', label='BF (Noiseless)', alpha=0.8)

#     # BF
#     ell, cl = sbf.get_ell_cl('cl_ee', trs[0], trs[1])
#     ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='orange', label='BF (Baryons)', alpha=0.8)

    t = ax[i, j].text(0.99, 0.94, f'({i}, {j})', horizontalalignment='right',
                      verticalalignment='top', transform=ax[i, j].transAxes, fontsize=12) 
    
    if i != j:
        ax[j, i].axis('off')

for i in range(nbin):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")

t = ax[0, 2].text(0.95, 0.95, f'{survey}', horizontalalignment='right',
                  verticalalignment='top', transform=ax[0, 2].transAxes, fontsize=18) 
ax[0, 0].legend(bbox_to_anchor=(1., 1.05))

plt.show()
plt.close()

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true toc-hr-collapsed=true
# # Comparison with Planck

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### Contours

# %% hidden=true
# DES+KiDS+HSC with lmax=4500 and 8192
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'P18_official'
         ]

parameters = ['Omega_m', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_single_plotter(width_inch=6)
g.plot_2d(MCSamples, parameters, filled=[False, False, False, False, False],
                colors=['blue', 'orange', 'cyan', 'gold', 'black'],
                lws=[1, 1, 1, 1, 1, 1],
                ls=['-', '-', '--', '--', '-'])
legend1 = g.add_legend([None, 4500, None, 8192, None], legend_loc='lower left', frameon=False);
legend2 = g.add_legend([None, None, 'No-Baryons', 'Baryons', 'Planck'], legend_loc='upper left', colored_text=True);

lines = legend1.get_lines()
plt.legend([lines[1], lines[3]], [r'$\ell_{\rm max} = 4500$', r'$\ell_{\rm max} = 8192$'], loc='upper left', frameon=False)
plt.gca().add_artist(legend2)

# %% hidden=true
# DES+KiDS+HSC with lmax=4500 and 8192
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'P18_official'
         ]

parameters = ['omega_m', 'S12']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_single_plotter(width_inch=6)
g.plot_2d(MCSamples, parameters, filled=[False, False, False, False, False],
                colors=['blue', 'orange', 'cyan', 'gold', 'black'],
                lws=[1, 1, 1, 1, 1, 1],
                ls=['-', '-', '--', '--', '-'])
legend1 = g.add_legend([None, 4500, None, 8192, None], legend_loc='lower left', frameon=False);
legend2 = g.add_legend([None, None, 'No-Baryons', 'Baryons', 'Planck'], legend_loc='upper left', colored_text=True);

lines = legend1.get_lines()
plt.legend([lines[1], lines[3]], [r'$\ell_{\rm max} = 4500$', r'$\ell_{\rm max} = 8192$'], loc='upper left', frameon=False)
plt.gca().add_artist(legend2)

# %% hidden=true
# DES+KiDS+HSC with lmax=4500 and 8192
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'P18_official'
         ]

parameters = ['omega_m', 'S12']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_single_plotter(width_inch=6)
g.plot_2d(MCSamples, parameters, filled=[False, False, False, False, False],
                colors=['blue', 'orange', 'green', 'black'],
                lws=[1, 1, 1, 1, 1, 1],
                ls=['-', '-', '--', '-'])

g.add_legend(['Baryons ($\ell < 4500$)', '$H_0$ SNe prior', '$H_0$ Planck18 prior', 'Planck 2018'])
# legend1 = g.add_legend([None, 4500, None, 8192, None], legend_loc='lower left', frameon=False);
# legend2 = g.add_legend([None, None, 'No-Baryons', 'Baryons', 'Planck'], legend_loc='upper left', colored_text=True);

# lines = legend1.get_lines()
# plt.legend([lines[1], lines[3]], [r'$\ell_{\rm max} = 4500$', r'$\ell_{\rm max} = 8192$'], loc='upper left', frameon=False)
# plt.gca().add_artist(legend2)

# %% hidden=true
# All surveys (lmax = 4500)
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
          'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange', 'red', 'green', 'brown', 'black']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, False, True, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[1, 1, 1, 1, None, None],
                contour_ls=['-', '-', '-', '--', None, None]
                )

# %% hidden=true
# All surveys (lmax = 4500)
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG', 
          'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange', 'red', 'green', 'brown', 'black']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, False, True, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[1, 1, 1, 1, None, None],
                contour_ls=['-', '-', '-', '--', None, None]
                )

# %% hidden=true
# All surveys except DES+KiDS case (lmax=4500)
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'P18_official'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange', 'red', 'brown', 'black']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[False, False, False, True, True],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[1, 1, 1, None, None])


# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### Metrics

# %% hidden=true
# S8 tension

def get_tension_with_Planck(samples):
    p = samples.getParams()
    w = samples.weights
    mean = np.average(p.S8, weights=w)
    cov = np.cov(p.S8, aweights=w)
    # Table 2 of https://arxiv.org/pdf/1807.06209.pdf
    # with lensing: 0.832 ± 0.013
    # w/o lensing: 0.834 ± 0.016
    # print(mean, samples.getInlineLatex('S8'))
    return (0.832 - mean) / np.sqrt((0.013**2 + cov))

keys = list(results.keys())
keys.sort()

print(f'{"Chain":<150}', f"{'S8':<50}", f'{"Tension with Planck with lensing: S8 = 0.832 ± 0.013"}')
print("")
for k in keys:
    if 'fixedcosmo' in k:
        continue
    samples = results[k]['MCSamples']
    label = results[k]['label']
    
    print(f'{label:<150}', f"{samples.getInlineLatex('S8'):<50}", f'{get_tension_with_Planck(samples):.1f}')


# %% hidden=true
def get_tension_with_Planck_Omegam(samples):
    p = results['P18_official']['MCSamples'].getParams()
    w = results['P18_official']['MCSamples'].weights
    mean_Planck = np.average(p.Omega_m, weights=w)
    cov_Planck = np.cov(p.Omega_m, aweights=w)
        
    p = samples.getParams()
    w = samples.weights
    mean = np.average(p.Omega_m, weights=w)
    cov = np.cov(p.Omega_m, aweights=w)
    # Table 2 of https://arxiv.org/pdf/1807.06209.pdf
    # with lensing: 0.832 ± 0.013
    # w/o lensing: 0.834 ± 0.016
    # return (0.832 - mean) / np.sqrt((0.013**2 + cov))
    return (mean_Planck - mean) / np.sqrt(cov_Planck  + cov)

keys = list(results.keys())
keys.sort()

# Omega_m numbers got from the chains directly
print(f'{"Chain":<150}', f"{'Omega_m':<50}", f'{"Tension with Planck with lensing: Omega_m = 0.3153 ± 0.0073"}')
print("")
for k in keys:
    if 'fixedcosmo' in k:
        continue
    samples = results[k]['MCSamples']
    label = results[k]['label']
    
    print(f'{label:<150}', f"{samples.getInlineLatex('Omega_m'):<50}", f'{get_tension_with_Planck_Omegam(samples):.1f}')

# %% [markdown] jp-MarkdownHeadingCollapsed=true toc-hr-collapsed=true
# # Baryons

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## Compare Baryonic models

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### lmax=4500

# %% hidden=true
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

for c in chains:
    print(c, results[c]['chi2bf'], results[c]['pvalue_bf'])

# %% hidden=true
# Plot the 1D distribution of A_AE
chains = ['desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']

g = plots.get_single_plotter(width_inch=4)
g.plot_1d(results[chains[0]]['MCSamples'], 'A_AE', filled=True,
          legend_label=results[chains[0]]['label'], colors=['green'])
plt.show()

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### lmax=8192

# %% hidden=true
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

for c in chains:
    print(c, results[c]['chi2bf'], results[c]['pvalue_bf'])

# %% hidden=true
# Plot the 1D distribution of A_AE
chains = ['desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['green', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_single_plotter(width_inch=4)
g.plot_1d(MCSamples, 'A_AE', filled=True,
          colors=colors, ls=['-', '--'])
g.add_legend(legend_labels=['$\ell < 4500$', '$\ell < 8192$'])
plt.show()

# %% hidden=true
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Effect of Baryons on Pk and Cell and xi

# %%
key = "desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG"
pars_bf = results[key]['pars_bf'].copy()
# pars_bf['eta'] = np.log10(0.54)
# pars_bf['beta'] = np.log10(0.12)
# pars_bf['M1_z0_cen'] = np.log10(8.6e11)

k = np.logspace(-2, 1, 100)

f, ax = plt.subplots(2, 1, figsize=(6, 6), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})

for iMc, Mc in enumerate(np.arange(10, 16)):
    pars = pars_bf.copy()
    pars['M_c'] = Mc
    pk2d = results[key].get_pk_data(pars)
    pk = pk2d['pk_ww'](k, 1)
    ax[0].loglog(k, pk, label=f"$\log_{{10}}(M_c) = {Mc}$")  
    pk = pk2d['Sk'](k, 1)
    ax[1].semilogx(k, pk, label=f"$\log_{{10}}(M_c) = {Mc}$")  

pk2d = results[key].get_pk_data(pars_bf)
pk = pk2d['pk_ww'](k, 1)
ax[0].loglog(k, pk, color='black', label=f"BF")  
pk = pk2d['Sk'](k, 1)
ax[1].semilogx(k, pk, color='black', label=f"BF") 
    
pk = results["desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG"].get_pk_data(pars_bf)['pk_ww'](k, 1)
ax[0].loglog(k, pk, label="No Baryons", color='gray', ls='--', lw=2)
ax[1].axhline(1, color='gray', label='No Baryons', ls='--', lw=2)

ax[1].set_xlabel("$k$ [1/Mpc]")
ax[0].set_ylabel("$P_k$")
ax[1].set_ylabel("$S_k$")

ax[1].legend(loc='upper left', ncol=2)
    
plt.show()
plt.close()

# %%
key = "desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG"
pars_bf = results[key]['pars_bf']

sdata = results[key].get_cl_data_sacc()
sbf = results[key].get_cl_theory_sacc_bf()
sbf_nb = results["desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG"].get_cl_theory_sacc(pars_bf)

nbin = 4
f, ax = plt.subplots(nbin, nbin, figsize=(14, 12), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
# list_Mc = np.arange(10, 16, 2)
for trs in sdata.get_tracer_combinations():
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])

    # Data
    ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8) 

    # BF No Baryons
    ell, cl = sbf_nb.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='x', label=f"BF No Baryons", color='blue')

    # BF Baryons
    ell, cl = sbf.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='+', label="BF Baryons", color='orange', alpha=0.8)
            
for i in range(nbin):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")
    h, l = ax[0, 0].get_legend_handles_labels()
    ax[0, 1].legend(h, l, loc="upper left")
    
plt.show()
plt.close()

# %%
key = "desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG"
pars_bf = results[key]['pars_bf']

sdata = results[key].get_cl_data_sacc()
sbf = results[key].get_cl_theory_sacc_bf()
sbf_nb = results["desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG"].get_cl_theory_sacc(pars_bf)

nbin = 4
f, ax = plt.subplots(nbin, nbin, figsize=(14, 12), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for iMc, Mc in enumerate(np.arange(10, 16)):
    pars = pars_bf.copy()
    pars['M_c'] = Mc
    sMc = results[key].get_cl_theory_sacc(pars)

    for trs in sdata.get_tracer_combinations():
        i = int(trs[1].split('__')[1])
        j = int(trs[0].split('__')[1])

        # Data
        if iMc == 0:
            ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
            err = np.sqrt(np.diag(cov))
            ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8) 

        # BF baryons
        ell, cl = sMc.get_ell_cl('cl_ee', trs[0], trs[1])
        ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', label=f"$\log_{{10}}(M_c) = {Mc}$", alpha=0.8)
        
        # No Baryons
        if Mc == 15:
            ell, cl = sbf_nb.get_ell_cl('cl_ee', trs[0], trs[1])
            ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='x', label=f"No Baryons", color='gray')

            ell, cl = sbf.get_ell_cl('cl_ee', trs[0], trs[1])
            ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='x', label="BF", color='black', alpha=0.8)
            
for i in range(nbin):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")
    h, l = ax[0, 0].get_legend_handles_labels()
    ax[0, 1].legend(h, l, loc="upper left")
    
plt.show()
plt.close()

# %%
# Load the model that has no binning and goes up to lmax=50000, as Giovanni (we have checked that it converges enough. See in tests below)
# model = get_model('test/desy3wl_baryons_nocuts_nla_lmax50000_testunbinned.yml')
# model_nb = get_model('test/desy3wl_nobaryons_nocuts_nla_lmax50000_testunbinned.yml')

# Data
sdata = results["desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG"].get_cl_data_sacc()

# Get DESY3 bf parameters
pars_bf = results["desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG"]['pars_bf']
sbf = get_cl_theory_sacc(model, pars_bf)

# Same but without baryons
# pars_nb = results["desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG"]['pars_bf']
# sbf_nb = get_cl_theory_sacc(model_nb, pars_nb)

# Cosmo
cosmo = model.theory['cl_like.CCL'].get_CCL()['cosmo']

angle = np.logspace(0, 2, 20)
ell_all = np.arange(50000)
nbin = 4
f, ax = plt.subplots(nbin, nbin, figsize=(14, 12), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for iMc, Mc in enumerate(np.arange(10, 16)):
    pars = pars_bf.copy()
    pars['M_c'] = Mc
    sMc = get_cl_theory_sacc(model, pars)
    
    for trs in s.get_tracer_combinations():
        i = int(trs[1].split('__')[1])
        j = int(trs[0].split('__')[1])
    
        # Data
        if iMc == 0:
            ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
            err = np.sqrt(np.diag(cov))
            ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8) 

        # BF baryons
        ell, cl = sMc.get_ell_cl('cl_ee', trs[0], trs[1])
        ax[i, j].semilogx(ell, ell * cl * 1e7, label=f"$\log_{{10}}(M_c) = {Mc}$", alpha=0.8)
        
        # No Baryons
        if Mc == 15:
#             ell, cl = sbf_nb.get_ell_cl('cl_ee', trs[0], trs[1])
#             ax[i, j].semilogx(ell, ell * cl * 1e7, label=f"No Baryons", color='gray')

            ell, cl = sbf.get_ell_cl('cl_ee', trs[0], trs[1])
            ax[i, j].semilogx(ell, ell * cl * 1e7, label="BF", color='black', alpha=0.8)
    
for i in range(nbin):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")
    ax[0, i].set_xlim([None, 5000])
    h, l = ax[0, 0].get_legend_handles_labels()
    ax[0, 1].legend(h, l, loc="upper left")

f.suptitle(f'DES', fontsize=16)

plt.show()
plt.close()

# %%
# Build sacc file with official DESY3 xi-
# sdata = sacc.Sacc()
# path = "/mnt/extraspace/damonge/Datasets/DES_Y3/data_products/2pt_NG_final_2ptunblind_02_26_21_wnz_maglim_covupdate.fits"
# fits = fitsio.FITS(path)
# # print(fits)
# # print(fits[2])

# for i in range(4):
#     sdata.add_tracer('NZ', f'DESY3__{i}',
#                          z=fits['nz_source']['Z_MID'].read(), nz=fits['nz_source'][f'BIN{i+1}'].read())
# official_xi = fitsio.read(path, ext='xim', columns=['BIN1', 'BIN2', 'VALUE', 'ANG'])
# for dp in official_xi:
#     i = dp[0] - 1
#     j = dp[1] - 1
#     value = dp[2]
#     theta = dp[3]
#     sdata.add_data_point('xi_minus_re', (f'DESY3__{i}', f'DESY3__{j}'), value, theta=theta)
# sdata.add_covariance(fits['COVMAT'][200:400, :][:, 200:400])

# Load the model that has no binning and goes up to lmax=50000, as Giovanni (we have checked that it converges enough. See in tests below)
# model = get_model('test/desy3wl_baryons_nocuts_nla_lmax50000_testunbinned.yml')

# Get DESY3 bf parameters
pars_bf = results["desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG"]['pars_bf']
sbf = get_cl_theory_sacc(model, pars_bf)

# Same but without baryons
# pars_nb = results["desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG"]['pars_bf']
# sbf_nb = get_cl_theory_sacc(model, pars_nb)

# Cosmo
cosmo = model.theory['cl_like.CCL'].get_CCL()['cosmo']

angle = np.logspace(0, np.log10(300), 20)
ell_all = np.arange(50000)
nbin = 4
f, ax = plt.subplots(nbin, nbin, figsize=(14, 12), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for iMc, Mc in enumerate(np.arange(10, 16)):
    pars = pars_bf.copy()
    pars['M_c'] = Mc
    sMc = get_cl_theory_sacc(model, pars)
    
    for trs in s.get_tracer_combinations():
        i = int(trs[1].split('__')[1])
        j = int(trs[0].split('__')[1])
        
        # Data (missing)
        theta, xi, cov = sdata.get_theta_xi('xi_minus_re', trs[0], trs[1], return_cov=True)
        err = np.sqrt(np.diag(cov))
        ax[i, j].errorbar(theta, 1e4 * theta * xi, yerr=1e4 * theta * err, fmt='.', color='k', label='Data' if iMc == 0 else None)
        
        # BF baryons
        ell, cl = sMc.get_ell_cl('cl_ee', trs[0], trs[1])
        cl = interp1d(ell, cl, bounds_error=False, fill_value=0)(ell_all)
        xi = ccl.correlation(cosmo, ell=ell_all, C_ell=cl, theta=angle/60, type='GG-', method='fftlog')
        ax[i, j].semilogx(angle, 1e4*angle*xi, label=f"$\log_{{10}}(M_c) = {Mc}$", alpha=0.8)
        
        # No Baryons
        if Mc == 15:
#             ell, cl = sbf_nb.get_ell_cl('cl_ee', trs[0], trs[1])
#             cl = interp1d(ell, cl, bounds_error=False, fill_value=0)(ell_all)
#             xi = ccl.correlation(cosmo, ell=ell_all, C_ell=cl, theta=angle/60, type='GG-', method='fftlog')
#             ax[i, j].semilogx(angle, 1e4*angle*xi, label=f"No Baryons", color='gray')
            

            ell, cl = sbf.get_ell_cl('cl_ee', trs[0], trs[1])
            cl = interp1d(ell, cl, bounds_error=False, fill_value=0)(ell_all)
            xi = ccl.correlation(cosmo, ell=ell_all, C_ell=cl, theta=angle/60, type='GG-', method='fftlog')
            ax[i, j].semilogx(angle, 1e4*angle*xi, label=f"BF", color='black', alpha=0.8)
    
for i in range(nbin):
    ax[i, 0].set_ylabel(r"$10^4 \theta \xi_-$")
    ax[-1, i].set_xlabel(r"$\theta$ [arcmin]")
#     ax[0, i].set_xscale("log")
#     ax[i, 0].set_ylim([-0.1, 0.1])
#     ax[0, i].set_xlim([1e3, None])
    ax[0, 0].legend()

f.suptitle(f'DES', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()


# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## All results

# %% hidden=true
def plot_results_baryons():
    keys = [k for k in results.keys() if (not ('nobaryons' in k) or ('P18_official' in k))]
    keys.sort()
    keys = keys[::-1]
    nkeys = len(keys)

    f, ax = plt.subplots(1, 4, sharey=True, gridspec_kw={'hspace': 0, 'wspace': 0},
                        figsize=(15, 12))
    yticks = []
    for ik, k in enumerate(keys):
        if 'P18_official' in k:
            color = 'black'
        else:
            color = 'blue'
        label = results[k]['label']

        yticks.append(label)

        s = results[k]['MCSamples']
        pars = s.getParamNames().list()
        params = ['S8', 'Omega_m', 'sigma8', 'M_c']
        for ipn, pn in enumerate(params):
            if pn not in pars:
                continue
            latex = s.getInlineLatex(pn)
            if '---' in latex:
                continue
            elif '=' in latex:
                mean, up, down = transaleInlineLatex(latex)
                ax[ipn].errorbar(mean, ik, xerr=([down], [up]), fmt='.', color=color)
            elif '<' in latex:
                uplim = float(latex.split('<')[1])
                color = 'cyan'
                ax[ipn].errorbar(uplim, ik, xerr=1, xuplims=True, fmt='.', color=color)
            elif '>' in latex:
                lolim = float(latex.split('>')[1])
                color = 'cyan'
                ax[ipn].errorbar(lolim, ik, xerr=1, xlolims=True, fmt='.', color=color)
            else:
                raise ValueEror(f"Something is wrong with this latex {latex} of chain {k}")
            if 'P18_official' in k:
                ax[ipn].fill_betweenx([0, nkeys], mean - down, mean + up, color='lightgray')
                
    ax[0].set_xlabel('S_8')
    ax[1].set_xlabel(r'$\Omega_{\rm m}$')
    ax[2].set_xlabel(r'$\sigma_8$')
    ax[3].set_xlabel(r'$\log_{10}(M_c)$')

    ax[0].set_yticks(np.arange(nkeys), yticks)
    ax[0].set_ylim([-0.5, nkeys+0.5])
    for i in range(4):
        ax[i].grid()
        
    plt.show()
    plt.close()
        
plot_results_baryons()

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## $\chi^2$ baryons vs $\chi^2$ gravity only

# %% hidden=true
keys = list(results.keys())
keys.sort()

print(f'{"Chain":<130}', f"{'chi2 baryons':<20}", f'{"chi2 nobaryons":<20}', 'chi2 baryons - chi2 nobaryons')
print("")
for k in keys:
    if ('baryons' not in k) or ('nobaryons' in k):
        continue
    label = results[k]['label']
    label = label.replace('with Baryons', '')
    knb = k.replace('baryons', 'nobaryons').replace('AE', '').replace('BCM', '')

    chi2 = results[k]['chi2bf']
    if knb not in results:
        continue
    chi2nb = results[knb]['chi2bf']
    
    print(f'{label:<130}', f"{chi2:<20.0f}", f'{chi2nb:<20.0f}', f'{chi2-chi2nb:.2f}')

# %% hidden=true
# Print a latex table with the results above using Texttable

table_1 = Texttable()
table_1.set_cols_align(['l', 'c', 'c', 'c'])
table_1.set_cols_valign(['m', 'm', 'm', 'm'])

rows = [["Chain", "chi2 baryons", "chi2 nobaryons", "chi2 baryons - chi2 nobaryons"]]
keys = list(results.keys())
keys.sort()
for k in keys:
    if ('baryons' not in k) or ('nobaryons' in k):
        continue
    label = results[k]['label']
    label = label.replace('with Baryons', '')
    knb = k.replace('baryons', 'nobaryons').replace('AE', '').replace('BCM', '')

    chi2 = results[k]['chi2bf']
    if knb not in results:
        continue
    chi2nb = results[knb]['chi2bf']
    rows.append([label, chi2, chi2nb, chi2-chi2nb])
    
table_1.add_rows(rows)
# print(table_1.draw())
print(latextable.draw_latex(table_1, caption="Summary of resuls", label="tab:all.results"))


# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## Shifts

# %% hidden=true
def print_shifts(param):
    keys = list(results.keys())
    keys.sort()

    print(f'{"Chain":<125}', f"{param+' baryons':<35}", f'{param+" nobaryons":<35}', f'{param} shift')
    print("")
    for k in keys:
        if ('baryons' not in k) or ('nobaryons' in k) or ('fixedcosmo' in k):
            continue
        label = results[k]['label']
        label = label.replace('with Baryons', '')
        knb = k.replace('baryons', 'nobaryons').replace('AE', '').replace('BCM', '')

        s = results[k]['MCSamples']
        pn = s.getParamNames()
        latex = s.getInlineLatex(param)
        ix = pn.list().index(param)
        mean = np.average(s.samples[:, ix], weights=s.weights)
        err = np.sqrt(np.cov(s.samples[:, ix], aweights=s.weights))

        if knb not in results:
            continue
        s = results[knb]['MCSamples']
        pn = s.getParamNames()
        latexnb = s.getInlineLatex(param)
        ix = pn.list().index(param)
        meannb = np.average(s.samples[:, ix], weights=s.weights)
        errnb = np.sqrt(np.cov(s.samples[:, ix], aweights=s.weights))
        
        shift = (mean - meannb) / np.sqrt(err**2 + errnb**2)
        
        print(f'{label:<125}', f"{latex:<35}", f'{latexnb:<35}', f'{shift:.2f}')


# %% hidden=true
# S8 shifts
print_shifts('S8')

# %% hidden=true
# Omega_m shifts
print_shifts('Omega_m')

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## Baryon boost constraints

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### lmax=4500

# %% hidden=true
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'Arico+23']

colors = ['blue', 'orange', 'black']
labels = ['DESY3 ($\ell < 4500$)', 'DESY3 + KiDS-1000 + HSC-DR1 ($\ell < 4500$)', 'Arico+23']

f, ax = plt.subplots()
for c in chains:
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    label = dict(zip(chains, labels))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color)

ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$')
ax.set_ylabel('$S_k(a=1)$')
ax.legend(loc='lower left') # center', bbox_to_anchor=(0.5, 1.31),)
plt.show()
plt.close()

# %% hidden=true
# This is using the GetDist InlineLatex to get the 1D
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'Arico+23']

colors = ['blue', 'orange', 'black']
labels = ['DESY3 ($\ell < 4500$)', 'DESY3 + KiDS-1000 + HSC-DR1 ($\ell < 4500$)', 'Arico+23']

f, ax = plt.subplots()
for c in chains:
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    label = dict(zip(chains, labels))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color)

ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$')
ax.set_ylabel('$S_k(a=1)$')
ax.legend(loc='lower left') # center', bbox_to_anchor=(0.5, 1.31),)
plt.show()
plt.close()

# %% hidden=true
chains = ['hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG']

colors = ['blue', 'orange', 'black']
labels = ['HSC ($\ell < 4500$)', 'DESY3 ($\ell < 4500$)']

f, ax = plt.subplots()
for c in chains:
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    label = dict(zip(chains, labels))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color)

ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$')
ax.set_ylabel('$S_k(a=1)$')
ax.legend(loc='lower left') # center', bbox_to_anchor=(0.5, 1.31),)
plt.show()
plt.close()

# %% hidden=true
chains = ['desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          # 'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
          ]

colors = ['blue', 'orange', 'black']
labels = ['ST15', 'AE']

f, ax = plt.subplots()
for c in chains:
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    label = dict(zip(chains, labels))[c]
    b = results[c]['boost']
    s = b.get_MCSamples_baryons()
    b.plot_baryon_post(ax, label, color, alpha=0.3)
    b.set_getdist_1D(True)
    b.plot_baryon_post(ax, label, color, alpha=0.1)
    b.set_getdist_1D(False)

ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$')
ax.set_ylabel('$S_k(a=1)$')
ax.legend(loc='lower left') # center', bbox_to_anchor=(0.5, 1.31),)
plt.show()
plt.close()

# %% hidden=true
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'Arico+23']

colors = ['blue', 'orange', 'black']

f, ax = plt.subplots()
for c in chains:
    print(f"Reading {c}")
    label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color)

ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$')
ax.set_ylabel('$S_k(a=1)$')
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.31),)
plt.show()
plt.close()

# %% hidden=true
chains = ['desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'Arico+23']

colors = ['blue', 'orange', 'red', 'black']

f, ax = plt.subplots()
for c in chains:
    print(f"Reading {c}")
    label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color)

ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$')
ax.set_ylabel('$S_k(a=1)$')
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.31),)
plt.show()
plt.close()


# %%

# %%
def get_a_k_Sk_from_table(pktable_b, pktable_dmo, h):
    z = np.unique(pktable_b[0])[::-1]
    k = np.unique(pktable_b[1]) * h

    assert np.all(pktable_dmo[0] == pktable_b[0])
    assert np.all(pktable_dmo[1] == pktable_b[1])

    k_arr = []
    Sk_arr = []
    for i, zi in enumerate(z):
        sel_b = pktable_b[0] == zi

        ki = pktable_b[1, sel_b] * h
        Sk = pktable_b[2, sel_b] / pktable_dmo[2, sel_b]
        
        assert np.all(ki == k)
        Sk_arr.append(Sk)
        
    Sk_arr = np.array(Sk_arr)
    return 1/(1+z), k, Sk_arr
    
# Load the baryon boost (BAHAMAS)
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_DMONLY_2fluid_nu0.06_Planck2015_L400N1024.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_BAHAMAS_nu0.06_Planck2015.dat", unpack=True, usecols=(0, 1, 2))


# Chains
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless',
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless',
         ]

labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange']
labels = ['Planck 2015 cosmology + BAHAMAS baryonic boost', 'BF cosmology + BAHAMAS baryonic boost']

f, ax = plt.subplots()

# Add BAHAMAS line (h as in Planck15)
pkfile = np.load('./test/EE2kmax10_Planck15_006.npz', allow_pickle=True)
cosmopars = dict(pkfile['cosmopars'].tolist())
a, k, Sk = get_a_k_Sk_from_table(pktable_b, pktable_dmo, cosmopars['hubble'])
ax.semilogx(k, Sk[-1], color='cyan', ls=':', label='BAHAMAS baryonic boost (Planck 2015)')

# Add BAHAMAS line (h as in BF)
pkfile = np.load('./test/BACCOEmu_kmax10_baryonbf.npz', allow_pickle=True)
cosmopars = dict(pkfile['cosmopars'].tolist())
a, k, Sk = get_a_k_Sk_from_table(pktable_b, pktable_dmo, cosmopars['hubble'])
ax.semilogx(k, Sk[-1], color='chocolate', ls=':', label='BAHAMAS baryonic boost (BF cosmology)')

# Add chain constraints
for i, c in enumerate(chains):
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    # label = dict(zip(chains, labels))[c]
    label = None
    results[c]['boost'].plot_baryon_post(ax, label, color, alpha=0.3 - 0.1*i)

# Finalize plot
ax.set_xlim([0.1, 2.55])
ax.set_ylim([0.85, 1.1])
ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$', fontsize=18)
ax.set_ylabel('$S_k(a=1)$', fontsize=18)
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=15)
ax.legend(loc='lower left', fontsize=13, frameon=False)

plt.show()
plt.close()

# %%
b = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless']['boost']

# %%
b.MCSamples_baryons.samples.shape

# %%
g = plots.get_single_plotter()
g.plot_1d(b.MCSamples_baryons, 'Sk0p65')

# %%
b = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless']['boost']
s = b.get_MCSamples_baryons()

f, ax = plt.subplots()
# ax.semilogx(b.k_arr, s.samples[::1000].T, alpha=0.4, zorder=1)
b.plot_baryon_post(ax, label, color, alpha=0.3)
b.set_getdist_1D(True)
b.plot_baryon_post(ax, label, 'gray', alpha=0.3)

ax.axhline(1, ls='--', color='gray')
plt.show()
plt.close()

# %%
b = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF']['boost']
s = b.MCSamples_baryons

f, ax = plt.subplots()
ax.semilogx(b.k_arr, s.samples[::1000].T, alpha=0.4, zorder=1)
b.plot_baryon_post(ax, label, color, alpha=0.3)
ax.axhline(1, ls='--', color='gray')
plt.show()
plt.close()

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### lmax=8192

# %% hidden=true
chains = ['desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
#           'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_halofit_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG']

colors = ['blue', 'orange', 'green', 'black']

f, ax = plt.subplots()
for c in chains:
    print(f"Reading {c}")
    label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color)

ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$')
ax.set_ylabel('$S_k(a=1)$')
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.31),)
plt.show()
plt.close()

# %% hidden=true
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_halofit_GNG',
          'Arico+23']


# Data sacc
sdata = results[chains[0]].get_cl_data_sacc()

# Best fit saccs
sbfs = [results[c].get_cl_theory_sacc_bf() for c in chains[:-1]]
pars = results['Arico+23']['pars_bf'].copy()
pars['A_sE9'] = pars['A_s'] * 1e9
sbfs.append(results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG'].get_cl_theory_sacc(pars))

# Labels
# labels = [results[c]['label'] for c in chains]
labels = ['No Baryons', 'Bacco', 'Amon-Efstathiou', 'Amon-Efstathiou (Halofit)', 'Arico+23']

# Colors
# colors = [results[c]['color'] for c in chains]
colors = ['blue', 'orange', 'green', 'purple', 'red']
# # chi2
# chi2bf = results[chains[0]]._get_chi2bf()
# chi2_Arico23 = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG'].evaluate_model(pars)[0]

# print(f'{survey}: chi2 = {chi2bf:.2f} vs {chi2_Arico23:.2f} = chi2(Arico+23)')

# # pvalue
# pvaluebf = results[chains[0]]['pvalue_bf']
# pvalue_Arico23 = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG'].get_pvalue(pars)
# print(f'{survey}: p-value = {pvaluebf[0]:.2f} vs {pvalue_Arico23[0]:.2f} = p-value(Arico+23)')

# Sruvey
survey = 'DES'
nbin = 4
f, ax = plt.subplots(nbin, nbin, figsize=(12, 10), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in sdata.get_tracer_combinations():
    if ('DES' not in trs[0]) or ('DES' not in trs[1]):
        continue
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])

    # Data
    ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8)    

    # BFs
    for ichain, sbf in enumerate(sbfs):
        ell, cl = sbf.get_ell_cl('cl_ee', trs[0], trs[1])
        ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color=colors[ichain], label=labels[ichain], alpha=0.8)

for i in range(nbin):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")
    ax[0, 0].legend()

f.suptitle(f'{survey}', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% hidden=true
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
          # 'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmin300',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'Arico+23']

colors = ['blue', 'orange', 'red', 'green', 'purple', 'black']

f, ax = plt.subplots()
for c in chains:
    print(f"Reading {c}")
    label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color)

ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$')
ax.set_ylabel('$S_k(a=1)$')
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.31),)
plt.show()
plt.close()

# %% hidden=true
chains = ['hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmin300',
        ]

colors = ['blue', 'orange', 'red', 'green', 'purple', 'black']

f, ax = plt.subplots()
for c in chains:
    print(f"Reading {c}")
    label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color)

ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$')
ax.set_ylabel('$S_k(a=1)$')
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.31),)
plt.show()
plt.close()

# %% hidden=true
chains = [  'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
        ]

colors = ['blue', 'orange', 'red', 'green', 'purple', 'black']

f, ax = plt.subplots()
for c in chains:
    print(f"Reading {c}")
    label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color)

ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$')
ax.set_ylabel('$S_k(a=1)$')
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.31),)
plt.show()
plt.close()

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### lmax=4500 vs 8192

# %% hidden=true
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'Arico+23']

colors = ['blue', 'orange', 'black']
labels = ['DESY3 ($\ell < 4500$)', 'DESY3 ($\ell < 8192$)', 'Arico+23']

f, ax = plt.subplots()
for c in chains:
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    label = dict(zip(chains, labels))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color)

ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$')
ax.set_ylabel('$S_k(a=1)$')
ax.legend(loc='lower left') # center', bbox_to_anchor=(0.5, 1.31),)
plt.show()
plt.close()

# %% hidden=true
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'Arico+23']

colors = ['blue', 'orange', 'black']

f, ax = plt.subplots()
for c in chains:
    print(f"Reading {c}")
    label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color)

ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$')
ax.set_ylabel('$S_k(a=1)$')
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.31),)
plt.show()
plt.close()

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true toc-hr-collapsed=true
# # All results in numbers

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Within baccoemu boundaries

# %% hidden=true
k = "CHAIN"
print(f"{k:<70} Nonlinear emulator   baryon emulator   baryon fraction")
for k in results:
    if 'inbounds_baccoemu' in results[k]:
        print (f"{k:<80}{results[k]['inbounds_baccoemu']['nonlinear']:<10.2f}   {results[k]['inbounds_baccoemu']['baryon']:<15.2f}   {results[k]['inbounds_baccoemu']['baryon_fraction']:.2f}")


# %% hidden=true
def plot_withinbounds():
    keys = [k for k, v in results.items() if 'inbounds_baccoemu' in v]
    # keys.sort()
    
    keys_sorted = [[], [], [], [], [], []]
    for k in keys:
        l = results[k]['label']
        desin = 'DES' in l
        kidsin = 'KiDS' in l
        hscin = 'HSC' in l
        
        if ('et al' in l) or not (desin or kidsin or hscin):
            keys_sorted[0].append(k)
        elif desin and not (kidsin or hscin):
            keys_sorted[1].append(k)
        elif kidsin and not (desin or hscin):
            keys_sorted[2].append(k)
        elif hscin and not (desin or kidsin):
            keys_sorted[3].append(k)
        elif desin and kidsin and not hscin:
            keys_sorted[4].append(k)
        elif desin and kidsin and hscin:
            keys_sorted[5].append(k)
        else:
            raise ValueError(f"Something went wrong with {k}")
    
    keys = []
    for k in keys_sorted[::-1]:
        keys.extend(sorted(k, key=lambda x: results[x]['label'], reverse=True))
    
    nkeys = len(keys)

    f, ax = plt.subplots(1, 3, sharey=True, gridspec_kw={'hspace': 0., 'wspace': 0.1},
                        figsize=(15, 15))
    yticks = []
    for ik, k in enumerate(keys):
        label = results[k]['label']

        yticks.append(label)

        s = results[k]['MCSamples']
        pars = s.getParamNames().list()
        params = ['nonlinear', 'baryon', 'baryon_fraction']
        for ipn, pn in enumerate(params):
            val = results[k]['inbounds_baccoemu'][pn]*100
            if val > 90:
                color = 'green'
            elif val < 50:
                color = 'red'
            else:
                color = 'blue'
            if 'nobaryons' in k and ipn>0:
                continue
            ax[ipn].barh(ik, val, color=color)
                
    ax[0].set_xlabel(r'Within bounds of the non-linear emulator [%]')
    ax[1].set_xlabel(r'Within bounds of the baryons emulator [%]')
    ax[2].set_xlabel(r'Within bounds of the baryon fraction [%]')
    ax[0].set_xlim([0, 100])
    ax[1].set_xlim([0, 100])
    ax[2].set_xlim([0, 100])

    
    ax[0].set_yticks(np.arange(nkeys), yticks)
    ax[0].set_ylim([-0.5, nkeys+0.5])
    # for i in range(3):
        # ax[i].grid()

    plt.show()
    plt.close()
        
plot_withinbounds()


# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ## Constraints

# %% hidden=true
def plot_results():
    keys = list(results.keys())
    # keys.sort()
    # keys = keys[::-1]
    
    keys_sorted = [[], [], [], [], [], []]
    for k in keys:
        l = results[k]['label']
        desin = 'DES' in l
        kidsin = 'KiDS' in l
        hscin = 'HSC' in l
        
        if ('et al' in l) or not (desin or kidsin or hscin):
            keys_sorted[0].append(k)
        elif desin and not (kidsin or hscin):
            keys_sorted[1].append(k)
        elif kidsin and not (desin or hscin):
            keys_sorted[2].append(k)
        elif hscin and not (desin or kidsin):
            keys_sorted[3].append(k)
        elif desin and kidsin and not hscin:
            keys_sorted[4].append(k)
        elif desin and kidsin and hscin:
            keys_sorted[5].append(k)
        else:
            raise ValueError(f"Something went wrong with {k}")
    
    keys = []
    for k in keys_sorted[::-1]:
        keys.extend(sorted(k, key=lambda x: results[x]['label'], reverse=True))
        
    nkeys = len(keys)

    f, ax = plt.subplots(1, 4, sharey=True, gridspec_kw={'hspace': 0, 'wspace': 0},
                        figsize=(15, 15))
    yticks = []
    for ik, k in enumerate(keys):
        if 'P18_official' in k:
            color = 'black'
        else:
            color = 'blue'
        label = results[k]['label']

        yticks.append(label)

        s = results[k]['MCSamples']
        pars = s.getParamNames().list()
        params = ['S8', 'Omega_m', 'sigma8', 'M_c']
        for ipn, pn in enumerate(params):
            if pn not in pars:
                continue
            latex = s.getInlineLatex(pn)
            if '---' in latex:
                continue
            elif '=' in latex:
                mean, up, down = transaleInlineLatex(latex)
                ax[ipn].errorbar(mean, ik, xerr=([down], [up]), fmt='.', color=color)
            elif '<' in latex:
                uplim = float(latex.split('<')[1])
                # color = 'cyan'
                ax[ipn].errorbar(uplim, ik, xerr=1, xuplims=True, fmt='.', color=color)
            elif '>' in latex:
                lolim = float(latex.split('>')[1])
                # color = 'cyan'
                ax[ipn].errorbar(lolim, ik, xerr=1, xlolims=True, fmt='.', color=color)
            else:
                raise ValueEror(f"Something is wrong with this latex {latex} of chain {k}")
            if 'P18_official' in k:
                ax[ipn].fill_betweenx([0, nkeys], mean - down, mean + up, color='lightgray')
                
    ax[0].set_xlabel('S_8')
    ax[1].set_xlabel(r'$\Omega_{\rm m}$')
    ax[2].set_xlabel(r'$\sigma_8$')
    ax[3].set_xlabel(r'$\log_{10}(M_c)$')

    ax[0].set_yticks(np.arange(nkeys), yticks)
    ax[0].set_ylim([-0.5, nkeys+0.5])
    for i in range(4):
        ax[i].grid(axis='y')

    plt.show()
    plt.close()
        
plot_results()


# %% hidden=true
def print_results():
    keys = list(results.keys())
    keys.sort()

    print(f'{"Chain":<150}', f"{'S8':<30}", f'{"Omega_m":<30}', f'{"sigma8":<30}',  f'{"log(Mc)":<35}')
    print("")
    for k in keys:
        label = results[k]['label']

        s = results[k]['MCSamples']
        pars = s.getParamNames()
        listpars = pars.list()
        output = [f'{label:<150}']
        for pn in ['S8', 'Omega_m', 'sigma8', 'M_c']:
            if pn not in listpars:
                latex = ''
            else:
                latex = s.getInlineLatex(pn)
                if '=' in latex:
                    latex = latex.split('=')[1].strip()
                else:
                    latex = latex.replace(pars.parWithName(pn).label, '').strip()
            output.append(f"{latex:<30}")
                
        print(*output)
        
print_results()


# %% hidden=true
def get_results_rows():
    keys = list(results.keys())
    keys.sort()

    print(f'{"Chain":<150}', f"{'S8':<30}", f'{"Omega_m":<30}', f'{"sigma8":<30}',  f'{"log(Mc)":<35}')
    print("")
    output = []
    for k in keys:
        label = results[k]['label']

        s = results[k]['MCSamples']
        pars = s.getParamNames()
        listpars = pars.list()
        output.append([label])
        for pn in ['S8', 'Omega_m', 'sigma8', 'M_c']:
            if pn not in listpars:
                latex = ''
            else:
                latex = s.getInlineLatex(pn)
                if '=' in latex:
                    latex = latex.split('=')[1].strip()
                else:
                    latex = latex.replace(pars.parWithName(pn).label, '').strip()
            output[-1].append(latex)
    return output                    

table_1 = Texttable()
table_1.set_cols_align(['l', 'c', 'c', 'c', 'c'])
table_1.set_cols_valign(['m', 'm', 'm', 'm', 'm'])
table_1.add_rows(get_results_rows())
# print(table_1.draw())
print(latextable.draw_latex(table_1, caption="Summary of resuls", label="tab:all.results"))

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true toc-hr-collapsed=true
# # Surveys comparison

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## Weak lensing kernel

# %% hidden=true
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmin300',
         ]

saccs = [results[i]['model'].likelihood['cl_like.ClLike'].sacc_file.copy() for i in chains]
labels = ['DESY3',  'KiDS1000', 'HSCDR1']
colors = ['green', 'blue', 'red']

cosmo = ccl.CosmologyVanillaLCDM()

f, ax = plt.subplots(3, 2, figsize=(10, 6), gridspec_kw={'hspace': 0, 'wspace': 0.3}, sharex='col')

for i, s in enumerate(saccs):
    for trn, tr in s.tracers.items():
        ccl_tracer = ccl.WeakLensingTracer(cosmo, (tr.z, tr.nz))
        chi = cosmo.comoving_radial_distance(1/(1+tr.z))
        q = ccl_tracer.get_kernel(chi)[0]
        ax[i, 1].plot(tr.z, q, color=colors[i], alpha=1-i*0.2, label=labels[i] if trn[-1] == '0' else '')
        ax[i, 1].legend(frameon=False)
        ax[i, 1].set_ylabel('$q(z)$')
        
        ax[i, 0].plot(tr.z, tr.nz, color=colors[i], alpha=1-i*0.2, label=labels[i] if trn[-1] == '0' else '')
        ax[i, 0].legend(frameon=False)
        ax[i, 0].set_ylabel('$p(z)$')
     
ax[-1, 0].set_xlabel('$z$')
ax[-1, 1].set_xlabel('$z$')
ax[-1, 0].set_xlim([0, 1.6])
ax[-1, 1].set_xlim([0, 1.6])

plt.show()
plt.close()

# %% hidden=true
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmin300',
         ]

saccs = [results[i]['model'].likelihood['cl_like.ClLike'].sacc_file.copy() for i in chains]
labels = ['DESY3',  'KiDS1000', 'HSCDR1']
colors = ['green', 'blue', 'red']

cosmo = ccl.CosmologyVanillaLCDM()

f, ax = plt.subplots(4, 1, figsize=(6, 8), gridspec_kw={'hspace': 0}, sharex='col')

for i, s in enumerate(saccs):
    for trn, tr in s.tracers.items():
        ccl_tracer = ccl.WeakLensingTracer(cosmo, (tr.z, tr.nz))
        chi = cosmo.comoving_radial_distance(1/(1+tr.z))
        q = ccl_tracer.get_kernel(chi)[0]
        ax[-1].plot(tr.z, q, color=colors[i], alpha=1-i*0.2, label=labels[i] if trn[-1] == '0' else '')
        ax[-1].legend(frameon=False)
        ax[-1].set_ylabel('$q(z)$')
        
        ax[i].plot(tr.z, tr.nz, color=colors[i], alpha=1-i*0.2, label=labels[i] if trn[-1] == '0' else '')
        ax[i].legend(frameon=False)
        ax[i].set_ylabel('$p(z)$')
     
ax[-1].set_xlabel('$z$')
ax[-1].set_xlim([0, 1.6])

plt.show()
plt.close()

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ## S/N

# %% hidden=true
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'desy3wl_noK1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_noK1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
          'k1000_nohsc_nobaryons_nocuts_nla_nside4096_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmin300',
          'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

saccs = [results[i]['model'].likelihood['cl_like.ClLike'].sacc_file.copy() for i in chains]
labels = ['DESY3', 'DESY3 w/o KiDS1000 overlap', 'DESY3 w/o KiDS1000 & HSC overlap',
          'KiDS1000', 'KiDS1000 w/o HSC overlap',
          'HSCDR1',
          'DESY3 + KiDS1000', 'DESY3 + KiDS1000 + HSCDR1']
colors = [results[i]['color'] for i in chains]
# lmin = [20, 20, 100, 30]

for i, s in enumerate(saccs):
    s.keep_selection(data_type='cl_ee')
    s.keep_selection(ell__gt=30)
    # s.keep_selection(ell__gt=lmin[i])

ell, _ = saccs[0].get_ell_cl('cl_ee', 'DESY3__0', 'DESY3__0')

# saccs.append(sacc.concatenate_data_sets(saccs[1], saccs[2]))

SN = np.zeros((len(saccs), ell.size))
for i, s in enumerate(saccs):
    for j, lmax in enumerate(ell):
        ix = s.indices(ell__lt=lmax)
        mean = s.mean[ix]
        icov = np.linalg.inv(s.covariance.dense[ix][:, ix])
        SN[i, j] = np.sqrt(mean.dot(icov).dot(mean))

    plt.plot(ell, SN[i], label=labels[i])

plt.axvline(1000, ls='--', color='gray')
plt.axvline(2048, ls='--', color='gray')
plt.axvline(4500, ls=':', color='gray')
# plt.axvline(8192, ls='--', color='gray')

plt.xlabel(r'$\ell_{\rm max}$')
plt.ylabel('S/N')
plt.xlim([None, 8200])
plt.legend()
plt.show()
plt.close()

# %% hidden=true
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'desy3wl_noK1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_noK1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
          'k1000_nohsc_nobaryons_nocuts_nla_nside4096_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmin300',
          'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

saccs = [results[i]['model'].likelihood['cl_like.ClLike'].sacc_file.copy() for i in chains]
labels = ['DESY3', 'DESY3 w/o KiDS1000 overlap', 'DESY3 w/o KiDS1000 & HSC overlap',
          'KiDS1000', 'KiDS1000 w/o HSC overlap',
          'HSCDR1',
          'DESY3 + KiDS1000', 'DESY3 + KiDS1000 + HSCDR1']
colors = [results[i]['color'] for i in chains]
# lmin = [20, 20, 100, 30]

for i, s in enumerate(saccs):
    s.keep_selection(data_type='cl_ee')
    s.keep_selection(ell__gt=30)
    # s.keep_selection(ell__gt=lmin[i])

ell, _ = saccs[0].get_ell_cl('cl_ee', 'DESY3__0', 'DESY3__0')

# saccs.append(sacc.concatenate_data_sets(saccs[1], saccs[2]))

SN = np.zeros((len(saccs), ell.size))
for i, s in enumerate(saccs):
    for j, lmax in enumerate(ell):
        ix = s.indices(ell__lt=lmax)
        mean = s.mean[ix]
        icov = np.linalg.inv(s.covariance.dense[ix][:, ix])
        SN[i, j] = np.sqrt(mean.dot(icov).dot(mean))

    norm = interp1d(ell, SN[i])(ell[11])
    plt.plot(ell, SN[i]/norm, label=labels[i])

plt.axvline(1000, ls='--', color='gray')
plt.axvline(2048, ls='--', color='gray')
plt.axvline(4500, ls=':', color='gray')
# plt.axvline(8192, ls='--', color='gray')

plt.xlabel(r'$\ell_{\rm max}$')
plt.ylabel(f'S/N normalized at $\ell={ell[11]:.1f}$')
plt.xlim([None, 8200])
plt.legend()
plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## $k_{\rm max}$ [Questionably within baccoemu range]

# %%
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmin300',
         ]

saccs = [results[i]['model'].likelihood['cl_like.ClLike'].sacc_file.copy() for i in chains]
labels = ['DESY3',  'KiDS1000', 'HSCDR1']
colors = ['green', 'blue', 'red']

cosmo = ccl.CosmologyVanillaLCDM()
z = np.linspace(0, 5, 1000)
chi = cosmo.comoving_radial_distance(1/(1+z))
z_from_chi = interp1d(chi, z)
kmax = 5 * 0.67  # 5 h/Mpc for baccoemu

f, ax = plt.subplots(3, 1, figsize=(6, 6), gridspec_kw={'hspace': 0, 'wspace': 0.3}, sharex='col')
for i, s in enumerate(saccs):
    for trn, tr in s.tracers.items():
        j = int(trn[-1])
        ccl_tracer = ccl.WeakLensingTracer(cosmo, (tr.z, tr.nz))
        q = ccl_tracer.get_kernel(chi)[0]
        norm = np.trapz(q, x=chi)
        q /= norm
        
        
        ax[i].plot(z, q, color=colors[i], alpha=1-j*0.2, label=labels[i] if trn[-1] == '0' else '')
    
    for lmax, ls in zip([2048, 4500], [':', '--']):
        chi_kmax_baccoemu = (lmax + 0.5) / (kmax)
        z_min = z_from_chi(chi_kmax_baccoemu)
        ax[i].axvline(z_min, ls=ls, color='gray', label=f'$\ell = {lmax}$')
        
    ax[i].legend(frameon=False)
    ax[i].set_ylabel('$q(z)/\int dz q(z)$')
     
ax[-1].set_xlabel('$z$')
ax[-1].set_xlim([0, 1.6])

plt.show()
plt.close()

# %%
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmin300',
         ]

saccs = [results[i]['model'].likelihood['cl_like.ClLike'].sacc_file.copy() for i in chains]
labels = ['DESY3',  'KiDS1000', 'HSCDR1']
colors = ['green', 'blue', 'red']


cosmo = ccl.CosmologyVanillaLCDM()
z = np.linspace(0, 5, 1000)
chi = cosmo.comoving_radial_distance(1/(1+z))
z_from_chi = interp1d(chi, z)
kmax = 5 * 0.67  # 5 h/Mpc for baccoemu

for lmax in [4500]:
    chi_kmax_baccoemu = (lmax + 0.5) / (kmax)
    print("##########")
    print(f'For lmax = {lmax}, z_min = {z_from_chi(chi_kmax_baccoemu):.2f} to be in the range k < 5 h/Mpc of baccoemu')
    for i, s in enumerate(saccs):
        print()
        print(chains[i])
        for trn, tr in s.tracers.items():
            ccl_tracer = ccl.WeakLensingTracer(cosmo, (tr.z, tr.nz))

            q = ccl_tracer.get_kernel(chi)[0]
            norm = np.trapz(q, x=chi)
            qn = interp1d(chi, q/norm)            
            out = scipy.integrate.quad(qn, 0, chi_kmax_baccoemu, limit=200, epsrel=1e-3)[0]

            print(f'{trn}: Normalized area of kernel function below z_min = {out*100:.1f}%')
        print()

# %%
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmin300',
         ]

saccs = [results[i]['model'].likelihood['cl_like.ClLike'].sacc_file.copy() for i in chains]
labels = ['DESY3',  'KiDS1000', 'HSCDR1']
colors = ['green', 'blue', 'red']


cosmo = ccl.CosmologyVanillaLCDM()
z = np.linspace(0, 5, 1000)
chi = cosmo.comoving_radial_distance(1/(1+z))

for lmax in [4500, 8192]:
    print("##########")
    print(f'lmax = {lmax}')
    for i, s in enumerate(saccs):
        print()
        print(chains[i])
        for trn, tr in s.tracers.items():
            ccl_tracer = ccl.WeakLensingTracer(cosmo, (tr.z, tr.nz))

            q = ccl_tracer.get_kernel(chi)[0]
            norm = np.trapz(q, x=chi)
            qn = q/norm

            chi_mean = np.trapz(qn * chi, x=chi)
            kmax = (lmax + 0.5) / chi_mean

            print(f'{trn}: kmax(chi_mean) = {kmax/0.67:.2f} [h/Mpc] with chi_mean = {chi_mean:.2f} [Mpc]')


# %% [markdown] jp-MarkdownHeadingCollapsed=true toc-hr-collapsed=true
# # Plots paper

# %%
def get_sorted_keys(within_baccoemu=False, exclude=None, exclude_mock=True):
    if exclude is None:
        exclude = []
    if exclude_mock is True:
        exclude += [k for k in results.keys() if 'mock' in k]

    if within_baccoemu:
        keys = [k for k, v in results.items() if ('inbounds_baccoemu' in v) and (k not in exclude)]
    else:
        keys = [k for k in results.keys() if k not in exclude]
        
    keys_sorted = [[], [], [], [], [], []]
    for k in keys:
        l = results[k]['label']
        desin = 'DES' in l
        kidsin = 'KiDS' in l
        hscin = 'HSC' in l
        wo = 'w/o' in l
        
        if ('et al' in l) or not (desin or kidsin or hscin):
            keys_sorted[0].append(k)
        elif desin and not (kidsin or hscin) or wo:
            keys_sorted[1].append(k)
        elif kidsin and not (desin or hscin) or wo:
            keys_sorted[2].append(k)
        elif hscin and not (desin or kidsin):
            keys_sorted[3].append(k)
        elif desin and kidsin and not hscin:
            keys_sorted[4].append(k)
        elif desin and kidsin and hscin:
            keys_sorted[5].append(k)
        else:
            raise ValueError(f"Something went wrong with {k}")
    
    keys = []
    for k in keys_sorted[::-1]:
        keys.extend(sorted(k, key=lambda x: results[x]['label'], reverse=True))
        
    return keys

def get_shift(k1, k2, param, return_change_sigma=True):
    means = []
    sigmas = []

    for k in [k1, k2]:
        s = results[k]['MCSamples']
        pn = s.getParamNames()
        latexnb = s.getInlineLatex(param)
        ix = pn.list().index(param)
        means.append(np.average(s.samples[:, ix], weights=s.weights))
        sigmas.append(np.sqrt(np.cov(s.samples[:, ix], aweights=s.weights)))

    shift = (means[0] - means[1]) / np.sqrt(sigmas[0]**2 + sigmas[1]**2)
    if return_change_sigma is False:
        return shirt
    rel_sigma = sigmas[1]/sigmas[0] - 1
    return shift, rel_sigma

def get_shift_mcsamples(k1, k2, param):
    means = []
    sigmas = []
    
    for s in [k1, k2]:
        pn = s.getParamNames()
        latexnb = s.getInlineLatex(param)
        ix = pn.list().index(param)
        means.append(np.average(s.samples[:, ix], weights=s.weights))
        sigmas.append(np.sqrt(np.cov(s.samples[:, ix], aweights=s.weights)))
    
    shift = (means[0] - means[1]) / np.sqrt(sigmas[0]**2 + sigmas[1]**2)

    rel_sigma = sigmas[1]/sigmas[0] - 1
    return shift, rel_sigma


# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## $N(z)$ and $q(z)$

# %%
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
         ]

saccs = [results[i]['model'].likelihood['cl_like.ClLike'].sacc_file.copy() for i in chains]
labels = ['DESY3',  'KiDS1000', 'HSCDR1']
colors = ['blue', 'orange', 'red']
ls = ['-', '--', ':']

cosmo = ccl.CosmologyVanillaLCDM()

f, ax = plt.subplots(4, 1, figsize=(6, 8), gridspec_kw={'hspace': 0}, sharex='col')

for i, s in enumerate(saccs):
    for j, (trn, tr) in enumerate(s.tracers.items()):
        ccl_tracer = ccl.WeakLensingTracer(cosmo, (tr.z, tr.nz))
        chi = cosmo.comoving_radial_distance(1/(1+tr.z))
        q = ccl_tracer.get_kernel(chi)[0]
        ax[-1].plot(tr.z, q, color=colors[i], alpha=1-j*0.2, ls=ls[i], label=labels[i] if trn[-1] == '0' else '')

        ax[i].plot(tr.z, tr.nz, color=colors[i], alpha=1-j*0.2, ls=ls[i], label=labels[i] if trn[-1] == '0' else '')
        ax[i].legend(frameon=False, fontsize=14)
        ax[i].set_ylabel('$p(z)$', fontsize=14)
        ax[i].set_yticks([])
     
# ax[-1].legend(frameon=False)
ax[-1].set_ylabel('$q(z)$', fontsize=14)
ax[-1].set_yticks([])
ax[-1].set_xlabel('$z$', fontsize=14)
ax[-1].set_xlim([0, 2])
ax[-1].xaxis.set_tick_params(labelsize=12)

plt.savefig('../paper_figures/pz.pdf', bbox_inches='tight')
plt.show()
plt.close()

# %% [markdown]
# ## S/N

# %%
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          # 'desy3wl_noK1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          # 'desy3wl_noK1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
          # 'k1000_nohsc_nobaryons_nocuts_nla_nside4096_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmin300',
          'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

saccs = [results[i]['model'].likelihood['cl_like.ClLike'].sacc_file.copy() for i in chains]
labels = ['DESY3',
          # 'DESY3 w/o KiDS1000 overlap', 'DESY3 w/o KiDS1000 & HSC overlap',
          'KiDS1000',
          # 'KiDS1000 w/o HSC overlap',
          'HSCDR1',
          'DESY3 + KiDS1000', 'DESY3 + KiDS1000 + HSCDR1']
colors = ['blue', 'orange', 'red', 'green', 'brown']
# lmin = [20, 20, 100, 30]

for i, s in enumerate(saccs):
    s.keep_selection(data_type='cl_ee')
    s.keep_selection(ell__gt=30)
    # s.keep_selection(ell__gt=lmin[i])

# ell, _ = saccs[0].get_ell_cl('cl_ee', 'DESY3wl__0', 'DESY3wl__0')

# saccs.append(sacc.concatenate_data_sets(saccs[1], saccs[2]))

ell_anchor = 450

f, ax = plt.subplots(1, 2, figsize=(9, 3))
for i, s in enumerate(saccs):
    trn = list(s.tracers.keys())[0]
    ell, _ = s.get_ell_cl('cl_ee', trn, trn)
    SN = np.zeros(ell.size)
    for j, lmax in enumerate(ell):
        ix = s.indices(ell__lt=lmax)
        mean = s.mean[ix]
        icov = np.linalg.inv(s.covariance.dense[ix][:, ix])
        SN[j] = np.sqrt(mean.dot(icov).dot(mean))

    ax[0].plot(ell, SN, label=labels[i], color=colors[i])

    norm = interp1d(ell, SN)(ell_anchor)
    ax[1].plot(ell, SN/norm, label=labels[i], color=colors[i])

for i in range(2):
    ax[i].axvline(1000, ls=':', color='gray')
    ax[i].axvline(2048, ls='-.', color='gray')
    ax[i].axvline(4500, ls='--', color='gray')
    # ax[i].axvline(8192, ls='--', color='gray')
    ax[i].set_xlim([None, 8200])

    ax[i].set_xlabel(r'$\ell_{\rm max}$')
    
ax[0].set_ylabel('S/N')
ax[1].set_ylabel(f'S/N normalized at $\ell={ell_anchor:d}$')

plt.legend(loc='upper center', bbox_to_anchor=(-0.15, 1.25),
          ncol=3)
plt.savefig("../paper_figures/snr.pdf", bbox_inches='tight')
plt.show()
plt.close()

# %%
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ]

saccs = [results[i]['model'].likelihood['cl_like.ClLike'].sacc_file.copy() for i in chains]
labels = ['DESY3', 'DESY3 + KiDS1000 + HSCDR1']
colors = [results[i]['color'] for i in chains]
# lmin = [20, 20, 100, 30]

for i, s in enumerate(saccs):
    s.keep_selection(data_type='cl_ee')
    s.keep_selection(ell__gt=30)
    # s.keep_selection(ell__gt=lmin[i])

ell, _ = saccs[0].get_ell_cl('cl_ee', 'DESY3__0', 'DESY3__0')

# saccs.append(sacc.concatenate_data_sets(saccs[1], saccs[2]))

lmax = 4500
SN = []
for i, s in enumerate(saccs):
    ix = s.indices(ell__lt=lmax)
    mean = s.mean[ix]
    icov = np.linalg.inv(s.covariance.dense[ix][:, ix])
    SN.append(np.sqrt(mean.dot(icov).dot(mean)))
print(f'All/DES [%] = {(SN[1]/SN[0] -1) * 100:.0f}')


# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Results

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### Tables & Plots

# %%
def plot_results(keys, labels):
    # keys = get_sorted_keys(exclude=exclude)
    nkeys = len(keys)

    f, ax = plt.subplots(1, 4, sharey=True, gridspec_kw={'hspace': 0, 'wspace': 0},
                        figsize=(5, nkeys * 0.3))
    yticks = []
    for ik, k in enumerate(keys):
        if 'P18_official' in k:
            color = 'black'
        else:
            color = 'blue'
        label = labels[k]
        yticks.append(label)

        s = results[k]['MCSamples']
        pars = s.getParamNames().list()
        params = ['S8', 'Omega_m', 'sigma8', 'M_c']
        for ipn, pn in enumerate(params):
            if pn not in pars:
                continue
            latex = s.getInlineLatex(pn)
            if '---' in latex:
                continue
            elif '=' in latex:
                mean, up, down = transaleInlineLatex(latex)
                ax[ipn].errorbar(mean, ik, xerr=([down], [up]), fmt='.', color=color)
            elif '<' in latex:
                uplim = float(latex.split('<')[1])
                # color = 'cyan'
                ax[ipn].errorbar(uplim, ik, xerr=1, xuplims=True, fmt='.', color=color)
            elif '>' in latex:
                lolim = float(latex.split('>')[1])
                # color = 'cyan'
                ax[ipn].errorbar(lolim, ik, xerr=1, xlolims=True, fmt='.', color=color)
            else:
                raise ValueEror(f"Something is wrong with this latex {latex} of chain {k}")
            if 'P18_official_mnu' == k:
                ax[ipn].fill_betweenx([-1, nkeys+0.1], mean - down, mean + up, color='lightgray')
            elif 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG' == k:
                ax[ipn].fill_betweenx([-1, nkeys+0.1], mean - down, mean + up, color='lightblue', alpha=0.5)

                
    ax[0].set_xlabel('$S_8$')
    ax[1].set_xlabel(r'$\Omega_{\rm m}$')
    ax[2].set_xlabel(r'$\sigma_8$')
    ax[3].set_xlabel(r'$\log_{10}(M_{\rm c})$')

    ax[0].set_yticks(np.arange(nkeys), yticks)
    ax[0].set_ylim([-0.5, nkeys+0.1])
    for i in range(4):
        ax[i].grid(axis='y')
    ax[1].set_xlim([0.15, None])

    ax[-1].set_xticks([11, 13, 15])
    ax[-1].set_xticklabels(['11', '13', '15'])

    plt.savefig('../paper_figures/results.pdf', bbox_inches='tight')
    plt.show()
    plt.close()

chains = [
          'P18_official_mnu',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedbaryonsBAHAMAS',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_clusters',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
]

labels = {
          'P18_official_mnu': r'Planck 2018 ($m_\nu$)',
          #
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG': 'DES',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG': 'DES, no baryons',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG': 'KiDS, no baryons',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG': 'KiDS, no baryons',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300': 'HSC',
          'hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin300': 'HSC, no baryons',
          #
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG': 'DES + KiDS',
          'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG': 'DES + KiDS, no baryons',
          #
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC ($\ell < 1000$)', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC ($\ell < 2048$)', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC ($\ell < 4500$)', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC ($\ell < 8192$)',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC, no baryons ($\ell < 1000$)', 
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC, no baryons ($\ell < 2048$)', 
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC, no baryons ($\ell < 4500$)', 
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC, no baryons ($\ell < 8192$)', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck': 'DES + KiDS + HSC + $H_0$ Planck 2018 prior ',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess': 'DES + KiDS + HSC + $H_0$ SH0ES prior',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl': 'DES + KiDS + HSC + BAO prior', 
          #
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedbaryonsBAHAMAS': 'DES + KiDS + HSC + BAHAMAS prior',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_clusters': 'DES + KiDS + HSC + clusters prior',
          #
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF': 'DES + KiDS + HSC + fixed cosmo (BF no-baryons)',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18': 'DES + KiDS + HSC + fixed cosmo (Planck 2018)',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop': 'DES + KiDS + HSC + Planck 2018',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG': 'DES + KiDS + HSC (AE)',
          'desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG': 'DES + KiDS + HSC (ST15)',
}

plot_results(chains[::-1], labels)


# %%
def get_results_rows(keys, labels):
    output = []
    for k in keys:
        # print(k)
        # Skip the % and & symbols for LaTeX table.
        label = labels[k]

        s = results[k]['MCSamples']
        pars = s.getParamNames()
        listpars = pars.list()
        if 'chi2__cl_like.ClLike' in listpars:
            chi2 = f"{results[k]['chi2bf']:.1f}"
            pvalue = f"{results[k]['pvalue_bf']:.2f}"
        else:
            chi2 = pvalue = '--'
            
        output.append([label, chi2, pvalue])
        for pn in ['S8', 'Omega_m', 'sigma8', 'M_c']:
            if pn not in listpars:
                latex = ''
            else:
                latex = s.getInlineLatex(pn)
                if '=' in latex:
                    latex = latex.split('=')[1].strip()
                else:
                    latex = latex.replace(pars.parWithName(pn).label, '').strip()
            output[-1].append(f"${latex}$" if latex != "" else "--")
    return output                    


chains = [
          'P18_official_mnu',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedbaryonsBAHAMAS',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_clusters',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
]

labels = {
          'P18_official_mnu': r'Planck 2018 ($m_\nu$)',
          #
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG': 'DES',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG': 'DES, no baryons',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG': 'KiDS, no baryons',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG': 'KiDS, no baryons',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300': 'HSC',
          'hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin300': 'HSC, no baryons',
          #
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG': 'DES + KiDS',
          'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG': 'DES + KiDS, no baryons',
          #
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC ($\ell < 1000$)', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC ($\ell < 2048$)', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC ($\ell < 4500$)', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC ($\ell < 8192$)',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC, no baryons ($\ell < 1000$)', 
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC, no baryons ($\ell < 2048$)', 
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC, no baryons ($\ell < 4500$)', 
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG': r'DES + KiDS + HSC, no baryons ($\ell < 8192$)', 
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck': 'DES + KiDS + HSC + $H_0$ Planck 2018 prior ',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess': 'DES + KiDS + HSC + $H_0$ SH0ES prior',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl': 'DES + KiDS + HSC + BAO prior', 
          #
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedbaryonsBAHAMAS': 'DES + KiDS + HSC + BAHAMAS prior',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_clusters': 'DES + KiDS + HSC + clusters prior',
          #
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF': 'DES + KiDS + HSC + fixed cosmo (BF no-baryons)',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18': 'DES + KiDS + HSC + fixed cosmo (Planck 2018)',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop': 'DES + KiDS + HSC + Planck 2018',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG': 'DES + KiDS + HSC (AE)',
          'desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG': 'DES + KiDS + HSC (ST15)',
}


table_1 = Texttable()
table_1.set_cols_align(['p{10cm}', 'c', 'c', 'c', 'c', 'c', 'c'])
table_1.set_cols_valign(['m', 'm', 'm', 'm', 'm', 'm', 'm'])
table_1.set_cols_dtype(['t', 't', 't', 't', 't', 't', 't'])
table_1.add_rows([['', r'$\chi^2_{\rm WL}$', '$p$-value', '$S_8$', r'$\Omega_{\rm m}$', r'$\sigma_8$', r'$\log_{10}(\tilde M_{\rm c})$']] + get_results_rows(chains, labels))
# print(table_1.draw())
print(latextable.draw_latex(table_1, caption="Summary of resuls.", label="tab:results"))


# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### For suplement material

# %%
def plot_results(exclude=None):
    keys = get_sorted_keys(exclude=exclude)
    nkeys = len(keys)

    f, ax = plt.subplots(1, 4, sharey=True, gridspec_kw={'hspace': 0, 'wspace': 0},
                        figsize=(5, 16))
    yticks = []
    for ik, k in enumerate(keys):
        if 'P18_official' in k:
            color = 'black'
        else:
            color = 'blue'
        label = results[k]['label']

        yticks.append(label)

        s = results[k]['MCSamples']
        pars = s.getParamNames().list()
        params = ['S8', 'Omega_m', 'sigma8', 'M_c']
        for ipn, pn in enumerate(params):
            if pn not in pars:
                continue
            latex = s.getInlineLatex(pn)
            if '---' in latex:
                continue
            elif '=' in latex:
                mean, up, down = transaleInlineLatex(latex)
                ax[ipn].errorbar(mean, ik, xerr=([down], [up]), fmt='.', color=color)
            elif '<' in latex:
                uplim = float(latex.split('<')[1])
                # color = 'cyan'
                ax[ipn].errorbar(uplim, ik, xerr=1, xuplims=True, fmt='.', color=color)
            elif '>' in latex:
                lolim = float(latex.split('>')[1])
                # color = 'cyan'
                ax[ipn].errorbar(lolim, ik, xerr=1, xlolims=True, fmt='.', color=color)
            else:
                raise ValueEror(f"Something is wrong with this latex {latex} of chain {k}")
            if 'P18_official_mnu' == k:
                ax[ipn].fill_betweenx([0, nkeys], mean - down, mean + up, color='lightgray')
                
    ax[0].set_xlabel('$S_8$')
    ax[1].set_xlabel(r'$\Omega_{\rm m}$')
    ax[2].set_xlabel(r'$\sigma_8$')
    ax[3].set_xlabel(r'$\log_{10}(M_{\rm c})$')

    ax[0].set_yticks(np.arange(nkeys), yticks)
    ax[0].set_ylim([-0.5, nkeys+0.5])
    for i in range(4):
        ax[i].grid(axis='y')
    ax[1].set_xticks([0.1,0.2, 0.3, 0.4])
    ax[-1].set_xticks([11, 13, 15])
    ax[-1].set_xticklabels(['11', '13', '15'])

    plt.savefig('../paper_figures/results_all.pdf', bbox_inches='tight')
    plt.show()
    plt.close()

mock_keys = [k for k in results.keys() if 'mock' in k]
plot_results(exclude=['P18_Gauss', 'priors_chain', 'priors', 'BAOlkl'] + mock_keys)


# %%
def get_results_rows():
    mock_keys = [k for k in results.keys() if 'mock' in k]
    keys = get_sorted_keys(exclude=['P18_Gauss', 'priors_chain', 'priors', 'hsc_gg2023'] + mock_keys)[::-1]

    output = []
    for k in keys:
        # print(k)
        # Skip the % and & symbols for LaTeX table.
        label = results[k]['label'].replace('%', '\%').replace('&', '\&')

        s = results[k]['MCSamples']
        pars = s.getParamNames()
        listpars = pars.list()
        if 'chi2__cl_like.ClLike' in listpars:
            chi2 = f"{results[k]['chi2bf']:.1f}"
            pvalue = f"{results[k]['pvalue_bf']:.2f}"
        else:
            chi2 = pvalue = '--'
            
        output.append([label, chi2, pvalue])
        for pn in ['S8', 'Omega_m', 'sigma8', 'M_c']:
            if pn not in listpars:
                latex = ''
            else:
                latex = s.getInlineLatex(pn)
                if '=' in latex:
                    latex = latex.split('=')[1].strip()
                else:
                    latex = latex.replace(pars.parWithName(pn).label, '').strip()
            output[-1].append(f"${latex}$" if latex != "" else "--")
    return output                    

table_1 = Texttable()
table_1.set_cols_align(['p{10cm}', 'c', 'c', 'c', 'c', 'c', 'c'])
table_1.set_cols_valign(['m', 'm', 'm', 'm', 'm', 'm', 'm'])
table_1.set_cols_dtype(['t', 't', 't', 't', 't', 't', 't'])
table_1.add_rows([['', r'$\chi^2_{\rm WL}$', '$p$-value', '$S_8$', r'$\Omega_{\rm m}$', r'$\sigma_8$', r'$\log_{10}(\tilde M_{\rm c})$']] + get_results_rows())
# print(table_1.draw())
print(latextable.draw_latex(table_1, caption="Summary of resuls.", label="tab:results"))

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### Shifts and quantities reported in the text

# %%
results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['pars_bf']

# %%
# Check if the prior log(eta) = [-0.7, 0.7] is biasing the results. Answer is NO

s = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples']
s_etaprior = s.copy()
p = s_etaprior.getParams()
sel = p.eta > 0
lkl = np.zeros_like(p.eta)
lkl[sel] = np.inf
s_etaprior.reweightAddingLogLikes(lkl)


#### Quantify shifts

print("S8 shift [sigma] = {:.2f}, error change [%] = {:.2f}".format(*get_shift_mcsamples(s, s_etaprior, 'S8')))
print("Mc shift [sigma] = {:.2f}, error change [%] = {:.2f}".format(*get_shift_mcsamples(s, s_etaprior, 'M_c')))


### Plot
parameters = ['Omega_m', 'S8',  'M_c', 'eta']

MCSamples = [s, s_etaprior]
labels = ['Fiducial', 'Fiducial + log(eta) < 0']
# colors = [results[i]['color'] for i in chains]
# labels = [r"$\ell < 1000$", r"$\ell < 2048$", r"$\ell < 4500$", r"$\ell < 8192$"]
colors= ['brown', 'red']

g = plots.get_subplot_plotter(width_inch=6)
g.settings.axes_fontsize = 14
g.settings.lab_fontsize = 16
g.settings.legend_fontsize = 14

g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1.25, 1.25, 1.25],
                contour_ls=['-', '--', '-.', '-'],
                contour_args=[{'alpha': 0.7}, {'alpha': 1}, {'alpha': 1}, {'alpha': 1}]
)
ax = g.get_axes((0, 1))
t = ax.text(0.05, 0.05, 'DESY3+KiDS-1000+HSCDR1', horizontalalignment='left',
            verticalalignment='bottom', transform=ax.transAxes, fontsize=16)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
ax.set_yticks([])
ax.set_xticks([])
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)

ax = g.get_axes((2, 2))
ax.set_xlabel("$\log_{10}(M_c)$", fontsize=13)
ax = g.get_axes((2, 0))
ax.set_ylabel("$\log_{10}(M_c)$", fontsize=13)

plt.show()
plt.close()


# %%
# Shifts reported in the text
key0 = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG'
key1 = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
params = ['S8', 'Omega_m', 'M_c']
for param in params: 
    print(f"param = {param}")
    print(key0, results[key0]['MCSamples'].getInlineLatex(param))
    print(key1, results[key1]['MCSamples'].getInlineLatex(param))
    
    print(f"Change of error [lmax4500/lmax1000 - 1 %]: {get_shift(key0, key1, param)[1] * 100:.1f}")
    print()

chains = [key0, key1]
MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]

g = plots.get_subplot_plotter(subplot_size=3)

# Set line style default
g.settings.line_styles = 'tab10'
g.plots_1d(MCSamples, params, nx=3, legend_ncol=2)
# g.add_legend(labels, legend_ncol=2, legend_loc='lower center')

# %%
# Shifts reported in the text
key0 = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
key1 = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18'
params = ['M_c']
for param in params: 
    print(f"param = {param}")
    print(key0, results[key0]['MCSamples'].getInlineLatex(param))
    print(key1, results[key1]['MCSamples'].getInlineLatex(param))
    
    print(f"Change of error [P18/freecosmo - 1 %]: {get_shift(key0, key1, param)[1] * 100:.1f}")
    print()

chains = [key0, key1]
MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]

g = plots.get_subplot_plotter(subplot_size=3)

# Set line style default
g.settings.line_styles = 'tab10'
g.plots_1d(MCSamples, params, nx=3, legend_ncol=1)
# g.add_legend(labels, legend_ncol=2, legend_loc='lower center')

# %%
# Shifts reported in the text
key0 = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
key1 = 'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
params = ['S8', 'sigma8']
for param in params: 
    print(f"param = {param}")
    print(key0, results[key0]['MCSamples'].getInlineLatex(param))
    print(key1, results[key1]['MCSamples'].getInlineLatex(param))
    
    print(f"Shift [baccoemu/AE - 1 sigma]: {get_shift(key0, key1, param)[0]:.1f}")
    print()

chains = [key0, key1]
MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]

g = plots.get_subplot_plotter(subplot_size=3)

# Set line style default
g.settings.line_styles = 'tab10'
g.plots_1d(MCSamples, params, nx=3, legend_ncol=1)
# g.add_legend(labels, legend_ncol=2, legend_loc='lower center')

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Within baccoemu bounds

# %%
def plot_withinbounds():
    keys = get_sorted_keys(within_baccoemu=True)
    
    nkeys = len(keys)

    f, ax = plt.subplots(1, 3, sharey=True, gridspec_kw={'hspace': 0, 'wspace': 0.2},
                        figsize=(5, 16))
    yticks = []
    for ik, k in enumerate(keys):
        label = results[k]['label']

        yticks.append(label)

        s = results[k]['MCSamples']
        pars = s.getParamNames().list()
        params = ['nonlinear', 'baryon', 'baryon_fraction']
        for ipn, pn in enumerate(params):
            val = results[k]['inbounds_baccoemu'][pn]*100
            if val > 90:
                color = 'green'
            elif val < 50:
                color = 'red'
            else:
                color = 'blue'
            if 'nobaryons' in k and ipn>0:
                continue
            ax[ipn].barh(ik, val, color=color)
                
    ax[0].set_xlabel(r'$P_k^{mm}$ [%]')
    ax[1].set_xlabel(r'Baryons [%]')
    ax[2].set_xlabel(r'$\eta_b = \frac{\Omega_{\rm b}}{\Omega_c + \Omega_b}$ [%]')
    ax[0].set_xlim([0, 100])
    ax[1].set_xlim([0, 100])
    ax[2].set_xlim([0, 100])
    
    ax[0].set_yticks(np.arange(nkeys), yticks)
    ax[0].set_ylim([-0.5, nkeys+0.5])
    # for i in range(3):
        # ax[i].grid()
    # ax[0].legend(handles=[ax[0].bar(0, 0, color='green'), ax[0].bar(0, 0, color='blue'), ax[0].bar(0, 0, color='red')],
    #           labels=[r'$>90\%$', r'$50 - 90 \%$', r'$<50\%$'], loc='upper center',
    #        bbox_to_anchor=(1, 1.03), fontsize=9, ncol=3)
    plt.legend(handles=[ax[0].bar(0, 0, color='green'), ax[0].bar(0, 0, color='blue'), ax[0].bar(0, 0, color='red')],
               labels=[r'$>90\%$', r'$50 - 90 \%$', r'$<50\%$'],
               loc='upper center', 
           bbox_to_anchor=(0.5, 0.9), fontsize=9, ncol=3, bbox_transform=f.transFigure)
    plt.savefig('../paper_figures/within_bounds.pdf', bbox_inches='tight')
    plt.show()
    plt.close()
        
plot_withinbounds()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Pixel window function

# %% [markdown] hidden=true
# 1. Choose theory $C_\ell^{th}$
# 1. Generate 8192 shear maps ($\gamma_1, \gamma_2$)
# 1. Using the galaxies from the DES catalog (RA, DEC, weights) evaluate the field in those positions
# 1. Project into 4096 maps
# 1. Meassure $C_q$ (q = bandpowers)
# 1. Repeat x100
# 1. Estimate $w_q = \sqrt{<C_q>/C_q^{th}}$

# %% hidden=true
# Load sacc file to get ell_effective
# s = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov_GNG.fits')
f, ax = plt.subplots(2, 2, sharex='col', sharey='row', gridspec_kw={'hspace': 0, 'wspace': 0}, figsize=(7, 4))

ell, cl = s.get_ell_cl('cl_ee', 'KiDS1000__3', 'KiDS1000__3')
sel = ell <= 8192

pw = {}
for isurvey, survey in enumerate(['DESY3wl', 'KiDS1000']):
    nbins = 4 if survey == 'DESY3wl' else 5
    for i in range(nbins):
        key = f'{survey}__{i}'
        if survey == 'DESY3wl':
            fpath = f'/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3wl__2x2pt_4096/desy3_pixwin_zbin{i}.npz'
            if not os.path.isfile(fpath):
                continue
        else:
            fpath = f'/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/k1000_pixwin_zbin{i}.npz'
        pw[key] = np.load(fpath)

        # Cell realizations
        # for j in range(100):
        #     ax[0, isurvey].loglog(ell[sel], pw[key]['cl_realization'][j][0][sel], alpha=0.8)
        # Cell theory
        ax[0, isurvey].loglog(ell[sel], pw[key]['clth_q'][0][sel], color='black')
        # Cell realizations mean
        ax[0, isurvey].loglog(ell[sel], np.mean(pw[key]['cl_realization'][:, 0, sel], axis=0), ls='--', label=f'Bin {i}')


        # wl estimated
        err = np.std(pw[key]['cl_realization'][:, 0, sel], axis=0)
        err = 0.5 * err / pw[key]['wl'][0, sel] / pw[key]['clth_q'][0, sel]
        ax[1, isurvey].errorbar(ell[sel], pw[key]['wl'][0][sel], yerr=err, fmt='.', label=f'Bin {i}', alpha=1 - 0.2 * i)


ax[1, 0].axhline(1, ls='--', color='gray')
ax[1, 1].axhline(1, ls='--', color='gray')
ax[1, 0].set_ylim([0.97, 1.03])

# ax[1, 0].legend()
# ax[1, 1].legend(ncol=5, loc='center', bbox_to_anchor=(0., 1.2))
ax[0, 1].legend(ncol=2, loc='lower left', bbox_to_anchor=(-1., 0))

ax[1, 0].set_xlabel(r'$\ell$', fontsize=12)
ax[1, 1].set_xlabel(r'$\ell$', fontsize=12)
ax[1, 0].tick_params(axis='x', labelsize=11)
ax[1, 1].tick_params(axis='x', labelsize=11)


ax[0, 0].set_ylabel(r'$C_\ell$', fontsize=12)
ax[1, 0].set_ylabel(r'$w_\ell = \sqrt{\langle C_\ell \rangle / C_\ell^{\rm theory}}$', fontsize=12)

ax[0, 0].tick_params(axis='y', labelsize=11)
ax[1, 0].tick_params(axis='y', labelsize=11)

ax[0, 0].set_title('DESY3', fontsize=12)
ax[0, 1].set_title('KiDS-1000', fontsize=12)

plt.savefig('../paper_figures/pixwin.pdf', bbox_inches='tight')

plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Main cosmology results (lmax=4500)

# %%
# All surveys (lmax = 4500)
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
          'P18_official_mnu'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = ['DESY3', 'KiDS-1000', 'HSCDR1', 'DESY3 + KiDS-1000', 'DESY3 + KiDS-1000 + HSCDR1', 'Planck 2018']
colors = ['blue', 'orange', 'red', 'green', 'brown', 'black']

g = plots.get_single_plotter(width_inch=6)
g.settings.axes_fontsize = 15
g.settings.lab_fontsize = 20
g.settings.legend_fontsize = 12

g.plot_2d(MCSamples, 'Omega_m', 'S8', filled=[False, False, False, False, True, True],
                colors=colors,
                lws=[1, 1, 1, 1, None, None],
                ls=['-', '-', '-', '--', None, None]
                )
g.add_legend(labels, legend_ncol=2, legend_loc='lower center', frameon=False)

plt.savefig('../paper_figures/deskidshsc_post_combination.pdf', bbox_inches='tight')

# %%
# DES+KiDS+HSC with lmax=4500
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'P18_official_mnu'
         ]

parameters = ['Omega_m', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_single_plotter(width_inch=6)
g.settings.axes_fontsize = 15
g.settings.lab_fontsize = 20
g.settings.legend_fontsize = 14

g.plot_2d(MCSamples, parameters, filled=[True, False, True],
                colors=['brown', 'blue', 'black'],
                lws=[1, 1, 1])
g.add_legend(['Baryons', 'No-Baryons', 'Planck 2018'], legend_loc='upper left', colored_text=True);

plt.savefig('../paper_figures/deskidshsc_post_baryons_S8.pdf', bbox_inches='tight')

# %%
# All surveys (lmax = 4500)
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl',    
          'P18_official_mnu'
         ]

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = ['DESY3 + KiDS-1000 + HSCDR1', '$H_0$ prior (SNe)', '$H_0$ prior (Planck)', 'BAO prior', 'Planck 2018']
colors = ['brown', 'blue', 'gold', 'green', 'blue', 'gold', 'black']
filled=[True, False, False, False, False, False, True]
line_w=[1, 1, 1, 1, 1, 1, None]
line_s=['-', '-', '-', '--', '--', '--', None]

# Plot
g = plots.get_single_plotter()
g.settings.legend_frame = False

f, ax = plt.subplots(1, 2, figsize=(9, 3),  gridspec_kw={'wspace': 0.25}, sharex='col')
ax = ax[None, :]
i = 0 

plot_roots = MCSamples[:4] + [MCSamples[-1]]
plot_colors = colors[:4] + [colors[-1]]
plot_filled = filled[:4] + [filled[-1]]
plot_lw = line_w[:4] + [line_w[-1]]
plot_ls = line_s[:4] + [line_s[-1]]
    
for j in range(2):
    if j == 0:
        params = ['Omega_m', 'S8']
    else:
        params = ['omega_m', 'S12']

    g.plot_2d(plot_roots, *params,
              ax=ax[i, j], colors=plot_colors,
              filled=plot_filled, lws=plot_lw, ls=plot_ls
              )

# Place the legends
plot_labels = labels[:3]
g.add_legend(plot_labels, ax=ax[0, 0], legend_loc='upper left', frameon=False, fontsize=10)

line1 = plt.Line2D([0], [0], color=colors[3], linestyle='--', label=labels[3])
ax[0, 1].legend(handles=[line1], loc='upper left', frameon=False)

rect = patches.Rectangle((0, 0), 1, 1, color="black", label=labels[-1])
ax[0, 1].legend(handles=[line1, rect], loc='upper left', ncol=2, frameon=False)

# Move the S12 label to the right
# ax[0, 1].yaxis.set_label_position("right")
# ax[0, 1].set_yticklabels(['0.75', '0.80', '0.85', '0.90'])

ax[0, 0].set_ylim([0.73, 0.93])
ax[0, 1].set_ylim([0.73, 0.93])

plt.savefig('../paper_figures/deskidshsc_post_priors.pdf', bbox_inches='tight')
plt.show()

# %%
s = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl']['MCSamples']
p = s.getParams()
ix = p.chi2__cl_like.ClLike.argmin()
chi2 = p.chi2__cl_like.BAOLike[ix]
print('chi2 (BAO at WL BF) =', chi2, 'p-value = ', 1 - stats.chi2.cdf(chi2, 7))

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Constraints on baryons and baryon's boost (lmax=4500)

# %%
# All surveys (lmax = 4500)
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
          'Arico+23'
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = ['DESY3', 'KiDS-1000', 'HSCDR1', 'DESY3 + KiDS-1000', 'DESY3 + KiDS-1000 + HSCDR1', 'Aricò et al. 2023']
colors = ['blue', 'orange', 'red', 'green', 'brown', 'black']

g = plots.get_single_plotter(width_inch=6)
g.settings.axes_fontsize = 15
g.settings.lab_fontsize = 20
g.settings.legend_fontsize = 13

g.plot_1d(MCSamples, 'M_c',
                lws=[1, 1, 1, 1, 4, 2],
                ls=['-', '-', '-', '--', '-'],
                colors=colors
                )
g.add_legend(labels, legend_ncol=2, legend_loc='lower center')
plt.xlabel("$\log_{10}(M_c)$")
plt.ylabel('$\mathcal{P}(\log_{10}(M_c))$', fontsize=20)

plt.savefig('../paper_figures/deskidshsc_post_Mc.pdf', bbox_inches='tight')
plt.show()
plt.close()


# %%
def get_k_Sk_from_table(pktable_dmo, pktable_b):
    sel_dmo = pktable_dmo[0] == 0 
    sel_b = pktable_b[0] == 0

    assert np.all(pktable_dmo[0, sel_dmo] == pktable_b[0, sel_b])
    assert np.all(pktable_dmo[1, sel_dmo] == pktable_b[1, sel_b])

    h = 0.678  # Planck 2015
    k = pktable_dmo[1, sel_dmo] * h
    Sk = pktable_b[2, sel_b] / pktable_dmo[2, sel_dmo]

    sel = (k > 0.1) * (k < 2.5)
    k = k[sel]
    Sk = Sk[sel]
    return k, Sk

chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'Arico+23']

colors = ['brown', 'black']
labels = ['DESY3 + KiDS-1000 + HSC-DR1', 'Aricò et al. 2023']

f, ax = plt.subplots()

# Add EAGLE line
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_eagle_Planck13_GrO.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_eagle_Planck13_Hydro.dat", unpack=True, usecols=(0, 1, 2))
k, Sk = get_k_Sk_from_table(pktable_dmo, pktable_b)
ax.semilogx(k, Sk, color='purple', ls='--', label='EAGLE')

# Add Illustris TNG-300 line
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_tng100_Planck15_GrO.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_tng100_Planck15_Hydro.dat", unpack=True, usecols=(0, 1, 2))
k, Sk = get_k_Sk_from_table(pktable_dmo, pktable_b)
ax.semilogx(k, Sk, color='lightgreen', ls='--', label='Illustris TNG-300')

# Add BAHAMAS low-AGN line
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_DMONLY_2fluid_nu0_WMAP9_L400N1024.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_BAHAMAS_Theat7.6_nu0_WMAP9.dat", unpack=True, usecols=(0, 1, 2))
k, Sk = get_k_Sk_from_table(pktable_dmo, pktable_b)
ax.semilogx(k, Sk, color='orange', ls='--', label='BAHAMAS low-AGN')

# Add BAHAMAS line
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_DMONLY_2fluid_nu0.06_Planck2015_L400N1024.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_BAHAMAS_nu0.06_Planck2015.dat", unpack=True, usecols=(0, 1, 2))
k, Sk = get_k_Sk_from_table(pktable_dmo, pktable_b)
ax.semilogx(k, Sk, color='red', ls='--', label='BAHAMAS')    

# Add OWLS line
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_OWLS_REF.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_OWLS_AGN.dat", unpack=True, usecols=(0, 1, 2))
k, Sk = get_k_Sk_from_table(pktable_dmo, pktable_b)
ax.semilogx(k, Sk, color='cyan', ls='--', label='OWLS-AGN')  

# Add BAHAMAS high-AGN line
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_DMONLY_2fluid_nu0_WMAP9_L400N1024.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_BAHAMAS_Theat8.0_nu0_WMAP9.dat", unpack=True, usecols=(0, 1, 2))
k, Sk = get_k_Sk_from_table(pktable_dmo, pktable_b)
ax.semilogx(k, Sk, color='goldenrod', ls='--', label='BAHAMAS high-AGN')

# Add Illustris line
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_illustris_WMAP9_GrO.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_illustris_WMAP9_Hydro.dat", unpack=True, usecols=(0, 1, 2))
k, Sk = get_k_Sk_from_table(pktable_dmo, pktable_b)
ax.semilogx(k, Sk, color='green', ls='--', label='Illustris')

# Add chain constraints
for i, c in enumerate(chains):
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    label = dict(zip(chains, labels))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color, alpha=0.3 - 0.1*i)

# Finalize plot
ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$', fontsize=18)
ax.set_ylabel('$S_k(a=1)$', fontsize=18)
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=15)
ax.legend(loc='lower left', fontsize=13, frameon=False)
ax.set_xlim([k[0], k[-1]])
ax.set_ylim([None, 1.08])

plt.savefig('../paper_figures/boost_baccoemu.pdf', bbox_inches='tight')
plt.show()
plt.close()

# %%
b = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['boost']
sb = b.get_MCSamples_baryons()
# sb.getInlineLatex('Sk_')
print(sb.getParamNames().list())
print(sb.getInlineLatex('Sk2p51'))


# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Results with different lmax

# %%
chains = []

chains.append(['desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG'])

chains.append(['k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG'])

chains.append(['hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmin300'
         ])

chains.append(['desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG'])

chains.append(['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
         ])

surveys = ['DESY3', 'KiDS-1000', 'HSCDR1', 'DESY3 + KiDS-1000', 'DESY3 + KiDS-1000 \n + HSCDR1']
labels = [fr'$\ell_{{\rm max}} = {lmax}$' for lmax in [1000, 2048, 4500, 8192]]
colors= ['blue', 'orange', 'black', 'green']
lines_s = ['--', '-.', '-', ':']
# Plot
g = plots.get_single_plotter()
g.settings.legend_frame = True

f, ax = plt.subplots(1, 5, figsize=(10, 2), gridspec_kw={'hspace': 0, 'wspace': 0}, sharey=True, sharex=True)
for i in range(5):
    MCSamples = [results[c]['MCSamples'] for c in chains[i]]    
    g.plot_1d(MCSamples, 'M_c',
              ax=ax[i], colors=colors, ls=lines_s,
              )
    ax[i].text(0.05, 0.01, surveys[i], horizontalalignment='left', verticalalignment='bottom', transform=ax[i].transAxes)
    ax[i].set_xlabel("$\log_{10}(M_c)$", fontsize=13)


g.add_legend(legend_labels=labels, ax=ax[2], legend_loc='upper center', bbox_to_anchor=(0.5, 1.3), legend_ncol=4)

ax[0].set_ylabel('$\mathcal{P}(\log_{10}(M_c))$', fontsize=13)
plt.savefig("../paper_figures/post_Mc.pdf", bbox_inches='tight')
plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no-baryons

# %%
# Baryons vs no Baryons with ell_max=2048

plot_roots = []
colors = []

# DESY3
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG',
         ]
chains2 = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
           'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG'
         ]
chains3 = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
           'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
         ]
chains4 = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNG',
           'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG',
         ]
MCSamples = [results[i]['MCSamples'] for i in chains]
MCSamples2 = [results[i]['MCSamples'] for i in chains2]
MCSamples3 = [results[i]['MCSamples'] for i in chains3]
MCSamples4 = [results[i]['MCSamples'] for i in chains4]

plot_roots.append([MCSamples, MCSamples2, MCSamples3, MCSamples4])
colors.append(['lightblue', 'blue'])

# KiDS-1000
chains = ['k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG',
         ]
chains2 = ['k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
         ]
chains3 = ['k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
         ]
chains4 = ['k1000_nobaryons_nocuts_nla_nside4096_lmin100_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmin100_GNG',
         ]

MCSamples = [results[i]['MCSamples'] for i in chains]
MCSamples2 = [results[i]['MCSamples'] for i in chains2]
MCSamples3 = [results[i]['MCSamples'] for i in chains3]
MCSamples4 = [results[i]['MCSamples'] for i in chains4]

plot_roots.append([MCSamples, MCSamples2, MCSamples3, MCSamples4])
colors.append(['moccasin', 'darkorange'])

# HSC-DR1
chains = ['hsc_nobaryons_nocuts_nla_nside4096_lmax1000_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin300',
         ]
chains2 = ['hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300',
         ]
chains3 = ['hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
         ]
chains4 = ['hsc_nobaryons_nocuts_nla_nside4096_lmin300',
          'hsc_baryons_nocuts_nla_nside4096_lmin300',
         ]

MCSamples = [results[i]['MCSamples'] for i in chains]
MCSamples2 = [results[i]['MCSamples'] for i in chains2]
MCSamples3 = [results[i]['MCSamples'] for i in chains3]
MCSamples4 = [results[i]['MCSamples'] for i in chains4]

plot_roots.append([MCSamples, MCSamples2, MCSamples3, MCSamples4])
colors.append(['lightcoral', 'red'])

# DESY3 + KiDS-1000
chains = ['desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
         ]
chains2 = ['desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG',
           'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG'
         ]
chains3 = ['desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
           'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG',
         ]
chains4 = ['desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
           'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG',
         ]
MCSamples = [results[i]['MCSamples'] for i in chains]
MCSamples2 = [results[i]['MCSamples'] for i in chains2]
MCSamples3 = [results[i]['MCSamples'] for i in chains3]
MCSamples4 = [results[i]['MCSamples'] for i in chains4]

plot_roots.append([MCSamples, MCSamples2, MCSamples3, MCSamples4])
colors.append(['lightgreen', 'green'])

# DESY3 + KiDS-1000 + HSCDR1wl
chains = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG',
         ]
chains2 = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG',
           'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG'
         ]
chains3 = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
           'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
         ]
chains4 = ['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
           'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG',
         ]
MCSamples = [results[i]['MCSamples'] for i in chains]
MCSamples2 = [results[i]['MCSamples'] for i in chains2]
MCSamples3 = [results[i]['MCSamples'] for i in chains3]
MCSamples4 = [results[i]['MCSamples'] for i in chains4]

plot_roots.append([MCSamples, MCSamples2, MCSamples3, MCSamples4])
colors.append(['coral', 'brown'])


# Plot
g = plots.get_single_plotter()

f, ax = plt.subplots(5, 4, figsize=(8, 8), gridspec_kw={'hspace': 0, 'wspace': 0}, sharey='row', sharex=True)
for i in range(5):
    for j in range(4):
        g.plot_2d(plot_roots[i][j], 'Omega_m', 'S8',
                  ax=ax[i, j], colors=colors[i],
                  filled=[True, False], lws=[None, 1],
                  )
        if j > 0:
            ax[i, j].set_ylabel("")

surveys = ['DESY3', 'KiDS-1000', 'HSCDR1', 'DESY3 +\n KiDS-1000', 'DESY3 +\n KiDS-1000 +\n HSCDR1']
for i in range(5):
    ax2 = ax[i, -1].twinx()
    ax2.set_ylabel(surveys[i])
    ax2.set_yticks([])

lmax = [1000, 2048, 4500, 8192]
for j in range(4):
    ax[0, j].set_title(fr'$\ell_{{\rm max}} = {lmax[j]}$')

ax[0, 0].set_xlim([None, 0.53])

g.add_legend(['No Baryons'], ax=ax[0, 0], legend_loc='upper right', fontsize=10)
blue_line = plt.Line2D([0], [0], color=colors[0][1], linestyle='-', label='Baryons')
ax[0, 1].legend(handles=[blue_line], loc='upper right')


plt.savefig('../paper_figures/post_baryons_nobaryons.pdf', bbox_inches='tight')
plt.show()

# %%
# Print a latex table with the results above using Texttable

table_1 = Texttable()
table_1.set_cols_align(['l', 'c', 'c', 'c', 'c', 'c'])
table_1.set_cols_valign(['m', 'm', 'm', 'm', 'm', 'm'])

rows = [["Chain", r"$\Delta \chi^2$", r"$\Delta S_8 [\sigma]$", r"$\Delta \Omega_m [\sigma]$", r"$\Delta \sigma_{S_8}[\%]$", r"$\Delta \sigma_{\Omega_{\rm m}} [\%]$"]]

keys = get_sorted_keys(exclude_mock=True)[::-1]

for k in keys:
    if ('baryons' not in k) or ('nobaryons' in k):
        continue
    label = results[k]['label']
    label = label.replace('with Baryons', '')
    knb = k.replace('baryons', 'nobaryons').replace('AE', '').replace('BCM', '')
    if knb not in results:
        continue

    # Delta chi2
    chi2 = results[k]['chi2bf']
    chi2nb = results[knb]['chi2bf']
    dchi2 = chi2 - chi2nb

    # Delta S8
    dS8, rsigmaS8 = get_shift(k, knb, 'S8')  
    # Delta Omega_m
    dOm, rsigmaOm = get_shift(k, knb, 'Omega_m')  
    
    rows.append([label, dchi2, dS8, dOm, int(np.round(rsigmaS8*100)), int(np.round(rsigmaOm*100))])


table_1.set_precision(1)
table_1.add_rows(rows)
# print(table_1.draw())
print(latextable.draw_latex(table_1, caption="Summary of resuls", label="tab:chi2.diff"))

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Cosmology fixing baryons

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
    'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedbaryonsBAHAMAS',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_clusters',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8',  'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'].split(" with ")[1] for i in chains]
# colors = [results[i]['color'] for i in chains]
# labels = [r"$\ell < 1000$", r"$\ell < 2048$", r"$\ell < 4500$", r"$\ell < 8192$"]
colors= ['brown', 'red', 'black', 'blue']

g = plots.get_subplot_plotter(width_inch=6)
g.settings.axes_fontsize = 17
g.settings.lab_fontsize = 19
g.settings.legend_fontsize = 14

g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1.25, 1.25, 1.25],
                contour_ls=['-', '--', '-.', '-'],
                contour_args=[{'alpha': 0.7}, {'alpha': 1}, {'alpha': 1}, {'alpha': 1}]
)
ax = g.get_axes((0, 1))
t = ax.text(0.05, 0.05, 'DESY3+KiDS-1000+HSCDR1', horizontalalignment='left',
            verticalalignment='bottom', transform=ax.transAxes, fontsize=16)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
ax.set_yticks([])
ax.set_xticks([])
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)

ax = g.get_axes((2, 2))
ax.set_xlabel("$\log_{10}(M_c)$", fontsize=16)
ax = g.get_axes((2, 0))
ax.set_ylabel("$\log_{10}(M_c)$", fontsize=16)

plt.savefig('../paper_figures/deskidshsc_baryons_priors.pdf', bbox_inches='tight')

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Baryons with fixed cosmology

# %%
# No Baryons with ell_max=8192
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF'
         ]

parameters = ['M_c', 'eta']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = ['Free cosmology', 'Planck18 cosmology', 'BF cosmology']
colors = ['brown', 'orange', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.settings.axes_fontsize = 18
g.settings.lab_fontsize = 20
# g.settings.legend_fontsize = 16

g.plots_1d(MCSamples, parameters, colors=colors, nx=2, share_y=True)
g.legend.remove()

ax = g.get_axes((0))
ax.set_xlabel("$\log_{10}(M_c)$", fontsize=20)
ax.set_ylabel("$\mathcal{P}$", fontsize=20)
ax = g.get_axes((1))
ax.set_xlabel("$\log_{10}(\eta)$", fontsize=20)



plt.savefig('../paper_figures/deskidshsc_baryons_fixedcosmo.pdf', bbox_inches='tight')
plt.show()

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF']

colors = ['brown', 'orange', 'green']
labels = ['Free cosmology', 'Planck18 cosmology',  'BF cosmology']

f, ax = plt.subplots()
# Add chain constraints
for i, c in enumerate(chains):
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    label = dict(zip(chains, labels))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color, alpha=0.5 - 0.1*i)

# Finalize plot
ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$', fontsize=18)
ax.set_ylabel('$S_k(a=1)$', fontsize=18)
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=15)
ax.legend(loc='lower left', fontsize=15, frameon=False)

ax.set_xlim([0.1, 2.5])
plt.savefig('../paper_figures/deskidshsc_baryons_fixedcosmo_Sk.pdf', bbox_inches='tight')

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Comparison of baryonic models

# %%
# Baryons vs no Baryons with ell_max=2048
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8', 'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
# MCSamples[1] = ST15 # Use the cut one
colors = ['brown', 'blue', 'orange']
# labels = [results[i]['label'].split(' with ')[1] for i in chains]
labels = ["Fiducial (baccoemu)", "Schneider & Teyssier 2015", "Amon & Efstathiou"]

g = plots.get_subplot_plotter(width_inch=6)
g.settings.axes_fontsize = 17
g.settings.lab_fontsize = 19
g.settings.legend_fontsize = 16

g.triangle_plot(MCSamples, parameters, filled=[True, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1],
                legend_ncol=1,
                # legend_loc='upper center'
                )

ax = g.get_axes((0, 1))
t = ax.text(0.05, 0.05, 'DESY3+KiDS-1000+HSCDR1', horizontalalignment='left',
            verticalalignment='bottom', transform=ax.transAxes, fontsize=16)
t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
ax.set_yticks([])
ax.set_xticks([])
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
# ax.spines['bottom'].set_visible(False)
# ax.spines['left'].set_visible(False)

# ax = g.get_axes((1, 2))
# g.plot_1d(MCSamples[-1], 'A_AE',
#           colors=[colors[-1]], ax=ax)

ax = g.get_axes((2, 2))
ax.set_xlabel("$\log_{10}(M_c)$", fontsize=16)
ax.set_xlim([12, None])
ax = g.get_axes((2, 0))
ax.set_ylabel("$\log_{10}(M_c)$", fontsize=16)
ax.set_ylim([12, None])

plt.savefig('../paper_figures/deskidshsc_baryons_models.pdf', bbox_inches='tight')
plt.show()

# %%
print('ST15')
shift_mean, shift_error = get_shift_mcsamples(results['desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'],
                                              results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'], 'S8')
print("S8 shift [sigma] = {:.2f}, error change [%] = {:.2f}".format(shift_mean, shift_error * 100))

shift_mean, shift_error = get_shift_mcsamples(results['desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'],
                                              results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'], 'Omega_m')
print("Omega_m shift [sigma] = {:.2f}, error change [%] = {:.2f}".format(shift_mean, shift_error * 100))


print()
print('AE')
shift_mean, shift_error = get_shift_mcsamples(results['desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'],
                                              results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'], 'S8')
print("S8 shift [sigma] = {:.2f}, error change [%] = {:.2f}".format(shift_mean, shift_error * 100))

shift_mean, shift_error = get_shift_mcsamples(results['desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'],
                                              results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'], 'Omega_m')
print("Omega_m shift [sigma] = {:.2f}, error change [%] = {:.2f}".format(shift_mean, shift_error * 100))

# %%
print('ST15 shifts respec to no-baryons')
shift_mean, shift_error = get_shift_mcsamples(results['desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'],
                                              results['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'], 'S8')
print("S8 shift [sigma] = {:.2f}, error change [%] = {:.2f}".format(shift_mean, shift_error * 100))

shift_mean, shift_error = get_shift_mcsamples(results['desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'],
                                              results['desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'], 'Omega_m')
print("Omega_m shift [sigma] = {:.2f}, error change [%] = {:.2f}".format(shift_mean, shift_error * 100))

# %%
print('ST15 shifts respec to Planck')
shift_mean, shift_error = get_shift_mcsamples(results['desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'],
                                              results['P18_official_mnu']['MCSamples'], 'S8')
print("S8 shift [sigma] = {:.2f}, error change [%] = {:.2f}".format(shift_mean, shift_error * 100))

shift_mean, shift_error = get_shift_mcsamples(results['desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'],
                                              results['P18_official_mnu']['MCSamples'], 'Omega_m')
print("Omega_m shift [sigma] = {:.2f}, error change [%] = {:.2f}".format(shift_mean, shift_error * 100))

# %%

# %%
g = plots.get_single_plotter(width_inch=3)
g.settings.axes_fontsize = 14
g.settings.lab_fontsize = 16
g.plot_1d(MCSamples[-1], 'A_AE', colors=[colors[-1]])
plt.ylabel(r'$\mathcal{P}(A_{\rm AE})$', fontsize=15)
plt.savefig('../paper_figures/deskidshsc_baryons_models_AE.pdf', bbox_inches='tight')
plt.show()
plt.close()

# %%
# Value of A_AE
key = 'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
print(key, results[key]['MCSamples'].getInlineLatex('A_AE'))

key = 'desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18'  # To be reported in the text
print(key, results[key]['MCSamples'].getInlineLatex('A_AE'))

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Comparison with official results

# %%
# Plot the 3 S8-Om plots in the same figure
plot_roots = []
plot_colors = []
plot_labels = []

# DESY3
chains = ['Doux+22',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_halofit_GNG',
          ]

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
labels = ['Doux et al. 2022 (NLA, $C_\ell$)', r'No Baryons ($\ell < \ell_{\rm max}^{\rm Doux+22}$)', r'HALOFIT ($\ell < \ell_{\rm max}^{\rm Doux+22}$)']
# colors = ['gray', 'black', 'blue']
colors = ['black', 'blue', 'lightblue']

plot_roots.append(MCSamples)
plot_colors.append(colors)
plot_labels.append(labels)

# KiDS-1000
chains = ['Asgari+20',
          'k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG',
          # 'k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
          # 'k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_halofit_GNG',
          ]

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
labels = ['Asgari et al. 2020 \n(Bandpowers)', r'No Baryons ($\ell < 1000$)', r'No Baryons ($\ell < 2048$)']
# colors = ['gray', 'black', 'blue']
colors = ['black', 'moccasin', 'darkorange']

plot_roots.append(MCSamples)
plot_colors.append(colors)
plot_labels.append(labels)


# DES+KiDS
chains = ['Abbot+23',
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG',
          # 'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG_hybridpriors_hmcode2020',
         ]

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
labels = ['Abbot et al 2023 ($\\xi,\, E_n$)', r'Baryons ($\ell < 1000$)', "Hybrid ($\ell < 1000$)"]
# colors= ['gray', 'red', 'blue']
colors = ['black', 'green', 'red']

plot_roots.append(MCSamples)
plot_colors.append(colors)
plot_labels.append(labels)

# Plot
g = plots.get_single_plotter()

f, ax = plt.subplots(1, 3, figsize=(8, 3), gridspec_kw={'hspace': 0, 'wspace': 0}, sharey='row')
ax = np.atleast_2d(ax)

for i in range(1):
    for j in range(3):
        g.plot_2d(plot_roots[j], 'Omega_m', 'S8',
                  ax=ax[i, j], colors=plot_colors[j],
                  filled=[True, False, False], lws=[None, 1.25, 1.25],
                  ls=['-', '-', '--']
                  )
        if j > 0:
            ax[i, j].set_ylabel("")

titles = ['DESY3', 'KiDS-1000', 'DESY3 + KiDS-1000']
for j in range(3):
    ax[0, j].set_title(titles[j])

    contour = patches.Rectangle((0.25, 0.25), 0.5, 0.5, facecolor=plot_colors[j][0], label=plot_labels[j][0])
    line1 = plt.Line2D([0], [0], color=plot_colors[j][1], linestyle='-', label=plot_labels[j][1])
    line2 = plt.Line2D([0], [0], color=plot_colors[j][2], linestyle='--', label=plot_labels[j][2])

    if j > 0:
        ax[0, j].legend(handles=[contour, line1], loc='upper right', fontsize=9)
    else:
        ax[0, j].legend(handles=[contour, line1, line2], loc='lower right', fontsize=8.5)
    # ax[0, j].set_xlim([0, None])

ax[0, 0].set_ylim([0.65, 0.9])

plt.savefig('../paper_figures/deskids_post_official.pdf', bbox_inches='tight')
plt.show()

# %%
# Shifts reported in the text
key0 = 'Doux+22'
key1 = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_GNG'
params = ['S8', 'sigma8']
for param in params: 
    print(f"param = {param}")
    print(key0, results[key0]['MCSamples'].getInlineLatex(param))
    print(key1, results[key1]['MCSamples'].getInlineLatex(param))
    
    print(f"Shift [official/ours - 1 sigma]: {get_shift(key0, key1, param)[0]:.1f}")
    print()

# %%
# Shifts reported in the text
key0 = 'Doux+22'
key1 = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_halofit_GNG'
params = ['S8', 'sigma8']
for param in params: 
    print(f"param = {param}")
    print(key0, results[key0]['MCSamples'].getInlineLatex(param))
    print(key1, results[key1]['MCSamples'].getInlineLatex(param))
    
    print(f"Shift [official/ours - 1 sigma]: {get_shift(key0, key1, param)[0]:.1f}")
    print()

# %%
# Shifts reported in the text
key0 = 'Abbot+23'
key1 = 'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG'
params = ['S8', 'sigma8']
for param in params: 
    print(f"param = {param}")
    print(key0, results[key0]['MCSamples'].getInlineLatex(param))
    print(key1, results[key1]['MCSamples'].getInlineLatex(param))
    
    print(f"Shift [official/ours - 1 sigma]: {get_shift(key0, key1, param)[0]:.1f}")
    print()

# %%
# Shifts reported in the text
key0 = 'Abbot+23'
key1 = 'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG_hybridpriors_hmcode2020'
params = ['S8', 'sigma8']
for param in params: 
    print(f"param = {param}")
    print(key0, results[key0]['MCSamples'].getInlineLatex(param))
    print(key1, results[key1]['MCSamples'].getInlineLatex(param))
    
    print(f"Shift [official/ours - 1 sigma]: {get_shift(key0, key1, param)[0]:.1f}")
    print()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Mock data

# %%
pkfile = np.load('./test/EE2kmax10_Planck15_006.npz', allow_pickle=True)

cosmopars = dict(pkfile['cosmopars'].tolist())
cosmopars.update({'sigma8_tot': float(pkfile['sigma8 tot'])})
cosmopars['omega_matter'] = cosmopars['omega_cold'] + cosmopars['neutrino_mass'] / 93.14 / cosmopars['hubble']**2

input_params_p15 = {'Omega_c': cosmopars['omega_cold'] - cosmopars['omega_baryon'],
                'Omega_m': cosmopars['omega_matter'],
                'Omega_b': cosmopars['omega_baryon'],
                'sigma8': cosmopars['sigma8_tot'],
                'S8': cosmopars['sigma8_tot'] * np.sqrt(cosmopars['omega_matter'] / 0.3),
                'A_s': cosmopars['A_s'],
                'h': cosmopars['hubble'],
                'n_s': cosmopars['ns'],
                'M_nu': cosmopars['neutrino_mass'],
                'omega_m': cosmopars['omega_matter'] * cosmopars['hubble']**2,
                'limber_DESY3wl__0_dz': 0,
                'limber_DESY3wl__1_dz': 0,
                'limber_DESY3wl__2_dz': 0, 
                'limber_DESY3wl__3_dz': 0,
                'limber_KiDS1000__0_dz': 0,
                'limber_KiDS1000__1_dz': -0.002,
                'limber_KiDS1000__2_dz': -0.013,
                'limber_KiDS1000__3_dz': -0.011,
                'limber_KiDS1000__4_dz': 0.006,
                'limber_HSCDR1wl__0_dz': 0,
                'limber_HSCDR1wl__1_dz': 0,
                'limber_HSCDR1wl__2_dz': 0,
                'limber_HSCDR1wl__3_dz': 0,
                'bias_DESY3wl__0_m': -0.0063,
                'bias_DESY3wl__1_m': -0.0198,
                'bias_DESY3wl__2_m': -0.0241, 
                'bias_DESY3wl__3_m': -0.0369,
                'bias_KiDS1000__0_m': 0,
                'bias_KiDS1000__1_m': 0.,
                'bias_KiDS1000__2_m': 0.,
                'bias_KiDS1000__3_m': 0.,
                'bias_KiDS1000__4_m': 0.,
                'bias_HSCDR1wl__0_m': 0,
                'bias_HSCDR1wl__1_m': 0,
                'bias_HSCDR1wl__2_m': 0,
                'bias_HSCDR1wl__3_m': 0,
                'bias_DESY3wl_A_IA': 0,
                'limber_DESY3wl_eta_IA': 0,
                'bias_KiDS1000_A_IA': 0,
                'limber_KiDS1000_eta_IA': 0,
                'bias_HSCDR1wl_A_IA': 0,
                'limber_HSCDR1wl_eta_IA': 0,
               }

pkfile = np.load('./test/BACCOEmu_kmax10_baryonbf.npz', allow_pickle=True)

cosmopars = dict(pkfile['cosmopars'].tolist())
cosmopars.update({'sigma8_tot': float(pkfile['sigma8 tot'])})
cosmopars['omega_matter'] = cosmopars['omega_cold'] + cosmopars['neutrino_mass'] / 93.14 / cosmopars['hubble']**2

input_params_bf = {'Omega_c': cosmopars['omega_cold'] - cosmopars['omega_baryon'],
                'Omega_m': cosmopars['omega_matter'],
                'Omega_b': cosmopars['omega_baryon'],
                'sigma8': cosmopars['sigma8_tot'],
                'S8': cosmopars['sigma8_tot'] * np.sqrt(cosmopars['omega_matter'] / 0.3),
                'A_s': cosmopars['A_s'],
                'h': cosmopars['hubble'],
                'n_s': cosmopars['ns'],
                'M_nu': cosmopars['neutrino_mass'],
                'omega_m': cosmopars['omega_matter'] * cosmopars['hubble']**2,
                'limber_DESY3wl__0_dz': 0,
                'limber_DESY3wl__1_dz': 0,
                'limber_DESY3wl__2_dz': 0, 
                'limber_DESY3wl__3_dz': 0,
                'limber_KiDS1000__0_dz': 0,
                'limber_KiDS1000__1_dz': -0.002,
                'limber_KiDS1000__2_dz': -0.013,
                'limber_KiDS1000__3_dz': -0.011,
                'limber_KiDS1000__4_dz': 0.006,
                'limber_HSCDR1wl__0_dz': 0,
                'limber_HSCDR1wl__1_dz': 0,
                'limber_HSCDR1wl__2_dz': 0,
                'limber_HSCDR1wl__3_dz': 0,
                'bias_DESY3wl__0_m': -0.0063,
                'bias_DESY3wl__1_m': -0.0198,
                'bias_DESY3wl__2_m': -0.0241, 
                'bias_DESY3wl__3_m': -0.0369,
                'bias_KiDS1000__0_m': 0,
                'bias_KiDS1000__1_m': 0.,
                'bias_KiDS1000__2_m': 0.,
                'bias_KiDS1000__3_m': 0.,
                'bias_KiDS1000__4_m': 0.,
                'bias_HSCDR1wl__0_m': 0,
                'bias_HSCDR1wl__1_m': 0,
                'bias_HSCDR1wl__2_m': 0,
                'bias_HSCDR1wl__3_m': 0,
                'bias_DESY3wl_A_IA': 0,
                'limber_DESY3wl_eta_IA': 0,
                'bias_KiDS1000_A_IA': 0,
                'limber_KiDS1000_eta_IA': 0,
                'bias_HSCDR1wl_A_IA': 0,
                'limber_HSCDR1wl_eta_IA': 0,
               }
# input_params_p15.update({k: cosmopars[k] for k in ['M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_out', 'theta_inn', 'M_inn']})
# input_params_bf.update({k: cosmopars[k] for k in ['M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_out', 'theta_inn', 'M_inn']})

# %%
print('Baryon fraction')
print('Planck15: fb =', input_params_p15['Omega_b'] / (input_params_p15['Omega_b'] + input_params_p15['Omega_c'] ))
print('BF: fb =', input_params_bf['Omega_b'] / (input_params_bf['Omega_b'] + input_params_bf['Omega_c'] ))

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless',
         ]

print(chains[0], 'p-value = ', results[chains[0]]['pvalue_bf'], 'chi2 = ', results[chains[0]]['chi2bf'])
print(chains[1], 'p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['blue', 'orange']
labels = ['BAHAMAS simulation (Planck 2015)', r'BF cosmology & BAHAMAS baryonic boost']

# Plot
g = plots.get_single_plotter(width_inch=6)
g.settings.axes_fontsize = 15
g.settings.lab_fontsize = 20
g.settings.legend_fontsize = 13
g.plot_2d(MCSamples, 'Omega_m', 'S8', filled=[False, False, False, False],
          colors=colors,
          lws=[1, 1, 1, 1])

g.add_legend(legend_labels=labels, legend_loc='upper right')

bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    if i > 0:
        break
    ax = g.get_axes((i, i))
    if p1 in bf1:
        # ax.axvline(bf1[p1], color=colors[0])
        if p1 in input_params_p15:
            ax.axvline(input_params_p15[p1], color=colors[0], ls=':')
    if p1 in bf2:
        # ax.axvline(bf2[p1], color=colors[1])
        if p1 in input_params_bf:
            ax.axvline(input_params_bf[p1], color=colors[1], ls=':')
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color=colors[0], marker='o')
            if p1 in input_params_p15:
                ax.axvline(input_params_p15[p1], color=colors[0], ls=':')
            if p2 in input_params_p15:
                ax.axhline(input_params_p15[p2], color=colors[0], ls=':')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o')
            if p1 in input_params_bf:
                ax.axvline(input_params_bf[p1], color=colors[1], ls=':')
            if p2 in input_params_bf:
                ax.axhline(input_params_bf[p2], color=colors[1], ls=':')
            
            
# ax = g.get_axes((0, 1))
# t = ax.text(0.05, 0.05, 'DESY3+KiDS-1000+HSCDR1 (mock data)', horizontalalignment='left',
#             verticalalignment='bottom', transform=ax.transAxes, fontsize=16)
# t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
# ax.set_yticks([])
# ax.set_xticks([])
# ax.spines['top'].set_visible(False)
# ax.spines['right'].set_visible(False)

plt.savefig('../paper_figures/mock_cosmo.pdf', bbox_inches='tight')
plt.show()

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = ['BAHAMAS simulation (Planck 2015)', r'BF cosmology & BAHAMAS baryonic boost']
colors = ['blue', 'orange']

bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

g = plots.get_single_plotter(width_inch=3.1)
g.plot_1d(MCSamples, 'M_c',
                lws=[1, 1,],
                ls=['-', '-'],
                colors=colors
                )
# g.add_legend(labels, legend_ncol=1, legend_loc='upper center')

plt.axvline(bf1['M_c'], ls='--', color=colors[0])
plt.axvline(bf2['M_c'], ls='--', color=colors[1])
plt.axvline(13.621974339706481, ls=':', color='gray', label='Best fit to BAHAMAS')

plt.legend(frameon=False)
# Giovanni's bf to this BAHAMAS sim
#{‘z_0.0’: {‘M1_z0_cen’: 10.21114784428575, ‘M_c’: 13.621974339706481, ‘M_inn’: 9.945200990844508, ‘beta’: -0.28415315058242374, ‘eta’: -0.33540573478739955, ‘expfactor’: 1.0, ‘hubble’: 0.6774, ‘neutrino_mass’: 0.06, ‘ns’: 0.9667, ‘omega_baryon’: 0.0486, ‘omega_matter’: 0.3075, ‘sigma8’: 0.8159, ‘theta_inn’: -0.6156345275175403, ‘theta_out’: 0.12058036532821859, ‘w0’: -1, ‘wa’: 0},
# ‘z_1.0’: {‘M1_z0_cen’: 12.36112559131747, ‘M_c’: 14.896902319095929, ‘M_inn’: 12.303811495596529, ‘beta’: 0.23622444594324896, ‘eta’: -0.4768807172894638, ‘expfactor’: 0.5, ‘hubble’: 0.6774, ‘neutrino_mass’: 0.06, ‘ns’: 0.9667, ‘omega_baryon’: 0.0486, ‘omega_matter’: 0.3075, ‘sigma8’: 0.8159, ‘theta_inn’: -1.652220224429473, ‘theta_out’: 0.14413629346636098, ‘w0’: -1, ‘wa’: 0}}

plt.xlabel("$\log_{10}(M_c)$", fontsize=12)

plt.ylabel(r"$\mathcal{P}(\log(M_c))$", fontsize=12)
plt.savefig('../paper_figures/mock_Mc.pdf', bbox_inches='tight')
plt.show()
plt.close()


# %%
def get_a_k_Sk_from_table(pktable_b, pktable_dmo, h):
    z = np.unique(pktable_b[0])[::-1]
    k = np.unique(pktable_b[1]) * h

    assert np.all(pktable_dmo[0] == pktable_b[0])
    assert np.all(pktable_dmo[1] == pktable_b[1])

    k_arr = []
    Sk_arr = []
    for i, zi in enumerate(z):
        sel_b = pktable_b[0] == zi

        ki = pktable_b[1, sel_b] * h
        Sk = pktable_b[2, sel_b] / pktable_dmo[2, sel_b]
        
        assert np.all(ki == k)
        Sk_arr.append(Sk)
        
    Sk_arr = np.array(Sk_arr)
    return 1/(1+z), k, Sk_arr
    
# Load the baryon boost (BAHAMAS)
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_DMONLY_2fluid_nu0.06_Planck2015_L400N1024.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_BAHAMAS_nu0.06_Planck2015.dat", unpack=True, usecols=(0, 1, 2))


# Chains
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless',
         ]

labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange']
labels = ['Planck 2015 cosmology + BAHAMAS baryonic boost', 'BF cosmology + BAHAMAS baryonic boost']

f, ax = plt.subplots()

# Add BAHAMAS line (h as in Planck15)
pkfile = np.load('./test/EE2kmax10_Planck15_006.npz', allow_pickle=True)
cosmopars = dict(pkfile['cosmopars'].tolist())
a, k, Sk = get_a_k_Sk_from_table(pktable_b, pktable_dmo, cosmopars['hubble'])
ax.semilogx(k, Sk[-1], color='cyan', ls=':', label='BAHAMAS baryonic boost (Planck 2015)')

# Add BAHAMAS line (h as in BF)
pkfile = np.load('./test/BACCOEmu_kmax10_baryonbf.npz', allow_pickle=True)
cosmopars = dict(pkfile['cosmopars'].tolist())
a, k, Sk = get_a_k_Sk_from_table(pktable_b, pktable_dmo, cosmopars['hubble'])
ax.semilogx(k, Sk[-1], color='chocolate', ls=':', label='BAHAMAS baryonic boost (BF cosmology)')

# Add chain constraints
for i, c in enumerate(chains):
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    # label = dict(zip(chains, labels))[c]
    label = None
    results[c]['boost'].plot_baryon_post(ax, label, color, alpha=0.3 - 0.1*i)

# Finalize plot
ax.set_xlim([0.1, 2.55])
ax.set_ylim([0.85, 1.1])
ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$', fontsize=18)
ax.set_ylabel('$S_k(a=1)$', fontsize=18)
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=15)
ax.legend(loc='lower left', fontsize=13, frameon=False)

plt.savefig('../paper_figures/mock_Sk.pdf', bbox_inches='tight')
plt.show()
plt.close()

# %%
for k in ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']:
    print(k)
    for pn in ['Omega_m', 'S8', 'M_c', 'omega_m']:
        print(results[k]['MCSamples'].getInlineLatex(pn))
        
    print()

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax8192_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless',
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless',
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax8192_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless'
]

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = ['BAHAMAS simulation (Planck 2015)', '8192', r'BF cosmology & BAHAMAS baryonic boost', '8192']
colors = ['blue', 'cyan',  'orange', 'gold']

bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']
# bf3 = results[chains[2]]['pars_bf']
# bf4 = results[chains[3]]['pars_bf']

g = plots.get_single_plotter(width_inch=3.1)
g.plot_1d(MCSamples, 'M_c',
                lws=[1, 1,],
                ls=['-', '-'],
                colors=colors
                )
# g.add_legend(labels, legend_ncol=1, legend_loc='upper center')

plt.axvline(bf1['M_c'], ls='--', color=colors[0])
plt.axvline(bf2['M_c'], ls='--', color=colors[1])
# plt.axvline(bf3['M_c'], ls='--', color=colors[2])
# plt.axvline(bf4['M_c'], ls='--', color=colors[3])

plt.axvline(13.621974339706481, ls=':', color='gray', label='Best fit to BAHAMAS')

plt.legend(frameon=False)
# Giovanni's bf to this BAHAMAS sim
#{‘z_0.0’: {‘M1_z0_cen’: 10.21114784428575, ‘M_c’: 13.621974339706481, ‘M_inn’: 9.945200990844508, ‘beta’: -0.28415315058242374, ‘eta’: -0.33540573478739955, ‘expfactor’: 1.0, ‘hubble’: 0.6774, ‘neutrino_mass’: 0.06, ‘ns’: 0.9667, ‘omega_baryon’: 0.0486, ‘omega_matter’: 0.3075, ‘sigma8’: 0.8159, ‘theta_inn’: -0.6156345275175403, ‘theta_out’: 0.12058036532821859, ‘w0’: -1, ‘wa’: 0},
# ‘z_1.0’: {‘M1_z0_cen’: 12.36112559131747, ‘M_c’: 14.896902319095929, ‘M_inn’: 12.303811495596529, ‘beta’: 0.23622444594324896, ‘eta’: -0.4768807172894638, ‘expfactor’: 0.5, ‘hubble’: 0.6774, ‘neutrino_mass’: 0.06, ‘ns’: 0.9667, ‘omega_baryon’: 0.0486, ‘omega_matter’: 0.3075, ‘sigma8’: 0.8159, ‘theta_inn’: -1.652220224429473, ‘theta_out’: 0.14413629346636098, ‘w0’: -1, ‘wa’: 0}}

plt.xlabel("$\log_{10}(M_c)$", fontsize=12)

plt.ylabel(r"$\mathcal{P}(\log(M_c))$", fontsize=12)
# plt.savefig('../paper_figures/mock_Mc.pdf', bbox_inches='tight')
plt.show()
plt.close()


# %%
# Check the effect of going to lmax=8192 on Sk in mock data (almost the same as lmax=4500)
def get_a_k_Sk_from_table(pktable_b, pktable_dmo, h):
    z = np.unique(pktable_b[0])[::-1]
    k = np.unique(pktable_b[1]) * h

    assert np.all(pktable_dmo[0] == pktable_b[0])
    assert np.all(pktable_dmo[1] == pktable_b[1])

    k_arr = []
    Sk_arr = []
    for i, zi in enumerate(z):
        sel_b = pktable_b[0] == zi

        ki = pktable_b[1, sel_b] * h
        Sk = pktable_b[2, sel_b] / pktable_dmo[2, sel_b]
        
        assert np.all(ki == k)
        Sk_arr.append(Sk)
        
    Sk_arr = np.array(Sk_arr)
    return 1/(1+z), k, Sk_arr
    
# Load the baryon boost (BAHAMAS)
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_DMONLY_2fluid_nu0.06_Planck2015_L400N1024.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_BAHAMAS_nu0.06_Planck2015.dat", unpack=True, usecols=(0, 1, 2))


# Chains
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless',
          "desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax8192_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless"

          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless',
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax8192_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless'
         ]

labels = [results[i]['label'] for i in chains]
colors = ['blue', 'orange']
labels = ['Planck 2015 cosmology + BAHAMAS baryonic boost ($\ell < 4500$)', "$(\ell < 8192)$"]
# labels = ['BF cosmology + BAHAMAS baryonic boost ($\ell < 4500$)', "$(\ell < 8192)$"]

f, ax = plt.subplots()

# Add BAHAMAS line (h as in Planck15)
pkfile = np.load('./test/EE2kmax10_Planck15_006.npz', allow_pickle=True)
cosmopars = dict(pkfile['cosmopars'].tolist())
a, k, Sk = get_a_k_Sk_from_table(pktable_b, pktable_dmo, cosmopars['hubble'])
ax.semilogx(k, Sk[-1], color='lightblue', ls='--', label='BAHAMAS baryonic boost (Planck 2015)')

# Add BAHAMAS line (h as in BF)
pkfile = np.load('./test/BACCOEmu_kmax10_baryonbf.npz', allow_pickle=True)
cosmopars = dict(pkfile['cosmopars'].tolist())
a, k, Sk = get_a_k_Sk_from_table(pktable_b, pktable_dmo, cosmopars['hubble'])
ax.semilogx(k, Sk[-1], color='gold', ls='--', label='BAHAMAS baryonic boost (BF cosmology)')

# Add chain constraints
for i, c in enumerate(chains):
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    label = dict(zip(chains, labels))[c]
    # label = None
    results[c]['boost'].plot_baryon_post(ax, label, color, alpha=0.3 - 0.1*i)

# Finalize plot
ax.set_xlim([0.1, 2.55])
ax.set_ylim([0.85, 1.1])
ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$', fontsize=14)
ax.set_ylabel('$S_k(a=1)$', fontsize=14)
ax.tick_params(axis='x', labelsize=12)
ax.tick_params(axis='y', labelsize=12)
ax.legend(loc='lower left', fontsize=12, frameon=False)

# plt.savefig('../paper_figures/mock_Sk.pdf', bbox_inches='tight')
plt.show()
plt.close()

# %%
b = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless']['boost']
sb = b.get_MCSamples_baryons()
# sb.getInlineLatex('Sk_')
print(sb.getParamNames().list())
print(sb.getInlineLatex('Sk2p51'))


# %%
stats.chi2

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## $C_\ell$ Data and BF

# %%
survey = 'DESY3'
nbin = 4

#######
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

sdata = results[chains[0]].get_cl_data_sacc()

sbf = results[chains[0]].get_cl_theory_sacc_bf()
sbf_nb = results[chains[1]].get_cl_theory_sacc_bf()

f, ax = plt.subplots(nbin, nbin, figsize=(12, 8), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in sdata.get_tracer_combinations():
    if survey not in ''.join(trs):
        continue
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])

    # Data
    ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8)    

    # BF No-Baryons
    ell, cl = sbf_nb.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='blue', label='BF (No Baryons)', alpha=0.8)

    # BF baryons
    ell, cl = sbf.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='orange', label='BF (Baryons)', alpha=0.8)

    t = ax[i, j].text(0.99, 0.94, f'({i}, {j})', horizontalalignment='right',
                      verticalalignment='top', transform=ax[i, j].transAxes, fontsize=12) 
    
    if i != j:
        ax[j, i].axis('off')

for i in range(nbin):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$", fontsize=15)
    ax[-1, i].set_xlabel("$\ell$", fontsize=15)
    ax[0, i].set_xscale("log")
    ax[i, 0].tick_params(axis='y', labelsize=13)
    ax[-1, i].tick_params(axis='x', labelsize=13)

t = ax[0, 2].text(0.95, 0.95, f'{survey}', horizontalalignment='right',
                  verticalalignment='top', transform=ax[0, 2].transAxes, fontsize=18) 
ax[0, 0].legend(bbox_to_anchor=(1., 1.05), fontsize=15)

# plt.savefig('../paper_figures/bf_des.pdf', bbox_inches='tight')

plt.show()
plt.close()

# %%
survey = 'KiDS1000'
nbin = 5

#######
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

sdata = results[chains[0]].get_cl_data_sacc()

sbf = results[chains[0]].get_cl_theory_sacc_bf()
sbf_nb = results[chains[1]].get_cl_theory_sacc_bf()

# f, ax = plt.subplots(nbin, nbin, figsize=(15, 10), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
f, ax = plt.subplots(nbin, nbin, figsize=(12, 8), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in sdata.get_tracer_combinations():
    if survey not in ''.join(trs):
        continue
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])

    # Data
    ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8)    

    # BF No-Baryons
    ell, cl = sbf_nb.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='blue', label='BF (No Baryons)', alpha=0.8)

    # BF baryons
    ell, cl = sbf.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='orange', label='BF (Baryons)', alpha=0.8)

    t = ax[i, j].text(0.99, 0.94, f'({i}, {j})', horizontalalignment='right',
                      verticalalignment='top', transform=ax[i, j].transAxes, fontsize=12) 
    
    if i != j:
        ax[j, i].axis('off')

for i in range(nbin):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$", fontsize=15)
    ax[-1, i].set_xlabel("$\ell$", fontsize=15)
    ax[0, i].set_xscale("log")
    ax[i, 0].tick_params(axis='y', labelsize=13)
    ax[-1, i].tick_params(axis='x', labelsize=13)

ax[2, 0].set_ylim([None, 6])


t = ax[0, 3].text(0.95, 0.95, f'KiDS-1000', horizontalalignment='right',
                  verticalalignment='top', transform=ax[0, 3].transAxes, fontsize=18) 
ax[0, 0].legend(bbox_to_anchor=(1., 1.05), fontsize=15)

plt.savefig('../paper_figures/bf_kids.pdf', bbox_inches='tight')

plt.show()
plt.close()

# %%
survey = 'HSC'
nbin = 4

#######
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

sdata = results[chains[0]].get_cl_data_sacc()

sbf = results[chains[0]].get_cl_theory_sacc_bf()
sbf_nb = results[chains[1]].get_cl_theory_sacc_bf()

f, ax = plt.subplots(nbin, nbin, figsize=(12, 8), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in sdata.get_tracer_combinations():
    if survey not in ''.join(trs):
        continue
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])

    # Data
    ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8)    

    # BF No-Baryons
    ell, cl = sbf_nb.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='blue', label='BF (No Baryons)', alpha=0.8)

    # BF baryons
    ell, cl = sbf.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='orange', label='BF (Baryons)', alpha=0.8)

    t = ax[i, j].text(0.99, 0.94, f'({i}, {j})', horizontalalignment='right',
                      verticalalignment='top', transform=ax[i, j].transAxes, fontsize=12) 
    
    if i != j:
        ax[j, i].axis('off')

for i in range(nbin):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$", fontsize=15)
    ax[-1, i].set_xlabel("$\ell$", fontsize=15)
    ax[0, i].set_xscale("log")
    ax[i, 0].tick_params(axis='y', labelsize=13)
    ax[-1, i].tick_params(axis='x', labelsize=13)
    
t = ax[0, 2].text(0.95, 0.95, f'HSCDR1', horizontalalignment='right',
                  verticalalignment='top', transform=ax[0, 2].transAxes, fontsize=18) 
ax[0, 0].legend(bbox_to_anchor=(1., 1.05), fontsize=15)

plt.savefig('../paper_figures/bf_hsc.pdf', bbox_inches='tight')

plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## DESY3 $C_\ell$ our reanalysis vs official at NSIDE=1024

# %%
# Load Cells computed with NSIDE 1024
# s = sacc.Sacc.load_fits("/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3wl__2x2pt/cls_cov.fits")
nbin = 4

# E-modes
f, ax = plt.subplots(nbin, nbin, figsize=(12, 8), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in s.get_tracer_combinations():
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])
    
    # Official data
    ell, cl, cov = sOfficial.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', c='k', label='Doux et al. 2022')
    
    # Data
    ell, cl, cov = s.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='orange', label='Reanalysis', alpha=0.8)

    
    t = ax[i, j].text(0.99, 0.94, f'({i}, {j})', horizontalalignment='right',
                      verticalalignment='top', transform=ax[i, j].transAxes, fontsize=12) 
    
    if i != j:
        ax[j, i].axis('off')

for i in range(nbin):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$", fontsize=15)
    ax[-1, i].set_xlabel("$\ell$", fontsize=15)
    ax[0, i].set_xscale("log")
    ax[i, 0].tick_params(axis='y', labelsize=13)
    ax[-1, i].tick_params(axis='x', labelsize=13)
    
t = ax[0, 2].text(0.95, 0.95, f'DESY3', horizontalalignment='right',
                  verticalalignment='top', transform=ax[0, 2].transAxes, fontsize=18) 
ax[0, 0].legend(bbox_to_anchor=(1., 1.05), fontsize=15)

plt.savefig('../paper_figures/des_cl_official.pdf', bbox_inches='tight')

plt.show()
plt.close()


# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Footprints

# %%
# Sampling the shear field or averaging?
# hscmask = hp.read_map("/mnt/extraspace/damonge/Datasets/HSC_DR1/lite/mask_mask_HSC_wl0_coordC_ns4096.fits.gz")
# desy3mask = hp.read_map("/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY3wl0_coordC_ns4096.fits.gz")
# k1000mask = hp.read_map("/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__0_coordC_ns4096.fits.gz")

def get_mean_ngal(mask):
    goodpix = mask != 0 
    return np.mean(mask[goodpix])
# These numbers seem too big. DESY3~17 for NSIDE~1024. I need to recompute them w/o their weights, I think.
print(f'HSCDR1: Ngal/pix = {get_mean_ngal(hscmask):.1f}')
print(f'DESY3: Ngal/pix = {get_mean_ngal(desy3mask):.1f}')
print(f'KiDS-1000: Ngal/pix = {get_mean_ngal(k1000mask):.1f}')

# %%
from matplotlib.colors import ListedColormap
desy3mask = hp.read_map("/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY3wl0_coordC_ns4096.fits.gz")
hscmask = hp.read_map("/mnt/extraspace/damonge/Datasets/HSC_DR1/lite/mask_mask_HSC_wl0_coordC_ns4096.fits.gz")
k1000mask = hp.read_map("/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__0_coordC_ns4096.fits.gz")

# For the transaprency to work you need to comment healpy/projaxes.py#L189
# See https://github.com/healpy/healpy/issues/765
desy3mask = scale_bin_map(desy3mask, 1)
k1000mask = scale_bin_map(k1000mask, 2)
hscmask = scale_bin_map(hscmask, 3)
cbound = {'min': 1, 
          'max': 3}

colors = ['blue', 'orange', 'red']
cmap = ListedColormap(colors)

hp.mollview(desy3mask, badcolor='lightgray', cbar=False, title='', alpha=0.8 * np.ones_like(desy3mask), **cbound, cmap=cmap)
hp.mollview(k1000mask, badcolor='none', alpha=np.ones_like(desy3mask), reuse_axes=True, cbar=False, title='', **cbound, cmap=cmap)
hp.mollview(hscmask, badcolor='none', alpha=0.8 * np.ones_like(desy3mask), reuse_axes=True, cbar=False, title='', **cbound, cmap=cmap)

hp.graticule()

plt.text(-0.95, -0.5, 'DESY3', fontsize=18, color=colors[0], horizontalalignment='right')
plt.text(1.8, 0.1, 'KiDS-1000', fontsize=18, color=colors[1], horizontalalignment='right')
plt.text(0.8, 0.45, 'HSCDR1', fontsize=18, color=colors[2], horizontalalignment='left')

plt.savefig('../paper_figures/footprint_deskidshsc.pdf', bbox_inches='tight')
plt.show()
plt.close()

# %%
desy3mask = hp.read_map("/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY3wl0_noK1000_noHSCDR1wl_coordC_ns4096.fits.gz")
k1000mask = hp.read_map("/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__0_noHSCDR1wl_coordC_ns4096.fits.gz")
hscmask = hp.read_map("/mnt/extraspace/damonge/Datasets/HSC_DR1/lite/mask_mask_HSC_wl0_coordC_ns4096.fits.gz")

# For the transaprency to work you need to comment healpy/projaxes.py#L189
# See https://github.com/healpy/healpy/issues/765
desy3mask = scale_bin_map(desy3mask, 1)
k1000mask = scale_bin_map(k1000mask, 2)
hscmask = scale_bin_map(hscmask, 3)
cbound = {'min': 1, 
          'max': 3}

colors = ['blue', 'orange', 'red']
cmap = ListedColormap(colors)

hp.mollview(desy3mask, badcolor='lightgray', cbar=False, title='', alpha=0.8 * np.ones_like(desy3mask), **cbound, cmap=cmap)
hp.mollview(k1000mask, badcolor='none', alpha=np.ones_like(desy3mask), reuse_axes=True, cbar=False, title='', **cbound, cmap=cmap)
hp.mollview(hscmask, badcolor='none', alpha=0.8 * np.ones_like(desy3mask), reuse_axes=True, cbar=False, title='', **cbound, cmap=cmap)

hp.graticule()

plt.text(-0.95, -0.5, 'DESY3', fontsize=18, color=colors[0], horizontalalignment='right')
plt.text(1.8, 0.1, 'KiDS-1000', fontsize=18, color=colors[1], horizontalalignment='right')
plt.text(0.8, 0.45, 'HSCDR1', fontsize=18, color=colors[2], horizontalalignment='left')

plt.savefig('../paper_figures/footprint_deskidshsc_cut.pdf', bbox_inches='tight')
plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Tension with Planck

# %%
# Print a latex table with the results above using Texttable

table_1 = Texttable()
table_1.set_precision(1)
table_1.set_cols_align(['l', 'c', 'c'])
table_1.set_cols_valign(['m', 'm', 'm'])

rows = [["Chain", r"$\Delta S_8 [\sigma]$", r"$\Delta \Omega_m [\sigma]$"]]

keys = get_sorted_keys(exclude_mock=True, exclude=['boost_with_fixedcosmoP18'])[::-1]

for k in keys:
    if ('baryons' not in k) or ('fixedcosmo' in k):
        continue
    
    label = results[k]['label'].replace('%', '\%').replace('&', '\&')
    # Delta S8
    dS8, rsigmaS8 = get_shift(k, 'P18_official_mnu', 'S8')  
    # Delta Omega_m
    dOm, rsigmaOm = get_shift(k, 'P18_official_mnu', 'Omega_m')  
    
    rows.append([label, dS8, dOm])

    
table_1.add_rows(rows)
# print(table_1.draw())
print(latextable.draw_latex(table_1, caption=r"Comparison with Planck 2018 TTTEEE+lowl+lensing+$m_\nu$", label="tab:P18"))

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## DESY3 $\chi^2$ our BF vs Giovanni's

# %%
# Our BF to our DESY3 data
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG'
chi2bf = results[key]._get_chi2bf()
pvaluebf = results[key]['pvalue_bf']

# Giovanni's BF applied to our data
pars = results['Arico+23']['pars_bf'].copy()
pars['A_sE9'] = pars['A_s'] * 1e9
chi2_Arico23 = results[key].evaluate_model(pars)[0]
pvalue_Arico23 = results[key].get_pvalue(pars)[0]

print('lmax = 4500 (fiducial)')
print(f'chi2(our BF) = {chi2bf:.2f} vs {chi2_Arico23:.2f} = chi2(Arico+23 BF)')
print(f'p-value(our BF) = {pvaluebf:.2f} vs {pvalue_Arico23:.2f} = p-value(Arico+23 BF)')

# %%
# Our BF to our DESY3 data
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG'
chi2bf = results[key]._get_chi2bf()
pvaluebf = results[key]['pvalue_bf']

# Giovanni's BF applied to our data
pars = results['Arico+23']['pars_bf'].copy()
pars['A_sE9'] = pars['A_s'] * 1e9
chi2_Arico23 = results[key].evaluate_model(pars)[0]
pvalue_Arico23 = results[key].get_pvalue(pars)[0]

print('lmax = 8192')
print(f'chi2(our BF) = {chi2bf:.2f} vs {chi2_Arico23:.2f} = chi2(Arico+23 BF)')
print(f'p-value(our BF) = {pvaluebf:.2f} vs {pvalue_Arico23:.2f} = p-value(Arico+23 BF)')

# %%
# Giovanni, using our fiducial BF (lmax=4500), gets chi2 =  418.80726102304703
print("Fiducial ell_max = 4500")
print("Our BF applied to Arico+23 real space analysis, p = ", 1 - stats.chi2.cdf(418.80726102304703, 397.16))

print()
# Giovanni, using our BF for lmax=8192, chi2 =  418.1147210123723
print("ell_max = 8192")
print("Our BF applied to Arico+23 real space analysis, p = ", 1 - stats.chi2.cdf(418.1147210123723, 397.16))

print()
print("Arico+23 BF in real space p = ", 1 - stats.chi2.cdf(414.14, 397.16))

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Real vs Fourier space

# %%
# theta = pi / ell
np.rad2deg(np.pi / 4500) * 60

# %%
# Load the sacc file only to get the tracer combinations
s = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG'].get_cl_data_sacc()

cosmo = ccl.CosmologyVanillaLCDM()
ccl_tracers = {}
for trn, tr in s.tracers.items():
    ccl_tracers[trn] = ccl.WeakLensingTracer(cosmo, dndz=(tr.z, tr.nz), ia_bias=None)

cls = {}
ell = np.concatenate([[0, 1], np.unique(np.logspace(np.log10(2), np.log10(1e5), 30, dtype=int))], dtype=int)
for trs in s.get_tracer_combinations():
    cls[trs] = (ell, ccl.angular_cl(cosmo, ccl_tracers[trs[0]], ccl_tracers[trs[1]], ell))

nbin = 1
f, ax = plt.subplots(nbin, nbin, figsize=(5, 3.3))

ell_all = np.arange(100000)
# J4 Bessel function of first kind
J4 = lambda x: scipy.special.jv(4, x)

color = {2.5: 'blue',
         4: 'orange'}
colorlight = {2.5: 'lightblue',
              4:  'lightcoral'}
for angle in [2.5, 4]:
    kernel_factor = lambda ell: ell * J4(ell * angle/60/180*np.pi)
    
    trs = ('DESY3wl__3', 'DESY3wl__3')
    ell, clc = cls[trs]
    cl = interp1d(ell, clc, bounds_error=False, fill_value=0)
    kernel = lambda ell: kernel_factor(ell) * cl(ell)
    xi_int = scipy.integrate.cumulative_trapezoid(kernel(ell_all), x=ell_all, initial=0)
    # Kernel
    ax.semilogx(ell_all, xi_int/xi_int[-1], label=f"$\\theta = {angle}'$", color=color[angle])

    if angle == 4:
        ax.axvline(4500, ls='--', color='gray', label=r'$\ell=4500$')
        ax.axvline(8192, ls=':', color='gray', label=r'$\ell=8192$')

    # Get ell at which the integral converges at 5%
    threshold = (xi_int/xi_int[-1] > 0.95) * (xi_int/xi_int[-1] < 1.05)
    lth = None
    for iell, ell in enumerate(ell_all):
        if np.all(threshold[iell:]):
            lth = ell
            break

    ax.axvline(lth, ls='--', color=colorlight[angle])

    # Get ell at which the integral converges at 1%
    threshold = (xi_int/xi_int[-1] > 0.99) * (xi_int/xi_int[-1] < 1.01)
    lth = None
    for iell, ell in enumerate(ell_all):
        if np.all(threshold[iell:]):
            lth = ell
            break

    ax.axvline(lth, ls=':', color=colorlight[angle]) #, label='$1\%$ error' if angle == 2.5 else None)

ax.axhline(1, ls='--', color='gray', alpha=0.5)

ax.set_ylabel(r"$\int_0^\ell d\ell \ell J_4(\ell \theta) C_\ell^{EE}$ / $\int_0^\infty d\ell \ell J_4(\ell \theta) C_\ell^{EE}$", fontsize=12)
ax.set_xlabel("$\ell$", fontsize=12)
ax.set_xlim([1e3, ell_all[-1]])
ax.set_ylim([None, 1.25])
ax.tick_params(axis='y', labelsize=11)
ax.tick_params(axis='x', labelsize=11)
ax.legend(loc='upper left', frameon=False)


ax.fill_between(ell_all, 0.5, 1.5, alpha=0.3, color="lightblue")
ax.fill_between(ell_all, 0.79, 1.21, alpha=0.3, color="yellow")


t = ax.text(0.51, 0.05, '5% error', horizontalalignment='left',
            verticalalignment='bottom', transform=ax.transAxes, fontsize=12)
t.set_bbox(dict(facecolor='coral', alpha=0.5, edgecolor='red'))

t = ax.text(0.75, 0.05, '1% error', horizontalalignment='left',
            verticalalignment='bottom', transform=ax.transAxes, fontsize=12)
t.set_bbox(dict(facecolor='coral', alpha=0.5, edgecolor='red'))

t = ax.text(0.99, 0.95, 'DESY3 (4th bin)', horizontalalignment='right',
            verticalalignment='top', transform=ax.transAxes, fontsize=12)

plt.savefig('../paper_figures/xi_int.pdf', bbox_inches='tight')
    
plt.tight_layout()
plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Extrapolation

# %%
# DES+KiDS+HSC with lmax=4500
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_single_plotter(width_inch=6)
g.settings.axes_fontsize = 14
g.settings.lab_fontsize = 16
# g.settings.legend_fontsize = 12
g.plot_2d(MCSamples, parameters, filled=[True, False, True],
                colors=['brown', 'blue', 'black'],
                lws=[1, 1, 1])
g.add_legend(['Fiducial (quadratic spline in log-space) extrapolation', 'HALOFIT extrapolation'], legend_loc='upper left', colored_text=True);

plt.savefig('../paper_figures/deskidshsc_extrap_cosmo.pdf', bbox_inches='tight')

# %%
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

colors = ['brown', 'blue']
labels = ['Fiducial (quadratic spline in log-space) extrapolation', 'HALOFIT extrapolation']

f, ax = plt.subplots()

# Add chain constraints
for i, c in enumerate(chains):
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    # label = dict(zip(chains, labels))[c]
    label=None
    results[c]['boost'].plot_baryon_post(ax, label, color, alpha=0.3 - 0.1*i)

# Finalize plot
ax.set_xlim([0.1, 2.5])
ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$', fontsize=16)
ax.set_ylabel('$S_k(a=1)$', fontsize=16)
ax.tick_params(axis='x', labelsize=14)
ax.tick_params(axis='y', labelsize=14)
ax.legend(loc='lower left', fontsize=14, frameon=False)

plt.savefig('../paper_figures/deskidshsc_extrap_Sk.pdf', bbox_inches='tight')
plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Full corner plot for appendix (fiducial only)

# %%
# All surveys (lmax = 4500)
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
         ]

parameters = ['A_sE9', 'Omega_m', 'Omega_b',  'h', 'n_s', 'm_nu',  'omega_m', 'S12', 'S8', 'sigma8']


MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = [ 'DESY3 + KiDS-1000 + HSCDR1', 'DESY3', 'KiDS-1000', 'HSCDR1' ]
colors = ['brown', 'blue', 'orange', 'red']


g = plots.get_subplot_plotter()
g.settings.axes_fontsize = 17
g.settings.lab_fontsize = 25
g.settings.legend_fontsize = 30

g.triangle_plot(MCSamples, parameters,
                filled=[True, False, False, False],
                contour_colors=colors,
                legend_labels=labels,
                contour_lws=[3, 1, 1, 1],
                label_order=[1, 2, 3, 0],
                contour_ls=['-', '-', '-', '-'],
                contour_args=[{'alpha': 0.7}, {'alpha': 1}, {'alpha': 1}, {'alpha': 1}]
                )
plt.savefig('../paper_figures/full_posteriors_cosmo.pdf', bbox_inches='tight')


# %%
# All surveys (lmax = 4500)
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
         ]

parameters = ['Omega_m', 'S8', 'sigma8', 'M_c', 'eta', 'beta', 'M1_z0_cen', 'theta_inn', 'theta_out', 'M_inn']
paramlabels = [r'\log_{10}(M_c)', r'\log_{10}(\eta)', r'\log_{10}(\beta)', r'\log_{10}(M_{z_0, {\rm cen}})', r'\log_{10}(\theta_{\rm inn})', r'\log_{10}(\theta_{\rm out})', r'\log_{10}(M_{\rm inn})']


MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = ['DESY3 + KiDS-1000 + HSCDR1', 'DESY3', 'KiDS-1000', 'HSCDR1', ]
colors = ['brown', 'blue', 'orange', 'red']


g = plots.get_subplot_plotter()
g.settings.axes_fontsize = 17
g.settings.lab_fontsize = 25
g.settings.legend_fontsize = 30

g.triangle_plot(MCSamples, parameters,
                filled=[True, False, False, False],
                contour_colors=colors,
                legend_labels=labels,
                label_order=[1, 2, 3, 0],
                contour_lws=[3, 1, 1, 1],
                contour_ls=['-', '-', '-', '-'],
                contour_args=[{'alpha': 0.7}, {'alpha': 1}, {'alpha': 1}, {'alpha': 1}]
                )

for i in range(7):
    ax = g.get_axes((i+3, 0))
    ax.set_ylabel(f"${paramlabels[i]}$", fontsize=20)
    ax = g.get_axes((-1, i+3))
    ax.set_xlabel(f"${paramlabels[i]}$", fontsize=20)

plt.savefig('../paper_figures/full_posteriors_baryons.pdf', bbox_inches='tight')

# %%
# All surveys (lmax = 4500)
chains = [
          'priors',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 

         ]

s = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples']
pn = s.getParamNames().list()
parameters = [k for k in pn if ('bias' in k and '_m' in k)]

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = [ 'Priors',  'DESY3', 'KiDS-1000', 'HSCDR1', 'DESY3 + KiDS-1000 + HSCDR1']
colors = ['gray', 'blue', 'orange', 'red', 'brown']


g = plots.get_subplot_plotter()
g.settings.axes_fontsize = 20
g.settings.lab_fontsize = 30
g.settings.legend_fontsize = 30

g.triangle_plot(MCSamples, parameters,
                filled=[False, False, False, False, True],
                contour_colors=colors,
                legend_labels=labels,
                contour_lws=[3, 3, 3, 3, 1],
                contour_ls=['--', '-', '-', '-', '-'],
                contour_args=[{'alpha': 1},  {'alpha': 1}, {'alpha': 1}, {'alpha': 1}, {'alpha': 0.7},]
                )


plt.savefig('../paper_figures/full_posteriors_mbias.pdf', bbox_inches='tight')

# %%
chains = [
          'priors',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 

         ]

s = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples']
pn = s.getParamNames().list()
parameters = [k for k in pn if ('limber' in k and '_dz' in k)]

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = [ 'Priors',  'DESY3', 'KiDS-1000', 'HSCDR1', 'DESY3 + KiDS-1000 + HSCDR1']
colors = ['gray', 'blue', 'orange', 'red', 'brown']


g = plots.get_subplot_plotter()
g.settings.axes_fontsize = 20
g.settings.lab_fontsize = 30
g.settings.legend_fontsize = 30

g.triangle_plot(MCSamples, parameters,
                filled=[False, False, False, False, True],
                contour_colors=colors,
                legend_labels=labels,
                contour_lws=[3, 3, 3, 3, 1],
                contour_ls=['--', '-', '-', '-', '-'],
                contour_args=[{'alpha': 1},  {'alpha': 1}, {'alpha': 1}, {'alpha': 1}, {'alpha': 0.7},]
                )


plt.savefig('../paper_figures/full_posteriors_dz.pdf', bbox_inches='tight')

# %%
s = results['hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300']['MCSamples']
p = s.getParams()
s.addDerived(p.bias_A_IA, 'bias_HSCDR1wl_A_IA', 'A^{\rm HSCDR1}_{\rm IA}')
s.addDerived(p.limber_eta_IA, 'limber_HSCDR1wl_eta_IA', '\eta^{\rm HSCDR1}_{\rm IA}')

s = results['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG']['MCSamples']
p = s.getParams()
s.addDerived(p.bias_A_IA, 'bias_DESY3wl_A_IA', 'A^{\rm DESY3}_{\rm IA}')
s.addDerived(p.limber_eta_IA, 'limber_DESY3wl_eta_IA', '\eta^{\rm DESY3}_{\rm IA}')

s = results['k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG']['MCSamples']
p = s.getParams()
s.addDerived(p.bias_A_IA, 'bias_KiDS1000_A_IA', 'A^{\rm KiDS-1000}_{\rm IA}')
s.addDerived(p.limber_eta_IA, 'limber_KiDS1000_eta_IA', '\eta^{\rm KiDS-1000}_{\rm IA}')

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
         ]

s = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples']
pn = s.getParamNames().list()
parameters = [k for k in pn if ('IA' in k)]

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = ['DESY3 + KiDS-1000 + HSCDR1', 'DESY3', 'KiDS-1000', 'HSCDR1' ]
colors = ['brown', 'blue', 'orange', 'red']


g = plots.get_subplot_plotter()
g.settings.axes_fontsize = 17
g.settings.lab_fontsize = 20
g.settings.legend_fontsize = 20

g.triangle_plot(MCSamples, parameters,
                filled=[True, False, False, False],
                contour_colors=colors,
                legend_labels=labels,
                label_order=[1, 2, 3, 0],
                contour_lws=[3, 1, 1, 1],
                contour_ls=['-', '-', '-', '-'],
                contour_args=[{'alpha': 0.7}, {'alpha': 1}, {'alpha': 1}, {'alpha': 1}]
                )


plt.savefig('../paper_figures/full_posteriors_IA.pdf', bbox_inches='tight')

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Why do we constrain $\Omega_m$ with small scales? ($C_\ell^{\rm BF}$ and with Omega_m += 0.1 keeping S_8 constant)

# %%
# Vary Omega_m keeping S8 constant
# pars = results[chains[0]]['pars_bf'].copy()
# pars2 = dict(pars)
# pars2['Omega_m'] += 0.1
# sigma8_target = pars2['S8'] * np.sqrt(0.3 / pars2['Omega_m'])
# # As_target = get_As_target(pars['sigma8'], pars['A_sE9'], sigma8_target)
# pars2['A_sE9'] = 1.41
# sbf_nb = results[chains[0]].get_cl_theory_sacc(pars2)

# lkl = results[chains[0]].get_lkl()
# cosmo = lkl.provider.get_CCL()['cosmo']
# print(cosmo.sigma8(), sigma8_target)

# %%
survey = 'DESY3'
nbin = 4

#######
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
         ]

sdata = results[chains[0]].get_cl_data_sacc()

sbf = results[chains[0]].get_cl_theory_sacc_bf()
pars = results[chains[0]]['pars_bf'].copy()
# Vary Omega_m keeping S8 constant
pars2 = dict(pars)
pars2['Omega_m'] += 0.1
sigma8_target = pars2['S8'] * np.sqrt(0.3 / pars2['Omega_m'])
pars2['A_sE9'] = 1.41 # Tuned by hand
sbf_nb = results[chains[0]].get_cl_theory_sacc(pars2)

lkl = results[chains[0]].get_lkl()
cosmo = lkl.provider.get_CCL()['cosmo']
print(cosmo.sigma8(), sigma8_target)

# pars3 = dict(pars)
# pars3['Omega_m'] += 0.05
# sbf_nb3 = results[chains[0]].get_cl_theory_sacc(pars3)

f, ax = plt.subplots(nbin, nbin, figsize=(12, 8), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in sdata.get_tracer_combinations():
    if survey not in ''.join(trs):
        continue
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])

    # Data
    ell, cl, cov, ind = sdata.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='k', label='Data', alpha=0.8)    

    # BF No-Baryons
    ell, cl = sbf_nb.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='blue', label='BF ($\Omega_m += 0.1$)', alpha=0.8)

    # # BF No-Baryons
    # ell, cl = sbf_nb3.get_ell_cl('cl_ee', trs[0], trs[1])
    # ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='green', label='BF ($\Omega_m += 0.05$)', alpha=0.8)
    
    # BF baryons
    ell, cl = sbf.get_ell_cl('cl_ee', trs[0], trs[1])
    ax[i, j].errorbar(ell, ell * cl * 1e7, fmt='.', color='orange', label='BF (Baryons)', alpha=0.8)

    t = ax[i, j].text(0.99, 0.94, f'({i}, {j})', horizontalalignment='right',
                      verticalalignment='top', transform=ax[i, j].transAxes, fontsize=12) 
    
    if i != j:
        ax[j, i].axis('off')

for i in range(nbin):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$", fontsize=15)
    ax[-1, i].set_xlabel("$\ell$", fontsize=15)
    ax[0, i].set_xscale("log")
    ax[i, 0].tick_params(axis='y', labelsize=13)
    ax[-1, i].tick_params(axis='x', labelsize=13)

t = ax[0, 2].text(0.95, 0.95, f'{survey}', horizontalalignment='right',
                  verticalalignment='top', transform=ax[0, 2].transAxes, fontsize=18) 
ax[0, 0].legend(bbox_to_anchor=(1., 1.05), fontsize=15)


plt.show()
plt.close()

# %%
pars['A'], pars2['Omega_m']

# %% [markdown]
# # Plots talks

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no baryons ($\ell = 2048$ and DESY3, KiDS and combination): we can trust 2105.12108 constraints

# %%
chains = []

chains.append(['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG',
               'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG'])

chains.append(['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG',
               'k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG'])


chains.append([
          'desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG',
          'desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG'])

chains.append([
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG'
         ])

surveys = ['DESY3', 'KiDS-1000', 'DESY3 + KiDS-1000']
# labels = [fr'$\ell_{{\rm max}} = {lmax}$' for lmax in [1000, 2048, 4500, 8192]]
labels = ['Baryons', 'No baryons']
colors= ['blue', 'orange', 'green']

# Plot
g = plots.get_single_plotter()
g.settings.legend_frame = True

f, ax = plt.subplots(1, 3, figsize=(10, 4), gridspec_kw={'hspace': 0, 'wspace': 0}, sharey=True, sharex=True)
for i in range(3):
    MCSamples = [results[c]['MCSamples'] for c in chains[i]]    
    g.plot_2d(MCSamples, 'Omega_m', 'S8', 
              ax=ax[i], colors=[colors[i], 'red'], ls=['-', '--'],
              filled=[True, False]
              )
    ax[i].set_title(surveys[i])
    if i > 0:
        ax[i].set_ylabel('')
    

ax[0].set_xlim([0.15, 0.55])
ax[0].set_ylim([0.68, 0.86])

ax[-1].text(0.95, 0.02, r"$\ell_{\rm max} = 2048$", horizontalalignment='right', verticalalignment='bottom', transform=ax[-1].transAxes, fontsize=15)


g.add_legend(legend_labels=labels, ax=ax[0], legend_loc='lower left') #, bbox_to_anchor=(0.5, 1.3), legend_ncol=1)

plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Baryons vs no baryons fiducial results

# %%
# All surveys (lmax = 4500)
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG',
          'hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
labels = ['DESY3', 'KiDS-1000', 'HSCDR1', 'DESY3 + KiDS-1000 + HSCDR1']
colors = ['blue', 'orange', 'red', 'brown']

g = plots.get_single_plotter(width_inch=6)
g.settings.axes_fontsize = 15
g.settings.lab_fontsize = 20
g.settings.legend_fontsize = 12

g.plot_2d(MCSamples, 'Omega_m', 'S8', filled=[False, False, False, True],
                colors=colors,
                lws=[1, 1, 1, None],
                ls=['-', '-', '-', None]
                )
g.add_legend(labels, legend_ncol=2, legend_loc='lower center', frameon=False)
g.add_text(r"$\ell_{\rm max} = 4500$", fontsize=14, horizontalalignment='right', y=0.95, x=0.98)

plt.show()
plt.close()

# %%
# DES+KiDS+HSC with lmax=4500
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          # 'P18_official_mnu'
         ]

parameters = ['Omega_m', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_single_plotter(width_inch=6)
g.settings.axes_fontsize = 15
g.settings.lab_fontsize = 20
g.settings.legend_fontsize = 14

g.plot_2d(MCSamples, parameters, filled=[True, False],
                colors=['brown', 'blue'],
                lws=[1, 1, ])
g.add_legend(['Baryons', 'No-Baryons'], legend_loc='upper left', colored_text=True);
g.add_text(r"$\ell_{\rm max} = 4500$", fontsize=14, horizontalalignment='right')

plt.xlim([None, 0.37])
plt.ylim([None, 0.87])
plt.title("DES-Y3 + KiDS-1000 + HSC-DR1", fontsize=15)

plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Baryon boost fiducial

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']

colors = ['brown']
labels = ['DESY3 + KiDS-1000 + HSC-DR1']

f, ax = plt.subplots()

# Add chain constraints
for i, c in enumerate(chains):
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    label = dict(zip(chains, labels))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color, alpha=0.3 - 0.1*i)

# Finalize plot
ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$', fontsize=18)
ax.set_ylabel('$S_k(a=1)$', fontsize=18)
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=15)
ax.legend(loc='lower left', fontsize=13, frameon=False)
ax.set_xlim([0.1, 2.5])
ax.set_ylim([None, 1.08])

plt.show()
plt.close()

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']

colors = ['brown']
labels = ['DESY3 + KiDS-1000 + HSC-DR1']

f, ax = plt.subplots()

# Add EAGLE line
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_eagle_Planck13_GrO.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_eagle_Planck13_Hydro.dat", unpack=True, usecols=(0, 1, 2))
k, Sk = get_k_Sk_from_table(pktable_dmo, pktable_b)
ax.semilogx(k, Sk, color='purple', ls='--', label='EAGLE')

# Add Illustris TNG-300 line
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_tng100_Planck15_GrO.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_tng100_Planck15_Hydro.dat", unpack=True, usecols=(0, 1, 2))
k, Sk = get_k_Sk_from_table(pktable_dmo, pktable_b)
ax.semilogx(k, Sk, color='lightgreen', ls='--', label='Illustris TNG-300')

# Add BAHAMAS low-AGN line
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_DMONLY_2fluid_nu0_WMAP9_L400N1024.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_BAHAMAS_Theat7.6_nu0_WMAP9.dat", unpack=True, usecols=(0, 1, 2))
k, Sk = get_k_Sk_from_table(pktable_dmo, pktable_b)
ax.semilogx(k, Sk, color='orange', ls='--', label='BAHAMAS low-AGN')

# Add BAHAMAS line
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_DMONLY_2fluid_nu0.06_Planck2015_L400N1024.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_BAHAMAS_nu0.06_Planck2015.dat", unpack=True, usecols=(0, 1, 2))
k, Sk = get_k_Sk_from_table(pktable_dmo, pktable_b)
ax.semilogx(k, Sk, color='red', ls='--', label='BAHAMAS')    

# Add OWLS line
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_OWLS_REF.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_OWLS_AGN.dat", unpack=True, usecols=(0, 1, 2))
k, Sk = get_k_Sk_from_table(pktable_dmo, pktable_b)
ax.semilogx(k, Sk, color='cyan', ls='--', label='OWLS-AGN')  

# Add BAHAMAS high-AGN line
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_DMONLY_2fluid_nu0_WMAP9_L400N1024.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_BAHAMAS_Theat8.0_nu0_WMAP9.dat", unpack=True, usecols=(0, 1, 2))
k, Sk = get_k_Sk_from_table(pktable_dmo, pktable_b)
ax.semilogx(k, Sk, color='goldenrod', ls='--', label='BAHAMAS high-AGN')

# Add Illustris line
pktable_dmo = np.loadtxt("./baryonic_suppressions_pk/powtable_illustris_WMAP9_GrO.dat", unpack=True, usecols=(0, 1, 2))
pktable_b = np.loadtxt("./baryonic_suppressions_pk/powtable_illustris_WMAP9_Hydro.dat", unpack=True, usecols=(0, 1, 2))
k, Sk = get_k_Sk_from_table(pktable_dmo, pktable_b)
ax.semilogx(k, Sk, color='green', ls='--', label='Illustris')


# Add chain constraints
for i, c in enumerate(chains):
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    label = dict(zip(chains, labels))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color, alpha=0.3 - 0.1*i)

# Finalize plot
ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$', fontsize=18)
ax.set_ylabel('$S_k(a=1)$', fontsize=18)
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=15)
ax.legend(loc='lower left', fontsize=13, frameon=False)
ax.set_xlim([0.1, 2.5])
ax.set_ylim([None, 1.08])

plt.show()
plt.close()


# %%
def get_k_Sk_from_table(pktable_dmo, pktable_b):
    sel_dmo = pktable_dmo[0] == 0 
    sel_b = pktable_b[0] == 0

    assert np.all(pktable_dmo[0, sel_dmo] == pktable_b[0, sel_b])
    assert np.all(pktable_dmo[1, sel_dmo] == pktable_b[1, sel_b])

    h = 0.678  # Planck 2015
    k = pktable_dmo[1, sel_dmo] * h
    Sk = pktable_b[2, sel_b] / pktable_dmo[2, sel_dmo]

    sel = (k > 0.1) * (k < 2.5)
    k = k[sel]
    Sk = Sk[sel]
    return k, Sk

chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'Arico+23']

colors = ['brown', 'black']
labels = ['DESY3 + KiDS-1000 + HSC-DR1', 'Aricò et al. 2023 (DESY3 in real space)']

f, ax = plt.subplots()

# Add chain constraints
for i, c in enumerate(chains):
    print(f"Reading {c}")
    # label = results[c]['label']
    color = dict(zip(chains, colors))[c]
    label = dict(zip(chains, labels))[c]
    results[c]['boost'].plot_baryon_post(ax, label, color, alpha=0.3 - 0.1*i)

# Finalize plot
ax.axhline(1, ls='--', color='gray')
ax.set_xlabel(r'$k {\rm [1/Mpc]}$', fontsize=18)
ax.set_ylabel('$S_k(a=1)$', fontsize=18)
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=15)
ax.legend(loc='lower left', fontsize=13, frameon=False)
ax.set_xlim([0.1, 2.5])
ax.set_ylim([None, 1.08])

plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Comparison with Planck

# %%
# DES+KiDS+HSC with lmax=4500
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'P18_official_mnu'
         ]

parameters = ['Omega_m', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]

g = plots.get_single_plotter(width_inch=6)
g.settings.axes_fontsize = 15
g.settings.lab_fontsize = 20
g.settings.legend_fontsize = 14

g.plot_2d(MCSamples, parameters, filled=[True, False, True],
                colors=['brown', 'blue', 'black'],
                lws=[1, 1, ])
g.add_legend(['Baryons marginalized', 'Baryons ignored', 'Planck 2018'], legend_loc='upper left', colored_text=True);
g.add_text(r"$\ell_{\rm max} = 4500$", fontsize=14, horizontalalignment='right')

plt.xlim([None, 0.37])
plt.ylim([None, 0.87])
plt.title("DES-Y3 + KiDS-1000 + HSC-DR1", fontsize=15)

plt.show()
plt.close()

# %%
# DES+KiDS+HSC with lmax=4500
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'P18_official_mnu'
         ]

parameters = ['Omega_m', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]

g = plots.get_single_plotter(width_inch=6)
g.settings.axes_fontsize = 15
g.settings.lab_fontsize = 20
g.settings.legend_fontsize = 14

g.plot_2d(MCSamples, parameters, filled=[True, True],
                colors=['brown', 'black'],
                lws=[1, 1, ])
g.add_legend(['Baryons', 'Planck 2018'], legend_loc='upper left', colored_text=True);
g.add_text(r"$\ell_{\rm max} = 4500$", fontsize=14, horizontalalignment='right')

plt.xlim([None, 0.37])
plt.ylim([None, 0.87])
plt.title("DES-Y3 + KiDS-1000 + HSC-DR1", fontsize=15)

plt.show()
plt.close()

# %%
# DES+KiDS+HSC with lmax=4500
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl',
          'P18_official_mnu'
         ]

parameters = ['Omega_m', 'S8']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]

g = plots.get_single_plotter(width_inch=6)
g.settings.axes_fontsize = 15
g.settings.lab_fontsize = 20
g.settings.legend_fontsize = 14

g.plot_2d(MCSamples, parameters, filled=[True, False, True],
                colors=['brown', 'green', 'black'],
                lws=[1, 2, 1,],
                ls=['-', '--', '-'])
g.add_legend(['Baryons', 'BAO prior', 'Planck 2018'], legend_loc='upper left', colored_text=True);
g.add_text(r"$\ell_{\rm max} = 4500$", fontsize=14, horizontalalignment='right')

plt.xlim([None, 0.37])
plt.ylim([None, 0.87])
plt.title("DES-Y3 + KiDS-1000 + HSC-DR1", fontsize=15)

plt.show()
plt.close()

# %%
# DES+KiDS+HSC with lmax=4500
chains = ['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck',
          'P18_official_mnu'
         ]

parameters = ['omega_m', 'S12']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]

g = plots.get_single_plotter(width_inch=6)
g.settings.axes_fontsize = 15
g.settings.lab_fontsize = 20
g.settings.legend_fontsize = 14

g.plot_2d(MCSamples, parameters, filled=[True, False, False, True],
                colors=['brown', 'blue', 'gold', 'black'],
                lws=[1, 1.5, 1.5, 1],
                ls=['-', '--', '--', '-'])
g.add_legend(['Baryons', '$H_0$ prior (SNe)', '$H_0$ prior (Planck)',  'Planck 2018'], legend_loc='upper left', colored_text=True) #, legend_ncol=2, label_order=[0, 3, 1, 2]);
g.add_text(r"$\ell_{\rm max} = 4500$", fontsize=14, horizontalalignment='right')

plt.xlim([None, 0.21])
# plt.ylim([None, 0.87])
plt.title("DES-Y3 + KiDS-1000 + HSC-DR1", fontsize=15)

plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## $M_c$ constraints

# %%
# All surveys (lmax = 4500)
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG', 
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

# for c in chains:
#     print(c)
#     for p in parameters:
#         print(results[c]['MCSamples'].getInlineLatex(p))
#     print('####')
#     print()


MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'] for i in chains]
# labels = ['DESY3', 'KiDS-1000', 'HSCDR1', 'DESY3 + KiDS-1000', 'DESY3 + KiDS-1000 + HSCDR1', 'Aricò et al. 2023']
colors = ['brown']

g = plots.get_single_plotter(width_inch=6)
g.settings.axes_fontsize = 15
g.settings.lab_fontsize = 20
g.settings.legend_fontsize = 13

g.plot_1d(MCSamples, 'M_c',
                # lws=[1, 1, 1, 1, 4, 2],
                # ls=['-', '-', '-', '--', '-'],
                colors=colors
                )
# g.add_legend(labels, legend_ncol=2, legend_loc='lower center')
plt.xlabel("$\log_{10}(M_c)$")
plt.ylabel('$\mathcal{P}(\log_{10}(M_c))$', fontsize=20)
plt.title("DES-Y3 + KiDS-1000 + HSC-DR1", fontsize=15)
plt.xlim([12.5, 15])

plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Real vs Fourier space (only $\theta = 2.5'$)

# %%
# Load the sacc file only to get the tracer combinations
s = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG'].get_cl_data_sacc()

cosmo = ccl.CosmologyVanillaLCDM()
ccl_tracers = {}
for trn, tr in s.tracers.items():
    ccl_tracers[trn] = ccl.WeakLensingTracer(cosmo, dndz=(tr.z, tr.nz), ia_bias=None)

cls = {}
ell = np.concatenate([[0, 1], np.unique(np.logspace(np.log10(2), np.log10(1e5), 30, dtype=int))], dtype=int)
for trs in s.get_tracer_combinations():
    cls[trs] = (ell, ccl.angular_cl(cosmo, ccl_tracers[trs[0]], ccl_tracers[trs[1]], ell))

nbin = 1
f, ax = plt.subplots(nbin, nbin, figsize=(6, 4))

ell_all = np.arange(100000)
# J4 Bessel function of first kind
J4 = lambda x: scipy.special.jv(4, x)

color = {2.5: 'blue',
         4: 'orange'}
colorlight = {2.5: 'lightblue',
              4:  'lightcoral'}
for angle in [2.5]:
    kernel_factor = lambda ell: ell * J4(ell * angle/60/180*np.pi)
    
    trs = ('DESY3wl__3', 'DESY3wl__3')
    ell, clc = cls[trs]
    cl = interp1d(ell, clc, bounds_error=False, fill_value=0)
    kernel = lambda ell: kernel_factor(ell) * cl(ell)
    xi_int = scipy.integrate.cumulative_trapezoid(kernel(ell_all), x=ell_all, initial=0)
    # Kernel
    ax.semilogx(ell_all, xi_int/xi_int[-1], label=f"$\\theta = {angle}'$", color=color[angle])

    # Get ell at which the integral converges at 5%
    threshold = (xi_int/xi_int[-1] > 0.95) * (xi_int/xi_int[-1] < 1.05)
    lth = None
    for iell, ell in enumerate(ell_all):
        if np.all(threshold[iell:]):
            lth = ell
            break

    ax.axvline(lth, ls='--', color=colorlight[angle])

    # Get ell at which the integral converges at 1%
    threshold = (xi_int/xi_int[-1] > 0.99) * (xi_int/xi_int[-1] < 1.01)
    lth = None
    for iell, ell in enumerate(ell_all):
        if np.all(threshold[iell:]):
            lth = ell
            break

    ax.axvline(lth, ls=':', color=colorlight[angle]) #, label='$1\%$ error' if angle == 2.5 else None)

ax.axvline(4500, ls='--', color='gray', label=r'$\ell=4500$')
ax.axvline(8192, ls=':', color='gray', label=r'$\ell=8192$')
ax.axhline(1, ls='--', color='gray', alpha=0.5)

ax.fill_between(ell_all, 0.5, 1.5, alpha=0.3, color="gray")

for i in range(nbin):
    ax.set_ylabel(r"$\int_0^\ell d\ell \ell J_4(\ell \theta) C_\ell^{EE}$ / $\int_0^\infty d\ell \ell J_4(\ell \theta) C_\ell^{EE}$", fontsize=12)
    ax.set_xlabel("$\ell$", fontsize=12)
    ax.set_xlim([1e3, ell_all[-1]])
    ax.tick_params(axis='y', labelsize=11)
    ax.tick_params(axis='x', labelsize=11)
    ax.legend(loc='upper left', frameon=False)

ax.set_ylim([None, 1.22])

t = ax.text(0.55, 0.05, '5% error', horizontalalignment='left',
            verticalalignment='bottom', transform=ax.transAxes, fontsize=12)
t.set_bbox(dict(facecolor='coral', alpha=0.5, edgecolor='red'))

t = ax.text(0.77, 0.05, '1% error', horizontalalignment='left',
            verticalalignment='bottom', transform=ax.transAxes, fontsize=12)
t.set_bbox(dict(facecolor='coral', alpha=0.5, edgecolor='red'))


t = ax.text(0.03, 0.5, '$1\sigma$ region', horizontalalignment='left',
            verticalalignment='bottom', transform=ax.transAxes, fontsize=12)
t.set_bbox(dict(facecolor='lightgray', alpha=0.5, edgecolor='black'))

t = ax.text(0.99, 0.95, 'DESY3 (4th bin)', horizontalalignment='right',
            verticalalignment='top', transform=ax.transAxes, fontsize=12)
    
plt.tight_layout()
plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Fixing baryons: improved cosmological constraints

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedbaryonsBAHAMAS',
          # 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_clusters',
          # 'desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

parameters = ['Omega_m', 'S8',  'M_c']

MCSamples = [results[i]['MCSamples'] for i in chains]
# labels = [results[i]['label'].split(" with ")[1] for i in chains]
# colors = [results[i]['color'] for i in chains]
# labels = [r"$\ell < 1000$", r"$\ell < 2048$", r"$\ell < 4500$", r"$\ell < 8192$"]
colors= ['brown', 'green'] # , 'black', 'blue']
labels = ['Baryons', 'Baryons + BAHAMAS prior']

g = plots.get_single_plotter(width_inch=6)
g.settings.axes_fontsize = 14
g.settings.lab_fontsize = 16
g.settings.legend_fontsize = 14

g.plot_2d(MCSamples, 'Omega_m', 'S8', filled=[True, False, False, False],
          colors=colors, legend_labels=labels,
          lws=[None, 1.5, 1.25, 1.25],
          ls=['-', '--', '-.', '-'],
          alpha=[0.7, 1, 1] # [{'alpha': 0.7}, {'alpha': 1}, {'alpha': 1}, {'alpha': 1}]
)
g.add_legend(legend_labels=labels)
ax = g.get_axes()
# t = ax.text(0.05, 0.05, 'DESY3+KiDS-1000+HSCDR1', horizontalalignment='left',
#             verticalalignment='bottom', transform=ax.transAxes, fontsize=16)
# t.set_bbox(dict(facecolor='white', alpha=1, edgecolor='white'))
# ax.set_yticks([])
# ax.set_xticks([])
# ax.set_xlabel(r"Amount of matter ($\Omega_{\rm m}$)")
# ax.set_ylabel(r"Amount of structure ($S_8$)")

# ax.spines['top'].set_visible(False)
# ax.spines['right'].set_visible(False)
plt.title("DES-Y3 + KiDS-1000 + HSC-DR1", fontsize=15)


# %%

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# # Tests

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Extrapolation (CCL vs HALOFIT): No effect :)

# %%
# Baryons + HALOFIT extrap (4096)
key = 'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons HALOFIT extrap ($20 < \ell < 4500$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# Baryons (4096) lmax=4500 (EuclidEmulator2 Pkmm + BAHAMAS Sk from baccoemu)
key = 'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless'
results[key] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with Baryons HALOFIT extrap, Mock Data EE2+BAHAMAS Sk table ($20 < \ell < 4500$)', 'gray', 0.15)
print(key, f"R-1 = {results[key]['R-1']}")

# %% [markdown]
# ### Data

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# #### DES

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG',
         ]

bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

print('chi2(BF CCL extrap) = ', results[chains[0]]['chi2bf'])
print('chi2(BF HALOFIT extrap) = ', results[chains[1]]['chi2bf'])

print('chi2(HALOFIT extrap using BF pars from CCL extrap) = ', results[chains[0]].evaluate_model(bf2))
print('chi2(CCL extrap using BF pars from HALOFIT extrap) = ', results[chains[1]].evaluate_model(bf1))

# %%
k = np.logspace(-3, 2, 500)

print('chi2(CCL extrap using BF pars from CCL extrap) = ', results[chains[0]].evaluate_model(bf1))
print('chi2(HALOFIT extrap using BF pars from CCL extrap) = ', results[chains[1]].evaluate_model(bf1))

pk1 = results[chains[0]].get_lkl().provider.get_Pk()['pk_data']['pk_ww']
pk2 = results[chains[1]].get_lkl().provider.get_Pk()['pk_data']['pk_ww']
plt.loglog(k, pk1(k, 1))
plt.loglog(k, pk2(k, 1))
plt.show()

# %%
k = np.logspace(-3, 2, 500)

print('chi2(CCL extrap using BF pars from HALOFIT extrap) = ', results[chains[0]].evaluate_model(bf2))
print('chi2(HALOFIT extrap using BF pars from HALOFIT extrap) = ', results[chains[1]].evaluate_model(bf2))

pk1 = results[chains[0]].get_lkl().provider.get_Pk()['pk_data']['pk_ww']
pk2 = results[chains[1]].get_lkl().provider.get_Pk()['pk_data']['pk_ww']
plt.loglog(k, pk1(k, 1))
plt.loglog(k, pk2(k, 1))
plt.show()

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless'
         ]

print('Omega_m')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('Omega_m'))
print()
print('S8')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('S8'))

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG',
         ]

# print('p-value = ', results[chains[0]]['pvalue_bf'])
# print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['gray', 'pink']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='black')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='black', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 

# %% [markdown]
# #### DES + KiDS + HSC

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
         ]

bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

print('chi2(BF CCL extrap) = ', results[chains[0]]['chi2bf'])
print('chi2(BF HALOFIT extrap) = ', results[chains[1]]['chi2bf'])

print('chi2(HALOFIT extrap using BF pars from CCL extrap) = ', results[chains[0]].evaluate_model(bf2))
print('chi2(CCL extrap using BF pars from HALOFIT extrap) = ', results[chains[1]].evaluate_model(bf1))

# %%
k = np.logspace(-3, 2, 500)

print('chi2(CCL extrap using BF pars from CCL extrap) = ', results[chains[0]].evaluate_model(bf1))
print('chi2(HALOFIT extrap using BF pars from CCL extrap) = ', results[chains[1]].evaluate_model(bf1))

pk1 = results[chains[0]].get_lkl().provider.get_Pk()['pk_data']['pk_ww']
pk2 = results[chains[1]].get_lkl().provider.get_Pk()['pk_data']['pk_ww']
plt.loglog(k, pk1(k, 1))
plt.loglog(k, pk2(k, 1))
plt.show()

# %%
k = np.logspace(-3, 2, 500)

print('chi2(CCL extrap using BF pars from HALOFIT extrap) = ', results[chains[0]].evaluate_model(bf2))
print('chi2(HALOFIT extrap using BF pars from HALOFIT extrap) = ', results[chains[1]].evaluate_model(bf2))

pk1 = results[chains[0]].get_lkl().provider.get_Pk()['pk_data']['pk_ww']
pk2 = results[chains[1]].get_lkl().provider.get_Pk()['pk_data']['pk_ww']
plt.loglog(k, pk1(k, 1))
plt.loglog(k, pk2(k, 1))
plt.show()

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          # 'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          # 'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless'
         ]

print('Omega_m')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('Omega_m'))
print()
print('S8')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('S8'))

# %%
chains = [
          'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG',
          'desy3wl_k1000_hsc_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG'
         ]

# print('p-value = ', results[chains[0]]['pvalue_bf'])
# print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['gray', 'pink']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='black')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='black', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### Mock

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless',
          'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless',
         ]


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

print('chi2(BF CCL extrap) = ', results[chains[0]]['chi2bf'])
print('chi2(BF HALOFIT extrap) = ', results[chains[1]]['chi2bf'])

print('chi2(HALOFIT extrap using BF pars from CCL extrap) = ', results[chains[0]].evaluate_model(bf2))
print('chi2(CCL extrap using BF pars from HALOFIT extrap) = ', results[chains[1]].evaluate_model(bf1))

# %%
k = np.logspace(-3, 2, 500)

print('chi2(CCL extrap using BF pars from CCL extrap) = ', results[chains[0]].evaluate_model(bf1))
print('chi2(HALOFIT extrap using BF pars from CCL extrap) = ', results[chains[1]].evaluate_model(bf1))

pk1 = results[chains[0]].get_lkl().provider.get_Pk()['pk_data']['pk_ww']
pk2 = results[chains[1]].get_lkl().provider.get_Pk()['pk_data']['pk_ww']
plt.loglog(k, pk1(k, 1))
plt.loglog(k, pk2(k, 1))
plt.show()

# %%
k = np.logspace(-3, 2, 500)

print('chi2(CCL extrap using BF pars from HALOFIT extrap) = ', results[chains[0]].evaluate_model(bf2))
print('chi2(HALOFIT extrap using BF pars from HALOFIT extrap) = ', results[chains[1]].evaluate_model(bf2))

pk1 = results[chains[0]].get_lkl().provider.get_Pk()['pk_data']['pk_ww']
pk2 = results[chains[1]].get_lkl().provider.get_Pk()['pk_data']['pk_ww']
plt.loglog(k, pk1(k, 1))
plt.loglog(k, pk2(k, 1))
plt.show()

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless',
          'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless'
         ]

print('Omega_m')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('Omega_m'))
print()
print('S8')
for c in chains:
    print(c, results[c]['MCSamples'].getInlineLatex('S8'))

# %%
chains = [
          'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless',
          'desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless',
         ]

# print('p-value = ', results[chains[0]]['pvalue_bf'])
# print('p-value = ', results[chains[1]]['pvalue_bf'], 'chi2 = ', results[chains[1]]['chi2bf'])


parameters = ['Omega_m', 'S8', 'sigma8',  'M_c', 'h', 'omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
# colors = [results[i]['color'] for i in chains]
colors = ['gray', 'pink']

g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


bf1 = results[chains[0]]['pars_bf']
bf2 = results[chains[1]]['pars_bf']

for i, p1 in enumerate(parameters):
    ax = g.get_axes((i, i))
    if p1 in bf1:
        ax.axvline(bf1[p1], ls='--', color='black')
    if p1 in bf2:
        ax.axvline(bf2[p1], ls='--', color=colors[1])
    for p2 in parameters[i+1:]:
        ax = g.get_axes_for_params(p1, p2)
        if (p1 in bf1) and (p2 in bf1):
            ax.plot(bf1[p1], bf1[p2], color='black', marker='o')
        if (p1 in bf2) and (p2 in bf2):
            ax.plot(bf2[p1], bf2[p2], color=colors[1], marker='o') 

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Hitting boundaries?

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### Omega_m

# %%
Omega_m = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'].getParams().Omega_m

# %%
Omega_m.min()

# %%
for i, parname in enumerate(mpk.emulator['nonlinear']['keys']):
    if parname in ['expfactor', 'w0', 'wa']:
        continue

    for k in ['nonlinear']:
        lower_bound = mpk.emulator[k]['bounds'][i][0]
        print(k, parname, lower_bound)

# %%
_ = plt.hist(Omega_m, bins=60)
plt.axvline(0.15, ls='--', color='black', label='Omega_cold boundary baccoemu')
plt.axvline(0.1, ls=':', color='gray', label='Prior MCMC')
plt.legend()
plt.xlabel('$\Omega_m$')

# %%
Omega_m = []
for lmax in [1000, 2048, 4500]:
    Omega_m.append(results[f'desy3wl_baryons_nocuts_nla_nside4096_lmax{lmax}_lmin20_GNG']['MCSamples'].getParams().Omega_m)
    print(Omega_m[-1].min())
Omega_m.append(results['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_halofit_GNG']['MCSamples'].getParams().Omega_m)
_ = plt.hist(Omega_m, bins=60, label=[fr'$\ell_{{\rm max}} = {lmax}$' for lmax in [1000, 2048, 4500]] + [r'$\ell_{\rm max} = 2048$ (HALOFIT)'],
             histtype='step', density=True)
print(Omega_m[-1].min())
plt.axvline(0.15, ls='--', color='black', label='Omega_cold boundary baccoemu')
plt.axvline(0.1, ls=':', color='gray', label='Prior MCMC')
plt.legend()
plt.xlabel('$\Omega_m$')
plt.title('DESY3 only')

# %%
Omega_m = []
for lmax in [1000, 2048, 4500]:
    Omega_m.append(results[f'k1000_baryons_nocuts_nla_nside4096_lmax{lmax}_lmin100_GNG']['MCSamples'].getParams().Omega_m)
    print(Omega_m[-1].min())
Omega_m.append(results['k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_halofit_GNG']['MCSamples'].getParams().Omega_m)
_ = plt.hist(Omega_m, bins=60, label=[fr'$\ell_{{\rm max}} = {lmax}$' for lmax in [1000, 2048, 4500]] + [r'$\ell_{\rm max} = 2048$ (HALOFIT)'],
             histtype='step', density=True)
print(Omega_m[-1].min())
plt.axvline(0.15, ls='--', color='black', label='Omega_cold boundary baccoemu')
plt.axvline(0.1, ls=':', color='gray', label='Prior MCMC')
plt.legend()
plt.xlabel('$\Omega_m$')
plt.title('KiDS-1000 only')

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ### sigma8_cold

# %%
sigma8_cold = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'].getParams().sigma8_cold

# %%
sigma8_cold.min(), sigma8_cold.max()

# %%
for i, parname in enumerate(mpk.emulator['nonlinear']['keys']):
    if parname in ['expfactor', 'w0', 'wa']:
        continue

    for k in ['nonlinear']:
        lower_bound = mpk.emulator[k]['bounds'][i][0]
        upper_bound = mpk.emulator[k]['bounds'][i][1]
        print(k, parname, lower_bound, upper_bound)

# %%
_ = plt.hist(sigma8_cold, bins=60)
plt.axvline(0.4, ls='--', color='black', label='Omega_cold boundary baccoemu')
plt.axvline(1.15, ls='--', color='black', label='Omega_cold boundary baccoemu')
# plt.axvline(0.1, ls=':', color='gray', label='Prior MCMC')
plt.legend()
plt.xlabel('$\sigma_{8, cold}$')

# %%
sigma8_cold = []
for lmax in [1000, 2048, 4500]:
    sigma8_cold.append(results[f'desy3wl_baryons_nocuts_nla_nside4096_lmax{lmax}_lmin20_GNG']['MCSamples'].getParams().sigma8_cold)
    print(sigma8_cold[-1].max())
_ = plt.hist(sigma8_cold, bins=60, label=[fr'$\ell_{{\rm max}} = {lmax}$' for lmax in [1000, 2048, 4500]] + [r'$\ell_{\rm max} = 2048$ (HALOFIT)'],
             histtype='step', density=True)
plt.axvline(0.4, ls='--', color='black', label='Omega_cold boundary baccoemu')
plt.axvline(1.15, ls='--', color='black')
plt.legend()
plt.xlabel('$\sigma_{8, cold}$')
plt.title('DESY3 only')

# %%
sigma8_cold = []
for lmax in [1000, 2048, 4500]:
    sigma8_cold.append(results[f'k1000_baryons_nocuts_nla_nside4096_lmax{lmax}_lmin100_GNG']['MCSamples'].getParams().sigma8_cold)
    print(sigma8_cold[-1].max())

# HALOFIT
p = results['k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_halofit_GNG']['MCSamples'].getParams()
sel = (0.5 < p.h) * (p.h < 0.9)
pars = {
'omega_cold':p.Omega_c[sel] + p.Omega_b[sel],
'omega_baryon': p.Omega_b[sel],
'A_s' : p.A_sE9[sel]*1e-9,
'ns': p.n_s[sel],
'hubble': p.h[sel],
'neutrino_mass': p.m_nu[sel],
'w0': np.full(p.m_nu[sel].size, -1),
'wa': np.full(p.m_nu[sel].size, 0),
'expfactor': np.full(p.m_nu[sel].size, 1)
}
sigma8_cold.append(mpk.get_sigma8(cold=True, **pars))
print(sigma8_cold[-1].max())

# Plot
_ = plt.hist(sigma8_cold, bins=60, label=[fr'$\ell_{{\rm max}} = {lmax}$' for lmax in [1000, 2048, 4500]] + [r'$\ell_{\rm max} = 2048$ (HALOFIT)'],
             histtype='step', density=True)
plt.axvline(0.4, ls='--', color='black', label='Omega_cold boundary baccoemu')
plt.axvline(1.15, ls='--', color='black')
plt.legend()
plt.xlabel('$\sigma_{8, cold}$')
plt.title('KiDS-1000 only')

# %%
s = results[f'desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG']['MCSamples']
ix = s.getParams().sigma8_cold.argmin()
s.getParamSampleDict(ix)

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## Pk with low Omega_m

# %%
chain = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless'
pars = results[chain]['pars_bf']
pkdata = results[chain].get_pk_data(pars)
pk = pkdata['pk_ww']
Sk = pkdata['Sk']

# %%
k = np.logspace(-2, 1, 100)
f, ax = plt.subplots(2, 1, sharex=True, gridspec_kw={'hspace': 0, 'wspace':0})

for Om in [0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4]:
    newpars = pars.copy()

    # Keep S8 constant
    sigma8_new = newpars['S8'] * np.sqrt(0.3 / Om)
    A_s_fid = 2.1e-9
    sigma8_fid = mpk.get_sigma8(cold=False,
                                    A_s=A_s_fid,
                                    omega_cold=newpars['Omega_c'] + newpars['Omega_b'],
                                    omega_matter=newpars['Omega_m'],
                                    omega_baryon=newpars['Omega_b'],
                                    hubble=newpars['h'],
                                    ns=newpars['n_s'],
                                    neutrino_mass=newpars['m_nu'],
                                    w0=-1,
                                    wa=0,
                                    expfactor=1)
    A_s = (sigma8_new / sigma8_fid)**2 * A_s_fid
    # Update dictionary
    newpars['A_sE9'] = A_s * 1e9
    newpars['Omega_m'] = Om

    # Compute Pk
    pkdata = results[chain].get_pk_data(newpars)
    pk = pkdata['pk_ww']
    Sk = pkdata['Sk']
    
    ax[0].loglog(k, pk(k, 1), label=fr'$\Omega_m = {Om}$')
    ax[1].semilogx(k, Sk(k, 1))

ax[1].axhline(1, ls='--', color='gray')
ax[1].set_xlabel('k [1/Mpc]')
ax[0].set_ylabel('Pk')
ax[1].set_ylabel('Sk')
ax[0].legend(ncols=2)
ax[0].set_title(fr"Keeping $S_8 = {newpars['S8']:.3f}$ constant (BF)")
plt.show()
plt.close()

# %%
sigma8_list = []
Omega_m_list = [0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4]
for Om in Omega_m_list:
    newpars = pars.copy()

    # Keep S8 constant
    sigma8_new = newpars['S8'] * np.sqrt(0.3 / Om)
    
    sigma8_list.append(sigma8_new)
    
g = plots.get_single_plotter(width_inch=6)
g.plot_2d(results[chain]['MCSamples'], 'Omega_m', 'sigma8')
# g.add_legend(chain)

ax = g.get_axes((0, 0))

ax.plot(Omega_m_list, sigma8_list, label=fr"$S_8 = {newpars['S8']:.3f}$")
ax.set_ylabel(r'$\sigma_8$')
ax.set_xlabel(r'$\Omega_m$')
ax.legend()
plt.show()
plt.close()

# %%
pars = results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['pars_bf'].copy()
pars['bias_A_IA'] = pars.pop('bias_DESY3wl_A_IA')
pars['limber_eta_IA'] = pars.pop('limber_DESY3wl_eta_IA')

chain = 'desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG'
pkdata = results[chain].get_pk_data(pars)
pk = pkdata['pk_ww']
Sk = pkdata['Sk']

# %%
k = np.logspace(-2, 1, 100)
f, ax = plt.subplots(2, 1, sharex=True, gridspec_kw={'hspace': 0, 'wspace':0})

for Om in [0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4]:
    newpars = pars.copy()

    # Keep S8 constant
    sigma8_new = newpars['S8'] * np.sqrt(0.3 / Om)
    A_s_fid = 2.1e-9
    sigma8_fid = mpk.get_sigma8(cold=False,
                                    A_s=A_s_fid,
                                    omega_cold=newpars['Omega_c'] + newpars['Omega_b'],
                                    omega_matter=newpars['Omega_m'],
                                    omega_baryon=newpars['Omega_b'],
                                    hubble=newpars['h'],
                                    ns=newpars['n_s'],
                                    neutrino_mass=newpars['m_nu'],
                                    w0=-1,
                                    wa=0,
                                    expfactor=1)
    A_s = (sigma8_new / sigma8_fid)**2 * A_s_fid
    # Update dictionary
    newpars['A_sE9'] = A_s * 1e9
    newpars['Omega_m'] = Om

    # Compute Pk
    pkdata = results[chain].get_pk_data(newpars)
    pk = pkdata['pk_ww']
    Sk = pkdata['Sk']
    
    ax[0].loglog(k, pk(k, 1), label=fr'$\Omega_m = {Om}$')
    ax[1].semilogx(k, Sk(k, 1))

ax[1].axhline(1, ls='--', color='gray')
ax[1].set_xlabel('k [1/Mpc]')
ax[0].set_ylabel('Pk')
ax[1].set_ylabel('Sk')
ax[0].legend(ncols=2)
ax[0].set_title(fr"Keeping $S_8 = {newpars['S8']:.3f}$ constant (BF)")
plt.show()
plt.close()

# %%
sigma8_list = []
Omega_m_list = [0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4]
for Om in Omega_m_list:
    newpars = pars.copy()

    # Keep S8 constant
    sigma8_new = newpars['S8'] * np.sqrt(0.3 / Om)
    
    sigma8_list.append(sigma8_new)
    
g = plots.get_single_plotter(width_inch=6)
g.plot_2d(results['desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG']['MCSamples'], 'Omega_m', 'sigma8')
# g.add_legend(chain)

ax = g.get_axes((0, 0))

ax.plot(Omega_m_list, sigma8_list, label=fr"$S_8 = {newpars['S8']:.3f}$")
ax.set_ylabel(r'$\sigma_8$')
ax.set_xlabel(r'$\Omega_m$')
ax.legend()
plt.show()
plt.close()

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## Null tests

# %% hidden=true
# saccs = {}
# saccs['DESY3'] = sacc.Sacc.load_fits("/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov_GNG.fits")
# saccs['KiDS-1000'] = sacc.Sacc.load_fits("/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov_GNG.fits")
# saccs['HSCDR1'] = sacc.Sacc.load_fits("/mnt/extraspace/gravityls_3/sacc_files/cls_signal_covGNG_HSC_Andrina_paper_newNz.fits")

lmins = {'DESY3': 20, 'KiDS-1000': 100, 'HSCDR1': 300}

for sn, s in saccs.items():
    print(sn)
    s.remove_selection(data_type='cl_ee')
    cov = s.covariance.covmat
    cl = s.mean
    pvalues = []
    for lmax in [1000, 2048, 4500, 8192]:
        sel = s.indices(ell__gt=lmins[sn], ell__lt=lmax)
        chi2 = cl[sel].dot(np.linalg.inv(cov[sel][:, sel])).dot(cl[sel])
        pvalue = 1 - scipy.stats.chi2.cdf(sel.size, chi2)
        pvalues.append(pvalue)
        print(f"lmax = {lmax}", f"p-value = {pvalue:.2f}")

    plt.plot([1000, 2048, 4500, 8192], pvalues, marker='.', label=f'{sn}')
    print()
plt.axhline(0.95, ls='--', color='black')
plt.axhline(0.05, ls='--', color='black')
plt.axhline(0.50, ls='--', color='gray')
plt.ylim([0, 1])
plt.xlabel('lmax')
plt.ylabel('p-value')
plt.title('Null test (B-modes)')
plt.legend()
plt.show()
plt.close()

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### DESY3 null tests

# %% hidden=true
# E-B modes
s = saccs['DESY3']

lmin = lmins['DESY3']
lmax = 4500

f, ax = plt.subplots(4, 4, figsize=(16, 16), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})

for i in range(4):
    for j in range(4):
        tr1 = f'DESY3__{i}'
        tr2 = f'DESY3__{j}'
        if i <=j:
            ell, cl, cov = s.get_ell_cl('cl_eb', tr1, tr2, return_cov=True)
        else:
            ell, cl, cov = s.get_ell_cl('cl_be', tr2, tr1, return_cov=True)
        err = np.sqrt(np.diag(cov))
        ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.')
        ax[i, j].text(0.99, 0.99, f"{i}-{j}", horizontalalignment='right',
         verticalalignment='top', transform=ax[i,j].transAxes, fontsize=14)
        ax[i, j].axhline(0, ls='--', color='gray')
        ax[i, j].axvline(lmax, ls='--', color='brown')
        ax[i, j].axvline(lmin, ls='--', color='brown')
        
        # Signal-to-Noise
        sel = (ell > lmin) * (ell < lmax)
        chi2 = cl[sel].dot(np.linalg.inv(cov[sel][:, sel])).dot(cl[sel])
        pvalue = 1 - scipy.stats.chi2.cdf(np.sum(sel), chi2)
        if (pvalue < 0.05) or (pvalue > 0.95):
            color = 'red'
        else:
            color = 'black'
        ax[i, j].text(0.01, 0.01, f"$\chi^2 = {chi2:.1f}$ ($p = {pvalue:.2f}$)", horizontalalignment='left',
         verticalalignment='bottom', transform=ax[i,j].transAxes, fontsize=14, color=color)
    
for i in range(4):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")

f.suptitle('EB', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% hidden=true
# B modes
s = saccs['DESY3']

lmin = lmins['DESY3']
lmax = 4500

f, ax = plt.subplots(4, 4, figsize=(14, 14), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in s.get_tracer_combinations():
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])
    ell, cl, cov = s.get_ell_cl('cl_bb', trs[0], trs[1], return_cov=True)
    err = np.sqrt(np.diag(cov))
    
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.')
    ax[i, j].text(0.99, 0.99, f"{i}-{j}", horizontalalignment='right',
     verticalalignment='top', transform=ax[i,j].transAxes, fontsize=14)
    ax[i, j].axhline(0, ls='--', color='gray')
    ax[i, j].axvline(lmax, ls='--', color='brown')
    ax[i, j].axvline(lmin, ls='--', color='brown')

    # Signal-to-Noise
    sel = (ell > lmin) * (ell < lmax)
    chi2 = cl[sel].dot(np.linalg.inv(cov[sel][:, sel])).dot(cl[sel])
    pvalue = 1 - scipy.stats.chi2.cdf(np.sum(sel), chi2)
    if (pvalue < 0.05) or (pvalue > 0.95):
        color = 'red'
    else:
        color = 'black'
    ax[i, j].text(0.01, 0.01, f"$\chi^2 = {chi2:.1f}$ ($p = {pvalue:.2f}$)", horizontalalignment='left',
     verticalalignment='bottom', transform=ax[i,j].transAxes, fontsize=14, color=color)
    

for i in range(4):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")

f.suptitle('BB', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### KiDS null tests

# %% hidden=true
# E-B modes
s = saccs['KiDS-1000']

lmin = lmins['KiDS-1000']
lmax = 4500


f, ax = plt.subplots(5, 5, figsize=(16, 16), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})

for i in range(5):
    for j in range(5):
        tr1 = f'KiDS1000__{i}'
        tr2 = f'KiDS1000__{j}'
        if i <=j:
            ell, cl, cov = s.get_ell_cl('cl_eb', tr1, tr2, return_cov=True)
        else:
            ell, cl, cov = s.get_ell_cl('cl_be', tr2, tr1, return_cov=True)
        err = np.sqrt(np.diag(cov))
        ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.')
        ax[i, j].text(0.99, 0.99, f"{i}-{j}", horizontalalignment='right',
         verticalalignment='top', transform=ax[i,j].transAxes, fontsize=14)
        ax[i, j].axhline(0, ls='--', color='gray')
        ax[i, j].axvline(lmax, ls='--', color='brown')
        ax[i, j].axvline(lmin, ls='--', color='brown')
        
        # Signal-to-Noise
        sel = (ell > lmin) * (ell < lmax)
        chi2 = cl[sel].dot(np.linalg.inv(cov[sel][:, sel])).dot(cl[sel])
        pvalue = 1 - scipy.stats.chi2.cdf(np.sum(sel), chi2)
        if (pvalue < 0.05) or (pvalue > 0.95):
            color = 'red'
        else:
            color = 'black'
        ax[i, j].text(0.01, 0.01, f"$\chi^2 = {chi2:.1f}$ ($p = {pvalue:.2f}$)", horizontalalignment='left',
         verticalalignment='bottom', transform=ax[i,j].transAxes, fontsize=14, color=color)
    
for i in range(5):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")

f.suptitle('EB', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% hidden=true
# E-B modes
s = saccs['KiDS-1000']

lmin = lmins['KiDS-1000']
lmax = 4500

f, ax = plt.subplots(5, 5, figsize=(14, 14), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in s.get_tracer_combinations():
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])
    ell, cl, cov = s.get_ell_cl('cl_bb', trs[0], trs[1], return_cov=True)
    err = np.sqrt(np.diag(cov))
    
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.')
    ax[i, j].text(0.99, 0.99, f"{i}-{j}", horizontalalignment='right',
     verticalalignment='top', transform=ax[i,j].transAxes, fontsize=14)
    ax[i, j].axhline(0, ls='--', color='gray')
    ax[i, j].axvline(lmax, ls='--', color='brown')
    ax[i, j].axvline(lmin, ls='--', color='brown')

    # Signal-to-Noise
    sel = (ell > lmin) * (ell < lmax)
    chi2 = cl[sel].dot(np.linalg.inv(cov[sel][:, sel])).dot(cl[sel])
    pvalue = 1 - scipy.stats.chi2.cdf(np.sum(sel), chi2)
    if (pvalue < 0.05) or (pvalue > 0.95):
        color = 'red'
    else:
        color = 'black'
    ax[i, j].text(0.01, 0.01, f"$\chi^2 = {chi2:.1f}$ ($p = {pvalue:.2f}$)", horizontalalignment='left',
     verticalalignment='bottom', transform=ax[i,j].transAxes, fontsize=14, color=color)
    

for i in range(5):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")

f.suptitle('BB', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### HSC null tests

# %% hidden=true
# E-B modes
s = saccs['HSCDR1']

lmin = lmins['HSCDR1']
lmax = 4500


f, ax = plt.subplots(4, 4, figsize=(16, 16), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})

for i in range(4):
    for j in range(4):
        tr1 = f'wl_{i}'
        tr2 = f'wl_{j}'
        if i <=j:
            ell, cl, cov = s.get_ell_cl('cl_eb', tr1, tr2, return_cov=True)
        else:
            ell, cl, cov = s.get_ell_cl('cl_be', tr2, tr1, return_cov=True)
        err = np.sqrt(np.diag(cov))
        ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.')
        ax[i, j].text(0.99, 0.99, f"{i}-{j}", horizontalalignment='right',
         verticalalignment='top', transform=ax[i,j].transAxes, fontsize=14)
        ax[i, j].axhline(0, ls='--', color='gray')
        ax[i, j].axvline(lmax, ls='--', color='brown')
        ax[i, j].axvline(lmin, ls='--', color='brown')

        # Signal-to-Noise
        sel = (ell > lmin) * (ell < lmax)
        chi2 = cl[sel].dot(np.linalg.inv(cov[sel][:, sel])).dot(cl[sel])
        pvalue = 1 - scipy.stats.chi2.cdf(np.sum(sel), chi2)
        if (pvalue < 0.05) or (pvalue > 0.95):
            color = 'red'
        else:
            color = 'black'
        ax[i, j].text(0.01, 0.01, f"$\chi^2 = {chi2:.1f}$ ($p = {pvalue:.2f}$)", horizontalalignment='left',
         verticalalignment='bottom', transform=ax[i,j].transAxes, fontsize=14, color=color)
    
for i in range(4):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")

f.suptitle('EB', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% hidden=true
# B modes
s = saccs['HSCDR1']

lmin = lmins['HSCDR1']
lmax = 4500

f, ax = plt.subplots(4, 4, figsize=(14, 14), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in s.get_tracer_combinations():
    i = int(trs[1].split('_')[1])
    j = int(trs[0].split('_')[1])
    ell, cl, cov = s.get_ell_cl('cl_bb', trs[0], trs[1], return_cov=True)
    err = np.sqrt(np.diag(cov))
    
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.')
    ax[i, j].text(0.99, 0.99, f"{i}-{j}", horizontalalignment='right',
     verticalalignment='top', transform=ax[i,j].transAxes, fontsize=14)
    ax[i, j].axhline(0, ls='--', color='gray')
    ax[i, j].axvline(lmax, ls='--', color='brown')
    ax[i, j].axvline(lmin, ls='--', color='brown')

    # Signal-to-Noise
    sel = (ell > lmin) * (ell < lmax)
    chi2 = cl[sel].dot(np.linalg.inv(cov[sel][:, sel])).dot(cl[sel])
    pvalue = 1 - scipy.stats.chi2.cdf(np.sum(sel), chi2)
    if (pvalue < 0.05) or (pvalue > 0.95):
        color = 'red'
    else:
        color = 'black'
    ax[i, j].text(0.01, 0.01, f"$\chi^2 = {chi2:.1f}$ ($p = {pvalue:.2f}$)", horizontalalignment='left',
     verticalalignment='bottom', transform=ax[i,j].transAxes, fontsize=14, color=color)
    

for i in range(4):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")

f.suptitle('BB', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% [markdown] jp-MarkdownHeadingCollapsed=true
# ## $\ell J_4(\ell \theta) C_\ell$ (Fig.1 in 2007.15633)

# %%
model = get_model('test/desy3wl_baryons_nocuts_nla_lmax50000_testunbinned.yml')
s = get_cl_theory_sacc(model, results['desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG']['pars_bf'])

# %%
cosmo = ccl.CosmologyVanillaLCDM()
ccl_tracers = {}
for trn, tr in s.tracers.items():
    ccl_tracers[trn] = ccl.WeakLensingTracer(cosmo, dndz=(tr.z, tr.nz), ia_bias=None)

cls = {}
ell = np.concatenate([[0, 1], np.unique(np.logspace(np.log10(2), np.log10(1e5), 30, dtype=int))], dtype=int)
for trs in s.get_tracer_combinations():
    cls[trs] = (ell, ccl.angular_cl(cosmo, ccl_tracers[trs[0]], ccl_tracers[trs[1]], ell))

nbin = 1
f, ax = plt.subplots(nbin, nbin, figsize=(10, 7))
ell_all = np.arange(100000)
# J4 Bessel function of first kind
J0 = lambda x: scipy.special.jv(0, x)

color = {2.5: 'blue',
         4: 'orange'}
colorlight = {2.5: 'lightblue',
              4:  'lightcoral'}
for angle in [2.5, 4]:
    kernel_factor = lambda ell: ell * J0(ell * angle/60/180*np.pi)
    
    trs = ('DESY3wl__3', 'DESY3wl__3')
    ell, clc = cls[trs]
    cl = interp1d(ell, clc, bounds_error=False, fill_value=0)
    kernel = lambda ell: kernel_factor(ell) * cl(ell)
    xi_int = scipy.integrate.cumulative_trapezoid(kernel(ell_all), x=ell_all, initial=0)
    # Kernel
    ax.semilogx(ell_all, xi_int/xi_int[-1], label=f"{angle}'", color=color[angle])

    if angle == 4:
        ax.axvline(4500, ls='--', color='gray', label=r'$\ell=4500$')
        ax.axvline(8192, ls=':', color='gray', label=r'$\ell=8192$')

    # Get ell at which the integral converges at 5%
    threshold = (xi_int/xi_int[-1] > 0.95) * (xi_int/xi_int[-1] < 1.05)
    lth = None
    for iell, ell in enumerate(ell_all):
        if np.all(threshold[iell:]):
            lth = ell
            break

    ax.axvline(lth, ls='--', color=colorlight[angle])#, label='$5\%$ error' if angle == 2.5 else None)

    # Get ell at which the integral converges at 1%
    threshold = (xi_int/xi_int[-1] > 0.99) * (xi_int/xi_int[-1] < 1.01)
    lth = None
    for iell, ell in enumerate(ell_all):
        if np.all(threshold[iell:]):
            lth = ell
            break

    ax.axvline(lth, ls=':', color=colorlight[angle]) #, label='$1\%$ error' if angle == 2.5 else None)
    
for i in range(nbin):
    ax.set_ylabel(r"$\int_0^\ell d\ell \ell J_0(\ell \theta) C_\ell^{EE}$ / int ($\ell=1e5$)")
    ax.set_xlabel("$\ell$")
    ax.set_xlim([1e3, None])
    ax.legend()

ax.fill_between(ell_all, 0.87, 1.13)

f.suptitle(f'DESY3 (3, 3)', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

## Check with pyccl -> It seems to work
# ell, clc = cls[trs]
# cl = interp1d(ell, clc, bounds_error=False, fill_value=0)(ell_all)
# xi = ccl.correlations.correlation(cosmo, ell=ell_all, C_ell=cl, theta=4/60, type='GG-', method='fftlog')
# print(xi, xi_int[-1]/(2*np.pi))

# %%
cosmo = ccl.CosmologyVanillaLCDM()
ccl_tracers = {}
for trn, tr in s.tracers.items():
    ccl_tracers[trn] = ccl.WeakLensingTracer(cosmo, dndz=(tr.z, tr.nz), ia_bias=None)

cls = {}
ell = np.concatenate([[0, 1], np.unique(np.logspace(np.log10(2), np.log10(1e5), 30, dtype=int))], dtype=int)
for trs in s.get_tracer_combinations():
    cls[trs] = (ell, ccl.angular_cl(cosmo, ccl_tracers[trs[0]], ccl_tracers[trs[1]], ell))

nbin = 1
f, ax = plt.subplots(nbin, nbin)
ell_all = np.arange(100000)
# J4 Bessel function of first kind
J4 = lambda x: scipy.special.jv(4, x)

color = {2.5: 'blue',
         4: 'orange'}
colorlight = {2.5: 'lightblue',
              4:  'lightcoral'}
for angle in [2.5, 4]:
    kernel_factor = lambda ell: ell * J4(ell * angle/60/180*np.pi)
    
    trs = ('DESY3__3', 'DESY3__3')
    ell, clc = cls[trs]
    cl = interp1d(ell, clc, bounds_error=False, fill_value=0)
    kernel = lambda ell: kernel_factor(ell) * cl(ell)
    xi_int = scipy.integrate.cumulative_trapezoid(kernel(ell_all), x=ell_all, initial=0)
    # Kernel
    ax.semilogx(ell_all, xi_int/xi_int[-1], label=f"{angle}'", color=color[angle])

    if angle == 4:
        ax.axvline(4500, ls='--', color='gray', label=r'$\ell=4500$')
        ax.axvline(8192, ls=':', color='gray', label=r'$\ell=8192$')

    # Get ell at which the integral converges at 5%
    threshold = (xi_int/xi_int[-1] > 0.95) * (xi_int/xi_int[-1] < 1.05)
    lth = None
    for iell, ell in enumerate(ell_all):
        if np.all(threshold[iell:]):
            lth = ell
            break

    ax.axvline(lth, ls='--', color=colorlight[angle])#, label='$5\%$ error' if angle == 2.5 else None)

    # Get ell at which the integral converges at 1%
    threshold = (xi_int/xi_int[-1] > 0.99) * (xi_int/xi_int[-1] < 1.01)
    lth = None
    for iell, ell in enumerate(ell_all):
        if np.all(threshold[iell:]):
            lth = ell
            break

    ax.axvline(lth, ls=':', color=colorlight[angle]) #, label='$1\%$ error' if angle == 2.5 else None)
    
for i in range(nbin):
    ax.set_ylabel(r"$\int_0^\ell d\ell \ell J_4(\ell \theta) C_\ell^{EE}$ / int ($\ell=1e5$)")
    ax.set_xlabel("$\ell$")
    ax.set_xlim([1e3, None])
    ax.legend()

f.suptitle(f'DESY3 (3, 3)', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

## Check with pyccl -> It seems to work
# ell, clc = cls[trs]
# cl = interp1d(ell, clc, bounds_error=False, fill_value=0)(ell_all)
# xi = ccl.correlations.correlation(cosmo, ell=ell_all, C_ell=cl, theta=4/60, type='GG-', method='fftlog')
# print(xi, xi_int[-1]/(2*np.pi))

# %%
cosmo = ccl.CosmologyVanillaLCDM()
ccl_tracers = {}
for trn, tr in s.tracers.items():
    ccl_tracers[trn] = ccl.WeakLensingTracer(cosmo, dndz=(tr.z, tr.nz), ia_bias=None)

cls = {}
ell = np.concatenate([[0, 1], np.unique(np.logspace(np.log10(2), np.log10(1e5), 30, dtype=int))], dtype=int)
for trs in s.get_tracer_combinations():
    cls[trs] = (ell, ccl.angular_cl(cosmo, ccl_tracers[trs[0]], ccl_tracers[trs[1]], ell))

nbin = 4
f, ax = plt.subplots(nbin, nbin, figsize=(12, 10), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
ell_all = np.arange(100000)
# J4 Bessel function of first kind
J4 = lambda x: scipy.special.jv(4, x)

color = {2.5: 'blue',
         4: 'orange'}
colorlight = {2.5: 'lightblue',
              4:  'lightcoral'}
for angle in [2.5, 4]:
    kernel_factor = lambda ell: ell * J4(ell * angle/60/180*np.pi)
    
    for trs in s.get_tracer_combinations():
        i = int(trs[1].split('__')[1])
        j = int(trs[0].split('__')[1])

        ell, clc = cls[trs]
        cl = interp1d(ell, clc, bounds_error=False, fill_value=0)
        kernel = lambda ell: kernel_factor(ell) * cl(ell)
        xi_int = scipy.integrate.cumulative_trapezoid(kernel(ell_all), x=ell_all, initial=0)
        # Kernel
        ax[i, j].semilogx(ell_all, xi_int/xi_int[-1], label=f"{angle}'", color=color[angle])
        
        if angle == 4:
            ax[i, j].axvline(4500, ls='--', color='gray', label=r'$\ell=4500$')
            ax[i, j].axvline(8192, ls=':', color='gray', label=r'$\ell=8192$')
            
#             ax[i, j].axhline(1.05, ls='--', color=colorlight[angle])
#             ax[i, j].axhline(0.95, ls='--', color=colorlight[angle])
            
        # Get ell at which the integral converges at 5%
        threshold = (xi_int/xi_int[-1] > 0.95) * (xi_int/xi_int[-1] < 1.05)
        lth = None
        for iell, ell in enumerate(ell_all):
            if np.all(threshold[iell:]):
                lth = ell
                break
            
        ax[i, j].axvline(lth, ls='--', color=colorlight[angle], label='$5\%$ error' if angle == 2.5 else None)

        # Get ell at which the integral converges at 1%
        threshold = (xi_int/xi_int[-1] > 0.99) * (xi_int/xi_int[-1] < 1.01)
        lth = None
        for iell, ell in enumerate(ell_all):
            if np.all(threshold[iell:]):
                lth = ell
                break
            
        ax[i, j].axvline(lth, ls=':', color=colorlight[angle], label='$1\%$ error' if angle == 2.5 else None)
    
for i in range(nbin):
    ax[i, 0].set_ylabel(r"$\int_0^\ell d\ell \ell J_4(\ell \theta) C_\ell^{EE}$ / int ($\ell=1e5$)")
    ax[-1, i].set_xlabel("$\ell$")
#     ax[0, i].set_xscale("log")
#     ax[i, 0].set_ylim([-0.1, 0.1])
    ax[0, i].set_xlim([1e3, None])
    ax[0, 0].legend()

f.suptitle(f'DES (Halofit, no baryons, CosmologyVanillaLCDM)', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

## Check with pyccl -> It seems to work
# ell, clc = cls[trs]
# cl = interp1d(ell, clc, bounds_error=False, fill_value=0)(ell_all)
# xi = ccl.correlations.correlation(cosmo, ell=ell_all, C_ell=cl, theta=4/60, type='GG-', method='fftlog')
# print(xi, xi_int[-1]/(2*np.pi))

# %%
cosmo = ccl.CosmologyVanillaLCDM()
ccl_tracers = {}
for trn, tr in s.tracers.items():
    ccl_tracers[trn] = ccl.WeakLensingTracer(cosmo, dndz=(tr.z, tr.nz), ia_bias=None)

cls = {}
ell = np.concatenate([[0, 1], np.unique(np.logspace(np.log10(2), np.log10(1e5), 30, dtype=int))], dtype=int)
for trs in s.get_tracer_combinations():
    cls[trs] = (ell, ccl.angular_cl(cosmo, ccl_tracers[trs[0]], ccl_tracers[trs[1]], ell))

nbin = 4
f, ax = plt.subplots(nbin, nbin, figsize=(12, 10), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
ell_all = np.arange(100000)
# J4 Bessel function of first kind
J4 = lambda x: scipy.special.jv(4, x)

color = {2.5: 'blue',
         4: 'orange'}
colorlight = {2.5: 'lightblue',
              4:  'lightcoral'}

g = y = r = 0
lg = ly = lr =0
for angle in np.logspace(0, 3, 40):
    kernel_factor = lambda ell: ell * J4(ell * angle/60/180*np.pi)
    
    for trs in s.get_tracer_combinations():

        i = int(trs[1].split('__')[1])
        j = int(trs[0].split('__')[1])

        ell, clc = cls[trs]
        cl = interp1d(ell, clc, bounds_error=False, fill_value=0)
        kernel = lambda ell: kernel_factor(ell) * cl(ell)
        xi_int = scipy.integrate.cumulative_trapezoid(kernel(ell_all), x=ell_all, initial=0)
        # Kernel
        ax[i, j].errorbar(angle, 1e4*angle*xi_int[-1], color='blue', fmt='.')

        # Get angle at which the integral converges at 1% for lmax=4500
        lmax = 4500
        if np.all((xi_int[lmax:]/xi_int[-1] > 0.99) * (xi_int[lmax:]/xi_int[-1] < 1.01)):
            ax[i, j].errorbar(angle, 1e4*angle*xi_int[-1], color='green', fmt='.', label=r'<1% error with $\ell_{\rm max} = 4500$' if g == 0 else None)
            g += 1
        elif np.all((xi_int[lmax:]/xi_int[-1] > 0.95) * (xi_int[lmax:]/xi_int[-1] < 1.05)):
            ax[i, j].errorbar(angle, 1e4*angle*xi_int[-1], color='yellow', fmt='.', label=r'<5% error with $\ell_{\rm max} = 4500$' if y == 0 else None)
            y += 1
        else:
            ax[i, j].errorbar(angle, 1e4*angle*xi_int[-1], color='red', fmt='.', label=r'>5% error with $\ell_{\rm max} = 4500$' if r == 0 else None)
            r += 1
            
        lmax = 8192
        if np.all((xi_int[lmax:]/xi_int[-1] > 0.99) * (xi_int[lmax:]/xi_int[-1] < 1.01)):
            ax[i, j].errorbar(angle, 1e4*angle*xi_int[-1]-0.5, color='olive', fmt='.', label=r'<1% error with $\ell_{\rm max} = 8192$' if lg == 0 else None)
            lg += 1
        elif np.all((xi_int[lmax:]/xi_int[-1] > 0.95) * (xi_int[lmax:]/xi_int[-1] < 1.05)):
            ax[i, j].errorbar(angle, 1e4*angle*xi_int[-1]-0.5, color='gold', fmt='.', label=r'<5% error with $\ell_{\rm max} = 8192$' if ly == 0 else None)
            ly += 1
        else:
            ax[i, j].errorbar(angle, 1e4*angle*xi_int[-1]-0.5, color='coral', fmt='.', label=r'>5% error with $\ell_{\rm max} = 8192$' if lr == 0 else None)
            lr += 1

    
for i in range(nbin):
    ax[i, 0].set_ylabel(r"$1e4 \theta \xi_{-}$")
    ax[-1, i].set_xlabel(r"$\theta$ [arcmin]")
    ax[0, i].set_xscale("log")
#     ax[i, 0].set_ylim([-0.1, 0.1])
#     ax[0, i].set_xlim([1e3, None])
    ax[0, 0].legend(loc='upper left')

f.suptitle(f'DES (Halofit, no baryons, CosmologyVanillaLCDM)', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

## Check with pyccl -> It seems to work
# ell, clc = cls[trs]
# cl = interp1d(ell, clc, bounds_error=False, fill_value=0)(ell_all)
# xi = ccl.correlations.correlation(cosmo, ell=ell_all, C_ell=cl, theta=4/60, type='GG-', method='fftlog')
# print(xi, xi_int[-1]/(2*np.pi))

# %%
cosmo = ccl.CosmologyVanillaLCDM()
ccl_tracers = {}
for trn, tr in s.tracers.items():
    ccl_tracers[trn] = ccl.WeakLensingTracer(cosmo, dndz=(tr.z, tr.nz), ia_bias=None)

cls = {}
ell = np.concatenate([[0, 1], np.unique(np.logspace(np.log10(2), np.log10(1e5), 30, dtype=int))], dtype=int)
for trs in s.get_tracer_combinations():
    cls[trs] = (ell, ccl.angular_cl(cosmo, ccl_tracers[trs[0]], ccl_tracers[trs[1]], ell))

nbin = 4
f, ax = plt.subplots(nbin, nbin, figsize=(12, 10), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
ell_all = np.arange(100000)
# J4 Bessel function of first kind
J4 = lambda x: scipy.special.jv(4, x)

color = {2.5: 'blue',
         4: 'orange'}
colorlight = {2.5: 'lightblue',
              4:  'lightcoral'}

g = y = r = 0
lg = ly = lr =0
for angle in np.logspace(0, 3, 40):
    kernel_factor = lambda ell: ell * J4(ell * angle/60/180*np.pi)
    
    for trs in s.get_tracer_combinations():

        i = int(trs[1].split('__')[1])
        j = int(trs[0].split('__')[1])

        ell, clc = cls[trs]
        cl = interp1d(ell, clc, bounds_error=False, fill_value=0)
        kernel = lambda ell: kernel_factor(ell) * cl(ell)
        xi_int = scipy.integrate.cumulative_trapezoid(kernel(ell_all), x=ell_all, initial=0)
        # Kernel
        ax[i, j].errorbar(angle, 1e4*angle*xi_int[-1], color='blue', fmt='.')

        # Get angle at which the integral converges at 1% for lmax=4500
        lmax = 50000
        if np.all((xi_int[lmax:]/xi_int[-1] > 0.99) * (xi_int[lmax:]/xi_int[-1] < 1.01)):
            ax[i, j].errorbar(angle, 1e4*angle*xi_int[-1], color='green', fmt='.', label=r'<1% error with $\ell_{\rm max} = 50000$' if g == 0 else None)
            g += 1
        elif np.all((xi_int[lmax:]/xi_int[-1] > 0.95) * (xi_int[lmax:]/xi_int[-1] < 1.05)):
            ax[i, j].errorbar(angle, 1e4*angle*xi_int[-1], color='yellow', fmt='.', label=r'<5% error with $\ell_{\rm max} = 50000$' if y == 0 else None)
            y += 1
        else:
            ax[i, j].errorbar(angle, 1e4*angle*xi_int[-1], color='red', fmt='.', label=r'>5% error with $\ell_{\rm max} = 50000$' if r == 0 else None)
            r += 1
            
        ax[i, j].axvline(2.5, color='gray', ls='--')

for i in range(nbin):
    ax[i, 0].set_ylabel(r"$1e4 \theta \xi_{-}$")
    ax[-1, i].set_xlabel(r"$\theta$ [arcmin]")
    ax[0, i].set_xscale("log")
#     ax[i, 0].set_ylim([-0.1, 0.1])
#     ax[0, i].set_xlim([1e3, None])
    ax[0, 0].legend(loc='upper left')

f.suptitle(f'DES (Halofit, no baryons, CosmologyVanillaLCDM)', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

## Check with pyccl -> It seems to work
# ell, clc = cls[trs]
# cl = interp1d(ell, clc, bounds_error=False, fill_value=0)(ell_all)
# xi = ccl.correlations.correlation(cosmo, ell=ell_all, C_ell=cl, theta=4/60, type='GG-', method='fftlog')
# print(xi, xi_int[-1]/(2*np.pi))

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## Comparison of Giovanni's $C_\ell$ vs ours

# %% hidden=true
# Cell computed with Giovanni's code
# import pickle
# with open('test/models_computed_with_giovannis_code.pickle', 'rb') as f:
#     dd = pickle.load(f)
    
# # Load sacc file used in chains
# sdata = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov_GNG.fits')

# Generate sacc file that we will use to compute unbinned Cells with our Likelihood
ell = np.unique(np.logspace(np.log10(2), np.log10(50000), 100, dtype=int))
ell_unb = np.arange(ell[-1]+1)
for who in ['carlos', 'giovanni']:
    if os.path.isfile(f'test/{who}bf_computed_with_giovannis_code.sacc'):
        continue
    s = sacc.Sacc()
    for trn, trv in sdata.tracers.items():
        s.tracers[trn] = trv

    for tr1, tr2 in sdata.get_tracer_combinations():
        i = tr1.split('__')[1]
        j = tr2.split('__')[1]
        model = dd[who]['model'][f'{i}_{j}']
        cl = interp1d(model['ell'], model['cell'])(ell)
        bpw = np.zeros((ell_unb.size, ell.size))
        for elli in range(ell.size):
            bpw[ell[elli]][elli] = 1
        win = sacc.BandpowerWindow(ell_unb, bpw)
        
        s.add_ell_cl('cl_ee', tr1, tr2, ell, cl, window=win)

    s.add_covariance(np.diag(np.ones_like(s.mean, dtype=int)))
    for k, v in dd[who]['bf'].items():
        s.metadata[f'bfpars/{k}'] = v
    s.save_fits(f'test/{who}bf_computed_with_giovannis_code.sacc', overwrite=True)
    
model = get_model('test/desy3wl_baryons_nocuts_nla_lmax50000_testunbinned.yml')
sacc_dicts = {}
for who in ['carlos', 'giovanni']:  
    sacc_dicts[who] = {'cobaya': get_cl_theory_sacc(model, dd[who]['bf']),
                       'giovanni': sacc.Sacc.load_fits(f'test/{who}bf_computed_with_giovannis_code.sacc')}


# %% hidden=true
nbin = 4
f, ax = plt.subplots(nbin, nbin, figsize=(12, 10), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in sacc_carlos.get_tracer_combinations():
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])
    
    for who in ['carlos', 'giovanni']:
        ell, clc = sacc_dicts[who]['cobaya'].get_ell_cl('cl_ee', trs[0], trs[1])
        ell, clg = sacc_dicts[who]['giovanni'].get_ell_cl('cl_ee', trs[0], trs[1])
        sel = ell > 2
        ax[i, j].semilogx(ell[sel], clc[sel]/clg[sel] - 1, label=who)
        
    ax[i, j].axhline(0, ls='--', color='gray')

for i in range(nbin):
    ax[i, 0].set_ylabel(r"$C_\ell^{\rm Carlos} / C_\ell^{\rm Giovanni} - 1$")
    ax[-1, i].set_xlabel("$\ell$")
    # ax[0, i].set_xscale("log")
    ax[i, 0].set_ylim([-0.1, 0.1])
    ax[0, 0].legend()

f.suptitle(f'DES', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% hidden=true
nbin = 4
f, ax = plt.subplots(nbin, nbin, figsize=(12, 10), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in sacc_carlos.get_tracer_combinations():
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])

    for who in ['carlos', 'giovanni']:
        ell, clc = sacc_dicts[who]['cobaya'].get_ell_cl('cl_ee', trs[0], trs[1])
        ax[i, j].loglog(ell, clc, ls='-' if who=='carlos' else '--')
        ell, clg = sacc_dicts[who]['giovanni'].get_ell_cl('cl_ee', trs[0], trs[1])
        ax[i, j].loglog(ell, clg, ls='-' if who=='carlos' else '--')
        
    # ax[i, j].axhline(0, ls='--', color='gray')

for i in range(nbin):
    ax[i, 0].set_ylabel(r"$C_\ell$")
    ax[-1, i].set_xlabel("$\ell$")
    # ax[0, i].set_xscale("log")
    ax[i, 0].set_ylim([1e-16, None])
    # ax[0, 0].legend()

f.suptitle(f'DES', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## DESY3 no KiDS1000 $C_\ell$ x KiDS1000

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### xCell

# %% hidden=true
s = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3_noK1000_K1000_4096/cls_cov.fits')

# %% hidden=true
s2 = s.copy()

s2.keep_selection(ell__gt=100)
d = s2.mean
icov = np.linalg.inv(s2.covariance.covmat)
chi2 = d.dot(icov).dot(d)
pvalue = 1 - stats.chi2.cdf(chi2, d.size)
print('All', chi2, pvalue, d.size)

s2.keep_selection(data_type='cl_ee')
d = s2.mean
icov = np.linalg.inv(s2.covariance.covmat)
chi2 = d.dot(icov).dot(d)
pvalue = 1 - stats.chi2.cdf(chi2, d.size)
print('EE', chi2, pvalue, d.size)

# %% hidden=true
f, ax = plt.subplots(4, 5, figsize=(15, 15))

for tr1, tr2 in s.get_tracer_combinations():
    i = int(tr1[-1])
    j = int(tr2[-1])
    ell, cl, cov = s.get_ell_cl('cl_ee', tr1, tr2, return_cov=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, cl, yerr=err, c='k', fmt='.')
    ax[i, j].text(0.95, 0.95, f"{tr1}-{tr2}",
                  horizontalalignment='right', verticalalignment='top',
                  transform=ax[i, j].transAxes)
    ax[i, j].axhline(0, ls='--', color='gray')
    ax[i, j].semilogx()desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG
    
for i in range(ax.shape[1]):
    ax[-1, i].set_xlabel('$\ell$')
for i in range(ax.shape[0]):
    ax[i, 0].set_ylabel('$C_\ell$')
plt.show()
plt.close()

# %% hidden=true
f, ax = plt.subplots(4, 5, figsize=(15, 15))

for tr1, tr2 in s.get_tracer_combinations():
    i = int(tr1[-1])
    j = int(tr2[-1])
    ell, cl, cov = s.get_ell_cl('cl_eb', tr1, tr2, return_cov=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, cl, yerr=err, c='k', fmt='.')
    ax[i, j].text(0.95, 0.95, f"{tr1}-{tr2}",
                  horizontalalignment='right', verticalalignment='top',
                  transform=ax[i, j].transAxes)
    ax[i, j].axhline(0, ls='--', color='gray')
    ax[i, j].semilogx()
    
for i in range(ax.shape[1]):
    ax[-1, i].set_xlabel('$\ell$')
for i in range(ax.shape[0]):
    ax[i, 0].set_ylabel('$C_\ell$')
plt.show()
plt.close()

# %% hidden=true
f, ax = plt.subplots(4, 5, figsize=(15, 15))

for tr1, tr2 in s.get_tracer_combinations():
    i = int(tr1[-1])
    j = int(tr2[-1])
    ell, cl, cov = s.get_ell_cl('cl_be', tr1, tr2, return_cov=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, cl, yerr=err, c='k', fmt='.')
    ax[i, j].text(0.95, 0.95, f"{tr1}-{tr2}",
                  horizontalalignment='right', verticalalignment='top',
                  transform=ax[i, j].transAxes)
    ax[i, j].axhline(0, ls='--', color='gray')
    ax[i, j].semilogx()
    
for i in range(ax.shape[1]):
    ax[-1, i].set_xlabel('$\ell$')
for i in range(ax.shape[0]):
    ax[i, 0].set_ylabel('$C_\ell$')
plt.show()
plt.close()

# %% hidden=true
f, ax = plt.subplots(4, 5, figsize=(15, 15))

for tr1, tr2 in s.get_tracer_combinations():
    i = int(tr1[-1])
    j = int(tr2[-1])
    ell, cl, cov = s.get_ell_cl('cl_bb', tr1, tr2, return_cov=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, cl, yerr=err, c='k', fmt='.')
    ax[i, j].text(0.95, 0.95, f"{tr1}-{tr2}",
                  horizontalalignment='right', verticalalignment='top',
                  transform=ax[i, j].transAxes)
    ax[i, j].axhline(0, ls='--', color='gray')
    ax[i, j].semilogx()
    
for i in range(ax.shape[1]):
    ax[-1, i].set_xlabel('$\ell$')
for i in range(ax.shape[0]):
    ax[i, 0].set_ylabel('$C_\ell$')
plt.show()
plt.close()

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### By hand [the MCM~noise so its inverse is problematic. Using the coupled Cells]

# %% hidden=true
import pymaster as nmt

# %% hidden=true
maps = {}
maps['des0'] = hp.read_map("/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/DESY3_bin0_removed_overlap_KiDS1000_signal_map_shear_coordC_ns4096.fits.gz", field=None)
maps['kids4'] = hp.read_map("/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/KiDS1000_bin0_signal_map_shear_coordC_ns4096.fits.gz", field=None)

mask = {}
mask['des0'] = hp.read_map("/mnt/extraspace/damonge/Datasets/DES_Y3/xcell_reruns/mask_mask_DESY30_noK1000_coordC_ns4096.fits.gz")
mask['kids4'] = hp.read_map("/mnt/extraspace/damonge/Datasets/KiDS1000/xcell_runs/mask_mask_KiDS1000__0_coordC_ns4096.fits.gz")

fields = {}
fields['des0'] = nmt.NmtField(mask['des0'], maps['des0'], spin=2, n_iter=0)
fields['kids4'] = nmt.NmtField(mask['kids4'], maps['kids4'], spin=2, n_iter=0)

# %% hidden=true
w = nmt.NmtWorkspace()
w.read_from("/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3_noK1000_K1000_4096/DESY3_KiDS1000/w__mask_DESY30_noK1000__mask_KiDS1000__4.fits")

cl = w.decouple_cell(cl_cp)
cl_cp = nmt.compute_coupled_cell(fields['des0'], fields['kids4'])

# cw = nmt.NmtCovarianceWorkspace()
# cw.read_from("/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3_noK1000_K1000_4096/cov/cw__mask_DESY30_noK1000__mask_KiDS1000__4__mask_DESY30_noK1000__mask_KiDS1000__4.fits")

# %% hidden=true
# clfile = np.load("/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3_noK1000_K1000_4096/DESY3_KiDS1000/cl_DESY3__0_KiDS1000__4.npz")

plt.semilogx(clfile['ell'], clfile['cl'][0], label="xCell")
plt.semilogx(clfile['ell'], cl[0], label='By Hand')
plt.xlabel("$\ell$")
plt.ylabel("$C_\ell$")
plt.legend()
plt.show()
plt.close()

# %% hidden=true
cl_cp.shape

plt.semilogx(np.arange(cl_cp[0].size), cl_cp[0], label="xCell")
plt.xlabel("$\ell$")
plt.ylabel("$C_\ell$")
plt.legend()
plt.show()
plt.close()

# %% hidden=true
# s = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3_noK1000_K1000_4096/cls_cov.fits')

f, ax = plt.subplots(4, 5, figsize=(15, 15))

for tr1, tr2 in s.get_tracer_combinations():
    i = int(tr1[-1])
    j = int(tr2[-1])
    clfile = np.load(f"/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3_noK1000_K1000_4096/DESY3_KiDS1000/cl_DESY3__{i}_KiDS1000__{j}.npz")

    cll = clfile['cl_cp'][0]
    n_rebin = 256
    cl_b = np.mean(cll.reshape([-1, n_rebin]), axis=-1)
    ls = np.mean(np.arange(len(cll)).reshape([-1, n_rebin]), axis=-1)
    ecl_b = np.std(cll.reshape([-1, n_rebin]), axis=-1)/np.sqrt(n_rebin)
    
    ax[i, j].errorbar(ls, cl_b/ecl_b, yerr=np.ones_like(ls), fmt='k.')

    ax[i, j].text(0.95, 0.95, f"{tr1}-{tr2}",
                  horizontalalignment='right', verticalalignment='top',
                  transform=ax[i, j].transAxes)
    
    ax[i, j].axhline(0, ls='--', color='gray')
    # ax[i, j].semilogx()
    
for i in range(ax.shape[1]):
    ax[-1, i].set_xlabel('$\ell$')
for i in range(ax.shape[0]):
    ax[i, 0].set_ylabel('Coupled $C_\ell$')
plt.show()
plt.close()

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## DESY3 pixwin test with 8192 $C_\ell$ and 4096 covariance

# %% hidden=true jupyter={"outputs_hidden": true}
# # No Baryons (4096)
# key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNG'
# results[key] = load_chain(f'./chains/{key}/{key}',
#                            r'DESY3 with No-Baryons ($20 < \ell < 8192$)', 'black', 0.15)
# print(key, f"R-1 = {results[key]['R-1']}")

# # No Baryons (4096)
# key = 'desy3wl_nobaryons_nocuts_nla_nside8192cov4096_lmin20_GNG'
# results[key] = load_chain(f'./chains/{key}/{key}',
#                            r'DESY3 with No-Baryons (8192 Cell 4096 cov, $20 < \ell < 8192$)', 'black', 0.15)
# print(key, f"R-1 = {results[key]['R-1']}")

# %% hidden=true
# No Baryons with ell_max=2048
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNG',
          'desy3wl_nobaryons_nocuts_nla_nside8192cov4096_lmin20_GNG',
         ]

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['blue', 'red', 'green'] # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])


### Full parameter space
parameters = results[chains[0]]['MCSamples'].getParamNames().list()
parameters.remove('minuslogprior__0')
parameters.remove('chi2__cl_like.ClLike')

g = plots.get_subplot_plotter()
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## DESY3 compare with bad neutrinos runs

# %% hidden=true
###############################
########### DESY3 (bad neutrinos) ###########
###############################
# nside = 1024 -> DES binning unless stated otherwise
# nside = 4098 -> Our binning

# ########## lmax = 2048 ##########
# ##### DES binning
# # Baryons (1024)
# key = 'desy3wl_baryons_nocuts_nla_nside1024'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 with Baryons (1024, $\ell_{\rm max} = 2048$) Bad$\nu$', 'gray', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# # No Baryons (1024)
# key = 'desy3wl_nobaryons_nocuts_nla_nside1024'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 with No-Baryons (1024, $\ell_{\rm max} = 2048$) Bad$\nu$', 'black', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# # Baryons (1024) (same as before, just rerun)
# key = 'desy3wl_nobaryons_nocuts_nla_nside1024_2'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 with No-Baryons (1024, $\ell_{\rm max} = 2048$) just rerun Bad$\nu$', 'purple', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")


# # No Baryons + HALOFIT (1024)
# key = 'desy3wl_nobaryons_nocuts_nla_nside1024_halofit'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 with No-Baryons + HALOFIT (1024, $\ell_{\rm max} = 2048$) Bad$\nu$', 'blue', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# # No Baryons (1024) lmax=200
# key = 'desy3wl_nobaryons_nocuts_nla_nside1024_lmax200'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 with No-Baryons (1024, $\ell_{\rm max} = 200$) Bad$\nu$', 'green', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# # No Baryons (1024) lmin=200, lmax=2048
# key = 'desy3wl_nobaryons_nocuts_nla_nside1024_lmin200'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 with No-Baryons (1024, $200 \leq \ell \leq 2048$) Bad$\nu$', 'orange', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# # No Baryons (1024) lmin=20, lmax=2048
# key = 'desy3wl_nobaryons_nocuts_nla_nside1024_lmin20'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 with No-Baryons (1024, $20 \leq \ell \leq 2048$) Bad$\nu$', 'orange', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# # No Baryons (1024) lmax=2048, lmin=30
# key = 'desy3wl_nobaryons_nocuts_nla_nside1024_lmin30'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 with No-Baryons (1024, $30 < \ell < 2048$) Bad$\nu$', 'purple', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# # No Baryons (1024) lmax=2048 ourbinning at ell > 500
# key = 'desy3wl_nobaryons_nocuts_nla_nside1024_ourbinningatsmallscales'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 with No-Baryons (1024, $\ell_{\rm max} = 2048$, our binning at $ell > 500$) Bad$\nu$', 'orange', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# #### Our binning
# # Baryons (4096) lmax=2048
# key = 'desy3wl_baryons_nocuts_nla_nside4096_lmax2048'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 with Baryons ($\ell_{\rm max} = 2048$) Bad$\nu$', 'gray', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# Baryons (4096) lmax=2048, lmin=20
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20'
results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
                           r'DESY3 with Baryons ($20 < \ell < 2048$) Bad$\nu$', 'orange', 0.15,
                                       Cobaya=False) # don't compute the model because we have changed the lkl
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")


# # No Baryons (4096) lmax=2048
# key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 with No-Baryons ($\ell_{\rm max} = 2048$) Bad$\nu$', 'black', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# # No Baryons (4096) lmax=2048 with Pixel Window function
# key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_pixwin'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 with No-Baryons ($\ell_{\rm max} = 2048$, PixWin) Bad$\nu$', 'black', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# # No Baryons (1024) lmax=2048 with our binning
# key = 'desy3wl_nobaryons_nocuts_nla_nside1024_ourbinning'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 with No-Baryons (1024, $\ell_{\rm max} = 2048$, 4096 binning) Bad$\nu$', 'blue', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# No Baryons (4096) lmax=2048, lmin=20
key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20'
results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
                           r'DESY3 with No-Baryons ($20 < \ell < 2048$) Bad$\nu$', 'orange', 0.15,
                           Cobaya=False) # don't compute the model because we have changed the lkl
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# ########## lmax = 8192 ##########
# # Baryons (4096)
# key = 'desy3wl_baryons_nocuts_nla_nside4096'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 with Baryons ($\ell_{\rm max} = 8192$) Bad$\nu$', 'gray', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# # No Baryons (4096)
# key = 'desy3wl_nobaryons_nocuts_nla_nside4096'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 with No-Baryons ($\ell_{\rm max} = 8192$) Bad$\nu$', 'black', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# #######################################
# ########### DESY3 no K1000###########
# #######################################
# # DES binning
# # No Baryons (1024)
# key = 'desy3wl_noK1000_nobaryons_nocuts_nla_nside1024'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'DESY3 w/o K1000 overlap with No-Baryons (1024) Bad$\nu$', 'pink', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# %% hidden=true
# Effect of "bad_neutrinos" in the case No-Baryons and lmax=2048
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20',
          'bad_neutrinos/desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20']

parameters = ['S8', 'sigma8', 'Omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['black', 'gray', 'blue', 'red'] #[results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Effect of "bad_neutrinos" in the case with Baryons and lmax=2048
chains = ['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20',
          'bad_neutrinos/desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20']

parameters = ['S8', 'sigma8', 'Omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['black', 'gray', 'blue', 'red'] #[results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## K1000 Comparison with Bad_neutrinos

# %% hidden=true
#################################
########### KiDS10000 ###########
#################################

########## lmax = 2048 ##########
## lmin = 100 as in KiDS papers
# # Baryons (4096)
# key = 'k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'KiDS1000 with Baryons (1024, $100 < \ell < 2048$)', 'gray', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# # No Baryons (4096)
# key = 'k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'KiDS1000 with No-Baryons (1024, $100 < \ell < 2048$)', 'black', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")


# ########## lmax = 8192 ##########
# ## lmin = 100 as in KiDS papers
# # Baryons (4096)
# key = 'k1000_baryons_nocuts_nla_nside4096_lmin100'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'KiDS1000 with Baryons (1024, $100 < \ell < 8192$)', 'gray', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# # No Baryons (4096)
# key = 'k1000_nobaryons_nocuts_nla_nside4096_lmin100'
# results[f"bad_neutrinos/{key}"] = load_chain(f'./chains/bad_neutrinos/{key}/{key}',
#                            r'KiDS1000 with No-Baryons (1024, $100 < \ell < 8192$)', 'black', 0.15)
# print(key, f"R-1 = {results[f"bad_neutrinos/{key}"]['R-1']}")

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## Sacc file DES+KiDS are ok

# %% hidden=true
sdk = results['desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG']['model'].likelihood['cl_like.ClLike'].sacc_file.copy()
sd = results['desy3wl_noK1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG']['model'].likelihood['cl_like.ClLike'].sacc_file.copy()
sk = results['k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG']['model'].likelihood['cl_like.ClLike'].sacc_file.copy()


# %% hidden=true
sdk.keep_selection(data_type='cl_ee')
sd.keep_selection(data_type='cl_ee')
sk.keep_selection(data_type='cl_ee')

# %% hidden=true
ix = []
for trs in sd.get_tracer_combinations():
    ix.extend(sdk.indices(tracers=trs))
    
plt.plot(np.diag(sdk.covariance.covmat[ix][:, ix]) - np.diag(sd.covariance.covmat))

np.max(np.abs(sdk.covariance.covmat[ix][:, ix] - sd.covariance.covmat))

# %% hidden=true
ix = []
for trs in sk.get_tracer_combinations():
    ix.extend(sdk.indices(tracers=trs))
    
plt.plot(np.diag(sdk.covariance.covmat[ix][:, ix]) - np.diag(sk.covariance.covmat))

np.max(np.abs(sdk.covariance.covmat[ix][:, ix] - sk.covariance.covmat))

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## DESY3 $\ell_{\rm max} = 2048$ with NSide = 1024 or 4096 -> $\ell_{\rm min} = 20$ (remove first $C_\ell$) [need the chains in bad_neutrinos]

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# #### $C_\ell$s

# %% hidden=true
s1024 = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt/cls_cov.fits')
s4096 = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov.fits')

s1024.keep_selection(data_type='cl_ee')
s1024.keep_selection(ell__lt=2049)

s4096.keep_selection(data_type='cl_ee')
s4096.keep_selection(ell__lt=2049)

m = results['desy3wl_nobaryons_nocuts_nla_nside1024']['model']
bf = results['desy3wl_nobaryons_nocuts_nla_nside1024']['pars_bf']
cl1024 = get_cl_theory(m, bf)

m = results['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048']['model']
bf = results['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048']['pars_bf']
cl4096 = get_cl_theory(m, bf)

# Check that we can use the indices in the sacc file
d = s1024.mean - cl1024
assert np.abs(d.dot(np.linalg.inv(s1024.covariance.covmat).dot(d)) / results['desy3wl_nobaryons_nocuts_nla_nside1024']['chi2_bf'] - 1) < 1e-3

d = s4096.mean - cl4096
assert np.abs((d.dot(np.linalg.inv(s4096.covariance.covmat).dot(d)) / results['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048']['chi2_bf'] - 1) < 1e-3)


# E-modes
f, ax = plt.subplots(4, 4, figsize=(14, 14), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in s1024.get_tracer_combinations():
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])
    
    # Data 1024
    ell, cl, cov, ind = s1024.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='blue', label='1024', alpha=0.8)
    
    ax[i, j].plot(ell, ell * cl1024[ind] * 1e7, color='cyan', label='1024 BF', alpha=0.8)
    
    
    # Data 4096
    ell, cl, cov, ind = s4096.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='orange', label='4096', alpha=0.8)
    ax[i, j].plot(ell, ell * cl4096[ind] * 1e7, color='darkorange', label=r'4096 BF ($\ell_{\rm max} = 2048$)', alpha=0.8)

#     # Scale cuts line
#     ax[i, j].axvline(2048, color='gray', ls='--')
#     ax[i, j].axvline(8192, color='black', ls='--')

#     ax[i, j].text(0.99, 0.99, f"{i}-{j}", horizontalalignment='right',
#      verticalalignment='top', transform=ax[i,j].transAxes, fontsize=14)
#     ax[i, j].axhline(0, ls='--', color='gray')
    
for i in range(4):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")
    ax[0, 0].legend()

f.suptitle('EE', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% hidden=true
# s1024 = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt/cls_cov.fits')
# s4096 = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov.fits')

# s1024.keep_selection(data_type='cl_ee')
# s1024.keep_selection(ell__lt=2049)

# s4096.keep_selection(data_type='cl_ee')
# s4096.keep_selection(ell__lt=2049)

# m = results['desy3wl_nobaryons_nocuts_nla_nside1024']['model']
# bf = results['desy3wl_nobaryons_nocuts_nla_nside1024']['pars_bf']
# cl1024 = get_cl_theory(m, bf)

# m = results['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048']['model']
# bf = results['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048']['pars_bf']
# cl4096 = get_cl_theory(m, bf)

# # Check that we can use the indices in the sacc file
# d = s1024.mean - cl1024
# assert np.abs(d.dot(np.linalg.inv(s1024.covariance.covmat).dot(d)) / results['desy3wl_nobaryons_nocuts_nla_nside1024']['chi2_bf'] - 1) < 1e-3

# d = s4096.mean - cl4096
# assert np.abs((d.dot(np.linalg.inv(s4096.covariance.covmat).dot(d)) / results['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048']['chi2_bf'] - 1) < 1e-3)

samples = results['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048']['MCSamples']
p = samples.getParams()
bf = samples.getParamSampleDict(np.argmin(np.abs(p.A_sE9 / 4 -1)))

m = results['desy3wl_nobaryons_nocuts_nla_nside1024']['model']
cl1024 = get_cl_theory(m, bf)
d = s1024.mean - cl1024
chi2 = d.dot(np.linalg.inv(s1024.covariance.covmat).dot(d))
print('1024 case chi2 = ', chi2, 'for', cl1024.size, 'points',
      'p-value = ', 1-scipy.stats.chi2.cdf(chi2, df=cl1024.size - 2))

m = results['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048']['model']
cl4096 = get_cl_theory(m, bf)
d = s4096.mean - cl4096
chi2 = d.dot(np.linalg.inv(s4096.covariance.covmat).dot(d))
print('4096 case chi2 = ', chi2, 'for', cl4096.size, 'points',
     'p-value = ', 1-scipy.stats.chi2.cdf(chi2, df=cl4096.size - 2))


# E-modes
f, ax = plt.subplots(4, 4, figsize=(14, 14), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
for trs in s1024.get_tracer_combinations():
    i = int(trs[1].split('__')[1])
    j = int(trs[0].split('__')[1])
    
    # Data 1024
    ell, cl, cov, ind = s1024.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='blue', label='1024', alpha=0.8)
    
    ax[i, j].plot(ell, ell * cl1024[ind] * 1e7, color='cyan', label='1024 binning', alpha=0.8)
    
    
    # Data 4096
    ell, cl, cov, ind = s4096.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
    err = np.sqrt(np.diag(cov))
    ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='orange', label='4096', alpha=0.8)
    ax[i, j].plot(ell, ell * cl4096[ind] * 1e7, color='darkorange', label=r'4096 binning ($\ell_{\rm max} = 2048$)', alpha=0.8)

#     # Scale cuts line
#     ax[i, j].axvline(2048, color='gray', ls='--')
#     ax[i, j].axvline(8192, color='black', ls='--')

#     ax[i, j].text(0.99, 0.99, f"{i}-{j}", horizontalalignment='right',
#      verticalalignment='top', transform=ax[i,j].transAxes, fontsize=14)
#     ax[i, j].axhline(0, ls='--', color='gray')
    
for i in range(4):
    ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
    ax[-1, i].set_xlabel("$\ell$")
    ax[0, i].set_xscale("log")
    ax[0, 0].legend()

f.suptitle('EE', fontsize=16)
plt.tight_layout()
plt.show()
plt.close()

# %% hidden=true
# No-Baryons with lmax=2048 but with Cells computed with nside 1024 or 4096
chains = ['desy3wl_nobaryons_nocuts_nla_nside1024',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048',
          'desy3wl_nobaryons_nocuts_nla_nside1024_2',
          'desy3wl_nobaryons_nocuts_nla_nside1024_ourbinning'
         ]

parameters = ['S8', 'sigma8', 'Omega_m', 'A_sE9', 'chi2__cl_like.ClLike']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


# Marker
samples = results['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048']['MCSamples']
p = samples.getParams()
bf = samples.getParamSampleDict(np.argmin(np.abs(p.A_sE9 / 4 -1)))


g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=bf)

# %% hidden=true
# Check a model that is about 2/3sigma away in the case of our binning and check the chi2 with the DES binning
s1024 = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt/cls_cov.fits')
s1024_lmin8 = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_ourbinninglmin8/cls_cov.fits')
s4096 = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_ourbinning/cls_cov.fits')
s1024_ls = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_ourbinningatlargescales//cls_cov.fits')

s1024.keep_selection(data_type='cl_ee')
s1024.keep_selection(ell__lt=2049)

s4096.keep_selection(data_type='cl_ee')
s4096.keep_selection(ell__lt=2049)

s1024_lmin8.keep_selection(data_type='cl_ee')
s1024_lmin8.keep_selection(ell__lt=2049)

s1024_ls.keep_selection(data_type='cl_ee')
s1024_ls.keep_selection(ell__lt=2049)

m1024_lmin8 = get_model('input/desy3wl_nobaryons_nocuts_nla_nside1024_ourbinninglmin8.yml')
m1024_ls = get_model('input/desy3wl_nobaryons_nocuts_nla_nside1024_ourbinningatlargescales.yml')

samples = results['desy3wl_nobaryons_nocuts_nla_nside1024_ourbinning']['MCSamples']
p = samples.getParams()
bf = samples.getParamSampleDict(np.argmin(np.abs(p.A_sE9 / 4 -1)))

def print_chi2(lmin, lmax, plot=False):
    ix = s1024.indices(ell__lt=lmax+1, ell__gt=lmin)
    
    m = results['desy3wl_nobaryons_nocuts_nla_nside1024']['model']
    st1024 = get_cl_theory_sacc(m, bf)
    d = s1024.mean[ix] - st1024.mean[ix]
    chi2 = d.dot(np.linalg.inv(s1024.covariance.covmat[ix][:, ix]).dot(d))
    print('DES binnig chi2 = ', chi2, 'for', d.size, 'points',
          'p-value = ', 1-scipy.stats.chi2.cdf(chi2, df=d.size - 2))
  
    ix = s4096.indices(ell__lt=lmax+1, ell__gt=lmin)
    m = results['desy3wl_nobaryons_nocuts_nla_nside1024_ourbinning']['model']
    st4096 = get_cl_theory_sacc(m, bf)
    d = s4096.mean[ix] - st4096.mean[ix]
    chi2 = d.dot(np.linalg.inv(s4096.covariance.covmat[ix][:, ix]).dot(d))
    print('Our binning case chi2 = ', chi2, 'for', d.size, 'points',
         'p-value = ', 1-scipy.stats.chi2.cdf(chi2, df=d.size - 2))
    
    # 1024 with our binning but lmin=8
    ix = s1024_lmin8.indices(ell__lt=lmax+1, ell__gt=lmin)
    st1024_lmin8 = get_cl_theory_sacc(m1024_lmin8, bf)
    d = s1024_lmin8.mean[ix] - st1024_lmin8.mean[ix]
    chi2 = d.dot(np.linalg.inv(s1024_lmin8.covariance.covmat[ix][:, ix]).dot(d))
    print('Our bining and lmin=8 chi2 = ', chi2, 'for', d.size, 'points',
         'p-value = ', 1-scipy.stats.chi2.cdf(chi2, df=d.size - 2))


    # 1024 with our binning at ell<500
    ix = s1024_ls.indices(ell__lt=lmax+1, ell__gt=lmin)
    st1024_ls = get_cl_theory_sacc(m1024_ls, bf)
    d = s1024_ls.mean[ix] - st1024_ls.mean[ix]
    chi2 = d.dot(np.linalg.inv(s1024_ls.covariance.covmat[ix][:, ix]).dot(d))
    print('Our bining at ell<500 chi2 = ', chi2, 'for', d.size, 'points',
         'p-value = ', 1-scipy.stats.chi2.cdf(chi2, df=d.size - 2))

    if not plot:
        return

    # E-modes
    f, ax = plt.subplots(4, 4, figsize=(14, 14), sharex='col', sharey='row', gridspec_kw={'hspace':0, 'wspace':0})
    for trs in s1024.get_tracer_combinations():
        i = int(trs[1].split('__')[1])
        j = int(trs[0].split('__')[1])

        # Data 1024
        ell, cl, cov, ind = s1024.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
        sel = (ell >= lmin) * (ell <=lmax)
        err = np.sqrt(np.diag(cov)[sel])
        ell = ell[sel]
        cl = cl[sel]
        ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='blue', label='DES binnig', alpha=0.8)
        ell, cl = st1024.get_ell_cl('cl_ee', trs[0], trs[1])
        ell = ell[sel]
        cl = cl[sel]
        ax[i, j].plot(ell, ell * cl * 1e7, color='cyan', label=fr'DES binning ($\ell_{{\rm max}} = {lmax}$)', alpha=0.8)

        # Data 4096
        ell, cl, cov, ind = s4096.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
        sel = (ell >= lmin) * (ell <=lmax)
        err = np.sqrt(np.diag(cov)[sel])
        ell = ell[sel]
        cl = cl[sel]
        ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='orange', label='Our binning', alpha=0.8)
        ell, cl = st4096.get_ell_cl('cl_ee', trs[0], trs[1])
        ell = ell[sel]
        cl = cl[sel]
        ax[i, j].plot(ell, ell * cl * 1e7, color='darkorange', label=fr'Our binning ($\ell_{{\rm max}} = {lmax}$)', alpha=0.8)

        # Data 1024, our binning with lmin=8
        ell, cl, cov, ind = s1024_lmin8.get_ell_cl('cl_ee', trs[0], trs[1], return_cov=True, return_ind=True)
        sel = (ell >= lmin) * (ell <=lmax)
        err = np.sqrt(np.diag(cov)[sel])
        ell = ell[sel]
        cl = cl[sel]
        ax[i, j].errorbar(ell, ell * cl * 1e7, yerr=ell*err * 1e7, fmt='.', color='red',
                          label=r'Our binning ($\ell_{\rm min} = 8$)', alpha=0.8)
        ell, cl = st1024_lmin8.get_ell_cl('cl_ee', trs[0], trs[1])
        ell = ell[sel]
        cl = cl[sel]
        ax[i, j].plot(ell, ell * cl * 1e7, color='darkred',
                      label=fr'Our binning ($\ell_{{\rm min}} = 8$, $\ell_{{\rm max}} = {lmax}$)', alpha=0.8)

    for i in range(4):
        ax[i, 0].set_ylabel("$\ell C_\ell (10^{-7})$")
        ax[-1, i].set_xlabel("$\ell$")
        ax[0, i].set_xscale("log")
        ax[0, 0].legend()

    f.suptitle('EE', fontsize=16)
    plt.tight_layout()
    plt.show()
    plt.close()

print('###### 0 < ell < 2048')
print_chi2(0, 2048, plot=False)

print('###### 0 < ell < 200')
print_chi2(0, 300, plot=False)

print('###### 20 < ell < 200')
print_chi2(20, 300, plot=False)

print('###### 40 < ell < 200')
print_chi2(40, 300, plot=False)

print('###### 200 < ell < 2048')
print_chi2(300, 2048, plot=False)

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# #### Contours

# %% hidden=true
# No-Baryons with lmax=2048 but with Cells computed with nside 1024 or 4096
chains = ['desy3wl_nobaryons_nocuts_nla_nside1024',
          'desy3wl_nobaryons_nocuts_nla_nside1024_lmin20',
          # 'desy3wl_nobaryons_nocuts_nla_nside1024_lmin30',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20',
         ]

parameters = ['S8', 'sigma8', 'Omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors[1] = 'blue'
labels = ['DES official binning', 'DES official binning ($\ell>20$)', 'Our binning', 'Our binnnig ($\ell > 20$)']

# Marker
samples = results['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048']['MCSamples']
p = samples.getParams()
bf = samples.getParamSampleDict(np.argmin(np.abs(p.A_sE9 / 4 -1)))


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1], markers=bf)

# %% hidden=true
# No-Baryons with lmax=2048 but with Cells computed with nside 1024 or 4096
chains = ['desy3wl_nobaryons_nocuts_nla_nside1024',
          'desy3wl_nobaryons_nocuts_nla_nside1024_lmin20',
          # 'desy3wl_nobaryons_nocuts_nla_nside1024_lmin30',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048',
         ]

parameters = ['S8', 'sigma8', 'Omega_m', 'A_sE9']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors[1] = 'blue'

# Marker
samples = results['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048']['MCSamples']
p = samples.getParams()
bf = samples.getParamSampleDict(np.argmin(np.abs(p.A_sE9 / 4 -1)))


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1], markers=bf)

# %% hidden=true
# No-Baryons with lmax=2048 but with Cells computed with nside 1024 or 4096
chains = ['desy3wl_nobaryons_nocuts_nla_nside1024_lmin20',
          'desy3wl_nobaryons_nocuts_nla_nside1024_lmin30',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20'
         ]

parameters = ['S8', 'sigma8', 'Omega_m', 'A_sE9']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors[-1] = 'blue'

# Marker
samples = results['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048']['MCSamples']
p = samples.getParams()
bf = samples.getParamSampleDict(np.argmin(np.abs(p.A_sE9 / 4 -1)))


g = plots.get_subplot_plotter(width_inch=8)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1, 1], markers=bf)

# %% hidden=true
# No-Baryons with lmax=2048 but with Cells computed with nside 1024 or 4096
chains = ['desy3wl_nobaryons_nocuts_nla_nside1024',
          'desy3wl_nobaryons_nocuts_nla_nside1024_ourbinning',
          'desy3wl_nobaryons_nocuts_nla_nside1024_lmax200',
          'desy3wl_nobaryons_nocuts_nla_nside1024_lmin200'
         ]

parameters = ['S8', 'sigma8', 'Omega_m', 'A_sE9']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors[1] = 'blue'

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# No-Baryons with lmax=2048 but with Cells computed with nside 1024 or 4096
chains = ['desy3wl_nobaryons_nocuts_nla_nside1024',
          'desy3wl_nobaryons_nocuts_nla_nside1024_ourbinning',
          'desy3wl_nobaryons_nocuts_nla_nside1024_ourbinningatsmallscales',
         ]

parameters = ['S8', 'sigma8', 'Omega_m', 'A_sE9']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]
colors[1] = 'blue'

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# No-Baryons with lmax=2048 but with Cells computed with nside 1024 or 4096
chains = ['desy3wl_nobaryons_nocuts_nla_nside1024',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_pixwin',
          'desy3wl_nobaryons_nocuts_nla_nside1024_ourbinning'
         ]

parameters = ['S8', 'sigma8', 'Omega_m', 'A_sE9', 'chi2__cl_like.ClLike']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]


# Marker
samples = results['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048']['MCSamples']
p = samples.getParams()
bf = samples.getParamSampleDict(np.argmin(np.abs(p.A_sE9 / 4 -1)))


g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1], markers=bf)

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['desy3wl_baryons_nocuts_nla_nside1024',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax2048'
         ]

parameters = ['S8', 'sigma8', 'Omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
# Baryons vs no Baryons with ell_max=2048
chains = ['desy3wl_nobaryons_nocuts_nla_nside1024',
          'desy3wl_baryons_nocuts_nla_nside1024',
          'desy3wl_baryons_nocuts_nla_nside4096_lmax2048',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048'
         ]

parameters = ['S8', 'sigma8', 'Omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## SSC

# %% hidden=true
import sys
sys.path.append('../data/')
from compute_SSC import SSC, get_masks_paths

def get_corr(covmat):
    d = np.diag(covmat)
    return covmat / np.sqrt(d[:, None] * d[None, :])

def log10_corr(covmat):
    return np.log10(np.abs(get_corr(covmat)))


# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### DESY3 no KiDS patch

# %% hidden=true
sGNG = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3_noK1000_2x2pt_4096/cls_cov_GNG_fsky.fits')
sG = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3_noK1000_2x2pt_4096/cls_cov.fits')
cov = sGNG.covariance.covmat - sG.covariance.covmat
ix = sG.indices(data_type='cl_ee')

plt.imshow(log10_corr(cov[ix][:, ix]))
plt.colorbar()
plt.title('DESY3 no KiDS1000 patch')
plt.show()
plt.close()

ell, cl, ix = sG.get_ell_cl(data_type='cl_ee', tracer1='DESY3__2', tracer2='DESY3__2', return_ind=True)
plt.plot(ell, np.diag(sGNG.covariance.covmat)[ix] / np.diag(sG.covariance.covmat)[ix] -1, label='Diagonal')
plt.plot(ell, np.diag(sGNG.covariance.covmat, 1)[ix] / np.diag(sG.covariance.covmat, 1)[ix] -1, label='Off-diagonal 1')
plt.plot(ell, np.diag(sGNG.covariance.covmat, 2)[ix] / np.diag(sG.covariance.covmat, 2)[ix] -1, label='Off-diagonal 2')

plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()


plt.loglog(ell, np.diag(sGNG.covariance.covmat)[ix], label='Diagonal')
plt.loglog(ell, np.diag(sG.covariance.covmat)[ix], ls='--')

plt.loglog(ell, np.diag(sGNG.covariance.covmat, 1)[ix], label='Off-diagonal 1')
plt.loglog(ell, np.diag(sG.covariance.covmat, 1)[ix], ls='--')

plt.loglog(ell, np.diag(sGNG.covariance.covmat, 2)[ix], label='Off-diagonal 2')
plt.loglog(ell, np.diag(sG.covariance.covmat, 2)[ix], ls='--')
         
plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()

# %% hidden=true
# ssc = SSC('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3_noK1000_2x2pt_4096/cls_cov.fits',
#           get_masks_paths(True))
# tr1 = tr2 = tr3 = tr4 = 'DESY3__0'
# wl = ssc.get_masks_wl(tr1, tr2, tr3, tr4)
plt.loglog(np.arange(wl.size), wl, label='DES no KiDS')

#####
ssc = SSC('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov.fits',
          get_masks_paths(False))
tr1 = tr2 = tr3 = tr4 = 'DESY3__0'
wl = ssc.get_masks_wl(tr1, tr2, tr3, tr4)
plt.loglog(np.arange(wl.size), wl, label='DES')

#####
ssc = SSC('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov.fits', 
         get_masks_paths(False))
tr1 = tr2 = tr3 = tr4 = 'KiDS1000__0'
wl = ssc.get_masks_wl(tr1, tr2, tr3, tr4)
plt.loglog(np.arange(wl.size), wl, label='KiDS')


plt.xlabel('$\ell$')
plt.ylabel('$w_\ell$')
plt.legend()
plt.show()
plt.close()

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### DESY3

# %% hidden=true
key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmin20'
if key not in results:
    results[key] = load_chain(f'./chains/{key}/{key}',
                              r'DESY3 with No-Baryons ($20 < \ell < 8192$)', 'orange', 0.15)

key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNG'
if key not in results:
    results[key] = load_chain(f'./chains/{key}/{key}',
                              r'DESY3 with No-Baryons ($20 < \ell < 8192$) GNG', 'orange', 0.15)

key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNGfsky'
if key not in results:
    results[key] = load_chain(f'./chains/{key}/{key}',
                              r'DESY3 with No-Baryons ($20 < \ell < 8192$) GNGfsky', 'orange', 0.15)
    
# Baryons vs no Baryons with ell_max=2048
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmin20',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNG',
          # 'desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNGfsky'
         ]

parameters = ['S8', 'sigma8', 'Omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = None # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
tr1 = tr2 = tr3 = tr4 = 'DESY3__0'

ssc = SSC('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov.fits',
          get_masks_paths(False))
#####
a, sigma2_b = ssc.get_sigma2_B(tr1, tr2, tr3, tr4)
plt.loglog(a, sigma2_b, label='DES (mask)')

#####
ssc.fsky_approx = True
ssc.sigma2_B = {}
a, sigma2_b = ssc.get_sigma2_B(tr1, tr2, tr3, tr4)
plt.loglog(a, sigma2_b, label='DES (disk)')

###
plt.xlabel('a')
plt.ylabel('$\sigma^2_b$')
plt.legend()
plt.show()
plt.close()

# %% hidden=true
tr1 = tr2 = tr3 = tr4 = 'DESY3__0'

ssc = SSC('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov.fits',
          get_masks_paths(False))
#####
a, sigma2_b = ssc.get_sigma2_B(tr1, tr2, tr3, tr4)
plt.loglog(a, sigma2_b, label='DES (mask)')

#####
ssc.fsky_approx = True
ssc.sigma2_B = {}
a, sigma2_b = ssc.get_sigma2_B(tr1, tr2, tr3, tr4)
plt.loglog(a, sigma2_b, label='DES (disk)')

####
tr1 = tr2 = tr3 = tr4 = 'KiDS1000__0'

ssc = SSC('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov.fits',
          get_masks_paths(False))
#####
a, sigma2_b = ssc.get_sigma2_B(tr1, tr2, tr3, tr4)
plt.loglog(a, sigma2_b, label='KiDS (mask)')

#####
ssc.fsky_approx = True
ssc.sigma2_B = {}
a, sigma2_b = ssc.get_sigma2_B(tr1, tr2, tr3, tr4)
plt.loglog(a, sigma2_b, label='KiDS (disk)')


###
plt.xlabel('a')
plt.ylabel('$\sigma^2_b$')
plt.legend()
plt.show()
plt.close()

# %% hidden=true
sG = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov.fits')
sGNG = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov_GNG.fits')
sGNGf = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov_GNG_fsky.fits')

# %% hidden=true
# Baryons (4096) lmax=2048, lmin=20
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20'
if key not in results:
    results[key] = load_chain(f'./chains/{key}/{key}',
                              r'DESY3 with Baryons ($20 < \ell < 2048$)', 'orange', 0.15)

lkl1024 = results['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20']['model'].likelihood['cl_like.ClLike']
s = lkl1024.get_cl_data_sacc()

m = results['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20']['model']
bf = results['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20']['pars_bf']
cl1024 = get_cl_theory_sacc(m, bf)

print('######## lmax=2048 #######')
print('chi2(BF with Gaussian) =', results['desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20']['chi2_bf'])
d = cl1024.mean - s.mean
ix = lkl1024.indices
icovG = np.linalg.inv(lkl1024.sacc_file.covariance.covmat[ix][:, ix])
# icovG = np.linalg.inv(sG.covariance.covmat[ix][:, ix])
icovGNG = np.linalg.inv(sGNG.covariance.covmat[ix][:, ix])
icovGNGf = np.linalg.inv(sGNGf.covariance.covmat[ix][:, ix])
print('chi2(BF with Gaussian) =', d.dot(icovG).dot(d)) 
print('chi2(BF with Gaussian + SSC) =', d.dot(icovGNG).dot(d)) 
print('chi2(BF with Gaussian + SSC (fsky)) =', d.dot(icovGNGf).dot(d)) 

# %% hidden=true
# Baryons (4096) lmax=8192, lmin=20
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmin20'
if key not in results:
    results[key] = load_chain(f'./chains/{key}/{key}',
                              r'DESY3 with Baryons ($20 < \ell < 8192$)', 'orange', 0.15)

lkl1024 = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20']['model'].likelihood['cl_like.ClLike']
s = lkl1024.get_cl_data_sacc()

m = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20']['model']
bf = results['desy3wl_baryons_nocuts_nla_nside4096_lmin20']['pars_bf']
cl1024 = get_cl_theory_sacc(m, bf)

print('######## lmax=8192 #######')
print('chi2(BF with Gaussian) =', results['desy3wl_baryons_nocuts_nla_nside4096_lmin20']['chi2_bf'])
d = cl1024.mean - s.mean
ix = lkl1024.indices
icovG = np.linalg.inv(lkl1024.sacc_file.covariance.covmat[ix][:, ix])
# icovG = np.linalg.inv(sG.covariance.covmat[ix][:, ix])
icovGNG = np.linalg.inv(sGNG.covariance.covmat[ix][:, ix])
icovGNGf = np.linalg.inv(sGNGf.covariance.covmat[ix][:, ix])
print('chi2(BF with Gaussian) =', d.dot(icovG).dot(d)) 
print('chi2(BF with Gaussian + SSC) =', d.dot(icovGNG).dot(d)) 
print('chi2(BF with Gaussian + SSC (fsky)) =', d.dot(icovGNGf).dot(d)) 

# %% hidden=true
sGNG = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov_GNG_fsky.fits')
sG = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov.fits')
cov = sGNG.covariance.covmat - sG.covariance.covmat
ix = sG.indices(data_type='cl_ee')
plt.imshow(log10_corr(cov[ix][:, ix]))
plt.colorbar()
plt.title('DESY3')
plt.show()
plt.close()

ell, cl, ix = sG.get_ell_cl(data_type='cl_ee', tracer1='DESY3__2', tracer2='DESY3__2', return_ind=True)
plt.plot(ell, np.diag(sGNG.covariance.covmat)[ix] / np.diag(sG.covariance.covmat)[ix] -1, label='Diagonal')

plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()

ell, cl, ix = sG.get_ell_cl(data_type='cl_ee', tracer1='DESY3__2', tracer2='DESY3__2', return_ind=True)
plt.plot(ell, np.diag(sGNG.covariance.covmat)[ix] / np.diag(sG.covariance.covmat)[ix] -1, label='Diagonal')
plt.plot(ell, np.diag(sGNG.covariance.covmat, 1)[ix] / np.diag(sG.covariance.covmat, 1)[ix] -1, label='Off-diagonal 1')
plt.plot(ell, np.diag(sGNG.covariance.covmat, 2)[ix] / np.diag(sG.covariance.covmat, 2)[ix] -1, label='Off-diagonal 2')

plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()

plt.loglog(ell, np.diag(sGNG.covariance.covmat)[ix], label='Diagonal')
plt.loglog(ell, np.diag(sG.covariance.covmat)[ix], ls='--')
plt.loglog(ell, np.diag(sGNG.covariance.covmat, 1)[ix], label='Off-diagonal 1')
plt.loglog(ell, np.diag(sG.covariance.covmat, 1)[ix], ls='--')
plt.loglog(ell, np.diag(sGNG.covariance.covmat, 2)[ix], label='Off-diagonal 2')
plt.loglog(ell, np.diag(sG.covariance.covmat, 2)[ix], ls='--')
         
plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()

# %% hidden=true
sGNG = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov_GNG.fits')
# sG = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/DESY3__2x2pt_4096/cls_cov.fits')
cov = sGNG.covariance.covmat - sG.covariance.covmat
ix = sG.indices(data_type='cl_ee')
plt.imshow(log10_corr(cov[ix][:, ix]))
plt.colorbar()
plt.title('DESY3')
plt.show()
plt.close()

ell, cl, ix = sG.get_ell_cl(data_type='cl_ee', tracer1='DESY3__2', tracer2='DESY3__2', return_ind=True)
plt.plot(ell, np.diag(sGNG.covariance.covmat)[ix] / np.diag(sG.covariance.covmat)[ix] -1, label='Diagonal')

plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()

ell, cl, ix = sG.get_ell_cl(data_type='cl_ee', tracer1='DESY3__2', tracer2='DESY3__2', return_ind=True)
plt.plot(ell, np.diag(sGNG.covariance.covmat)[ix] / np.diag(sG.covariance.covmat)[ix] -1, label='Diagonal')
plt.plot(ell, np.diag(sGNG.covariance.covmat, 1)[ix] / np.diag(sG.covariance.covmat, 1)[ix] -1, label='Off-diagonal 1')
plt.plot(ell, np.diag(sGNG.covariance.covmat, 2)[ix] / np.diag(sG.covariance.covmat, 2)[ix] -1, label='Off-diagonal 2')

plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()

plt.loglog(ell, np.diag(sGNG.covariance.covmat)[ix], label='Diagonal')
plt.loglog(ell, np.diag(sG.covariance.covmat)[ix], ls='--')
plt.loglog(ell, np.diag(sGNG.covariance.covmat, 1)[ix], label='Off-diagonal 1')
plt.loglog(ell, np.diag(sG.covariance.covmat, 1)[ix], ls='--')
plt.loglog(ell, np.diag(sGNG.covariance.covmat, 2)[ix], label='Off-diagonal 2')
plt.loglog(ell, np.diag(sG.covariance.covmat, 2)[ix], ls='--')
         
plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()

# %% [markdown] hidden=true jp-MarkdownHeadingCollapsed=true
# ### KiDS1000

# %% hidden=true
key = 'k1000_nobaryons_nocuts_nla_nside4096_lmin100'
if key not in results:
    results[key] = load_chain(f'./chains/{key}/{key}',
                              r'KiDS1000 with No-Baryons ($100 < \ell < 8192$)', 'orange', 0.15)

key = 'k1000_nobaryons_nocuts_nla_nside4096_lmin100_GNG'
if key not in results:
    results[key] = load_chain(f'./chains/{key}/{key}',
                              r'KiDS1000 with No-Baryons ($100 < \ell < 8192$) GNG', 'orange', 0.15)

key = 'k1000_nobaryons_nocuts_nla_nside4096_lmin100_GNGfsky'
if key not in results:
    results[key] = load_chain(f'./chains/{key}/{key}',
                              r'KiDS1000 with No-Baryons ($100 < \ell < 8192$) GNGfsky', 'orange', 0.15)
    
# Baryons vs no Baryons with ell_max=2048
chains = ['k1000_nobaryons_nocuts_nla_nside4096_lmin100',
          'k1000_nobaryons_nocuts_nla_nside4096_lmin100_GNG',
          'k1000_nobaryons_nocuts_nla_nside4096_lmin100_GNGfsky'
         ]

parameters = ['S8', 'sigma8', 'Omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = None # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
sG = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov.fits')
sGNG = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov_GNG.fits')
sGNGf = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov_GNG_fsky.fits')

# %% hidden=true
# Baryons (4096) lmax=2048, lmin=20
key = 'k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100'
if key not in results:
    results[key] = load_chain(f'./chains/{key}/{key}',
                              r'KiDS1000 with Baryons ($100 < \ell < 2048$)', 'orange', 0.15)

lkl1024 = results['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100']['model'].likelihood['cl_like.ClLike']
s = lkl1024.get_cl_data_sacc()

m = results['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100']['model']
bf = results['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100']['pars_bf']
cl1024 = get_cl_theory_sacc(m, bf)

print('######## lmax=2048 #######')
print('chi2(BF with Gaussian) =', results['k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100']['chi2_bf'])
d = cl1024.mean - s.mean
ix = lkl1024.indices
icovG = np.linalg.inv(lkl1024.sacc_file.covariance.covmat[ix][:, ix])
# icovG = np.linalg.inv(sG.covariance.covmat[ix][:, ix])
icovGNG = np.linalg.inv(sGNG.covariance.covmat[ix][:, ix])
icovGNGf = np.linalg.inv(sGNGf.covariance.covmat[ix][:, ix])
print('chi2(BF with Gaussian) =', d.dot(icovG).dot(d)) 
print('chi2(BF with Gaussian + SSC) =', d.dot(icovGNG).dot(d)) 
print('chi2(BF with Gaussian + SSC (fsky)) =', d.dot(icovGNGf).dot(d)) 

# %% hidden=true
# Baryons (4096) lmax=8192, lmin=20
key = 'k1000_baryons_nocuts_nla_nside4096_lmin100'
if key not in results:
    results[key] = load_chain(f'./chains/{key}/{key}',
                              r'KiDS1000 with Baryons ($100 < \ell < 8192$)', 'orange', 0.15)

lkl1024 = results['k1000_baryons_nocuts_nla_nside4096_lmin100']['model'].likelihood['cl_like.ClLike']
s = lkl1024.get_cl_data_sacc()

m = results['k1000_baryons_nocuts_nla_nside4096_lmin100']['model']
bf = results['k1000_baryons_nocuts_nla_nside4096_lmin100']['pars_bf']
cl1024 = get_cl_theory_sacc(m, bf)

print('######## lmax=8192 #######')
print('chi2(BF with Gaussian) =', results['k1000_baryons_nocuts_nla_nside4096_lmin100']['chi2_bf'])
d = cl1024.mean - s.mean
ix = lkl1024.indices
icovG = np.linalg.inv(lkl1024.sacc_file.covariance.covmat[ix][:, ix])
# icovG = np.linalg.inv(sG.covariance.covmat[ix][:, ix])
icovGNG = np.linalg.inv(sGNG.covariance.covmat[ix][:, ix])
icovGNGf = np.linalg.inv(sGNGf.covariance.covmat[ix][:, ix])
print('chi2(BF with Gaussian) =', d.dot(icovG).dot(d)) 
print('chi2(BF with Gaussian + SSC) =', d.dot(icovGNG).dot(d)) 
print('chi2(BF with Gaussian + SSC (fsky)) =', d.dot(icovGNGf).dot(d)) 

# %% hidden=true
sGNG = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov_GNG_fsky.fits')
sG = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov.fits')
cov = sGNG.covariance.covmat - sG.covariance.covmat
ix = sG.indices(data_type='cl_ee')
plt.imshow(log10_corr(cov[ix][:, ix]))
plt.colorbar()
plt.title('KiDS1000')
plt.show()
plt.close()

ell, cl, ix = sG.get_ell_cl(data_type='cl_ee', tracer1='KiDS1000__1', tracer2='KiDS1000__1', return_ind=True)
plt.plot(ell, np.diag(sGNG.covariance.covmat)[ix] / np.diag(sG.covariance.covmat)[ix] -1, label='Diagonal')
plt.plot(ell, np.diag(sGNG.covariance.covmat, 1)[ix] / np.diag(sG.covariance.covmat, 1)[ix] -1, label='Off-diagonal 1')
plt.plot(ell, np.diag(sGNG.covariance.covmat, 2)[ix] / np.diag(sG.covariance.covmat, 2)[ix] -1, label='Off-diagonal 2')

plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()

plt.loglog(ell, np.diag(sGNG.covariance.covmat)[ix], label='Diagonal')
plt.loglog(ell, np.diag(sG.covariance.covmat)[ix], ls='--')

plt.loglog(ell, np.diag(sGNG.covariance.covmat, 1)[ix], label='Off-diagonal 1')
plt.loglog(ell, np.diag(sG.covariance.covmat, 1)[ix], ls='--')

plt.loglog(ell, np.diag(sGNG.covariance.covmat, 2)[ix], label='Off-diagonal 2')
plt.loglog(ell, np.diag(sG.covariance.covmat, 2)[ix], ls='--')
         
plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()

# %% hidden=true
sGNG = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov_GNG.fits')
sG = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/xCell_run1_shear_baryons/KiDS1000__2x2pt_4096/cls_cov.fits')
cov = sGNG.covariance.covmat - sG.covariance.covmat
ix = sG.indices(data_type='cl_ee')
plt.imshow(log10_corr(cov[ix][:, ix]))
plt.colorbar()
plt.title('KiDS1000')
plt.show()
plt.close()

ell, cl, ix = sG.get_ell_cl(data_type='cl_ee', tracer1='KiDS1000__1', tracer2='KiDS1000__1', return_ind=True)
plt.plot(ell, np.diag(sGNG.covariance.covmat)[ix] / np.diag(sG.covariance.covmat)[ix] -1, label='Diagonal')
plt.plot(ell, np.diag(sGNG.covariance.covmat, 1)[ix] / np.diag(sG.covariance.covmat, 1)[ix] -1, label='Off-diagonal 1')
plt.plot(ell, np.diag(sGNG.covariance.covmat, 2)[ix] / np.diag(sG.covariance.covmat, 2)[ix] -1, label='Off-diagonal 2')

plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()

plt.loglog(ell, np.diag(sGNG.covariance.covmat)[ix], label='Diagonal')
plt.loglog(ell, np.diag(sG.covariance.covmat)[ix], ls='--')

plt.loglog(ell, np.diag(sGNG.covariance.covmat, 1)[ix], label='Off-diagonal 1')
plt.loglog(ell, np.diag(sG.covariance.covmat, 1)[ix], ls='--')

plt.loglog(ell, np.diag(sGNG.covariance.covmat, 2)[ix], label='Off-diagonal 2')
plt.loglog(ell, np.diag(sG.covariance.covmat, 2)[ix], ls='--')
         
plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()

# %% [markdown] hidden=true
# ### HSC

# %% hidden=true
key = 'hsc_nobaryons_nocuts_nla_nside4096_lmin300'
if key not in results:
    results[key] = load_chain(f'./chains/{key}/{key}',
                              r'HSCDR1wl with No-Baryons ($300 < \ell < 8192$)', 'orange', 0.15)

key = 'hsc_nobaryons_nocuts_nla_nside4096_lmin300_GNG'
if key not in results:
    results[key] = load_chain(f'./chains/{key}/{key}',
                              r'HSCDR1wl with No-Baryons ($100 < \ell < 8192$) GNG', 'orange', 0.15)

key = 'hsc_nobaryons_nocuts_nla_nside4096_lmin300_GNGfsky'
if key not in results:
    results[key] = load_chain(f'./chains/{key}/{key}',
                              r'HSCDR1wl with No-Baryons ($100 < \ell < 8192$) GNGfsky', 'orange', 0.15)
    
# Baryons vs no Baryons with ell_max=2048
chains = ['hsc_nobaryons_nocuts_nla_nside4096_lmin300',
          'hsc_nobaryons_nocuts_nla_nside4096_lmin300_GNG',
          # 'hsc_nobaryons_nocuts_nla_nside4096_lmin300_GNGfsky'
         ]

parameters = ['S8', 'sigma8', 'Omega_m']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = None # [results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% hidden=true
sG = sAG = sacc.Sacc.load_fits('/mnt/extraspace/gravityls_3/sacc_files/cls_signal_covG_HSC_Andrina_paper_newNz.fits')
sAGNG = sacc.Sacc.load_fits('/mnt/extraspace/gravityls_3/sacc_files/cls_signal_covGNG_HSC_Andrina_paper_newNz.fits')
sGNG = sacc.Sacc.load_fits('../data/cls_signal_covG_HSC_Andrina_paper_newNz_GNG.fits')
sGNGf = sacc.Sacc.load_fits('../data/cls_signal_covG_HSC_Andrina_paper_newNz_GNG_fsky.fits')

# %% hidden=true
# Baryons (4096) lmax=2048, lmin=20
key = 'hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300'
if key not in results:
    results[key] = load_chain(f'./chains/{key}/{key}',
                              r'HSCDR1wl with Baryons ($100 < \ell < 2048$)', 'orange', 0.15)

lkl1024 = results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['model'].likelihood['cl_like.ClLike']
s = lkl1024.get_cl_data_sacc()

m = results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['model']
bf = results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['pars_bf']
cl1024 = get_cl_theory_sacc(m, bf)

print('######## lmax=2048 #######')
print('chi2(BF with Gaussian + SSC (Andrina)) =', results['hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300']['chi2_bf'])
d = cl1024.mean - s.mean
ix = lkl1024.indices
icovAGNG = np.linalg.inv(lkl1024.sacc_file.covariance.covmat[ix][:, ix])
icovG = np.linalg.inv(sG.covariance.covmat[ix][:, ix])
icovGNG = np.linalg.inv(sGNG.covariance.covmat[ix][:, ix])
icovGNGf = np.linalg.inv(sGNGf.covariance.covmat[ix][:, ix])
print('chi2(BF with Gaussian) =', d.dot(icovG).dot(d)) 
print('chi2(BF with Gaussian + SSC (Andrina)) =', d.dot(icovAGNG).dot(d)) 
print('chi2(BF with Gaussian + SSC) =', d.dot(icovGNG).dot(d)) 
print('chi2(BF with Gaussian + SSC (fsky)) =', d.dot(icovGNGf).dot(d)) 

# %% hidden=true
# Baryons (4096) lmax=8192, lmin=20
key = 'hsc_baryons_nocuts_nla_nside4096_lmin300'
if key not in results:
    results[key] = load_chain(f'./chains/{key}/{key}',
                              r'HSCDR1wl with Baryons ($100 < \ell < 8192$)', 'orange', 0.15)

lkl1024 = results['hsc_baryons_nocuts_nla_nside4096_lmin300']['model'].likelihood['cl_like.ClLike']
s = lkl1024.get_cl_data_sacc()

m = results['hsc_baryons_nocuts_nla_nside4096_lmin300']['model']
bf = results['hsc_baryons_nocuts_nla_nside4096_lmin300']['pars_bf']
cl1024 = get_cl_theory_sacc(m, bf)

print('######## lmax=8192 #######')
print('chi2(BF with Gaussian + SSC (Andrina)) =', results['hsc_baryons_nocuts_nla_nside4096_lmin300']['chi2_bf'])
d = cl1024.mean - s.mean
ix = lkl1024.indices
icovAGNG = np.linalg.inv(lkl1024.sacc_file.covariance.covmat[ix][:, ix])
icovG = np.linalg.inv(sG.covariance.covmat[ix][:, ix])
icovGNG = np.linalg.inv(sGNG.covariance.covmat[ix][:, ix])
icovGNGf = np.linalg.inv(sGNGf.covariance.covmat[ix][:, ix])
print('chi2(BF with Gaussian) =', d.dot(icovG).dot(d)) 
print('chi2(BF with Gaussian + SSC (Andrina)) =', d.dot(icovAGNG).dot(d)) 
print('chi2(BF with Gaussian + SSC) =', d.dot(icovGNG).dot(d)) 
print('chi2(BF with Gaussian + SSC (fsky)) =', d.dot(icovGNGf).dot(d))  

# %% hidden=true
# SSC computed with with fsky patch
sAG = sG
cov = sGNGf.covariance.covmat - sAG.covariance.covmat
covA = sAGNG.covariance.covmat - sAG.covariance.covmat

ix = sG.indices(data_type='cl_ee')
plt.imshow(log10_corr(cov[ix][:, ix]))
plt.colorbar()
plt.title('HSC (mine)')
plt.show()
plt.close()

ix = sAG.indices(data_type='cl_ee')
plt.imshow(log10_corr(covA[ix][:, ix]))
plt.colorbar()
plt.title('HSC (Andrina)')
plt.show()
plt.close()


ix = sG.indices(data_type='cl_ee', tracers=('wl_2', 'wl_2'))
plt.imshow(log10_corr(cov[ix][:, ix]))
plt.colorbar()
plt.title('HSC zbin2 (mine)')
plt.show()
plt.close()

ell, cl, ix = sG.get_ell_cl(data_type='cl_ee', tracer1='wl_2', tracer2='wl_2', return_ind=True)
plt.plot(ell, np.diag(sGNG.covariance.covmat)[ix] / np.diag(sG.covariance.covmat)[ix] -1, label='Diagonal (Mine)')

ell, cl, ix = sAG.get_ell_cl(data_type='cl_ee', tracer1='wl_2', tracer2='wl_2', return_ind=True)
plt.plot(ell, np.diag(sAGNG.covariance.covmat)[ix] / np.diag(sAG.covariance.covmat)[ix] -1, label='Diagonal (Andrina)')

plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()

ell, cl, ix = sG.get_ell_cl(data_type='cl_ee', tracer1='HSCDR1wl__2', tracer2='HSCDR1wl__2', return_ind=True)
plt.plot(ell, np.diag(sGNG.covariance.covmat)[ix] / np.diag(sG.covariance.covmat)[ix] -1, label='Diagonal')
plt.plot(ell, np.diag(sGNG.covariance.covmat, 1)[ix] / np.diag(sG.covariance.covmat, 1)[ix] -1, label='Off-diagonal 1')
plt.plot(ell, np.diag(sGNG.covariance.covmat, 2)[ix] / np.diag(sG.covariance.covmat, 2)[ix] -1, label='Off-diagonal 2')

ell, cl, ix = sAG.get_ell_cl(data_type='cl_ee', tracer1='wl_2', tracer2='wl_2', return_ind=True)
plt.plot(ell, np.diag(sAGNG.covariance.covmat)[ix] / np.diag(sAG.covariance.covmat)[ix] -1, label='Diagonal (Andrina)')
plt.plot(ell, np.diag(sAGNG.covariance.covmat, 1)[ix] / np.diag(sAG.covariance.covmat, 1)[ix] -1, label='Off-diagonal 1 (Andrina)')
plt.plot(ell, np.diag(sAGNG.covariance.covmat, 2)[ix] / np.diag(sAG.covariance.covmat, 2)[ix] -1, label='Off-diagonal 2 (Andrina)')

plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()

plt.loglog(ell, np.diag(sGNG.covariance.covmat)[ix], label='Diagonal')
plt.loglog(ell, np.diag(sG.covariance.covmat)[ix], ls='--')
plt.loglog(ell, np.diag(sGNG.covariance.covmat, 1)[ix], label='Off-diagonal 1')
plt.loglog(ell, np.diag(sG.covariance.covmat, 1)[ix], ls='--')
plt.loglog(ell, np.diag(sGNG.covariance.covmat, 2)[ix], label='Off-diagonal 2')
plt.loglog(ell, np.diag(sG.covariance.covmat, 2)[ix], ls='--')
         
plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()

plt.loglog(ell, np.diag(sAGNG.covariance.covmat)[ix], label='Diagonal (Andrina)')
plt.loglog(ell, np.diag(sAG.covariance.covmat)[ix], ls='--')
plt.loglog(ell, np.diag(sAGNG.covariance.covmat, 1)[ix], label='Off-diagonal 1 (Andrina)')
plt.loglog(ell, np.diag(sAG.covariance.covmat, 1)[ix], ls='--')
plt.loglog(ell, np.diag(sAGNG.covariance.covmat, 2)[ix], label='Off-diagonal 2 (Andrina)')
plt.loglog(ell, np.diag(sAG.covariance.covmat, 2)[ix], ls='--')
         
plt.ylabel('(GC+SSC)/GC (zbin2)')
plt.xlabel('$\ell$')
plt.legend()
plt.show()
plt.close()

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## R-1 = 0.01 vs 0.03

# %% hidden=true
# No-Baryons (4096) lmax=2048, lmin=20, R-1=0.03 (instead of 0.01 as in the others)
key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_Rminus1003'
results[f"{key}"] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with No-Baryons ($20 < \ell < 2048$) $R-1=0.03$', 'orange', 0.15,
                                       Cobaya=True) # don't compute the model because we have changed the lkl
print(key, f"R-1 = {results[key]['R-1']}")

# %% hidden=true
# Effect of "bad_neutrinos" in the case with Baryons and lmax=2048
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_Rminus1003']

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['black', 'blue'] #[results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## 4chains vs 2chains

# %% hidden=true
# No-Baryons (4096) lmax=2048, lmin=20, R-1=0.03 (instead of 0.01 as in the others)
key = 'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_2chains'
results[f"{key}"] = load_chain(f'./chains/{key}/{key}',
                           r'DESY3 with No-Baryons ($20 < \ell < 2048$) 2chains', 'orange', 0.15,
                                       Cobaya=False) # don't compute the model because we have changed the lkl
print(key, f"R-1 = {results[key]['R-1']}")

# %% hidden=true
# Effect of "bad_neutrinos" in the case with Baryons and lmax=2048
chains = ['desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20',
          'desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_2chains']

parameters = ['Omega_m', 'S8', 'sigma8']

MCSamples = [results[i]['MCSamples'] for i in chains]
labels = [results[i]['label'] for i in chains]
colors = ['black', 'blue'] #[results[i]['color'] for i in chains]

g = plots.get_subplot_plotter(width_inch=6)
g.triangle_plot(MCSamples, parameters, filled=[True, False, False, False],
                contour_colors=colors, legend_labels=labels,
                contour_lws=[None, 1, 1, 1])

# %% [markdown] heading_collapsed=true jp-MarkdownHeadingCollapsed=true
# ## Compute baryon boost live

# %% hidden=true
from abc import ABC, abstractmethod

class ComputeBaryonBoost(ABC):
    def __init__(self, MCSamples, k_arr=np.logspace(-1, 1, 10), use_samples=1e4):
        self.k_arr = k_arr
        self.boost = None
        self.post1D = None
        self.post_interp = None
        
        # For the MCSamples
        self.MCSamples = MCSamples
        self.MCSamples_baryons = None
        self.use_samples = int(use_samples)
        self.added_boost_to_MCSamples = False
        self._names =  [f'Sk{ki:.2f}'.replace('.', 'p') for ki in k_arr]
        self._labels = [f'S(k={ki:.2f})' for ki in k_arr]
        
    def get_k_arr(self):
        return self.k_arr
    
    def get_names(self):
        return self._names
    
    def get_labels(self):
        return self._labels
        
    @abstractmethod
    def get_baryon_boost_from_pars(self, pars, k_arr):
        """
        Returns: k_arr, boost
        """
        raise NotImplementedError()
        
    def get_baryon_boost(self):
        if self.boost is None:
            pnames = self.MCSamples.getParamNames().list()
            totalsamples = self.MCSamples.samples.shape[0]
            nsamples = self.MCSamples.samples[-self.use_samples:].shape[0]
            boost = np.zeros((nsamples,
                              self.k_arr.size))
            
            c = 0
            pars_tmp = {}
            for i, pars in enumerate(self.MCSamples.samples[-self.use_samples:]):
                if (i == 0) or ((i+1) % int(nsamples / 20) == 0):
                    print(f"Step {i+1}/{nsamples}")
                pars = dict(zip(pnames, pars))
                
                for pn in pnames:
                    if ('limber' in pn) or ('bias' in pn) or ('prior' in pn) or ('chi2' in pn):
                        del pars[pn]

                if pars == pars_tmp:
                    boost[i] = boost[i - 1]
                    continue
                
                boost[i] = self.get_baryon_boost_from_pars(pars, self.k_arr)

                pars_tmp = pars.copy()
            self.boost = boost
        return self.k_arr, self.boost
    
    def get_MCSamples_baryons(self):
        if self.MCSamples_baryons is None:
            k_arr, boost = self.get_baryon_boost()
            weights = self.MCSamples.weights[-self.use_samples:]
            # for i in range(k_arr.size):
            #     self.MCSamples.addDerived(boost[i],
            #                               names=self.names[i],
            #                               labels=self.labels[i])
            self.MCSamples_baryons = getdist.MCSamples(samples=np.array(boost),
                                                       weights=weights,
                                                       names=self._names,
                                                       labels=self._labels)
        return self.MCSamples_baryons
    
    def _transaleInlineLatex(self, InlineLatex):
        if ' = ' in InlineLatex:
            rhs = InlineLatex.split(' = ')[-1]
            if '\pm' in rhs:
                mean, errors = rhs.split('\pm')
                up = down = float(errors)
            else:
                mean, errors = rhs.split('^{')
                up, down = errors.split('}_{-')
                up = float(up)
                down = float(down[:-1])
            mean = float(mean)
        elif '<' in InlineLatex:
            lhs, rhs = InlineLatex.split('<')
            mean = float(rhs) / 2.
            up = down = mean
        elif '>' in InlineLatex:
            lhs, rhs = InlineLatex.split('>')
            mean = (float(rhs) + 1.) / 2
            down = mean - float(rhs) 
            up = 1 - mean
        
        # Return the upper and lower bounds instead of the errors
        return mean, up, down
    
    def get_baryon_post1D(self):
        if self.post1D is None:
            samples = self.get_MCSamples_baryons()
            self.post1D = [self._transaleInlineLatex(samples.getInlineLatex(namei)) 
                           for namei in self.get_names()]
            self.post1D = np.array(self.post1D)
        return self.post1D
    
    def get_baryon_post_interp(self, kind='quadratic'):
        if self.post_interp is None:
            mean, up, down = self.get_baryon_post1D().T
            post_interp = interp1d(self.k_arr, mean, kind=kind)
            post_interp_p1sigma = interp1d(self.k_arr, mean + up, kind=kind)
            post_interp_m1sigma = interp1d(self.k_arr, mean - down, kind=kind)
        
            self.post_interp = (post_interp, post_interp_p1sigma, post_interp_m1sigma)
        return self.post_interp
    
    def get_baryon_post_at_k(self, k):
        mean, up, down = self.get_baryon_post_interp(kind)
        return mean(k), up(k), down(k)
    
    def plot_baryon_post(self, ax, label, color, kind='quadratic', k_fine=None):
        if k_fine is None:
            k_fine = np.logspace(-1, np.log10(self.k_arr.max()), 100)
        mean, up, down = self.get_baryon_post_interp(kind)
        ax.semilogx(k_fine, mean(k_fine), label=label, color=color)
        ax.fill_between(k_fine, up(k_fine), down(k_fine),
                        alpha=.5, color=color)
####################################

class QuickCosmo(dict):
    def sigma8(self):
        return self['sigma8']

class BaryonBoostBacco(ComputeBaryonBoost):
    # Baryon boost
    from cl_like.bacco import BaccoCalculator

    # min k 3D power spectra
    l10k_min_pks: float = -4.0
    # max k 3D power spectra
    l10k_max_pks: float = 2.0
    # #k for 3D power spectra
    nk_per_dex_pks: int = 25

    nk_pks = int((l10k_max_pks - l10k_min_pks) * nk_per_dex_pks)
    a_s_pks = np.array([1]) # We only care about today
    ptc = BaccoCalculator(nonlinear_emu_path='/mnt/zfsusers/gravityls_3/codes/NN_emulator_PCA6_0.95_300_400n_paired_comb', 
                          nonlinear_emu_details='details.pickle', use_baryon_boost=True,
                          ignore_lbias=True, allow_bcm_emu_extrapolation_for_shear=True,
                          a_arr=a_s_pks,
                          allow_halofit_extrapolation_for_shear=True)
    
    def get_baryon_boost_from_pars(self, pars, k_arr):
        cosmopars = {'Omega_c': pars['Omega_c'],
             'Omega_b': pars['Omega_b'],
             'n_s': pars['n_s'],
             'h': pars['h'],
             'm_nu': pars['m_nu'],
             'A_s': pars['A_sE9']*1e-9 if 'A_sE9' in pars else np.nan,
             'w0': -1,
             'wa': 0}
        if ('A_sE9' not in pars) and ('sigma8' in pars):
            cosmopars['sigma8'] = pars['sigma8']
            cosmopars = QuickCosmo(**cosmopars)        
        
        bcmpar = {
            'M_c'  : pars['M_c'],
            'eta' : pars['eta'],
            'beta' : pars['beta'],
            'M1_z0_cen' : pars['M1_z0_cen'],
            'theta_out' : pars['theta_out'],
            'theta_inn' : pars['theta_inn'],
            'M_inn' : pars['M_inn']
        }

        Sk = self.ptc.get_baryonic_boost(cosmopars, bcmpar=bcmpar, k_arr=k_arr/pars['h'])
        
        return Sk


# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### Script to launch job

# %% hidden=true
from cl_like.bacco import BaccoCalculator

# min k 3D power spectra
l10k_min_pks: float = -4.0
# max k 3D power spectra
l10k_max_pks: float = 2.0
# #k for 3D power spectra
nk_per_dex_pks: int = 25

nk_pks = int((l10k_max_pks - l10k_min_pks) * nk_per_dex_pks)
a_s_pks = np.array([1]) # We only care about today
ptc = BaccoCalculator(nonlinear_emu_path='/mnt/zfsusers/gravityls_3/codes/NN_emulator_PCA6_0.95_300_400n_paired_comb', 
                      nonlinear_emu_details='details.pickle', use_baryon_boost=True,
                      ignore_lbias=True, allow_bcm_emu_extrapolation_for_shear=True,
                      a_arr=a_s_pks,
                      allow_halofit_extrapolation_for_shear=True)

def get_baryon_boost_from_pars(pars, k_arr):
    cosmopars = {'Omega_c': pars['Omega_c'],
         'Omega_b': pars['Omega_b'],
         'n_s': pars['n_s'],
         'h': pars['h'],
         'm_nu': pars['m_nu'],
         'A_s': pars['A_sE9']*1e-9 if 'A_sE9' in pars else np.nan,
         'w0': -1,
         'wa': 0}
    if ('A_sE9' not in pars) and ('sigma8' in pars):
        cosmopars['sigma8'] = pars['sigma8']
        cosmopars = QuickCosmo(**cosmopars)        

    bcmpar = {
        'M_c'  : pars['M_c'],
        'eta' : pars['eta'],
        'beta' : pars['beta'],
        'M1_z0_cen' : pars['M1_z0_cen'],
        'theta_out' : pars['theta_out'],
        'theta_inn' : pars['theta_inn'],
        'M_inn' : pars['M_inn']
    }

    Sk = ptc.get_baryonic_boost(cosmopars, bcmpar=bcmpar, k_arr=k_arr/pars['h'])

    return Sk


# %% hidden=true
fname = './chains/desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG/desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG.1.txt'

with open(fname, 'r') as f:
    header = f.readline().replace('#', '')
    column_names = header.split()
    
# chain = np.loadtxt(fname)
print(chain.shape)
nsamples = chain.shape[0]

params = []
pars_tmp = {}
for i, c in enumerate(chain[:1000]):
    if (i == 0) or ((i+1) % int(nsamples / 20) == 0):
        print(f"Step {i+1}/{nsamples}")
    pars = dict(zip(column_names, c))

    for pn in column_names:
        if ('limber' in pn) or ('bias' in pn) or ('prior' in pn) or ('chi2' in pn):
            del pars[pn]
    
    if pars == pars_tmp:
        params.append(cospar_for_bcm)
        continue
    
    cosmopars = {'Omega_c': pars['Omega_c'],
         'Omega_b': pars['Omega_b'],
         'n_s': pars['n_s'],
         'h': pars['h'],
         'm_nu': pars['m_nu'],
         'A_s': pars['A_sE9']*1e-9 if 'A_sE9' in pars else np.nan,
         'w0': -1,
         'wa': 0}
    if ('A_sE9' not in pars) and ('sigma8' in pars):
        cosmopars['sigma8'] = pars['sigma8']
        cosmopars = QuickCosmo(**cosmopars)      

    bcmpar = {
        'M_c'  : pars['M_c'],
        'eta' : pars['eta'],
        'beta' : pars['beta'],
        'M1_z0_cen' : pars['M1_z0_cen'],
        'theta_out' : pars['theta_out'],
        'theta_inn' : pars['theta_inn'],
        'M_inn' : pars['M_inn']
    }
    
    cospar = ptc._get_bacco_pars_from_cosmo(cosmopars)
    cospar_for_bcm, these_a_s = ptc._check_baccoemu_baryon_pars_for_extrapolation(cospar)
    cospar_for_bcm.update(bcmpar)
    params.append(cospar_for_bcm)
# print(params)
# print()

combined_pars = {}
for key in cospar_for_bcm.keys():
    combined_pars[key] = np.zeros(len(params))
combined_pars['expfactor'] = np.ones(len(params))
# print(combined_pars)
# print()

for i, pars in enumerate(params):
    for key, val in pars.items():
        combined_pars[key][i] = val
# print(combined_pars)
# print()
        
k_arr = np.logspace(-1, 0.4, 10)
Sk = ptc.mpk.get_baryonic_boost(k=k_arr, **combined_pars)[1]
print(Sk)

# %% hidden=true jupyter={"outputs_hidden": true}
fname = './chains/desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG/desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG.1.txt'

with open(fname, 'r') as f:
    header = f.readline().replace('#', '')
    column_names = header.split()
    
# chain = np.loadtxt(fname)
print(chain.shape)
nsamples = chain.shape[0]
k_arr = np.logspace(-1, 0.4, 10)

Sk = []
pars_tmp = {}
for i, c in enumerate(chain[:1000]):
    if (i == 0) or ((i+1) % int(nsamples / 20) == 0):
        print(f"Step {i+1}/{nsamples}")
    pars = dict(zip(column_names, c))

    for pn in column_names:
        if ('limber' in pn) or ('bias' in pn) or ('prior' in pn) or ('chi2' in pn):
            del pars[pn]
    
    if pars == pars_tmp:
        Sk.append(Sk[-1])
        continue
        
    Sk.append(get_baryon_boost_from_pars(pars, k_arr))
    
Sk_colnames =  [f'Sk{ki:.2f}'.replace('.', 'p') for ki in k_arr]
np.savez_compressed('prueba.npz', Sk=Sk, colnames=Sk_colnames)

# %% [markdown] heading_collapsed=true hidden=true jp-MarkdownHeadingCollapsed=true
# ### Plots

# %% hidden=true
key = 'desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG'

boostdesy3 = BaryonBoostBacco(results[key]['MCSamples'], k_arr=np.logspace(-1, 0.4, 20), use_samples=200000)

f, ax = plt.subplots()
boostdesy3.plot_baryon_post(ax, 'bacco', 'red')
ax.axhline(1, color='gray', ls='--')
ax.set_ylabel(r'$S(k, a=1)$')
ax.set_xlabel(r'$k [{\rm 1/Mpc}]$')
plt.show()

# %% hidden=true
# Arico+23
# key = 'Arico+23'
# results[key] = load_Arico23(r'Arico+23', 'black', results['desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG']['MCSamples'])
# print(key, f"R-1 = {results[key]['R-1']}")

boostArico = BaryonBoostBacco(results[key]['MCSamples'], k_arr=np.logspace(-1, 0.4, 20), use_samples=200000)
f, ax = plt.subplots()
boostArico.plot_baryon_post(ax, 'bacco', 'red')
ax.axhline(1, color='gray', ls='--')
ax.set_ylabel(r'$S(k, a=1)$')
ax.set_xlabel(r'$k [{\rm 1/Mpc}]$')
plt.show()

# %% hidden=true
# # Baryons (4096) lmax=8192
# key = 'desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG'
# results[key] = load_chain(f'./chains/{key}/{key}',
#                            r'DESY3 + KiDS1000 + HSC with Baryons ($\ell < 8192$, $\ell_{\rm min} = 20 (DES), 100 (KiDS), 300 (HSC)$)', 'black', 0.15)
# print(key, f"R-1 = {results[key]['R-1']}")

boost = BaryonBoostBacco(results[key]['MCSamples'], k_arr=np.logspace(-1, 0.4, 20), use_samples=200000)
f, ax = plt.subplots()
boost.plot_baryon_post(ax, 'bacco', 'red')
ax.axhline(1, color='gray', ls='--')
ax.set_ylabel(r'$S(k, a=1)$')
ax.set_xlabel(r'$k [{\rm 1/Mpc}]$')
plt.show()

# %% hidden=true
f, ax = plt.subplots()
boostArico.plot_baryon_post(ax, 'Arico+23', 'gray')
boost.plot_baryon_post(ax, 'DES+KiDS+HSC', 'blue')
boostdesy3.plot_baryon_post(ax, 'DES', 'orange')

ax.axhline(1, color='gray', ls='--')

ax.set_ylabel(r'$S(k, a=1)$')
ax.set_xlabel(r'$k [{\rm 1/Mpc}]$')

ax.legend()
plt.show()

# %% hidden=true
