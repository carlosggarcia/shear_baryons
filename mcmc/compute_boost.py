"""
Compute the boost factor for the given chain
"""

from cl_like.bacco import BaccoCalculator
import numpy as np
import sys
import os


# UNITS = ['1/Mpc', 'h/Mpc'][0]
# UNITS = '1/Mpc'
UNITS = 'h/Mpc'

# THE ONLY VARIABLE TO MODIFY
# Choose array of k's to sample
if UNITS == 'h/Mpc':
    # limits of bacco 0.01 h/Mpc to 4.692772528625323 h/Mpc
    k_arr = np.logspace(-2, 0.67, 20)
else:
    k_arr = np.logspace(-1, 0.4, 20)

# Read chain name
fname = sys.argv[1]

# Define a convenient Cosmology-like object
class QuickCosmo(dict):
    def sigma8(self):
        return self['sigma8']


# Initialize BaccoCalculator
print("Loading BaccoCalculator")
# min k 3D power spectra
l10k_min_pks: float = -4.0
# max k 3D power spectra
l10k_max_pks: float = 2.0
# #k for 3D power spectra
nk_per_dex_pks: int = 25

nk_pks = int((l10k_max_pks - l10k_min_pks) * nk_per_dex_pks)
a_s_pks = np.array([1]) # We only care about today
ptc = BaccoCalculator(nonlinear_emu_path='/mnt/zfsusers/gravityls_3/codes/NN_emulator_PCA6_0.95_300_400n_paired_comb',
                      nonlinear_emu_details='details.pickle', use_baryon_boost=True,
                      ignore_lbias=True, allow_bcm_emu_extrapolation_for_shear=True,
                      a_arr=a_s_pks,
                      allow_halofit_extrapolation_for_shear=True)

def get_baryon_boost_from_pars(pars, k_arr):
    cosmopars = {'Omega_c': pars['Omega_c'],
         'Omega_b': pars['Omega_b'],
         'n_s': pars['n_s'],
         'h': pars['h'],
         'm_nu': pars['m_nu'],
         'A_s': pars['A_sE9']*1e-9 if 'A_sE9' in pars else np.nan,
         'w0': -1,
         'wa': 0}
    if ('A_sE9' not in pars) and ('sigma8' in pars):
        cosmopars['sigma8'] = pars['sigma8']
        cosmopars = QuickCosmo(**cosmopars)

    bcmpar = {
        'M_c'  : pars['M_c'],
        'eta' : pars['eta'],
        'beta' : pars['beta'],
        'M1_z0_cen' : pars['M1_z0_cen'],
        'theta_out' : pars['theta_out'],
        'theta_inn' : pars['theta_inn'],
        'M_inn' : pars['M_inn']
    }

    if UNITS == 'h/Mpc':
        Sk = ptc.get_baryonic_boost(cosmopars, bcmpar=bcmpar, k_arr=k_arr)
    else:
        Sk = ptc.get_baryonic_boost(cosmopars, bcmpar=bcmpar, k_arr=k_arr/pars['h'])

    return Sk


# Open chain
print("Reading chain")
with open(fname, 'r') as f:
    header = f.readline().replace('#', '')
    column_names = header.split()

chain = np.loadtxt(fname)
print(chain.shape)
nsamples = chain.shape[0]

print("Computing Sk")
Sk = []
pars_tmp = {}
for i, c in enumerate(chain):
    if (i == 0) or ((i+1) % int(nsamples / 20) == 0):
        print(f"Step {i+1}/{nsamples}")
    pars = dict(zip(column_names, c))

    for pn in column_names:
        if ('limber' in pn) or ('bias' in pn) or ('prior' in pn) or ('chi2' in pn):
            del pars[pn]

    if pars == pars_tmp:
        Sk.append(Sk[-1])
        continue

    Sk.append(get_baryon_boost_from_pars(pars, k_arr))
print("Sk computed for all steps")

# Save the output
print(f"Saving computed Sk into {fname}")
Sk_colnames =  [f'Sk{ki:.2f}'.replace('.', 'p') for ki in k_arr]
if UNITS == 'h/Mpc':
    fname = fname.replace('.txt', '_Sk_in_hoverMpc.npz')
else:
    fname = fname.replace('.txt', '_Sk.npz')
if os.path.exists(fname):
    fname =+ '.new'
    print("FILE EXISTS!!! SAVING INTO", fname)

np.savez_compressed(fname, Sk=Sk, colnames=Sk_colnames, k_arr=k_arr,
                    weights=chain[:, 0], loglikes=chain[:, 1])
print("Finished successfully")
