"""
Compute the boost factor for Arico+23 chain
"""

from cl_like.bacco import BaccoCalculator
import numpy as np
import sys
import h5py

# THE ONLY VARIABLE TO MODIFY
# Choose array of k's to sample
k_arr = np.logspace(-1, 0.4, 20)

# Read chain name
fname = 'chains/posteriors_DESY3_arico (2).hdf5'

def load_Arico23(fname):
    # Arico 2022 results (only contains S8 & Omega_m)
    Arico2cobaya = {'omega_matter': 'Omega_m',
                    'omega_baryon': 'Omega_b',
                    'hubble': 'h',
                    'ns': 'n_s',
                    'neutrino_mass': 'm_nu',
                    'm1': 'bias_DESY3wl__0_m',
                    'm2': 'bias_DESY3wl__1_m',
                    'm3': 'bias_DESY3wl__2_m',
                    'm4': 'bias_DESY3wl__3_m',
                    'dz_s1': 'limber_DESY3wl__0_dz',
                    'dz_s2': 'limber_DESY3wl__1_dz',
                    'dz_s3': 'limber_DESY3wl__2_dz',
                    'dz_s4': 'limber_DESY3wl__3_dz',
                    'eta1_ia': 'limber_eta_IA',
                    'a1_ia': 'bias_A_IA'
               }

    d = {}
    samples = []
    names = []
    with h5py.File(fname, 'r') as f:
        for n in f.keys():
            nts = n if n not in Arico2cobaya else Arico2cobaya[n]
            names.append(nts)
            samples.append(f[n][:])

        m_nu = f['neutrino_mass'][:]
        Omega_b = f['omega_baryon'][:]
        Omega_m = f['omega_matter'][:]
        h = f['hubble'][:]
        Omega_c = Omega_m - Omega_b - m_nu/(93.14 * h ** 2)
        names.append('Omega_c')
        samples.append(Omega_c)

    return np.array(samples).T, names

# Define a convenient Cosmology-like object
class QuickCosmo(dict):
    def sigma8(self):
        return self['sigma8']


# Initialize BaccoCalculator
print("Loading BaccoCalculator")
# min k 3D power spectra
l10k_min_pks: float = -4.0
# max k 3D power spectra
l10k_max_pks: float = 2.0
# #k for 3D power spectra
nk_per_dex_pks: int = 25

nk_pks = int((l10k_max_pks - l10k_min_pks) * nk_per_dex_pks)
a_s_pks = np.array([1]) # We only care about today
ptc = BaccoCalculator(nonlinear_emu_path='/mnt/zfsusers/gravityls_3/codes/NN_emulator_PCA6_0.95_300_400n_paired_comb',
                      nonlinear_emu_details='details.pickle', use_baryon_boost=True,
                      ignore_lbias=True, allow_bcm_emu_extrapolation_for_shear=True,
                      a_arr=a_s_pks,
                      allow_halofit_extrapolation_for_shear=True)

def get_baryon_boost_from_pars(pars, k_arr):
    cosmopars = {'Omega_c': pars['Omega_c'],
         'Omega_b': pars['Omega_b'],
         'n_s': pars['n_s'],
         'h': pars['h'],
         'm_nu': pars['m_nu'],
         'A_s': pars['A_sE9']*1e-9 if 'A_sE9' in pars else np.nan,
         'w0': -1,
         'wa': 0}
    if ('A_sE9' not in pars) and ('sigma8' in pars):
        cosmopars['sigma8'] = pars['sigma8']
        cosmopars = QuickCosmo(**cosmopars)

    bcmpar = {
        'M_c'  : pars['M_c'],
        'eta' : pars['eta'],
        'beta' : pars['beta'],
        'M1_z0_cen' : pars['M1_z0_cen'],
        'theta_out' : pars['theta_out'],
        'theta_inn' : pars['theta_inn'],
        'M_inn' : pars['M_inn']
    }

    Sk = ptc.get_baryonic_boost(cosmopars, bcmpar=bcmpar, k_arr=k_arr/pars['h'])

    return Sk


# Open chain
print("Reading chain")
chain, column_names = load_Arico23(fname)
print(chain.shape)
nsamples = chain.shape[0]

print("Computing Sk")
Sk = []
pars_tmp = {}
for i, c in enumerate(chain):
    if (i == 0) or ((i+1) % int(nsamples / 20) == 0):
        print(f"Step {i+1}/{nsamples}")
    pars = dict(zip(column_names, c))

    for pn in column_names:
        if ('limber' in pn) or ('bias' in pn) or ('prior' in pn) or ('chi2' in pn):
            del pars[pn]

    if pars == pars_tmp:
        Sk.append(Sk[-1])
        continue

    Sk.append(get_baryon_boost_from_pars(pars, k_arr))
print("Sk computed for all steps")

# Save the output
print(f"Saving computed Sk into {fname}")
Sk_colnames =  [f'Sk{ki:.2f}'.replace('.', 'p') for ki in k_arr]
fname = fname.replace('.hdf5', '_Sk.npz')
np.savez_compressed(fname, Sk=Sk, colnames=Sk_colnames, k_arr=k_arr,
                    weights=np.ones(nsamples))
print("Finished successfully")
