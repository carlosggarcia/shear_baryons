#!/bin/bash
# Modify these three variables and add the chains that you want to
# launch/resume at the end of the script
queue=berg
nodes=1
chains=4


######
# Automatic part
if [[ $queue == "cmb" ]]; then
    cores=24
elif  [[ $queue == "berg" ]]; then
    cores=28
elif  [[ $queue == "normal" ]]; then
    cores=1
elif  [[ $queue == "redwood" ]]; then
    cores=64
else
    echo "queue not recognized. Choose cmb or berg"
    exit 1
fi

chains_per_node=$(($chains/$nodes))
threads=$(($cores/$chains_per_node))

if (( $chains % $nodes != 0 )); then
    echo "The number of chains or nodes is wrong"
    exit 1
elif (( $cores % $chains_per_node != 0 )); then
    echo "The number of chains per node ($chains_per_node) is not compatible with the number of cores ($cores) in the nodes"
    exit 1
fi


# Launcher
#########
create_launcher() {
    param=$1

    if [[ ! -d "./tmp" ]]; then
        mkdir ./tmp
    fi

    code=$(mktemp -p ./tmp)
    chmod +x $code
    cat <<EOF > $code
#!/bin/bash
export OMP_NUM_THREADS=$threads
export COBAYA_USE_FILE_LOCKING=false

/usr/local/shared/slurm/bin/srun -N $nodes -n $chains --ntasks-per-node $chains_per_node -m cyclic --mpi=pmi2 $HOME/mambaforge/envs/baryons-sigma12/bin/python -m cobaya run $param
EOF
    echo $code

}

add_job_to_queue() {
    param=$1
    logname=$2
    launcher=$(create_launcher $param)
    addqueue -n ${nodes}x${cores} -s -q $queue -m 1  -c $name -o log/$logname.log $launcher
}

launch_chain() {
    name=$1
    logname=$name
    param=input/$name.yml
    add_job_to_queue $param $logname
}

resume_chain() {
    name=$1
    param=chains/$name/$name
    logname=${name}_resume
    add_job_to_queue $param $logname
}

compute_boost() {
    name=$1

    for i in {1..4}; do
        chain=${name}.${i}
        logname=boost_${chain}
        fname=chains/$name/$chain.txt
        addqueue -n 1x1 -s -q $queue -m 10 -c $logname -o log/$logname.log $HOME/mambaforge/envs/baryons-sigma12/bin/python compute_boost.py $fname
    done
}

compute_boost_BAHAMAS() {
    name=$1

    for i in {1..4}; do
        chain=${name}.${i}
        logname=boost_${chain}
        fname=chains/$name/$chain.txt
        addqueue -n 1x1 -s -q $queue -m 10 -c $logname -o log/$logname.log $HOME/mambaforge/envs/baryons/bin/python compute_boost_BAHAMAS.py $fname
    done
}

compute_boost_fixedcosmoP18() {
    name=$1

    for i in {1..4}; do
        chain=${name}.${i}
        logname=boost_${chain}
        fname=chains/$name/$chain.txt
        addqueue -n 1x1 -s -q $queue -m 10 -c $logname -o log/$logname.log $HOME/mambaforge/envs/baryons/bin/python compute_boost_fixedcosmoP18.py $fname
    done
}

compute_boost_fixedcosmoBF() {
    name=$1

    for i in {1..4}; do
        chain=${name}.${i}
        logname=boost_${chain}
        fname=chains/$name/$chain.txt
        addqueue -n 1x1 -s -q $queue -m 10 -c $logname -o log/$logname.log $HOME/mambaforge/envs/baryons/bin/python compute_boost_fixedcosmoBF.py $fname
    done
}

compute_boost_lkl() {
    name=$1

    for i in {1..4}; do
        chain=${name}.${i}
        logname=boost_${chain}
        fname=chains/$name/$chain.txt
        addqueue -n 1x8 -s -q $queue -m 10 -c $logname -o log/$logname.log $HOME/mambaforge/envs/baryons/bin/python compute_boost_lkl.py $fname
    done
}


compute_boost_Arico() {
    logname=boost_Arico23
    addqueue -n 1x1 -s -q $queue -m 10 -c $logname -o log/$logname.log $HOME/mambaforge/envs/baryons/bin/python compute_boost_Arico.py
}

compute_sigma8_S8() {
    name=$1

    for i in {1..4}; do
        chain=${name}.${i}
        logname=sigma8_S8_${chain}
        fname=chains/$name/$chain.txt
        addqueue -n 1x7 -s -q $queue -m 1 -c $logname -o log/$logname.log $HOME/mambaforge/envs/baryons-sigma12/bin/python compute_sigma8_S8.py $fname
    done
}

# Chains
############
#### BAOlkl
# launch_chain BAOlkl
# launch_chain priors
# launch_chain P18_Gauss
# compute_sigma8_S8 priors

#### DESY3wl
# launch_chain desy3wl_baryons_nocuts_nla_nside1024
# launch_chain desy3wl_nobaryons_nocuts_nla_nside1024
# launch_chain desy3wl_nobaryons_nocuts_nla_nside1024_lmin30
# launch_chain desy3wl_nobaryons_nocuts_nla_nside1024_lmin20
# launch_chain desy3wl_nobaryons_nocuts_nla_nside1024_2
# launch_chain desy3wl_nobaryons_nocuts_nla_nside1024_ourbinning
# launch_chain desy3wl_nobaryons_nocuts_nla_nside1024_ourbinningatsmallscales
# launch_chain desy3wl_nobaryons_nocuts_nla_nside1024_ourbinninglmin8
# launch_chain desy3wl_nobaryons_nocuts_nla_nside1024_halofit
# launch_chain desy3wl_noK1000_nobaryons_nocuts_nla_nside1024
# launch_chain desy3wl_noK1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20
# launch_chain desy3wl_baryons_nocuts_nla_nside4096
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin30
# 
# launch_chain desy3wl_nobaryons_nocuts_nla_nside1024_lmax200
# launch_chain desy3wl_nobaryons_nocuts_nla_nside1024_lmin200
# 
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmax2048
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_pixwin

# launch_chain desy3wl_nobaryons_nocuts_nla_nside8192_lmax8192
# launch_chain desy3wl_baryons_nocuts_nla_nside8192_lmax8192

# launch_chain desy3wl_baryons_nocuts_nla_nside1024_0.99
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_0.99

# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_2chains
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_Rminus1003

# With SSC
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNG
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNGfsky

# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmin20
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmin20
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_halofit
# launch_chain desy3wl_noK1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20

# Test window function
# launch_chain desy3wl_baryons_nocuts_nla_nside8192cov4096_lmin20_GNG
# launch_chain desy3wl_nobaryons_nocuts_nla_nside8192cov4096_lmin20_GNG

# Hybrid params
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG_hybridpriors
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG_hybridpriors
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG_hybridpriors_hmcode2020
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmax400_lmin20_GNG_hybridpriors_hmcode2020

# Paper
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_GNG
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmaxDoux22_lmin20_halofit_GNG

# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_halofit_GNG
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmax1000_lmin20_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless

# launch_chain desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG

# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_halofit_GNG

# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAS_noiseless
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockBAHAMAStable_noiseless
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_GrO_5kmax_noiseless
# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_GrO_10kmax_noiseless

# Halofit extrap at k>5h/Mpc
# launch_chain desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG
# launch_chain desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless
# launch_chain desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG_mockBAHAMAStable_noiseless

# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmax6000_lmin20_GNG_pixwin
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_pixwin

# launch_chain desy3wl_nobaryons_nocuts_nla_nside4096_lmin20_GNG
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG

# Pixelization corrected
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin

# Noise marginalization
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_nlmarg

# Pixelization corrected + Noise marginalization
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin_nlmarg
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_nlmarg_prior10
# launch_chain desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG_pixwin_nlmarg_prior10

# Test patch removal
# launch_chain desy3wl_noK1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG
# launch_chain desy3wl_noK1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_GNG
# launch_chain desy3wl_noK1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG
# launch_chain desy3wl_noK1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG
# launch_chain desy3wl_noK1000_nohsc_nobaryons_nocuts_nla_nside4096_lmin20_GNG

#### KiDS1000
# launch_chain k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100
# launch_chain k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100
# launch_chain k1000_nobaryons_nocuts_nla_nside4096_lmin100
# launch_chain k1000_nobaryons_nocuts_nla_nside4096_lmin100_GNGfsky
# launch_chain k1000_baryons_nocuts_nla_nside4096_lmin100
# launch_chain k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_halofit

# Paper
# launch_chain k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG
# launch_chain k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin100_GNG
# launch_chain k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin100_halofit_GNG

# launch_chain k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG
# launch_chain k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG
# launch_chain k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_halofit_GNG

# launch_chain k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300_GNG
# launch_chain k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin300_GNG

# launch_chain k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG
# launch_chain k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG

# launch_chain k1000_nobaryons_nocuts_nla_nside4096_lmin100_GNG
# launch_chain k1000_baryons_nocuts_nla_nside4096_lmin100_GNG
# launch_chain k1000_baryons_nocuts_nla_nside4096_lmin100_GNG_pixwin

# Test patch removal
# launch_chain k1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin100_GNG
# launch_chain k1000_nohsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG
# launch_chain k1000_nohsc_nobaryons_nocuts_nla_nside4096_lmin100_GNG

#### HSCDR1
# launch_chain hsc_nobaryons_nocuts_nla_nside4096_lmax1000_lmin300
# launch_chain hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin300
# 
# launch_chain hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin300
# launch_chain hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin300
#
# launch_chain hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin300
# launch_chain hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300
# 
# launch_chain hsc_nobaryons_nocuts_nla_nside4096_lmin300
# launch_chain hsc_baryons_nocuts_nla_nside4096_lmin300

# With our SSC to test it
# launch_chain hsc_nobaryons_nocuts_nla_nside4096_lmin300_GNG
# launch_chain hsc_nobaryons_nocuts_nla_nside4096_lmin300_GNGfsky

#### DESY3 + KiDS1000
# launch_chain desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100
# launch_chain desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100
# launch_chain desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmin20_lmin100
# launch_chain desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100

# Compare with official (2305.17173)
# launch_chain desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG_hybridpriors_hmcode2020
# launch_chain desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG_hybridpriors

# Paper
# launch_chain desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG
# launch_chain desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_GNG

# launch_chain desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG
# launch_chain desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_GNG

# launch_chain desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG
# launch_chain desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG

# launch_chain desy3wl_k1000_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_GNG
# launch_chain desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG

# lmin=2048
# launch_chain desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin2048_GNG

##### DESY3 + KiDS1000 + HSCDR1
# lmax=1000
# launch_chain desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax1000_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless

# lmax=2048
# launch_chain desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax2048_lmin20_lmin100_lmin300_GNG

# lmax=4500
# launch_chain desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BBNprior
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF
# launch_chain desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG
# launch_chain desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG
# launch_chain desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18

# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_clusters
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedbaryonsBAHAMAS
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless

# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless

# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl
# launch_chain desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_BAOlkl
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Riess_BAOlkl
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_H0Planck_BAOlkl

# launch_chain lsst_baryons_nocuts_nla_nside4096_lmax4500_lmin20_mockEuclidEmulator2_BAHAMAS
# launch_chain lsst_baryons_nocuts_nla_nside4096_lmax4500_lmin20_mockEuclidEmulator2_BAHAMAS_noiseless

# HALOFIT extrap
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG


# lmax=8192
# launch_chain desy3wl_k1000_hsc_nobaryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoBF
# launch_chain desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG
# launch_chain desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_halofit_GNG
# launch_chain desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG

# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedbaryonsBAHAMAS

# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax8192_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless
# launch_chain desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax8192_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless

# launch_chain lsst_baryons_nocuts_nla_nside4096_lmax8192_lmin20_mockEuclidEmulator2_BAHAMAS
# launch_chain lsst_baryons_nocuts_nla_nside4096_lmax8192_lmin20_mockEuclidEmulator2_BAHAMAS_noiseless

###### Boost computation
# lmax = 4500
# compute_boost desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG
# compute_boost k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin100_GNG
# compute_boost hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin300
# compute_boost desy3wl_k1000_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_GNG
# compute_boost desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG
# compute_boost desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_noiseless
# compute_boost desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockEuclidEmulator2_BAHAMAS_BF_noiseless

# compute_boost desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockBAHAMAStable_noiseless
# compute_boost desy3wl_baryons_nocuts_nla_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless
# compute_boost desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG_mockBAHAMAStable_noiseless
# compute_boost desy3wl_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_GNG_mockEuclidEmulator2_BAHAMAStable_noiseless
# compute_boost desy3wl_k1000_hsc_baryons_nocuts_nla_hfitkextrap_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG
# compute_boost desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_P18_newprop

# compute_boost_fixedcosmoP18 desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18
# compute_boost_fixedcosmoP18 desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF  # Use P18 cosmology with the baryon parameters of the fixedcosmoBF to check impact since the BF is at the edges of the emulator
# compute_boost_fixedcosmoBF desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedcosmoBF
# compute_boost_BAHAMAS desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_fixedbaryonsBAHAMAS
# compute_boost desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_clusters

# compute_boost desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless
# compute_boost desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless

# compute_boost_lkl desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG
# compute_boost_lkl desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmax4500_lmin20_lmin100_lmin300_GNG


# lmax = 8192
# compute_boost desy3wl_baryons_nocuts_nla_nside4096_lmin20_GNG
# compute_boost k1000_baryons_nocuts_nla_nside4096_lmin100_GNG
# compute_boost hsc_baryons_nocuts_nla_nside4096_lmin300
# compute_boost desy3wl_k1000_baryons_nocuts_nla_nside4096_lmin20_lmin100_GNG
# compute_boost desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG
# compute_boost desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18
# compute_boost desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoBF
# compute_boost_lkl desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG
# compute_boost_lkl desy3wl_k1000_hsc_baryonsAE_nocuts_nla_nside4096_lmin20_lmin100_lmin300_halofit_GNG
# compute_boost_lkl desy3wl_k1000_hsc_baryonsBCM_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG

# compute_boost_fixedcosmoP18 desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoPlanck18
# compute_boost desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmin20_lmin100_lmin300_GNG_fixedcosmoBF

# compute_boost desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax8192_lmin20_lmin100_lmin300_GNG_mockBACCOEmu_BAHAMAStable_BFb_noiseless
# compute_boost desy3wl_k1000_hsc_baryons_nocuts_nla_nside4096_lmax8192_lmin20_lmin100_lmin300_GNG_mockBAHAMAStable-EE21Mpc_noiseless

# Arico
# compute_boost_Arico

# vim: tw=0
