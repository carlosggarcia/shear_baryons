"""
Compute the boost factor for the given chain
"""

from cobaya.model import get_model
from cobaya.input import load_input
import numpy as np
import sys

# THE ONLY VARIABLE TO MODIFY
# Choose array of k's to sample
k_arr = np.logspace(-1, 0.4, 20)

# Read chain name
fname = sys.argv[1]
print(f"Computing Sk for chain {fname}", flush=True)

# Initialize BaccoCalculator
config_fname = '.'.join(fname.split('.')[:-2] + ['input.yaml'])
print(f"Loading model from config file {config_fname}", flush=True)
model = get_model(config_fname)
th_CCL = model.theory['cl_like.CCL']
th_Pk = model.theory['cl_like.Pk']

sampled_params = list(model.sampled_dependence.keys())

def get_baryon_boost_from_pars(pars, k_arr):
    pars.update(model.parameterization.to_input(pars))
    model.provider.set_current_input_params(pars)
    th_CCL.check_cache_and_compute(pars)
    th_Pk.check_cache_and_compute(pars)

    Sk = th_Pk.current_state['Pk']['pk_data']['Sk'].eval(k_arr, 1)

    return Sk

# Open chain
print("Reading chain")
with open(fname, 'r') as f:
    header = f.readline().replace('#', '')
    column_names = header.split()

chain = np.loadtxt(fname)
print(chain.shape, flush=True)
nsamples = chain.shape[0]

print("Computing Sk")
Sk = []
pars_tmp = {}
for i, c in enumerate(chain):
    if (i == 0) or ((i+1) % int(nsamples / 20) == 0):
        print(f"Step {i+1}/{nsamples}", flush=True)
    pars = dict(zip(column_names, c))

    pars = {pn: pars[pn] for pn in sampled_params}

    if pars == pars_tmp:
        Sk.append(Sk[-1])
        continue

    Sk.append(get_baryon_boost_from_pars(pars, k_arr))
    pars_tmp = pars.copy()
print("Sk computed for all steps", flush=True)

# Save the output
print(f"Saving computed Sk into {fname}")
Sk_colnames =  [f'Sk{ki:.2f}'.replace('.', 'p') for ki in k_arr]
fname = fname.replace('.txt', '_Sk.npz')
np.savez_compressed(fname, Sk=Sk, colnames=Sk_colnames, k_arr=k_arr,
                    weights=chain[:, 0], loglikes=chain[:, 1])
print("Finished successfully", flush=True)
