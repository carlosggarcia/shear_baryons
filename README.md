# Cosmic shear with small scales: DES-Y3, KiDS-1000 and HSC-DR1

This repository contains the codes used to produce *Cosmic shear with small
scales: DES-Y3, KiDS-1000 and HSC-DR1*
([arXiv:2403.13794](https://arxiv.org/pdf/2403.13794.pdf)). 

You can cite this work copying the following bibentry:

```
@ARTICLE{2024arXiv240313794G,
       author = {{Garc{\'\i}a-Garc{\'\i}a}, Carlos and {Zennaro}, Matteo and {Aric{\`o}}, Giovanni and {Alonso}, David and {Angulo}, Raul E.},
        title = "{Cosmic shear with small scales: DES-Y3, KiDS-1000 and HSC-DR1}",
      journal = {arXiv e-prints},
     keywords = {Astrophysics - Cosmology and Nongalactic Astrophysics},
         year = 2024,
        month = mar,
          eid = {arXiv:2403.13794},
        pages = {arXiv:2403.13794},
          doi = {10.48550/arXiv.2403.13794},
archivePrefix = {arXiv},
       eprint = {2403.13794},
 primaryClass = {astro-ph.CO},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2024arXiv240313794G},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
```
